//
//  CreateGroupViewController.swift
//  Techmea
//
//  Created by Dhaval Panchani on 05/09/18.
//  Copyright © 2018 VISHAL SETH. All rights reserved.
//

import UIKit
import Alamofire
import MBProgressHUD
import AVFoundation
import Photos
import Toast_Swift

//MARK:- AddUserGroupCell
class add_user_group_cell : UITableViewCell
{
    
    @IBOutlet weak var btn_check: UIButton!
    @IBOutlet weak var lbl_username: UILabel!
    @IBOutlet weak var user_profile_imageview: UIImageView!
    @IBOutlet weak var lbl_City: UILabel!
    @IBOutlet weak var lbl_profession: UILabel!
    @IBOutlet weak var img_coach: UIImageView!
    @IBOutlet weak var container_view: UIView!
}

//MARK:- SelectedFriendsCollectionCell
class SelectedFriendsCollectionCell : UICollectionViewCell
{
    @IBOutlet weak var user_profile_imageview: UIImageView!
    
    @IBOutlet weak var btnRemove: UIButton!
    @IBOutlet weak var backgroundBorderView: UIView!
}

class CreateGroupViewController: UIViewController,UIImagePickerControllerDelegate,UINavigationControllerDelegate,UITableViewDataSource,UITableViewDelegate,UICollectionViewDelegate,UICollectionViewDataSource,UITextFieldDelegate {
    
    
    //MARK:- Outlets
    @IBOutlet weak var tbl_user: UITableView!
    @IBOutlet weak var view_addFriend: UIView!
    @IBOutlet weak var groupProfile_imageview: UIImageView!
    @IBOutlet weak var textfield_name: UITextField!
    @IBOutlet weak var collectiongroupmembers: UICollectionView!
    @IBOutlet weak var SelectedMembersView: UIView!
    @IBOutlet weak var ProfilePictureView: UIView!
    @IBOutlet weak var ChooseFriendView: UIView!
    @IBOutlet weak var lblNoMembers: UILabel!
    @IBOutlet weak var btnCreateGroup: UIButton!
    @IBOutlet weak var lblCountSelectedMembers: UILabel!
    @IBOutlet weak var btnCamera: UIButton!
    @IBOutlet weak var lblTitle: UILabel!
    
    
    let picker = UIImagePickerController()
    var SelectedImage : UIImage?
    var SelectedImgUrl : URL?
    var friendListArr = [[String:Any]]()
    var selected_friends_id = [Int]()
    var imageData = NSData()
    var Selected_friends = [[String:Any]]()
    var is_edit = Bool()
    var group_id = Int()
    var group_image = String()
    var group_name = String()
    var is_admin = Bool()
    
    @IBOutlet weak var create_top_constraints: NSLayoutConstraint!
    
    //MARK:- viewDidLoad
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        self.callgetFriendsApi(user_id_str: "\(UserDefaults.standard.value(forKey: Constants.USERID) as! Int)")
        tbl_user.tableFooterView = UIView()
        
        self.ProfilePictureView.layer.cornerRadius = 5.0
        self.ProfilePictureView.layer.borderColor = UIColor.lightGray.cgColor
        self.ProfilePictureView.layer.borderWidth = 0.2
        self.ProfilePictureView.layer.shadowColor = UIColor(red: 225.0 / 255.0, green: 228.0 / 255.0, blue: 228.0 / 255.0, alpha: 1.0).cgColor
        self.ProfilePictureView.layer.shadowOpacity = 1.0
        self.ProfilePictureView.layer.shadowRadius = 5.0
        self.ProfilePictureView.layer.shadowOffset = CGSize(width: 5.0, height: 5.0)
        
        self.ChooseFriendView.layer.cornerRadius = 5.0
        self.ChooseFriendView.layer.borderColor = UIColor.lightGray.cgColor
        self.ChooseFriendView.layer.borderWidth = 0.2
        self.ChooseFriendView.layer.shadowColor = UIColor(red: 225.0 / 255.0, green: 228.0 / 255.0, blue: 228.0 / 255.0, alpha: 1.0).cgColor
        self.ChooseFriendView.layer.shadowOpacity = 1.0
        self.ChooseFriendView.layer.shadowRadius = 5.0
        self.ChooseFriendView.layer.shadowOffset = CGSize(width: 5.0, height: 5.0)
        
        self.lblCountSelectedMembers.text = "You have added 0 Friends into your group"
        
        
        if Selected_friends.count == 0
        {
            self.collectiongroupmembers.isHidden = true
            self.lblNoMembers.isHidden = false
        }
        else
        {
            self.collectiongroupmembers.isHidden = false
            self.lblNoMembers.isHidden = true
        }
        if(is_edit)
        {
            //groupProfile_imageview.sd_setImage(with: URL(string: group_image), placeholderImage: UIImage(named: "userPlaceHolder"))
            self.lblTitle.text = "Edit Group"
            groupProfile_imageview.image = self.SelectedImage
            textfield_name.text = group_name.decodeEmoji
            if(is_admin)
            {
                textfield_name.isUserInteractionEnabled = true
                btnCamera.isUserInteractionEnabled = true
                btnCreateGroup.setTitle("Update", for: .normal)
            }
            else
            {
                textfield_name.isUserInteractionEnabled = false
                btnCamera.isUserInteractionEnabled = false
                btnCreateGroup.setTitle("Leave", for: .normal)
            }
            lblCountSelectedMembers.isHidden = true
            self.ChooseFriendView.isHidden = true
            self.SelectedMembersView.isHidden = true
            create_top_constraints.constant = -160
//           if let urlstr = URL(string:group_image)
//           {
//            if let imgdata = try? Data(contentsOf: urlstr)
//                {
//                    SelectedImage = UIImage(data: imgdata)
//                }
//            }
        }
        else
        {
            self.lblTitle.text = "Create Group"
            lblCountSelectedMembers.isHidden = false
            self.ChooseFriendView.isHidden = false
            self.SelectedMembersView.isHidden = false
            create_top_constraints.constant = 30
        }
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        self.tabBarController?.tabBar.isHidden = true
    }

    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
    }
    
    
    //MARK:- textFieldShouldReturn
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        return false
    }

    //MARK:- Back Action
    @IBAction func btnBackAction(_ sender: UIButton)
    {
        if(view_addFriend.isHidden==true)
        {
            self.navigationController?.popViewController(animated: true)
        }
        else
        {
            selected_friends_id.removeAll()
            Selected_friends.removeAll()
            view_addFriend.isHidden = true
            tbl_user.reloadData()
        }
    }
    
    
    //MARK:- Button Choose Friend Action
    @IBAction func btnChoosefriendAction(_ sender: UIButton)
    {
        view_addFriend.isHidden = false
        tbl_user.reloadData()
    }
    
    //MARK:- Button Camera Action
    @IBAction func btnCameraAction(_ sender: UIButton)
    {
        let alert = UIAlertController(title: Constants.APP_NAME, message: "Browse Photo using", preferredStyle: UIAlertControllerStyle.actionSheet)
        
        let galleryBtn = UIAlertAction(title: "Gallery", style: UIAlertActionStyle.default) { (alert) -> Void in
            self.openGallery()
        }
        
        let cameraBtn = UIAlertAction(title: "Camera", style: UIAlertActionStyle.default) { (alert) -> Void in
            self.openCamera()
        }
        
        let cancelButton = UIAlertAction(title: "Cancel", style: UIAlertActionStyle.destructive) { (alert) -> Void in
            //self.txtUserType.text="Trader"
        }
        
        alert.addAction(galleryBtn)
        alert.addAction(cameraBtn)
        alert.addAction(cancelButton)
        DispatchQueue.main.async {
        self.present(alert, animated: true, completion: nil)
        }
    }
    
    //MARK:- Open Camera
    func openCamera()
    {
        if(Platform.isSimulator)
        {
            //self.displayAlert(msg: "No camera available", title_str: Constants.APP_NAME)
            self.view.makeToast("No camera available")
        }
        else
        {
            //let authorizationStatus = AVCaptureDevice.authorizationStatus(for: AVMediaType.video)
            AVCaptureDevice.requestAccess(for: AVMediaType.video, completionHandler: { (granted :Bool) -> Void in
                if granted == true
                {
                    self.picker.allowsEditing = true
                    self.picker.sourceType = UIImagePickerControllerSourceType.camera
                    self.picker.cameraCaptureMode = .photo
                    self.picker.mediaTypes = ["public.image"]
                    self.picker.delegate = self
                    self.picker.modalPresentationStyle = .fullScreen
                    DispatchQueue.main.async {
                    self.present(self.picker,animated: true,completion: nil)
                    }
                }
                else
                {
                    self.show_deny_alert(msg: "You deny to use camera.To allow camera you should allow camera permission from setting.")
                }
            })
        }
        
    }
    func show_deny_alert(msg:String)
    {
        let alert = UIAlertController(title: Constants.APP_NAME, message: msg, preferredStyle: UIAlertControllerStyle.alert)
        
        let okBtn = UIAlertAction(title: "OK", style: UIAlertActionStyle.default) { (alert) -> Void in
            guard let settingsUrl = URL(string: UIApplicationOpenSettingsURLString) else {
                return
            }
            
            if UIApplication.shared.canOpenURL(settingsUrl) {
                UIApplication.shared.open(settingsUrl, completionHandler: { (success) in
                    print("Settings opened: \(success)") // Prints true
                })
            }
        }
        alert.addAction(okBtn)
        DispatchQueue.main.async {
        self.present(alert, animated: true, completion: nil)
        }
            
    }
    
    
    //MARK:- Open Gallery
    func openGallery()
    {
        PHPhotoLibrary.requestAuthorization
            { (status) in
                let status = PHPhotoLibrary.authorizationStatus()
                if (status == PHAuthorizationStatus.authorized)
                {
                    self.picker.allowsEditing = true
                    self.picker.sourceType = .photoLibrary
                    self.picker.delegate=self
                    self.picker.mediaTypes = UIImagePickerController.availableMediaTypes(for: .photoLibrary)!
                    DispatchQueue.main.async {
                    self.present(self.picker, animated: true, completion: nil)
                    }
                }
                else
                {
                    self.show_deny_alert(msg: "You deny to use photo library.To allow photo library you should allow photos permission from setting.")
                }
        }
    }
    
    
    //MARK:- didFinishPickingMediaWithInfo
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any])
    {
        let chosenImage = info[UIImagePickerControllerOriginalImage] as! UIImage
        SelectedImage = chosenImage
        
        groupProfile_imageview.image = SelectedImage
        dismiss(animated:true, completion: nil)
    }
    
    //MARK:- imagePickerControllerDidCancel
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        picker.dismiss(animated: true, completion: nil)
    }
    
    //MARK:- Button Cloase Add Friend Action
    @IBAction func btnCloseAddFriendAction(_ sender: UIButton)
    {
        view_addFriend.isHidden = true
    }
    
    
    //MARK:- Get Friends Api
    func callgetFriendsApi(user_id_str : String)
    {
        if(CommonFunction.isInternetAvailable())
        {
            //let params = ["user_id":user_id_str]
            
            Alamofire.request(Constants.BASEURL+"friend/friend-list" + "?access-token=\(UserDefaults.standard.value(forKey: "Access_Token") as! String)", method: .get, parameters: nil, encoding: JSONEncoding.default, headers: nil).responseJSON { response in
                
                if let result_dict = response.result.value as? [String:Any]
                {
                    if(result_dict["code"] as! Int == 200)
                    {
                        if let user_arr = result_dict["data"] as? [[String:Any]]
                        {
                            self.friendListArr = user_arr
                        }
                        self.tbl_user.reloadData()
                    }
                    else
                    {
                        if let error_arr = result_dict["errorData"] as? [[String:Any]]
                        {
                            if let error_msg = error_arr[0]["message"] as? String
                            {
                                self.view.makeToast(error_msg)
                            }
                        }
                    }
                }
            }
        }
        else
        {
            self.view.makeToast("Please Check Your Internet Connection!!")
        }
    }
    
    //MARK:- Tableview Delegate Methods
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return friendListArr.count
        
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell = tableView.dequeueReusableCell(withIdentifier: "add_user_group_cell") as! add_user_group_cell
        CommonFunction.addshadow_view(view: cell.container_view, shadow_radius: 15.0, shadow_color: UIColor.lightGray, shadow_opecity: 0.5, shadow_offset: CGSize(width: 2.0, height: 2.0), is_clipBound: false, corner_radius: 10.0)
        let user_dict = friendListArr[indexPath.row]
        print(user_dict)
        if(selected_friends_id.contains(user_dict["user_id"] as! Int))
        {
            cell.btn_check.setBackgroundImage(UIImage(named: "checkfill"), for: .normal)
        }
        else
        {
            cell.btn_check.setBackgroundImage(UIImage(named: "checkempty"), for: .normal)
        }
        if let img_str = user_dict["user_profile_image"] as? String
        {
            cell.user_profile_imageview.sd_setImage(with: URL(string: img_str), placeholderImage: UIImage(named: "userPlaceHolder"))
        }
        if let name_str = user_dict["fullname"] as? String
        {
            cell.lbl_username.text = name_str
        }
        if let is_coach = user_dict["is_coach"] as? Bool
        {
            if(is_coach)
            {
                cell.img_coach.isHidden = false
            }
            else
            {
                cell.img_coach.isHidden = true
            }
        }
        if let city = user_dict["city"] as? String
        {
            cell.lbl_City.text = city
        }
        if let proffesion = user_dict["aboutme"] as? String
        {
            cell.lbl_profession.text = proffesion.decodeEmoji
        }
        cell.btn_check.addTarget(self, action: #selector(btnselectMemberAction(_:)), for: .touchUpInside)
        cell.selectionStyle = .none
        return cell
    }
    
    //MARK:- Button Select Member Action
   @objc func btnselectMemberAction(_ sender: UIButton)
    {
        guard let cell = sender.superview?.superview?.superview as? add_user_group_cell else {
            return // or fatalError() or whatever
        }
        let selected_indexPath = tbl_user.indexPath(for: cell)
        if(sender.backgroundImage(for: .normal) == UIImage(named: "checkempty"))
        {
            selected_friends_id.append(friendListArr[(selected_indexPath?.row)!]["user_id"] as! Int)
            Selected_friends.append(friendListArr[(selected_indexPath?.row)!])
            sender.setBackgroundImage(UIImage(named: "checkfill"), for: .normal)
        }
        else
        {
            let delete_index = selected_friends_id.index(of: friendListArr[(selected_indexPath?.row)!]["user_id"] as! Int)
            selected_friends_id.remove(at: delete_index!)
            Selected_friends.remove(at: (selected_indexPath?.row)!)
            sender.setBackgroundImage(UIImage(named: "checkempty"), for: .normal)
        }
        
    }
    
    
    //MARK:- Button Done Action
    @IBAction func btnDoneAction(_ sender: UIButton) {
        
        view_addFriend.isHidden = true
        self.lblCountSelectedMembers.text = "You have added \(Selected_friends.count) Friends into your group"
        
            if Selected_friends.count > 0
            {
                self.collectiongroupmembers.isHidden = false
                self.lblNoMembers.isHidden = true
                collectiongroupmembers.reloadData()
            }
            else
            {
                self.collectiongroupmembers.isHidden = true
                self.lblNoMembers.isHidden = false
            }
    }
    
    //MARK:- Button Save action
    @IBAction func btnSaveAction(_ sender: UIButton)
    {
        if(view_addFriend.isHidden == true)
        {
            if(btnCreateGroup.title(for: .normal) == "Create")
            {
                if(SelectedImage != nil && !((textfield_name.text?.isEmpty)!))
                {
                    self.createGroup()
                }
                else
                {
                    if(SelectedImage == nil)
                    {
                        //self.displayAlert(msg: "Please add group image", title_str: Constants.APP_NAME)
                        self.view.makeToast("Please add group image")
                    }
                    else if((textfield_name.text?.isEmpty)!)
                    {
                        //self.displayAlert(msg: "Please add group name", title_str: Constants.APP_NAME)
                        self.view.makeToast("Please add group name")
                    }
                    else if(self.selected_friends_id.count==0)
                    {
                        //self.displayAlert(msg: "Please add atleast one member in group", title_str: Constants.APP_NAME)
                        self.view.makeToast("Please add atleast one member in group")
                    }
                }
            }
            else if(btnCreateGroup.title(for: .normal) == "Update")
            {
                if(!((textfield_name.text?.isEmpty)!))
                {
                    self.callEditGroupAPI()
                }
                else
                {
                    if((textfield_name.text?.isEmpty)!)
                    {
                        //self.displayAlert(msg: "Please add group name", title_str: Constants.APP_NAME)
                        self.view.makeToast("Please add group name")
                    }
                }
            }
            else
            {
                self.callLeavegroupAPI()
            }
        }
        else
        {
            view_addFriend.isHidden = true

            if Selected_friends.count > 0
            {
                self.collectiongroupmembers.isHidden = false
                self.lblNoMembers.isHidden = true
                collectiongroupmembers.reloadData()
            }
            else
            {
                self.collectiongroupmembers.isHidden = true
                self.lblNoMembers.isHidden = false
            }
        }
    }
    
    
    //MARK:- Create Group Api
    func createGroup()
    {
        let convertedArray: [String] = selected_friends_id.map{ String($0)}
        let parameters = ["group_name":textfield_name.text!,"memberList":"\(convertedArray.joined(separator: ","))"]
        print(parameters)
        
        let timestamp = NSDate().timeIntervalSince1970
        
        
         MBProgressHUD.showAdded(to: self.view, animated: true)

        Alamofire.upload(multipartFormData: { (multipartFormData) in

            multipartFormData.append(UIImageJPEGRepresentation(self.SelectedImage!, 0.5)!, withName: "group_image", fileName: "\(timestamp).jpeg", mimeType: "image/jpeg")
            for (key, value) in parameters {
                multipartFormData.append((value as AnyObject).data(using: String.Encoding.utf8.rawValue)!, withName: key)
            }
        }, to:Constants.BASEURL+MethodName.createGroup + "?access-token=\(UserDefaults.standard.value(forKey: "Access_Token") as! String)")
        { (result) in
            switch result {
            case .success(let upload, _, _):

                upload.uploadProgress(closure: { (progress) in
                    //Print progress
                    print(progress)
                })

                upload.responseJSON { response in
                   MBProgressHUD.hide(for: self.view, animated: true)
                    if let result = response.result.value as? [String:Any]
                    {
                        if let rcode = result["code"] as? Int
                        {
                            if rcode == 200
                            {
                                print("Success")
                                self.SelectedImage = nil
                                self.selected_friends_id.removeAll()
                                self.Selected_friends.removeAll()
                                let objGroupListViewController = Constants.mainStoryboard.instantiateViewController(withIdentifier: "GroupListViewController") as! GroupListViewController
                                self.navigationController?.pushViewController(objGroupListViewController, animated: true)
                            }
                            else
                            {
                                if let error_arr = result["errorData"] as? [[String:Any]]
                                {
                                    if let error_msg = error_arr[0]["message"] as? String
                                    {
                                        self.view.makeToast(error_msg)
                                    }
                                }
                            }
                        }
                    }
                }

            case .failure(let encodingError):
                //print encodingError.description
                self.view.makeToast(encodingError.localizedDescription)
                break
            }
        }
        
        
        
    }
    
    
    //MARK:- Collectionview Delegate Methods
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return Selected_friends.count
        
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "SelectedFriendsCollectionCell", for: indexPath) as! SelectedFriendsCollectionCell
        
       // let user_dict = Selected_friends[indexPath.row]
//        cell.user_profile_imageview.image = UIImage(named: "user")
//        cell.user_profile_imageview.layer.cornerRadius = cell.user_profile_imageview.frame.size.height/2
//        cell.btnRemove.addTarget(self, action: #selector(btnRemoveMemberAction(_:)), for: .touchUpInside)
//        cell.backgroundBorderView.layer.cornerRadius = cell.backgroundBorderView.frame.size.height/2
//        cell.backgroundBorderView.layer.borderWidth = 2.0
//        cell.backgroundBorderView.layer.borderColor = UIColor.black.cgColor
        
        let user_dict = Selected_friends[indexPath.row]
        
        if let userProfileImg = user_dict["user_profile_image"] as? String
        {
            cell.user_profile_imageview.sd_setImage(with: URL.init(string: userProfileImg)!, placeholderImage: UIImage(named: "userPlaceHolder") )
        }
        else
        {
            cell.user_profile_imageview.image = UIImage(named: "userPlaceHolder")
        }
        cell.user_profile_imageview.layer.cornerRadius = cell.user_profile_imageview.frame.size.height/2
        cell.user_profile_imageview.clipsToBounds = true
        if(is_edit)
        {
            cell.btnRemove.isHidden = true
        }
        else
        {
            cell.btnRemove.isHidden = false
        }
        cell.btnRemove.addTarget(self, action: #selector(btnRemoveMemberAction(_:)), for: .touchUpInside)
        cell.backgroundBorderView.layer.cornerRadius = cell.backgroundBorderView.frame.size.height/2
        cell.backgroundBorderView.layer.borderWidth = 2.0
        cell.backgroundBorderView.layer.borderColor = UIColor.init(red: 252/255, green: 89/255, blue: 138/255, alpha: 1.0).cgColor
        return cell
    }
    
    
    //MARK:- Button Remove Member Action
    @objc func btnRemoveMemberAction(_ sender: UIButton)
    {
        guard let cell = sender.superview?.superview as? SelectedFriendsCollectionCell else {
            return // or fatalError() or whatever
        }
        let selected_indexPath = collectiongroupmembers.indexPath(for: cell)
        
            let delete_index = selected_friends_id.index(of: Selected_friends[(selected_indexPath?.row)!]["user_id"] as! Int)
            selected_friends_id.remove(at: delete_index!)
            Selected_friends.remove(at: (selected_indexPath?.row)!)
            collectiongroupmembers.reloadData()

    }
    
    //MARK:- Edit Group Api
    func callEditGroupAPI()
    {

        let parameters = ["group_name":textfield_name.text!]
        let timestamp = NSDate().timeIntervalSince1970
        MBProgressHUD.showAdded(to: self.view, animated: true)
        
        Alamofire.upload(multipartFormData: { (multipartFormData) in
        
            multipartFormData.append(UIImageJPEGRepresentation(self.SelectedImage!, 0.5)!, withName: "group_image", fileName: "\(timestamp).jpeg", mimeType: "image/jpeg")
            for (key, value) in parameters {
                multipartFormData.append((value as AnyObject).data(using: String.Encoding.utf8.rawValue)!, withName: key)
            }
        }, to:Constants.BASEURL+MethodName.editGroup + "?access-token=\(UserDefaults.standard.value(forKey: "Access_Token") as! String)&id=\(self.group_id)",method: .post, headers: ["Content-type": "multipart/form-data"])
        { (result) in
            switch result {
            case .success(let upload, _, _):
                
                upload.uploadProgress(closure: { (progress) in
                    //Print progress
                    print(progress)
                })
                
                upload.responseJSON { response in
                    MBProgressHUD.hide(for: self.view, animated: true)
                    if let result = response.result.value as? [String:Any]
                    {
                        print(result)
             
                        if let rcode = result["code"] as? Int
                        {
                            if rcode == 200
                            {
                                print("Success")
                                self.SelectedImage = nil
                                self.selected_friends_id.removeAll()
                                self.Selected_friends.removeAll()
                                let objGroupListViewController = Constants.mainStoryboard.instantiateViewController(withIdentifier: "GroupListViewController") as! GroupListViewController
                                self.navigationController?.pushViewController(objGroupListViewController, animated: true)
                            }
                            else
                            {
                                if let error_arr = result["errorData"] as? [[String:Any]]
                                {
                                    if let error_msg = error_arr[0]["message"] as? String
                                    {
                                        self.view.makeToast(error_msg)
                                    }
                                }
                            }
                        }
                    }
                }
                
            case .failure(let encodingError):
                self.view.makeToast(encodingError.localizedDescription)
                break
            }
        }
    }
    
    
    //MARK:- Leave Group Api
    func callLeavegroupAPI()
    {
        if(CommonFunction.isInternetAvailable())
        {
            Alamofire.request(Constants.BASEURL+MethodName.leaveGroup + "?access-token=\(UserDefaults.standard.value(forKey: "Access_Token") as! String)&group_id=\(self.group_id)", method: .post, parameters: nil, encoding: JSONEncoding.default, headers: nil).responseJSON { response in
                
                if let result_dict = response.result.value as? [String:Any]
                {
                    if(result_dict["code"] as! Int == 200)
                    {
                        print(result_dict)
                    }
                    else
                    {
                        if let error_arr = result_dict["errorData"] as? [[String:Any]]
                        {
                            if let error_msg = error_arr[0]["message"] as? String
                            {
                                self.view.makeToast(error_msg)
                            }
                        }
                    }
                }
            }
        }
        else
        {
            self.view.makeToast("Please Check Your Internet Connection!!")
        }
    }
    
}
