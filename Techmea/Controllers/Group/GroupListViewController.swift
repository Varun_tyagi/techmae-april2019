//
//  GroupListViewController.swift
//  Techmea
//
//  Created by Dhaval Panchani on 05/09/18.
//  Copyright © 2018 VISHAL SETH. All rights reserved.
//

import UIKit
import Alamofire
import SDWebImage
import MBProgressHUD
import Toast_Swift
import GoogleMobileAds
//MARK:- GroupListCell
class grouplist_cell : UICollectionViewCell
{
    @IBOutlet weak var lbl_groupName: UILabel!
    @IBOutlet weak var groupProfile_imageview: UIImageView!
    @IBOutlet weak var lbl_memberCount: UILabel!
    @IBOutlet weak var btn_join: UIButton!
    @IBOutlet weak var btn_remove: UIButton!
    @IBOutlet weak var btn_leave: UIButton!
}
    
class GroupListViewController: UIViewController,UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout,UITextFieldDelegate,GADBannerViewDelegate {

    //MARK:- Outlets
    
    @IBOutlet weak var bannnerView: DFPBannerView!
    @IBOutlet weak var search_textfield: UITextField!
    @IBOutlet weak var lbl_nodata: UILabel!
    @IBOutlet weak var lbl_memberCount: UILabel!
    @IBOutlet weak var groupList_collectionview: UICollectionView!
    var group_arr = [[String:Any]]()
    var ArrGroupList = [GroupListModel]()
    var ArrSearchGroupList = [SearchGroupList]()
    var is_search_active = Bool()
    var group_select_index = Int()
    var user_id_str = ""
    
    
    //MARK:- viewDidLoad
    override func viewDidLoad()
    {
        super.viewDidLoad()
        search_textfield.attributedPlaceholder = NSAttributedString(string: "Search Group",
                                                                    attributes: [NSAttributedStringKey.foregroundColor: UIColor.white])
        
        addBannerViewToView(bannnerView)
        bannnerView.adUnitID = Constants.kGOOGLE_AD_ID
        bannnerView.rootViewController = self
        bannnerView.delegate = self
        bannnerView.load(DFPRequest())
    }
    
    /// Tells the delegate an ad request loaded an ad.
    func adViewDidReceiveAd(_ bannerView: GADBannerView) {
        print("adViewDidReceiveAd")
        addBannerViewToView(bannnerView)
        bannnerView.alpha = 0
        UIView.animate(withDuration: 1, animations: {
            self.bannnerView.alpha = 1
        })
    }
    
    /// Tells the delegate an ad request failed.
    func adView(_ bannerView: GADBannerView,
                didFailToReceiveAdWithError error: GADRequestError) {
        print("adView:didFailToReceiveAdWithError: \(error.localizedDescription)")
    }
    
    /// Tells the delegate that a full-screen view will be presented in response
    /// to the user clicking on an ad.
    func adViewWillPresentScreen(_ bannerView: GADBannerView) {
        print("adViewWillPresentScreen")
    }
    
    /// Tells the delegate that the full-screen view will be dismissed.
    func adViewWillDismissScreen(_ bannerView: GADBannerView) {
        print("adViewWillDismissScreen")
    }
    
    /// Tells the delegate that the full-screen view has been dismissed.
    func adViewDidDismissScreen(_ bannerView: GADBannerView) {
        print("adViewDidDismissScreen")
    }
    
    /// Tells the delegate that a user click will open another app (such as
    /// the App Store), backgrounding the current app.
    func adViewWillLeaveApplication(_ bannerView: GADBannerView) {
        print("adViewWillLeaveApplication")
    }

    
    
    func addBannerViewToView(_ bannerView: UIView) {
        bannerView.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(bannerView)
        view.addConstraints(
            [NSLayoutConstraint(item: bannerView,
                                attribute: .bottom,
                                relatedBy: .equal,
                                toItem: bottomLayoutGuide,
                                attribute: .top,
                                multiplier: 1,
                                constant: 0),
             NSLayoutConstraint(item: bannerView,
                                attribute: .centerX,
                                relatedBy: .equal,
                                toItem: view,
                                attribute: .centerX,
                                multiplier: 1,
                                constant: 0)
            ])
    }
    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
    }
    
    //MARK:- viewWillAppear
    override func viewWillAppear(_ animated: Bool) {
        
        self.tabBarController?.tabBar.isHidden = true
        self.getGroupList()
    }
    
    
    //MARK:- textFieldShouldReturn
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        is_search_active = true
        self.callSearchgroupAPI()
        return false
    }
    
    
    //MARK:- Collection View Delegate Methods
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int
    {
        if(is_search_active)
        {
            return ArrSearchGroupList.count
        }
        else
        {
            return ArrGroupList.count
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell
    {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "grouplist_cell", for: indexPath) as! grouplist_cell
        if(is_search_active)
        {
            var  group_list_model = SearchGroupList()
            group_list_model = ArrSearchGroupList[indexPath.row]
            cell.btn_remove.isHidden = true
            if(group_list_model.is_join)
            {
                cell.btn_join.isHidden = true
                if(UserDefaults.standard.value(forKey: Constants.USERID) as! Int == group_list_model.groupAdminID)
                {
                    cell.btn_remove.isHidden = false
                    cell.btn_leave.isHidden = true
                }
                else
                {
                    cell.btn_remove.isHidden = true
                    cell.btn_leave.isHidden = false
                }
            }
            else
            {
                cell.btn_join.isHidden = false
                cell.btn_remove.isHidden = true
                cell.btn_leave.isHidden = true
            }
            cell.lbl_groupName.text = group_list_model.groupName
            cell.groupProfile_imageview.sd_setImage(with: URL(string:group_list_model.imageUrl), placeholderImage: UIImage(named: "userPlaceHolder"))
            cell.lbl_memberCount.text = "\(group_list_model.noOfMember)"
            
        }
        else
        {
            var  group_list_model = GroupListModel()
            group_list_model = ArrGroupList[indexPath.row]
            cell.btn_join.isHidden = true
            if(UserDefaults.standard.value(forKey: Constants.USERID) as! Int == group_list_model.groupAdminID)
            {
                cell.btn_remove.isHidden = false
                cell.btn_leave.isHidden = true
            }
            else
            {
                cell.btn_remove.isHidden = true
                cell.btn_leave.isHidden = false
            }
            cell.lbl_groupName.text = group_list_model.groupName
            
            cell.groupProfile_imageview.sd_setImage(with: URL(string:group_list_model.imageUrl), placeholderImage: UIImage(named: "userPlaceHolder"))
            
            if(group_list_model.isMember)
            {
                cell.btn_join.isHidden = true
                if(UserDefaults.standard.value(forKey: Constants.USERID) as! Int == group_list_model.groupAdminID)
                {
                    cell.btn_remove.isHidden = false
                    cell.btn_leave.isHidden = true
                }
                else
                {
                    cell.btn_remove.isHidden = true
                    cell.btn_leave.isHidden = false
                }
            }
            else
            {
                cell.btn_join.isHidden = false
                cell.btn_remove.isHidden = true
                cell.btn_leave.isHidden = true
            }
            
            cell.lbl_memberCount.text = "\(group_list_model.noOfMember)"
            
        }
        cell.btn_remove.addTarget(self, action: #selector(btnRemoveAction(_:)), for: .touchUpInside)
        cell.btn_leave.addTarget(self, action: #selector(btnLeaveAction(_:)), for: .touchUpInside)
        cell.btn_join.addTarget(self, action: #selector(btnJoinAction(_:)), for: .touchUpInside)
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize
    {
        let cellsAcross: CGFloat = 3
            let spaceBetweenCells: CGFloat = 15
            let dim = (collectionView.bounds.width - (cellsAcross - 1) * spaceBetweenCells) / cellsAcross
            return CGSize(width: dim, height: 150)
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath)
    {
        let storyBoard = UIStoryboard(name: "Main", bundle: nil)
        let objGroupDetailViewController = storyBoard.instantiateViewController(withIdentifier: "GroupDetailViewController") as! GroupDetailViewController
        if(is_search_active)
        {
            var  group_list_model = SearchGroupList()
            group_list_model = ArrSearchGroupList[indexPath.row]
            if(group_list_model.is_join)
            {
                objGroupDetailViewController.is_newmember = false
            }
            else
            {
                objGroupDetailViewController.is_newmember = true
            }
            objGroupDetailViewController.groupID = group_list_model.groupId
        }
        else
        {
            var  group_list_model = GroupListModel()
            group_list_model = ArrGroupList[indexPath.row]
            objGroupDetailViewController.is_newmember = false
            objGroupDetailViewController.groupID = group_list_model.groupId
        }
        self.navigationController?.pushViewController(objGroupDetailViewController, animated: true)
    }
    
    //MARK:- Button Create Group Action
    @IBAction func btnAddgroupAction(_ sender: UIButton)
    {
        let createGroupPage = self.storyboard?.instantiateViewController(withIdentifier: "CreateGroupViewController") as! CreateGroupViewController
        self.navigationController?.pushViewController(createGroupPage, animated: true)
    }
    
    //MARK:- Button Join Group Action
    @objc func btnJoinAction(_ sender: UIButton)
    {
        guard let cell = sender.superview?.superview as? grouplist_cell else
        {
            return // or fatalError() or whatever
        }
        let selected_indexPath = groupList_collectionview.indexPath(for: cell)
        var delete_group_id = String()
        if(is_search_active)
        {
            var user_model = SearchGroupList()
            user_model = ArrSearchGroupList[(selected_indexPath?.row)!]
            delete_group_id = "\(user_model.groupId)"
        }
        else
        {
            var user_model = GroupListModel()
            user_model = ArrGroupList[(selected_indexPath?.row)!]
            delete_group_id = "\(user_model.groupId)"
        }
        group_select_index = selected_indexPath!.row
        //self.callJoingroupAPI(group_id_str: delete_group_id)
        
        
        if(CommonFunction.isInternetAvailable())
        {
            MBProgressHUD.showAdded(to: self.view, animated: true)
            Alamofire.request(Constants.BASEURL + MethodName.joinGroup + "?access-token=\(UserDefaults.standard.value(forKey: "Access_Token") as! String)&group_id=\(delete_group_id)", method: .post, parameters: nil, encoding: JSONEncoding.default, headers: ["Content-Type":"application/json"]).responseJSON { response in
                
                MBProgressHUD.hide(for: self.view, animated: true)
                if let result_dict = response.result.value as? [String:Any]
                {
                    if let code = result_dict["code"] as? Int
                    {
                        if(code == 200)
                        {
                            //cell.btn_join.setTitle("Leave", for: .normal)
                            
                            let btnJoinAttributes = NSAttributedString(string: "Leave",attributes: [NSAttributedStringKey.foregroundColor : UIColor(red: 252.0/255.0, green: 89.0/255.0, blue: 138.0/255.0, alpha: 1.0) ,NSAttributedStringKey.underlineStyle : NSUnderlineStyle.styleSingle.rawValue,NSAttributedStringKey.underlineColor : UIColor(red: 252.0/255.0, green: 89.0/255.0, blue: 138.0/255.0, alpha: 1.0)])
                            cell.btn_join.setAttributedTitle(btnJoinAttributes, for: .normal)
                            
                            self.Reloadcollection()
                            self.view.makeToast("Success")
                        }
                        else
                        {
                            if let error_arr = result_dict["errorData"] as? [[String:Any]]
                            {
                                if let error_msg = error_arr[0]["message"] as? String
                                {
                                    self.view.makeToast(error_msg)
                                }
                            }
                        }
                    }
                }
            }
        }
        else
        {
            self.view.makeToast("Please Check Your Internet Connection!!")
        }
    }
    
    //MARK:- Button Remove Group Action
    @objc func btnRemoveAction(_ sender: UIButton)
    {
        
        guard let cell = sender.superview?.superview as? grouplist_cell else
        {
            return // or fatalError() or whatever
        }
        let selected_indexPath = groupList_collectionview.indexPath(for: cell)
        var delete_group_id = String()
        if(is_search_active)
        {
            var user_model = SearchGroupList()
            user_model = ArrSearchGroupList[(selected_indexPath?.row)!]
            delete_group_id = "\(user_model.groupId)"
        }
        else
        {
            var user_model = GroupListModel()
            user_model = ArrGroupList[(selected_indexPath?.row)!]
            delete_group_id = "\(user_model.groupId)"
        }
        group_select_index = selected_indexPath!.row
        let view = ModalView.instantiateFromNib()
        view.updateModal("Delete Group", descMessage: "Are you sure you want to delete this group?", firstTitle: "YES", secondTitle: "NO", type: .modalAlert)
        let modal = PathDynamicModal()
        modal.showMagnitude = 200.0
        modal.closeMagnitude = 130.0
        modal.closeByTapBackground = false
        modal.closeBySwipeBackground = false
        view.Cancel2ButtonHandler = {[weak modal] in
            
            modal?.closeWithLeansRandom()
            return
        }
        view.OkButtonHandler = {[weak modal] in
            
            self.calldDeleteGroupAPI(group_id_str: delete_group_id)
            modal?.closeWithLeansRandom()
            return
        }
        modal.show(modalView: view, inView: (UIApplication.shared.delegate?.window!)!)
    }
    
    //MARK:- Buttn Leave Group Action
    @objc func btnLeaveAction(_ sender: UIButton)
    {
        
        guard let cell = sender.superview?.superview as? grouplist_cell else
        {
            return // or fatalError() or whatever
        }
        let selected_indexPath = groupList_collectionview.indexPath(for: cell)
        var delete_group_id = String()
        if(is_search_active)
        {
            var user_model = SearchGroupList()
            user_model = ArrSearchGroupList[(selected_indexPath?.row)!]
            delete_group_id = "\(user_model.groupId)"
        }
        else
        {
            var user_model = GroupListModel()
            user_model = ArrGroupList[(selected_indexPath?.row)!]
            delete_group_id = "\(user_model.groupId)"
        }
        group_select_index = selected_indexPath!.row
        let view = ModalView.instantiateFromNib()
        view.updateModal("Leave Group", descMessage: "Are you sure you want to leave this group?", firstTitle: "YES", secondTitle: "NO", type: .modalAlert)
        let modal = PathDynamicModal()
        modal.showMagnitude = 200.0
        modal.closeMagnitude = 130.0
        modal.closeByTapBackground = false
        modal.closeBySwipeBackground = false
        view.Cancel2ButtonHandler = {[weak modal] in
            
            modal?.closeWithLeansRandom()
            return
        }
        view.OkButtonHandler = {[weak modal] in
            
            self.callLeavegroupAPI(group_id_str: delete_group_id)
            modal?.closeWithLeansRandom()
            return
        }
        modal.show(modalView: view, inView: (UIApplication.shared.delegate?.window!)!)
    }
    
    //MARK:- Delete Group Api
    func calldDeleteGroupAPI(group_id_str: String)
    {
        
        if(CommonFunction.isInternetAvailable())
        {
            MBProgressHUD.showAdded(to: self.view, animated: true)
            Alamofire.request(Constants.BASEURL + MethodName.deleteGroup + "?access-token=\(UserDefaults.standard.value(forKey: "Access_Token") as! String)&id=\(group_id_str)", method: .delete, parameters: nil , encoding: JSONEncoding.default, headers:nil).responseJSON
                { response in
                    MBProgressHUD.hide(for: self.view, animated: true)
                    
                    if let result_dict = response.result.value as? [String:Any]
                    {
                        if let code = result_dict["code"] as? Int
                        {
                            if(code==200)
                            {
                                if let data_dict = result_dict["data"] as? [String:Any]
                                {
                                    self.displayAlert(msg: data_dict["message"] as! String, title_str: Constants.APP_NAME)
                                }
                                if(self.is_search_active)
                                {
                                    self.ArrSearchGroupList.remove(at: self.group_select_index)
                                }
                                else
                                {
                                    self.ArrGroupList.remove(at: self.group_select_index)
                                }
                                self.Reloadcollection()
                            }
                            else
                            {
                                if let error_arr = result_dict["errorData"] as? [[String:Any]]
                                {
                                    if let error_msg = error_arr[0]["message"] as? String
                                    {
                                        self.view.makeToast(error_msg)
                                    }
                                }
                            }
                        }
                    }
            }
        }
        else
        {
            self.view.makeToast("Please Check Your Internet Connection!!")
        }
    }
    
    //MARK:- Group List Api
    func getGroupList()
    {
        
        if(CommonFunction.isInternetAvailable())
        {
            MBProgressHUD.showAdded(to: self.view, animated: true)
            var strUrl = String()
            let headerString = ["Content-Type":"application/json"]
            
            if user_id_str == ""
            {
                strUrl = Constants.BASEURL + MethodName.groupList + "?access-token=\(UserDefaults.standard.value(forKey: "Access_Token") as! String)"
            }
            else
            {
                
                if UserDefaults.standard.value(forKey: Constants.USERID) as? Int == Int(user_id_str)
                {
                    
                   strUrl =  Constants.BASEURL + MethodName.groupList + "?access-token=\(UserDefaults.standard.value(forKey: "Access_Token") as! String)"
                }
                else
                {
                    
                    strUrl = Constants.BASEURL + MethodName.groupList + "?access-token=\(UserDefaults.standard.value(forKey: "Access_Token") as! String)&user_id=\(user_id_str)"
                    
                }
            }
            //let paramString = ["user_id":"\(UserDefaults.standard.value(forKey: Constants.USERID) as! Int)"]
            
            Alamofire.request(strUrl , method: .post, parameters: nil, encoding: JSONEncoding.default, headers: headerString).responseJSON { response in
                
                MBProgressHUD.hide(for: self.view, animated: true)
                
                if let result = response.result.value as? [String:Any]
                {
                    if let status = result["code"] as? Int
                    {
                        if status == 200
                        {
                            self.ArrGroupList.removeAll()
                            
                            if let grouplist = result["data"] as? [[String:Any]]
                            {
                                
                                for i in 0..<grouplist.count
                                {
                                    let group_list_model = GroupListModel()
                                    
                                    if let dict = grouplist[i] as? [String:Any]
                                    {
                                        
                                        if let groupId = dict["groupId"] as? Int
                                        {
                                            group_list_model.groupId = groupId
                                        }
                                        if let groupName = dict["groupName"] as? String
                                        {
                                            group_list_model.groupName = groupName
                                        }
                                        if let imageUrl = dict["imageUrl"] as? String
                                        {
                                            group_list_model.imageUrl = imageUrl
                                        }
                                        if let noOfMembers = dict["noOfMembers"] as? String
                                        {
                                            group_list_model.noOfMember = noOfMembers
                                        }
                                        if let adminID = dict["groupAdminID"] as? Int
                                        {
                                            group_list_model.groupAdminID = adminID
                                        }
                                        if let isJoin = dict["is_join"] as? Bool
                                        {
                                            group_list_model.isMember = isJoin
                                        }
                                    }
                                    self.ArrGroupList.append(group_list_model)
                                }
                                
                                self.Reloadcollection()
                                
                            }
                        }
                        else
                        {
                            if let error_arr = result["errorData"] as? [[String:Any]]
                            {
                                if let error_msg = error_arr[0]["message"] as? String
                                {
                                    self.view.makeToast(error_msg)
                                }
                            }
                        }
                    }
                }
            }
        }
        else
        {
            self.view.makeToast("Please Check Your Internet Connection!!")
        }
    }
    
    //MARK:- Search Group Api
    func callSearchgroupAPI()
    {
        self.view.endEditing(true)
        if(CommonFunction.isInternetAvailable())
        {
            
        let params = ["searchKey": search_textfield.text!]
        print(params)
        print(Constants.BASEURL + MethodName.searchGroup + "?access-token=\(UserDefaults.standard.value(forKey: "Access_Token") as! String)")
        MBProgressHUD.showAdded(to: self.view, animated: true)
        Alamofire.request(Constants.BASEURL + MethodName.searchGroup + "?access-token=\(UserDefaults.standard.value(forKey: "Access_Token") as! String)", method: .post, parameters: params, encoding: JSONEncoding.default, headers: ["Content-Type":"application/json"]).responseJSON { response in
            
            MBProgressHUD.hide(for: self.view, animated: true)
            if let result_dict = response.result.value as? [String:Any]
            {
                print(result_dict)
                self.ArrSearchGroupList.removeAll()
                if let code = result_dict["code"] as? Int
                {
                    if(code==200)
                    {
                        if let user_list = result_dict["data"] as? [[String:Any]]
                        {
                            for i in 0..<user_list.count
                            {
                                let searchModel =  SearchGroupList()
                                if let userInfo_dict = user_list[i] as? [String:Any]
                                {
                                    if let groupname = userInfo_dict["groupName"] as? String
                                    {
                                        searchModel.groupName = groupname
                                    }
                                    if let groupId = userInfo_dict["groupId"] as? Int
                                    {
                                        searchModel.groupId = groupId
                                    }
                                    if let imageurl = userInfo_dict["imageUrl"] as? String
                                    {
                                        searchModel.imageUrl = imageurl
                                    }
                                    if let is_join = userInfo_dict["is_join"] as? Bool
                                    {
                                        searchModel.is_join = is_join
                                    }
                                    if let member_arr = userInfo_dict["userData"] as? [[String:Any]]
                                    {
                                        searchModel.all_member = member_arr
                                    }
                                    if let totalmember = userInfo_dict["noOfMembers"] as? String
                                    {
                                        searchModel.noOfMember = totalmember
                                    }
                                    if let adminID = userInfo_dict["groupAdminID"] as? Int
                                    {
                                        searchModel.groupAdminID = adminID
                                    }
                                }
                                self.ArrSearchGroupList.append(searchModel)
                            }
                            
                            self.Reloadcollection()
                        }
                    }
                    else
                    {
                        if let error_arr = result_dict["errorData"] as? [[String:Any]]
                        {
                            if let error_msg = error_arr[0]["message"] as? String
                            {
                                self.view.makeToast(error_msg)
                            }
                        }
                    }
                }
            }
        }
        }
        else
        {
            self.view.makeToast("Please Check Your Internet Connection!!")
        }
    }

    //MARK:- Button Search Action
    @IBAction func btnSearchAction(_ sender: Any) {
        self.view.endEditing(true)
        if((search_textfield.text?.isEmpty)!)
        {
            is_search_active = false
            self.getGroupList()
        }
        else
        {
            is_search_active = true
            self.callSearchgroupAPI()
        }
    }
    
    //MARK:- Back Action
    @IBAction func btnBackAction(_ sender: Any) {

        for vc in self.navigationController!.viewControllers
        {
            if vc is FriendListViewController
            {
                self.navigationController!.popToViewController(vc, animated: false)
                break
            }
            else
            {
                self.navigationController?.popViewController(animated: true)
            }
        }
    }
    
    //MARK:- Reload Collection
    func Reloadcollection()
    {
        self.groupList_collectionview.reloadData()
        if(ArrGroupList.count>0 || ArrSearchGroupList.count>0)
        {
            lbl_nodata.isHidden = true
        }
        else
        {
            lbl_nodata.isHidden = false
        }
        if(is_search_active)
        {
            self.lbl_memberCount.text = "\(self.ArrSearchGroupList.count) Groups Listed"
        }
        else
        {
            self.lbl_memberCount.text = "\(self.ArrGroupList.count) Groups Listed"
        }
    }
    
    
    
    //MARK:- Leave Group Api
    func callLeavegroupAPI(group_id_str:String)
    {
        if(CommonFunction.isInternetAvailable())
        {
            MBProgressHUD.showAdded(to: self.view, animated: true)
            Alamofire.request(Constants.BASEURL + MethodName.leaveGroup + "?access-token=\(UserDefaults.standard.value(forKey: "Access_Token") as! String)&group_id=\(group_id_str)", method: .post, parameters: nil, encoding: JSONEncoding.default, headers: ["Content-Type":"application/json"]).responseJSON { response in
                
                MBProgressHUD.hide(for: self.view, animated: true)
                if let result_dict = response.result.value as? [String:Any]
                {
                    if let code = result_dict["code"] as? Int
                    {
                        if(code==200)
                        {
                            if(self.is_search_active)
                            {
                                let search_model = self.ArrSearchGroupList[self.group_select_index]
                                search_model.is_join = false
                                self.ArrSearchGroupList[self.group_select_index] = search_model
                            }
                            else
                            {
                                self.ArrGroupList.remove(at: self.group_select_index)
                            }
                            self.Reloadcollection()
                        }
                        else
                        {
                            if let error_arr = result_dict["errorData"] as? [[String:Any]]
                            {
                                if let error_msg = error_arr[0]["message"] as? String
                                {
                                    self.view.makeToast(error_msg)
                                }
                            }
                        }
                    }
                }
            }
        }
        else
        {
            self.view.makeToast("Please Check Your Internet Connection!!")
        }
    }
}
