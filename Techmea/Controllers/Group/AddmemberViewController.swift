//
//  AddmemberViewController.swift
//  Techmea
//
//  Created by Dhaval Panchani on 18/09/18.
//  Copyright © 2018 VISHAL SETH. All rights reserved.
//

import UIKit
import Alamofire
import MBProgressHUD
import Toast_Swift

class AddmemberViewController: UIViewController,UITableViewDataSource,UITableViewDelegate {
    
   
    //MARK:- Outlets
    var current_groupid = Int()
    var friendListArr = [[String:Any]]()
    var selected_friends_id = [Int]()
    var deleted_row_index = Int()
    @IBOutlet weak var tblUser: UITableView!
    
    //MARK:- viewDidLoad
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tblUser.tableFooterView = UIView()
        tblUser.reloadData()
        self.callgetFriendsApi()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        self.tabBarController?.tabBar.isHidden = true
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK:- TableView Delegate Methods
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return friendListArr.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell = tableView.dequeueReusableCell(withIdentifier: "add_user_group_cell") as! add_user_group_cell
        CommonFunction.addshadow_view(view: cell.container_view, shadow_radius: 15.0, shadow_color: UIColor.lightGray, shadow_opecity: 0.5, shadow_offset: CGSize(width: 2.0, height: 2.0), is_clipBound: false, corner_radius: 10.0)
        let user_dict = friendListArr[indexPath.row]
        if(selected_friends_id.contains(user_dict["user_id"] as! Int))
        {
            cell.btn_check.setBackgroundImage(UIImage(named: "checkfill"), for: .normal)
        }
        else
        {
            cell.btn_check.setBackgroundImage(UIImage(named: "checkempty"), for: .normal)
        }
        if let img_str = user_dict["user_profile_image"] as? String
        {
            cell.user_profile_imageview.sd_setImage(with: URL(string: img_str), placeholderImage: UIImage(named: "userPlaceHolder"))
        }
        if let name_str = user_dict["fullname"] as? String
        {
            cell.lbl_username.text = name_str.decodeEmoji
        }
        if let is_coach = user_dict["is_coach"] as? Bool
        {
            if(is_coach)
            {
                cell.img_coach.isHidden = false
            }
            else
            {
                cell.img_coach.isHidden = true
            }
        }
        if let city = user_dict["city"] as? String
        {
            cell.lbl_City.text = city.decodeEmoji
        }
        if let proffesion = user_dict["aboutme"] as? String
        {
            cell.lbl_profession.text = proffesion.decodeEmoji
        }
        cell.btn_check.addTarget(self, action: #selector(btnselectMemberAction(_:)), for: .touchUpInside)
        cell.selectionStyle = .none
        return cell
      
    }
    
    //MARK:- Button Select Members Action
    @objc func btnselectMemberAction(_ sender: UIButton)
    {
        guard let cell = sender.superview?.superview?.superview as? add_user_group_cell else {
            return // or fatalError() or whatever
        }
        let selected_indexPath = tblUser.indexPath(for: cell)
        deleted_row_index = (selected_indexPath?.row)!
        if(sender.backgroundImage(for: .normal) == UIImage(named: "checkempty"))
        {
            selected_friends_id.append(friendListArr[(selected_indexPath?.row)!]["user_id"] as! Int)
            sender.setBackgroundImage(UIImage(named: "checkfill"), for: .normal)
            self.callAddMembersAPI()
        }
        else
        {
            let delete_index = selected_friends_id.index(of: friendListArr[(selected_indexPath?.row)!]["user_id"] as! Int)
            if let index = selected_friends_id.index(of: delete_index!) {
                selected_friends_id.remove(at: index)
            }
            sender.setBackgroundImage(UIImage(named: "checkempty"), for: .normal)
            self.callRemoveMembersAPI(userId_str: "\(friendListArr[(selected_indexPath?.row)!]["user_id"] as! Int)")
        }
        
    }
    
    //MARK:- Add Members Api
    func callAddMembersAPI()
    {
        if(CommonFunction.isInternetAvailable())
        {
            MBProgressHUD.showAdded(to: self.view, animated: true)
            let convertedArray: [String] = selected_friends_id.map{ String($0)}

            let params = ["group_id":"\(current_groupid)","memberList":"\(convertedArray.joined(separator: ","))"]
            print(params)
            Alamofire.request(Constants.BASEURL+MethodName.addMember + "?access-token=\(UserDefaults.standard.value(forKey: "Access_Token") as! String)", method: .post, parameters: params, encoding: JSONEncoding.default, headers: ["Content-Type":"application/json"]).responseJSON { response in
                MBProgressHUD.hide(for: self.view, animated: true)
                if let result_dict = response.result.value as? [String:Any]
                {
                    if(result_dict["code"] as! Int == 200)
                    {
                        self.view.makeToast("Member added successfully")
                    }
                    else
                    {
                        if let error_arr = result_dict["errorData"] as? [[String:Any]]
                        {
                            if let error_msg = error_arr[0]["message"] as? String
                            {
                                self.view.makeToast(error_msg)
                            }
                        }
                    }
                }
            }
        }
        else
        {
            self.view.makeToast("Please Check Your Internet Connection!!")
        }
    }
    
    
    //MARK:- Remove Members  Api
    func callRemoveMembersAPI(userId_str:String)
    {
        if(CommonFunction.isInternetAvailable())
        {
            MBProgressHUD.showAdded(to: self.view, animated: true)
            //let convertedArray: [String] = selected_friends_id.map{ String($0)}
            
            let params = ["user_id":userId_str]
            print(params)
            Alamofire.request(Constants.BASEURL+MethodName.removeGroupMember + "?access-token=\(UserDefaults.standard.value(forKey: "Access_Token") as! String)&group_id=\(current_groupid)", method: .post, parameters: params, encoding: JSONEncoding.default, headers: ["Content-Type":"application/json"]).responseJSON { response in
                MBProgressHUD.hide(for: self.view, animated: true)
                if let result_dict = response.result.value as? [String:Any]
                {
                    print(result_dict)
                    if(result_dict["code"] as! Int == 200)
                    {
                        self.view.makeToast("Member remove successfully")
                    }
                    else
                    {
                        if let error_arr = result_dict["errorData"] as? [[String:Any]]
                        {
                            if let error_msg = error_arr[0]["message"] as? String
                            {
                                self.view.makeToast(error_msg)

                            }
                        }
                    }
                }
            }
        }
        else
        {
            self.view.makeToast("Please Check Your Internet Connection!!")
        }
    }
    
    
    
    //MARK:- Get Friends Api
    func callgetFriendsApi()
    {
        if(CommonFunction.isInternetAvailable())
        {
            //let params = ["user_id":"\(UserDefaults.standard.value(forKey: Constants.USERID))"]
            MBProgressHUD.showAdded(to: self.view, animated: true)
            Alamofire.request(Constants.BASEURL+"friend/friend-list" + "?access-token=\(UserDefaults.standard.value(forKey: "Access_Token") as! String)", method: .get, parameters: nil, encoding: JSONEncoding.default, headers: nil).responseJSON { response in
                MBProgressHUD.hide(for: self.view, animated: true)
                if let result_dict = response.result.value as? [String:Any]
                {
                    if(result_dict["code"] as! Int == 200)
                    {
                        if let user_arr = result_dict["data"] as? [[String:Any]]
                        {
                            self.friendListArr = user_arr
                        }
                        self.tblUser.reloadData()
                    }
                    else
                    {
                        if let error_arr = result_dict["errorData"] as? [[String:Any]]
                        {
                            if let error_msg = error_arr[0]["message"] as? String
                            {
                                self.view.makeToast(error_msg)
                            }
                        }
                    }
                }
            }
        }
    }

    //MARK:- Back Action
    @IBAction func btnBackAction(_ sender: Any)
    {
        self.navigationController?.popViewController(animated: true)
    }
}
