//
//  BlockListViewController.swift
//  Techmea
//
//  Created by varun on 20/12/19.
//  Copyright © 2019 varun tyagi. All rights reserved.
//

import UIKit
import Alamofire
import SDWebImage
import MBProgressHUD
import Quickblox
import QMServices
import Toast_Swift

//MARK:- FriendsTableViewCell
   class BlockListTableViewCell : UITableViewCell
   {
       @IBOutlet weak var coach_imageview: UIImageView!
       @IBOutlet weak var user_profile_imageview: UIImageView!
       @IBOutlet weak var user_namelbl: UILabel!
       @IBOutlet weak var proffesion_namelbl: UILabel!
       
       @IBOutlet weak var container_view: UIView!
       @IBOutlet weak var country_namelbl: UILabel!
       
      
   }
class BlockListViewController: UIViewController ,UITableViewDelegate,UITableViewDataSource {
        
        
        //MARK:- Outlets
    var feed:PostListModel?
    var isGroupPost=false
        var friendListArr = [UserModel]()
        var is_from_group = Bool()
        @IBOutlet weak var tblFriends: UITableView!
        @IBOutlet weak var ViewNoData: UIView!
        var selected_btn = UIButton()
        var image_str = String()
        var current_index = Int()
        var user_id_str = ""
        
        var isTab : Bool = true
        
        var profile_user_id = Int()
        
        var perpage_data_count = Int()
           var is_next_page_available = Bool()
           var current_page = 1
        
        @IBOutlet weak var btnSearch: UIButton!
        @IBOutlet weak var back_white: UIImageView!
        @IBOutlet weak var btnBack: UIButton!
        
        //MARK:- viewDidLoad
        override func viewDidLoad() {
            super.viewDidLoad()
           
            getFriends()
            tblFriends.tableFooterView = UIView()
            
//            search_textfield.attributedPlaceholder = NSAttributedString(string: "Search Friend",
//                                                                        attributes: [NSAttributedStringKey.foregroundColor: UIColor.white])
            self.tblFriends.estimatedRowHeight = 123
            self.tblFriends.rowHeight = UITableViewAutomaticDimension
        }

        override func didReceiveMemoryWarning() {
            super.didReceiveMemoryWarning()
            // Dispose of any resources that can be recreated.
        }
        
        //MARK:- viewWillAppear
        override func viewWillAppear(_ animated: Bool) {
            
            if isTab == true
            {
                self.back_white.isHidden = true
                self.btnBack.isHidden = true
                self.tabBarController?.tabBar.frame.size.height = 49
                self.tabBarController?.tabBar .isHidden = false
            }
            else
            {
                self.back_white.isHidden = false
                self.btnBack.isHidden = false
                 self.tabBarController?.tabBar.isHidden = true
            }
        }
        

        //MARK:- TableView Delegate Methods
        func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
            return friendListArr.count
        }
        
        func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
            let cell = tableView.dequeueReusableCell(withIdentifier: "BlockListTableViewCell") as! BlockListTableViewCell
            
//            cell.contentView.backgroundColor = .red
//            cell.container_view.backgroundColor = .yellow
            cell.container_view.isHidden=false
            CommonFunction.addshadow_view(view: cell.container_view, shadow_radius: 15.0, shadow_color: UIColor.lightGray, shadow_opecity: 0.5, shadow_offset: CGSize(width: 2.0, height: 2.0), is_clipBound: false, corner_radius: 10.0)
            
            let user_model = friendListArr[indexPath.row]
            cell.user_namelbl.text = user_model.userName
            cell.country_namelbl.text = "\(user_model.city) , \(user_model.country)"
            cell.proffesion_namelbl.text = user_model.aboutme.decodeEmoji
            
            if(user_model.is_coach == true)
            {
                cell.coach_imageview.isHidden = false
            }
            else
            {
                cell.coach_imageview.isHidden = true
                
            }
            
            cell.user_profile_imageview.sd_setImage(with: URL(string: user_model.userImage), placeholderImage: UIImage(named: "userPlaceHolder"))
          //  self.add_tap_gester_chat(cell.user_profile_imageview)
           
         
            cell.contentView.layoutIfNeeded()
            return cell
        }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let user_model = friendListArr[indexPath.row]
        
        let view = ModalView.instantiateFromNib()
        view.updateModal("TechMae", descMessage: "Are you sure you want to unblock \(user_model.userName)?", firstTitle: "YES", secondTitle: "NO", type: .modalAlert)
               let modal = PathDynamicModal()
               modal.showMagnitude = 200.0
               modal.closeMagnitude = 130.0
               modal.closeByTapBackground = false
               modal.closeBySwipeBackground = false
               view.Cancel2ButtonHandler = {[weak modal] in
                   modal?.closeWithLeansRandom()
                   return
               }
               view.OkButtonHandler = {[weak modal] in
                   self.callUnBlockApi(user_id_str: user_model.userId)
                   modal?.closeWithLeansRandom()
                   return
               }
               modal.show(modalView: view, inView: (UIApplication.shared.delegate?.window!)!)
    }
    
               
        
        //MARK:- Back Action
        @IBAction func btnBackAction(_ sender: UIButton)
        {
            self.navigationController?.popViewController(animated: false)
        }
        
        
        //MARK:- Get Friend Api
        func getFriends()
        {
            
            if(CommonFunction.isInternetAvailable())
            {
                MBProgressHUD.showAdded(to: self.view, animated: true)
                let strUrl = Constants.BASEURL + MethodName.getBlockList()
     
                Alamofire.request(strUrl, method: .get, parameters: nil , encoding: URLEncoding.default, headers: ["Content-Type":"application/json"]).responseJSON { response in
                    MBProgressHUD.hide(for: self.view, animated: true)
                    if let result_dict = response.result.value as? [String:Any]
                    {
                        print(result_dict)
                        if(result_dict["code"] as! Int == 200)
                        {
                            self.friendListArr.removeAll()
                            if let user_arr = result_dict ["data"] as? [[String:Any]]
                            {
                                for i in 0..<user_arr.count
                                {
                                    let user_model = UserModel()
                                    if let user_dict = user_arr[i] as? [String:Any]
                                    {
                                        if let about_str = user_dict["aboutme"] as? String
                                        {
                                            user_model.aboutme = about_str
                                        }
                                        if let name_str = user_dict["userName"] as? String
                                        {
                                            user_model.userName = name_str
                                        }
                                        if let img_str = user_dict["userImage"] as? String
                                        {
                                            user_model.userImage = img_str
                                        }
                                        if let city_str = user_dict["city"] as? String
                                        {
                                            user_model.city = city_str
                                        }
                                        if let country_str = user_dict["country"] as? String
                                        {
                                            user_model.country = country_str
                                        }
                                        if let userid_str = user_dict["userId"] as? Int
                                        {
                                            user_model.userId = "\(userid_str)"
                                            self.profile_user_id = userid_str
                                        }
                                        if let friend_request_id = user_dict["friend_request_id"] as? Int
                                        {
                                            user_model.friend_request_id = friend_request_id
                                        }
                                        if let is_coach = user_dict["is_coach"] as? Bool
                                        {
                                            user_model.is_coach = is_coach
                                        }
                                        if let is_like = user_dict["is_like"] as? Bool
                                        {
                                            user_model.is_like = is_like
                                        }
                                        if let is_friend = user_dict["is_friend"] as? Int
                                        {
                                            user_model.is_friend = is_friend
                                        }
                                        if let hourly_rate = user_dict["hourly_rate"] as? Int
                                        {
                                            user_model.hourly_rate = hourly_rate
                                        }
                                        
                                        if let quickblox_data_dict = user_dict["quickblox_data"] as? [String:Any]
                                        {
                                            user_model.quickblox_data = quickblox_data_dict
                                        }
                                        
                                        self.friendListArr.append(user_model)
                                    }
                                }
                                
                            }
                            if self.friendListArr.count > 0
                            {
                                self.tblFriends.isHidden = false
                                self.ViewNoData.isHidden = true
                                
                            }
                            else
                            {
                                self.tblFriends.isHidden = true
                                self.ViewNoData.isHidden = false
                            }
                            self.ReloadTable()
                        }
                        else
                        {
                            if let error_arr = result_dict["errorData"] as? [[String:Any]]
                            {
                                if let error_msg = error_arr[0]["message"] as? String
                                {
                                    self.view.makeToast(error_msg)
                                }
                            }
                        }
                    }
                }
            }
            else
            {
                self.view.makeToast("Please Check Your Internet Connection!!")
            }
            
        }
        
        //MARK:-  UnFriend Api
        func callUnBlockApi(user_id_str : String)
        {
            if(CommonFunction.isInternetAvailable())
            {
                MBProgressHUD.showAdded(to: self.view, animated: true)
                Alamofire.request(Constants.BASEURL + MethodName.unblockUser() , method: .post, parameters: ["user_id":user_id_str], encoding: JSONEncoding.default, headers: ["Content-Type":"application/json"]).responseJSON { response in
                    print(response.result.value!)
                    MBProgressHUD.hide(for: self.view, animated: true)
                    if let result_dict = response.result.value as? [String:Any]
                    {
                        if let code  = result_dict["code"] as? Int
                        {
                            if(code==200)
                            {
                                DispatchQueue.main.async{
                          self.getFriends()
                                }
                            }
                            else
                            {
                                if let error_arr = result_dict["errorData"] as? [[String:Any]]
                                {
                                    if let error_msg = error_arr[0]["message"] as? String
                                    {
                                        self.view.makeToast(error_msg)
                                    }
                                }
                            }
                        }
                    }
                }
            }
            else
            {
                self.view.makeToast("Please Check Your Internet Connection!!")
            }
        }
        
        
        
        
        @IBAction func btnSearchTapped(_ sender: UIButton) {
            
            let searchPage = self.storyboard?.instantiateViewController(withIdentifier: "SearchFriendViewController") as! SearchFriendViewController
            self.navigationController?.pushViewController(searchPage, animated: false)
            
        }
        
        //MARK:- Reload Table
        func ReloadTable()
        {
            self.tblFriends.reloadData()
            if self.friendListArr.count > 0
            {
                self.tblFriends.isHidden = false
                self.ViewNoData.isHidden = true
                
            }
            else
            {
                self.tblFriends.isHidden = true
                self.ViewNoData.isHidden = false
            }
        }
}

