//
//  NotificationViewController.swift
//  Techmea
//
//  Created by Dhaval Panchani on 15/10/18.
//  Copyright © 2018 VISHAL SETH. All rights reserved.
//

import UIKit
import Alamofire
import MBProgressHUD
import SDWebImage
import Toast_Swift
import GoogleMobileAds

class VideoLikeCommentNotificationCell : UITableViewCell
{
    @IBOutlet weak var imgUserProfile: UIImageView!
    @IBOutlet weak var lblUserName: UILabel!
    @IBOutlet weak var imgPost: UIImageView!
    @IBOutlet weak var lbltime: UILabel!
    @IBOutlet weak var mainView: UIView!
    @IBOutlet weak var playBtn: UIImageView!
}

class ImageLikeCommentNotificationCell : UITableViewCell
{
    
    @IBOutlet weak var imgUserProfile: UIImageView!
    @IBOutlet weak var lblUserName: UILabel!
    @IBOutlet weak var imgPost: UIImageView!
    @IBOutlet weak var lbltime: UILabel!
    @IBOutlet weak var mainView: UIView!
}

class TextLikeCommentNotificationCell : UITableViewCell
{
    @IBOutlet weak var viewMain: UIView!
    @IBOutlet weak var imgProfile: UIImageView!
    @IBOutlet weak var lblMessage: UILabel!
    @IBOutlet weak var lbltimeago: UILabel!
}

///MARK:- RequestNotificationCell
class RequestNotificationCell : UITableViewCell
{
    
    @IBOutlet weak var imgUserProfile_Request: UIImageView!
    @IBOutlet weak var lblUserName_Request: UILabel!
    @IBOutlet weak var btnAccept_Request: UIButton!
    @IBOutlet weak var btnCancel_Request: UIButton!
    @IBOutlet weak var viewMain: UIView!
}

class NotificationViewController: UIViewController,UITableViewDelegate,UITableViewDataSource {
    
    
    @IBOutlet weak var bannerView: DFPBannerView!
    //MARK:- Outlets
    @IBOutlet weak var tblNotifications: UITableView!
    var arr_notification = [NotificationListModel]()
    var refreshControl = UIRefreshControl()
    @IBOutlet weak var noNotificationView: UIView!
    
    //MARK:- viewDidLoad
    override func viewDidLoad()
    {
        super.viewDidLoad()
        self.tblNotifications.estimatedRowHeight = 71
        self.tblNotifications.rowHeight = UITableViewAutomaticDimension
        self.refreshControl.addTarget(self, action: #selector(NotificationViewController.pullto_reloadData),for: UIControlEvents.valueChanged)
        self.tblNotifications.addSubview(refreshControl)
        self.callNotificationlistAPI()
        
       self.ConfigureBanner()
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        
        self.tabBarController?.tabBar.isHidden = true
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK:- Pull To Reload
    @objc func pullto_reloadData()
    {
        self.callNotificationlistAPI()
    }
    

    
    //MARK:- TableView Delegate Methods
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
       
        return arr_notification.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        
            let type_str = arr_notification[indexPath.row].notification_code
        
            if type_str=="GROUP_POST" || type_str=="FRIEND_POST" || type_str=="FRIEND_POST_LIKE" || type_str == "GROUP_POST_COMMENT" || type_str == "GROUP_POST_LIKE" || type_str == "FRIEND_POST_COMMENT"
            {
                
                if let postContent = arr_notification[indexPath.row].detail["postContent"] as? NSArray
                {
                    if let postContent_Dict = postContent[0] as? [String:Any]
                    {
                        if let postType = postContent_Dict["postType"] as? String
                        {
                            if postType == "text"
                            {
                                
                                let cell = tableView.dequeueReusableCell(withIdentifier: "TextLikeCommentNotificationCell", for: indexPath) as! TextLikeCommentNotificationCell
                                
                                if self.arr_notification[indexPath.row].status == 0
                                {
                                    cell.viewMain.backgroundColor = UIColor(red: 256.0/255.0, green: 236.0/255.0, blue: 242.0/255.0, alpha: 1.0)
                                }
                                else
                                {
                                    cell.viewMain.backgroundColor = UIColor.white
                                }
                                
                                if let user_profile_img = arr_notification[indexPath.row].author["user_profile_image"] as? String
                                {
                                    cell.imgProfile.sd_setImage(with: URL(string: user_profile_img), placeholderImage: UIImage(named: "user") )
                                }
                                
                                cell.lblMessage.text = arr_notification[indexPath.row].message.decodeEmoji
                                
                                let timestampcreated = arr_notification[indexPath.row].created_at
                                let strDate = Date(timeIntervalSince1970: Double(timestampcreated))
                                cell.lbltimeago.text = "\(timeAgoSinceDate(strDate, numericDates: true))"
                                
                                 self.add_tap_gester_chat_text(cell.imgProfile)
                                
                                return cell
                            }
                            else if postType == "image"
                            {
                                let cell = tableView.dequeueReusableCell(withIdentifier: "ImageLikeCommentNotificationCell", for: indexPath) as! ImageLikeCommentNotificationCell
                                
                                if self.arr_notification[indexPath.row].status == 0
                                {
                                    cell.mainView.backgroundColor = UIColor(red: 256.0/255.0, green: 236.0/255.0, blue: 242.0/255.0, alpha: 1.0)
                                }
                                else
                                {
                                    cell.mainView.backgroundColor = UIColor.white
                                }
                                
                                if let user_profile_img = arr_notification[indexPath.row].author["user_profile_image"] as? String
                                {
                                    cell.imgUserProfile.sd_setImage(with: URL(string: user_profile_img), placeholderImage: UIImage(named: "user") )
                                }
                                
                               self.add_tap_gester_chat_img(cell.imgUserProfile)
                                cell.lblUserName.text = arr_notification[indexPath.row].message.decodeEmoji
                                
                                if let post_img_str = postContent_Dict["post"] as? String
                                {
                                    cell.imgPost.sd_showActivityIndicatorView()
                                    cell.imgPost.sd_setImage(with: URL(string: post_img_str), placeholderImage: UIImage.init(), completed: { (image, error, type, url) in
                                        cell.imgPost.sd_removeActivityIndicator()
                                    })
                                    
                                }
                                else
                                {
                                    cell.imgPost.image = nil
                                }
                                
                                let timestampcreated = arr_notification[indexPath.row].created_at
                                let strDate = Date(timeIntervalSince1970: Double(timestampcreated))
                                cell.lbltime.text = "\(timeAgoSinceDate(strDate, numericDates: true))"
                                
                                return cell
                            }
                            else if postType == "video"
                            {
                                let cell = tableView.dequeueReusableCell(withIdentifier: "VideoLikeCommentNotificationCell", for: indexPath) as! VideoLikeCommentNotificationCell
                                
                                if self.arr_notification[indexPath.row].status == 0
                                {
                                    cell.mainView.backgroundColor = UIColor(red: 256.0/255.0, green: 236.0/255.0, blue: 242.0/255.0, alpha: 1.0)
                                }
                                else
                                {
                                    cell.mainView.backgroundColor = UIColor.white
                                }
                                
                                if let user_profile_img = arr_notification[indexPath.row].author["user_profile_image"] as? String
                                {
                                    cell.imgUserProfile.sd_setImage(with: URL(string: user_profile_img), placeholderImage: UIImage(named: "user") )
                                }
                                
                                self.add_tap_gester_chat_video(cell.imgUserProfile)
                                cell.lblUserName.text = arr_notification[indexPath.row].message.decodeEmoji
                                
                                
                                if let post_video_str = postContent_Dict["preview_image"] as? String
                                {
                                    cell.imgPost.sd_showActivityIndicatorView()
                                    cell.imgPost.sd_setImage(with: URL(string: post_video_str), placeholderImage: UIImage.init(), completed: { (image, error, type, url) in
                                        cell.imgPost.sd_removeActivityIndicator()
                                    })//sd_setImage(with: URL(string: post_video_str), placeholderImage: UIImage(named: "user") )
                                    cell.playBtn.isHidden = false
                                }
                                else
                                {
                                    cell.imgPost.image = nil
                                }
                                
                                let timestampcreated = arr_notification[indexPath.row].created_at
                                let strDate = Date(timeIntervalSince1970: Double(timestampcreated))
                                cell.lbltime.text = "\(timeAgoSinceDate(strDate, numericDates: true))"
                                
                                return cell
                            }
                            else
                            {
                                return UITableViewCell()
                            }
                        }
                    }
                }
                
            }
            else if type_str == "FRIEND_REQUEST"
            {
               let cell = tableView.dequeueReusableCell(withIdentifier: "RequestNotificationCell", for: indexPath) as! RequestNotificationCell
                if let user_profile_img = arr_notification[indexPath.row].author["user_profile_image"] as? String
                {
                    cell.imgUserProfile_Request.sd_setImage(with: URL(string: user_profile_img), placeholderImage: UIImage(named: "user") )
                }
                cell.lblUserName_Request.text = arr_notification[indexPath.row].message
                cell.btnAccept_Request.addTarget(self, action: #selector(self.btnAcceptAction(sender:)), for: .touchUpInside)
                cell.btnCancel_Request.addTarget(self, action: #selector(self.btnCancelAction(sender:)), for: .touchUpInside)
                
                self.add_tap_gester_chat_request(cell.imgUserProfile_Request)
                
                return cell
            }
            else if type_str == "FRIEND_REQUEST_ACCEPT"
            {
                let cell = tableView.dequeueReusableCell(withIdentifier: "TextLikeCommentNotificationCell", for: indexPath) as! TextLikeCommentNotificationCell
                
                if self.arr_notification[indexPath.row].status == 0
                {
                    cell.viewMain.backgroundColor = UIColor(red: 256.0/255.0, green: 236.0/255.0, blue: 242.0/255.0, alpha: 1.0)
                }
                else
                {
                    cell.viewMain.backgroundColor = UIColor.white
                }
                
                if let user_profile_img = arr_notification[indexPath.row].author["user_profile_image"] as? String
                {
                    cell.imgProfile.sd_setImage(with: URL(string: user_profile_img), placeholderImage: UIImage(named: "user") )
                }
                
                cell.lblMessage.text = arr_notification[indexPath.row].message.decodeEmoji
                
                let timestampcreated = arr_notification[indexPath.row].created_at
                let strDate = Date(timeIntervalSince1970: Double(timestampcreated))
                cell.lbltimeago.text = "\(timeAgoSinceDate(strDate, numericDates: true))"
                
                self.add_tap_gester_chat_text(cell.imgProfile)
                
                return cell
            }
            else
            {
              return UITableViewCell()
            }
        return UITableViewCell()
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
//            DispatchQueue.main.async {
//                                           let postDetailVc = Constants.mainStoryboard.instantiateViewController(withIdentifier: "PostDetailViewController") as! PostDetailViewController
//                                           postDetailVc.currentPostId = "\(self.arr_notification[indexPath.row].object_id)"
//
//                                           self.navigationController?.pushLikePresent(postDetailVc)
//                                       }
//
//        return
        
        
        var type = String()
        if self.arr_notification[indexPath.row].status == 0
        {
            type = "1"
            
            if (UserDefaults.standard.value(forKey: Constants.USERID) as? Int) != nil
            {
                
                if(CommonFunction.isInternetAvailable())
                {
                    MBProgressHUD.showAdded(to: self.view, animated: true)
                    
                    let notification_id = self.arr_notification[indexPath.row].id
                    
                    Alamofire.request(Constants.BASEURL + MethodName.readunread + "?access-token=\(UserDefaults.standard.value(forKey: "Access_Token") as! String)&id=\(notification_id)&status=\(type)", method: .post, parameters: nil , encoding: JSONEncoding.default, headers: ["Content-Type":"application/json"]).responseJSON { response in
                        MBProgressHUD.hide(for: self.view, animated: true)
                        if let result_dict = response.result.value as? [String:Any]
                        {
                            print(result_dict)
                            if(result_dict["code"] as! Int == 200)
                            {
                                self.arr_notification[indexPath.row].status = 1
                                self.tblNotifications.reloadData()
                                
                                DispatchQueue.main.async {
if !self.arr_notification[indexPath.row].notification_code.lowercased().contains("comment"){
                                        let postDetailVc = Constants.mainStoryboard.instantiateViewController(withIdentifier: "PostDetailViewController") as! PostDetailViewController
                                        postDetailVc.currentPostId = "\(self.arr_notification[indexPath.row].object_id)"
    if self.arr_notification[indexPath.row].notification_code.lowercased().contains("group"){
        postDetailVc.isGroup=true
    }
                                        self.navigationController?.pushLikePresent(postDetailVc)
                                    }
                                    
                                }
                            }
                            else
                            {
                                if let error_arr = result_dict["errorData"] as? [[String:Any]]
                                {
                                    if let error_msg = error_arr[0]["message"] as? String
                                    {
                                        self.view.makeToast(error_msg)
                                    }
                                }
                            }
                        }
                    }
                }
                else
                {
                    self.view.makeToast("Please Check Your Internet Connection!!")
                }
            }
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return UITableViewAutomaticDimension
    }
    
    
    
    //MARK:- Text Cell Gestures
    func add_tap_gester_chat_text(_ imgview : UIImageView)
    {
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.imageview_tapFunction_txt))
        imgview.isUserInteractionEnabled = true
        imgview.addGestureRecognizer(tap)
    }
    
    @objc func imageview_tapFunction_txt(sender:UITapGestureRecognizer)
    {
        guard let cell = sender.view?.superview?.superview?.superview as? TextLikeCommentNotificationCell else {
            return // or fatalError() or whatever
        }
        let selected_indexPath = tblNotifications.indexPath(for: cell)
        self.open_profile(user_id: "\(self.arr_notification[(selected_indexPath?.row)!].author["user_id"] as! Int)")
    }
    
    
    //MARK:- Image Cell Gestures
    func add_tap_gester_chat_img(_ imgview : UIImageView)
    {
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.imageview_tapFunction_img))
        imgview.isUserInteractionEnabled = true
        imgview.addGestureRecognizer(tap)
    }
    
    @objc func imageview_tapFunction_img(sender:UITapGestureRecognizer)
    {
        guard let cell = sender.view?.superview?.superview?.superview as? ImageLikeCommentNotificationCell else {
            return // or fatalError() or whatever
        }
        let selected_indexPath = tblNotifications.indexPath(for: cell)
        self.open_profile(user_id: "\(self.arr_notification[(selected_indexPath?.row)!].author["user_id"] as! Int)")
    }
    
    //MARK:- Video Cell Gestures
    func add_tap_gester_chat_video(_ imgview : UIImageView)
    {
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.imageview_tapFunction_video))
        imgview.isUserInteractionEnabled = true
        imgview.addGestureRecognizer(tap)
    }
    
    @objc func imageview_tapFunction_video(sender:UITapGestureRecognizer)
    {
        guard let cell = sender.view?.superview?.superview?.superview as? VideoLikeCommentNotificationCell else {
            return // or fatalError() or whatever
        }
        let selected_indexPath = tblNotifications.indexPath(for: cell)
        self.open_profile(user_id: "\(self.arr_notification[(selected_indexPath?.row)!].author["user_id"] as! Int)")
    }

    
    //MARK:- Friend Request Cell Gestures
    func add_tap_gester_chat_request(_ imgview : UIImageView)
    {
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.imageview_tapFunction_request))
        imgview.isUserInteractionEnabled = true
        imgview.addGestureRecognizer(tap)
    }
    
    @objc func imageview_tapFunction_request(sender:UITapGestureRecognizer)
    {
        guard let cell = sender.view?.superview?.superview?.superview as? RequestNotificationCell else {
            return // or fatalError() or whatever
        }
        let selected_indexPath = tblNotifications.indexPath(for: cell)
        self.open_profile(user_id: "\(self.arr_notification[(selected_indexPath?.row)!].author["user_id"] as! Int)")
    }

    
    
    //MARK:- Open Profile
    func open_profile(user_id:String)
    {
        let profile_page = Constants.mainStoryboard.instantiateViewController(withIdentifier: "FriendListViewController") as! FriendListViewController
        profile_page.user_id_str = user_id
        self.navigationController?.pushViewController(profile_page, animated: true)
    }
    
    
    
    //MARK:- Button Accept Request Action
    @objc func btnAcceptAction(sender : UIButton)
    {
        guard let cell = sender.superview?.superview?.superview as? RequestNotificationCell else
        {
            return // or fatalError() or whatever
        }
        let selected_indexPath = tblNotifications.indexPath(for: cell)
        
        let dictUser = self.arr_notification[(selected_indexPath?.row)!].author
        
        if let user_id_str = dictUser["user_id"] as? Int
        {
            if let friend_request_id_str = dictUser["friend_request_id"] as? Int
            {
                acceptFriendRequest(friend_user_id: "\(user_id_str)", status: "1",friend_request_id : "\(friend_request_id_str)", ind: (selected_indexPath?.row)!)
            }
        }
        
    }
    
    //MARK:- Button Cancel Request Action
    @objc func btnCancelAction(sender : UIButton)
    {
        guard let cell = sender.superview?.superview?.superview as? RequestNotificationCell else
        {
            return // or fatalError() or whatever
        }
        let selected_indexPath = tblNotifications.indexPath(for: cell)
        
        let dictUser = self.arr_notification[(selected_indexPath?.row)!].author
        
        if let user_id_str = dictUser["user_id"] as? Int
        {
            if let friend_request_id_str = dictUser["friend_request_id"] as? Int
            {
                self.cancelFriendRequest(friend_user_id: "\(user_id_str)", status: "2", friend_request_id : "\(friend_request_id_str)", ind: (selected_indexPath?.row)!)
            }
        }
    }
    
//    //MARK:- Button Booking Accept Request Action
//    @objc func btnBookingAcceptTapped(_ sender:UIButton)
//    {
//        guard let cell = sender.superview?.superview?.superview as? BookingRequestCell else
//        {
//            return // or fatalError() or whatever
//        }
//        let selected_indexPath = tblNotifications.indexPath(for: cell)
//        if let booking_id_str = arr_notification[(selected_indexPath?.row)!].detail["coach_booking_id"] as? Int
//        {
//            if let user_id_str = arr_notification[(selected_indexPath?.row)!].detail["user_id"] as? Int
//            {
//                self.AcceptBookingAPI(id_str: "\(booking_id_str)",user_id_str: "\(user_id_str)")
//            }
//        }
//    }
    
//    //MARK:- Button Bookig Reject Request Action
//    @objc func btnBookingRejectTapped(_ sender:UIButton)
//    {
//        guard let cell = sender.superview?.superview?.superview as? BookingRequestCell else
//        {
//            return // or fatalError() or whatever
//        }
//        let selected_indexPath = tblNotifications.indexPath(for: cell)
//        if let booking_id_str = arr_notification[(selected_indexPath?.row)!].detail["coach_booking_id"] as? Int
//        {
//            if let user_id_str = arr_notification[(selected_indexPath?.row)!].detail["user_id"] as? Int
//            {
//                self.RejectBookingAPI(id_str: "\(booking_id_str)",user_id_str: "\(user_id_str)")
//            }
//        }
//
//    }
    
    
    //MARK:- Get NotificationList Api
    func callNotificationlistAPI()
    {
        if(CommonFunction.isInternetAvailable())
        {
            MBProgressHUD.showAdded(to: self.view, animated: true)
            //let param = ["user_id" : "\(UserDefaults.standard.value(forKey: Constants.USERID) as! Int)"]
            Alamofire.request(Constants.BASEURL + MethodName.notificationList + "?access-token=\(UserDefaults.standard.value(forKey: "Access_Token") as! String)&expand=author", method: .get, parameters: nil , encoding: URLEncoding.default, headers: ["Content-Type":"application/json"]).responseJSON { response in
                MBProgressHUD.hide(for: self.view, animated: true)
                if let result_dict = response.result.value as? [String:Any]
                {
                     print(result_dict)
                    self.arr_notification.removeAll()
                    if(result_dict["code"] as! Int == 200)
                    {
                        if let notificationList = result_dict["data"] as? NSArray
                        {
                            for dict in notificationList
                            {
                                let tempDict = dict as! NSDictionary
                                self.arr_notification.append(NotificationListModel.init(dict: tempDict))
                            }
                            
                            if self.arr_notification.count > 0
                            {
                                self.noNotificationView.isHidden = true
                            }
                            else
                            {
                                self.noNotificationView.isHidden = false
                            }
                            self.tblNotifications.reloadData()
                            self.refreshControl.endRefreshing()
                        }
                    }
                    else
                    {
                        if let error_arr = result_dict["errorData"] as? [[String:Any]]
                        {
                            if let error_msg = error_arr[0]["message"] as? String
                            {
                                self.view.makeToast(error_msg)
                            }
                        }
                    }
                }
            }
        }
        else
        {
            self.view.makeToast("Please Check Your Internet Connection!!")
        }
    }
    
    
    //MARK:- Accept Booking Request Api
    func AcceptBookingAPI(id_str:String, user_id_str:String)
    {
        if(CommonFunction.isInternetAvailable())
        {
            MBProgressHUD.showAdded(to: self.view, animated: true)
            Alamofire.request(Constants.BASEURL + MethodName.acceptBooking + "?access-token=\(UserDefaults.standard.value(forKey: "Access_Token") as! String)&id=\(id_str)&user_id=\(user_id_str)", method: .post, parameters: nil , encoding: URLEncoding.default, headers: ["Content-Type":"application/json"]).responseJSON { response in
                MBProgressHUD.hide(for: self.view, animated: true)
                if let result_dict = response.result.value as? [String:Any]
                {
                    print(result_dict)
                    if(result_dict["code"] as! Int == 200)
                    {
                       // self.CloseNotificationAPI(id_str: id_str)
                    }
                    else
                    {
                        if let error_arr = result_dict["errorData"] as? [[String:Any]]
                        {
                            if let error_msg = error_arr[0]["message"] as? String
                            {
                                self.view.makeToast(error_msg)
                            }
                        }
                    }
                }
            }
        }
        else
        {
            self.view.makeToast("Please Check Your Internet Connection!!")
        }
    }
    
    //MARK:- Reject Booking Request Api
    func RejectBookingAPI(id_str:String, user_id_str:String)
    {
        if(CommonFunction.isInternetAvailable())
        {
            MBProgressHUD.showAdded(to: self.view, animated: true)
            Alamofire.request(Constants.BASEURL + MethodName.cancelBooking + "?access-token=\(UserDefaults.standard.value(forKey: "Access_Token") as! String)&id=\(id_str)&mode=reject&user_id=\(user_id_str)", method: .post, parameters: nil , encoding: URLEncoding.default, headers: ["Content-Type":"application/json"]).responseJSON { response in
                MBProgressHUD.hide(for: self.view, animated: true)
                if let result_dict = response.result.value as? [String:Any]
                {
                    print(result_dict)
                    if(result_dict["code"] as! Int == 200)
                    {
                       // self.CloseNotificationAPI(id_str: id_str)
                    }
                    else
                    {
                        if let error_arr = result_dict["errorData"] as? [[String:Any]]
                        {
                            if let error_msg = error_arr[0]["message"] as? String
                            {
                                self.view.makeToast(error_msg)
                            }
                        }
                    }
                }
            }
        }
        else
        {
            self.view.makeToast("Please Check Your Internet Connection!!")
        }
    }
    
    
    //MARK:- Close Notification Api
    func CloseNotificationAPI(id_str : String)
    {
        if(CommonFunction.isInternetAvailable())
        {
            MBProgressHUD.showAdded(to: self.view, animated: true)
            Alamofire.request(Constants.BASEURL + MethodName.closeNotification + "?access-token=\(UserDefaults.standard.value(forKey: "Access_Token") as! String)&id=\(id_str)", method: .get, parameters: nil , encoding: URLEncoding.default, headers: ["Content-Type":"application/json"]).responseJSON { response in
                MBProgressHUD.hide(for: self.view, animated: true)
                if let result_dict = response.result.value as? [String:Any]
                {
                    print(result_dict)
                    if(result_dict["code"] as! Int == 200)
                    {
                        
                    }
                    else
                    {
                        if let error_arr = result_dict["errorData"] as? [[String:Any]]
                        {
                            if let error_msg = error_arr[0]["message"] as? String
                            {
                                self.view.makeToast(error_msg)
                            }
                        }
                    }
                }
            }
        }
        else
        {
            self.view.makeToast("Please Check Your Internet Connection!!")
        }
    }
    
    
    //MARK:- Accept Friend Request Api
    func acceptFriendRequest(friend_user_id : String, status : String, friend_request_id: String,ind : Int)
    {
        if(CommonFunction.isInternetAvailable())
        {
            let params = ["friend_user_id":friend_user_id, "status" : "0"]
            
            let headerString = ["Content-Type":"application/json"]
            MBProgressHUD.showAdded(to: self.view, animated: true)
            Alamofire.request(Constants.BASEURL+MethodName.acceptFriendRequest  + "?access-token=\(UserDefaults.standard.value(forKey: "Access_Token") as! String)", method: .post, parameters: params, encoding: JSONEncoding.default, headers: headerString).responseJSON { response in
                MBProgressHUD.hide(for: self.view, animated: true)
                if let result_dict = response.result.value as? [String:Any]
                {
                    if let code = result_dict["code"] as? Int
                    {
                        if(code==200)
                        {
                            //self.CloseNotificationAPI(id_str : friend_request_id)
                            
                            self.arr_notification.remove(at: ind)
                            self.tblNotifications.reloadData()
                            //self.displayAlert(msg: (result_dict["data"] as! [String:Any])["message"] as! String, title_str: Constants.APP_NAME)
                            self.view.makeToast("Friend Request Accepted")
                        }
                        else
                        {
                            if let error_arr = result_dict["errorData"] as? [String:Any]
                            {
                                if let error_msg = error_arr["message"] as? String
                                {
                                    self.view.makeToast(error_msg)
                                }
                            }
                        }
                    }
                }
            }
        }
        else
        {
            self.view.makeToast("Please Check Your Internet Connection!!")
        }
    }
    
    //MARK:- Cancel Friend Request Api
    func cancelFriendRequest(friend_user_id : String, status : String, friend_request_id: String,ind : Int)
    {
        if(CommonFunction.isInternetAvailable())
        {
            MBProgressHUD.showAdded(to: self.view, animated: true)
            Alamofire.request(Constants.BASEURL+MethodName.cancelFriendRequest + "?access-token=\(UserDefaults.standard.value(forKey: "Access_Token") as! String)&id=\(friend_user_id)", method: .post, parameters: nil, encoding: URLEncoding.default, headers: nil).responseJSON
                { response in
                    MBProgressHUD.hide(for: self.view, animated: true)
                    if let result_dict = response.result.value as? [String:Any]
                    {
                        if let code = result_dict["code"] as? Int
                        {
                            if(code==200)
                            {
                                
                                self.arr_notification.remove(at: ind)
                                self.tblNotifications.reloadData()
                                if let data_dic = result_dict["data"] as? [String:Any]
                                {
                                    if let msg = data_dic["message"] as? String
                                    {
                                        self.view.makeToast(msg)
                                    }
                                }
                            }
                            else
                            {
                                if let error_arr = result_dict["errorData"] as? [String:Any]
                                {
                                    if let error_msg = error_arr["message"] as? String
                                    {
                                        self.view.makeToast(error_msg)
                                    }
                                }
                            }
                        }
                    }
            }
        }
        else
        {
            self.view.makeToast("Please Check Your Internet Connection!!")
        }
    }

    
    
    
    //MARK:- ForumQuestion List Api
    func callForumQuestionListAPI()
    {
        if(CommonFunction.isInternetAvailable())
        {
            MBProgressHUD.showAdded(to: self.view, animated: true)
            Alamofire.request(Constants.BASEURL + MethodName.fourmQuestionList + "slug=about-us", method: .get, parameters: nil , encoding: URLEncoding.default, headers: ["Content-Type":"application/json"]).responseJSON { response in
                MBProgressHUD.hide(for: self.view, animated: true)
                if let result_dict = response.result.value as? [String:Any]
                {
                    print(result_dict)
                    if(result_dict["code"] as! Int == 200)
                    {
                        
                    }
                    else
                    {
                        if let error_arr = result_dict["errorData"] as? [[String:Any]]
                        {
                            if let error_msg = error_arr[0]["message"] as? String
                            {
                                self.view.makeToast(error_msg)
                            }
                        }
                    }
                }
            }
        }
        else
        {
            self.view.makeToast("Please Check Your Internet Connection!!")
        }
    }

    //MARK:- Back Action
    @IBAction func btnBackTapped(_ sender: UIButton) {
        
        self.navigationController?.popViewController(animated: true)
    }
    
    //MARK:- Survey Question List Api
    func callSurveyQuestionListAPI()
    {
        if(CommonFunction.isInternetAvailable())
        {
            MBProgressHUD.showAdded(to: self.view, animated: true)
            Alamofire.request(Constants.BASEURL + MethodName.surveyQuestion + "?access-token=\(UserDefaults.standard.value(forKey: "Access_Token") as! String)", method: .get, parameters: nil , encoding: URLEncoding.default, headers: ["Content-Type":"application/json"]).responseJSON { response in
                MBProgressHUD.hide(for: self.view, animated: true)
                if let result_dict = response.result.value as? [String:Any]
                {
                    print(result_dict)
                    if(result_dict["code"] as! Int == 200)
                    {
                        
                    }
                    else
                    {
                        if let error_arr = result_dict["errorData"] as? [[String:Any]]
                        {
                            if let error_msg = error_arr[0]["message"] as? String
                            {
                                self.view.makeToast(error_msg)
                            }
                        }
                    }
                }
            }
        }
        else
        {
            self.view.makeToast("Please Check Your Internet Connection!!")
        }
    }
    
    //MARK:- Survey Question Answer Api
    func callAnswerSurveyQuestionAPI()
    {
        if(CommonFunction.isInternetAvailable())
        {
            MBProgressHUD.showAdded(to: self.view, animated: true)
            Alamofire.request(Constants.BASEURL + MethodName.answerSurveyQuestion + "?access-token=\(UserDefaults.standard.value(forKey: "Access_Token") as! String)", method: .post, parameters: nil , encoding: URLEncoding.default, headers: ["Content-Type":"application/json"]).responseJSON { response in
                MBProgressHUD.hide(for: self.view, animated: true)
                if let result_dict = response.result.value as? [String:Any]
                {
                    print(result_dict)
                    if(result_dict["code"] as! Int == 200)
                    {
                        
                    }
                    else
                    {
                        if let error_arr = result_dict["errorData"] as? [[String:Any]]
                        {
                            if let error_msg = error_arr[0]["message"] as? String
                            {
                                self.view.makeToast(error_msg)
                            }
                        }
                    }
                }
            }
        }
        else
        {
            self.view.makeToast("Please Check Your Internet Connection!!")
        }
    }
    
    //MARK:- TimeAgoSince Date
    func timeAgoSinceDate(_ date:Date, numericDates:Bool) -> String {
        let calendar = Calendar.current
        let now = Date()
        let earliest = (now as NSDate).earlierDate(date)
        let latest = (earliest == now) ? date : now
        let components:DateComponents = (calendar as NSCalendar).components([NSCalendar.Unit.minute , NSCalendar.Unit.hour , NSCalendar.Unit.day , NSCalendar.Unit.weekOfYear , NSCalendar.Unit.month , NSCalendar.Unit.year , NSCalendar.Unit.second], from: earliest, to: latest, options: NSCalendar.Options())
        
        if (components.year! >= 2) {
            return "\(components.year!) years ago"
        } else if (components.year! >= 1){
            if (numericDates){
                return "1 year ago"
            } else {
                return "Last year"
            }
        } else if (components.month! >= 2) {
            return "\(components.month!) months ago"
        } else if (components.month! >= 1){
            if (numericDates){
                return "1 month ago"
            } else {
                return "Last month"
            }
        } else if (components.weekOfYear! >= 2) {
            return "\(components.weekOfYear!) weeks ago"
        } else if (components.weekOfYear! >= 1){
            if (numericDates){
                return "1 week ago"
            } else {
                return "Last week"
            }
        } else if (components.day! >= 2) {
            return "\(components.day!) days ago"
        } else if (components.day! >= 1){
            if (numericDates){
                return "1 day ago"
            } else {
                return "Yesterday"
            }
        } else if (components.hour! >= 2) {
            return "\(components.hour!) hours ago"
        } else if (components.hour! >= 1){
            if (numericDates){
                return "1 hour ago"
            } else {
                return "An hour ago"
            }
        } else if (components.minute! >= 2) {
            return "\(components.minute!) minutes ago"
        } else if (components.minute! >= 1){
            if (numericDates){
                return "1 minute ago"
            } else {
                return "A minute ago"
            }
        } else if (components.second! >= 3) {
            return "\(components.second!) seconds ago"
        } else {
            return "Just now"
        }
        
    }
}


extension NotificationViewController: GADBannerViewDelegate{
    func ConfigureBanner(){
        addBannerViewToView(bannerView)
        bannerView.adUnitID = Constants.kGOOGLE_AD_ID
        bannerView.rootViewController = self
        bannerView.load(DFPRequest())
        bannerView.delegate = self
    }
    
    func addBannerViewToView(_ bannerView: UIView) {
        bannerView.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(bannerView)
        view.addConstraints(
            [NSLayoutConstraint(item: bannerView,
                                attribute: .bottom,
                                relatedBy: .equal,
                                toItem: bottomLayoutGuide,
                                attribute: .top,
                                multiplier: 1,
                                constant: 0),
             NSLayoutConstraint(item: bannerView,
                                attribute: .centerX,
                                relatedBy: .equal,
                                toItem: view,
                                attribute: .centerX,
                                multiplier: 1,
                                constant: 0)
            ])
    }
    
    /// Tells the delegate an ad request loaded an ad.
    func adViewDidReceiveAd(_ bannerView: GADBannerView) {
        print("adViewDidReceiveAd")
        addBannerViewToView(bannerView)
        bannerView.alpha = 0
        UIView.animate(withDuration: 1, animations: {
            bannerView.alpha = 1
        })
    }
    
    /// Tells the delegate an ad request failed.
    func adView(_ bannerView: GADBannerView,
                didFailToReceiveAdWithError error: GADRequestError) {
        print("adView:didFailToReceiveAdWithError: \(error.localizedDescription)")
    }
    
    /// Tells the delegate that a full-screen view will be presented in response
    /// to the user clicking on an ad.
    func adViewWillPresentScreen(_ bannerView: GADBannerView) {
        print("adViewWillPresentScreen")
    }
    
    /// Tells the delegate that the full-screen view will be dismissed.
    func adViewWillDismissScreen(_ bannerView: GADBannerView) {
        print("adViewWillDismissScreen")
    }
    
    /// Tells the delegate that the full-screen view has been dismissed.
    func adViewDidDismissScreen(_ bannerView: GADBannerView) {
        print("adViewDidDismissScreen")
    }
    
    /// Tells the delegate that a user click will open another app (such as
    /// the App Store), backgrounding the current app.
    func adViewWillLeaveApplication(_ bannerView: GADBannerView) {
        print("adViewWillLeaveApplication")
    }
}
