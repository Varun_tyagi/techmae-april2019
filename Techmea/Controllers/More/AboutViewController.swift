//
//  AboutViewController.swift
//  Techmea
//
//  Created by Dhaval Panchani on 22/10/18.
//  Copyright © 2018 VISHAL SETH. All rights reserved.
//

import UIKit
import Alamofire
import MBProgressHUD
import Toast_Swift
import GoogleMobileAds

class AboutViewController: UIViewController,GADBannerViewDelegate {
 

    //MARK:- Outlets
    @IBOutlet weak var lblAboutDetail: UILabel!
    @IBOutlet weak var lblAboutDetailBottom: NSLayoutConstraint!
    
    @IBOutlet weak var bannerView: DFPBannerView!
    //MARK:- viewDidLoad
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.lblAboutDetailBottom.constant = 10.0
        self.getAbout()
        
        addBannerViewToView(bannerView)
        bannerView.adUnitID = Constants.kGOOGLE_AD_ID
        bannerView.rootViewController = self
        bannerView.load(DFPRequest())
        bannerView.delegate = self
    }
    
    func addBannerViewToView(_ bannerView: UIView) {
        bannerView.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(bannerView)
        view.addConstraints(
            [NSLayoutConstraint(item: bannerView,
                                attribute: .bottom,
                                relatedBy: .equal,
                                toItem: bottomLayoutGuide,
                                attribute: .top,
                                multiplier: 1,
                                constant: 0),
             NSLayoutConstraint(item: bannerView,
                                attribute: .centerX,
                                relatedBy: .equal,
                                toItem: view,
                                attribute: .centerX,
                                multiplier: 1,
                                constant: 0)
            ])
    }
    
    /// Tells the delegate an ad request loaded an ad.
    func adViewDidReceiveAd(_ bannerView: GADBannerView) {
        print("adViewDidReceiveAd")
        addBannerViewToView(bannerView)
        bannerView.alpha = 0
        UIView.animate(withDuration: 1, animations: {
            bannerView.alpha = 1
        })
    }
    
    /// Tells the delegate an ad request failed.
    func adView(_ bannerView: GADBannerView,
                didFailToReceiveAdWithError error: GADRequestError) {
        print("adView:didFailToReceiveAdWithError: \(error.localizedDescription)")
    }
    
    /// Tells the delegate that a full-screen view will be presented in response
    /// to the user clicking on an ad.
    func adViewWillPresentScreen(_ bannerView: GADBannerView) {
        print("adViewWillPresentScreen")
    }
    
    /// Tells the delegate that the full-screen view will be dismissed.
    func adViewWillDismissScreen(_ bannerView: GADBannerView) {
        print("adViewWillDismissScreen")
    }
    
    /// Tells the delegate that the full-screen view has been dismissed.
    func adViewDidDismissScreen(_ bannerView: GADBannerView) {
        print("adViewDidDismissScreen")
    }
    
    /// Tells the delegate that a user click will open another app (such as
    /// the App Store), backgrounding the current app.
    func adViewWillLeaveApplication(_ bannerView: GADBannerView) {
        print("adViewWillLeaveApplication")
    }

    
    
    override func viewWillAppear(_ animated: Bool) {
        
        self.tabBarController?.tabBar.isHidden = true
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    //MARK:- Get About Api
    func getAbout()
    {
        if(CommonFunction.isInternetAvailable())
        {
            MBProgressHUD.showAdded(to: self.view, animated: true)
            Alamofire.request(Constants.BASEURL + MethodName.aboutus + "?slug=about-us", method: .get, parameters: nil , encoding: URLEncoding.default, headers: ["Content-Type":"application/json"]).responseJSON { response in
                MBProgressHUD.hide(for: self.view, animated: true)
                if let result_dict = response.result.value as? [String:Any]
                {
                    print(result_dict)
                    if(result_dict["code"] as! Int == 200)
                    {
                        if let aboutData = result_dict["data"] as? [String:Any]
                        {
                            if let about_detail = aboutData["detail"] as? String
                            {
                                self.lblAboutDetail.text = about_detail
                            }
                        }
                    }
                    else
                    {
                        if let error_arr = result_dict["errorData"] as? [[String:Any]]
                        {
                            if let error_msg = error_arr[0]["message"] as? String
                            {
                                self.view.makeToast(error_msg)
                            }
                        }
                    }
                }
            }
        }
        else
        {
            self.view.makeToast("Please Check Your Internet Connection!!")
            
        }
    }
    

    //MARK:- Back Action
    @IBAction func btnBackTapped(_ sender: UIButton) {
        
        self.navigationController?.popViewController(animated: true)
    }
}
