//
//  MoreViewController.swift
//  Techmae
//
//  Created by varun tyagi on 22/08/18.
//  Copyright © 2018 VISHAL SETH. All rights reserved.
//

import UIKit
import Alamofire
import MBProgressHUD
import Toast_Swift
import FBAudienceNetwork

class moreCell : UITableViewCell
{
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblNotificationCount: UILabel!
    
}

class MoreViewController: UIViewController
{
    
    //MARK:- Outlets
    var friendListArr:[String] = []
    var notification_count = Int()
    
    @IBOutlet weak var signoutBtn: ButtonWithShadow!
    @IBOutlet weak var user_profile_image: UIImageView!
    @IBOutlet weak var lblusername: UILabel!
    @IBOutlet weak var moreTbl: UITableView!
    @IBOutlet weak var lblcity: UILabel!
    @IBOutlet weak var btnBackgroundDeleteapp: UIButton!
    @IBOutlet weak var viewDeleteappPop: UIView!
    @IBOutlet weak var btnDeletePop: UIButton!
    @IBOutlet weak var btnCancelPop: UIButton!
    @IBOutlet weak var imgCoach: UIImageView!
    
    var bannerAdView: FBAdView!

    //MARK:- viewDidLoad
    override func viewDidLoad()
    {
        super.viewDidLoad()
        setupView()
        self.navigationController?.setNavigationBarHidden(true, animated: false)
        moreTbl.separatorColor = UIColor.lightGray
        
        bannerAdView = FBAdView(placementID: Constants.kFACEBOOK_AD_ID, adSize: kFBAdSizeHeight50Banner, rootViewController: self)
        bannerAdView.frame = CGRect(x:0.0,y: self.view.frame.size.height-50, width:UIScreen.main.bounds.size.width, height:50.0)
               bannerAdView.delegate = self
               bannerAdView.isHidden = true
        
               self.view.addSubview(bannerAdView)
               
               bannerAdView.loadAd()
    }
    
    //MARK:- viewWillAppear
    override func viewWillAppear(_ animated: Bool) {
        
        self.navigationController?.isNavigationBarHidden = true
        self.tabBarController?.tabBar.isHidden = false
        self.tabBarController?.tabBar.frame.size.height = 49
        getProfileDetails()
    }
    
    //MARK:- viewDidAppear
    override func viewDidAppear(_ animated: Bool)
    {
        super.viewDidAppear(true)
        self.navigationController?.setNavigationBarHidden(true, animated: false)
    }
    
    //MARK:- Button Signout Action
    @IBAction func signoutAction(_ sender: Any)
    {
        Helper.resetDefaults()
        let vc = Constants.mainStoryboard.instantiateViewController(withIdentifier:  "LoginRegisterViewController") as! LoginRegisterViewController
         let nav = UINavigationController.init(rootViewController: vc)
        nav.navigationBar.isHidden=true
        nav.setNavigationBarHidden(true, animated: true)
        Constants.appDelegate.window?.rootViewController = nav
    }
    
    //MARK:- setupView
    func setupView()
    {
        friendListArr = ["Friends","Groups","Notifications","About App",
                         "Friend Requests","Settings"]
        moreTbl.reloadData()
        //signout btn
        signoutBtn.layer.cornerRadius=22
        signoutBtn.clipsToBounds=true
        signoutBtn.layer.shadowColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.25).cgColor
        signoutBtn.layer.shadowOffset = CGSize(width: 0, height: 3)
        signoutBtn.layer.shadowOpacity = 1.0
        signoutBtn.layer.shadowRadius = 3.0
        signoutBtn.layer.masksToBounds = true
    }
    
    //MARK:- Deactivate Account Api
    func deactivateAccount()
    {
        if(CommonFunction.isInternetAvailable())
        {
            MBProgressHUD.showAdded(to: self.view, animated: true)
            
            let headerString = ["Token" : MethodName.Token,"Content-Type" : "application/json"]
            let paramString = ["user_id":"\(UserDefaults.standard.value(forKey: Constants.USERID) as! Int)"]

            Alamofire.request(Constants.BASEURL + MethodName.deactivateAccount + "?access-token=\(UserDefaults.standard.value(forKey: "Access_Token") as! String)", method: .get, parameters: paramString , encoding: JSONEncoding.default, headers:headerString).responseJSON
                { response in
                 MBProgressHUD.hide(for: self.view, animated: true)
                    if let result_dict = response.result.value as? [String:Any]
                    {
                        if let code = result_dict["code"] as? Int
                        {
                            if(code==200)
                            {
                            }
                            else
                            {
                                if let error_arr = result_dict["errorData"] as? [[String:Any]]
                                {
                                    if let error_msg = error_arr[0]["message"] as? String
                                    {
                                        self.view.makeToast(error_msg)
                                    }
                                }
                            }
                        }
                    }
            }
        }
        else
        {
            self.view.makeToast("Please Check Your Internet Connection!!")
        }
    }
    
    //MARK:- Button Background DeleteApp
    @IBAction func btnBackgroundDeleteappTapped(_ sender: UIButton) {
        self.btnBackgroundDeleteapp.isHidden = true
        self.viewDeleteappPop.isHidden = true
    }
    
    //MARK:- Button Delete Pop
    @IBAction func btnDeletePopTapped(_ sender: UIButton) {
        
        self.deactivateAccount()
    }
    
    //MARK:- Button Cancel Pop
    @IBAction func btnCancelPopTapped(_ sender: UIButton) {
        
        self.btnBackgroundDeleteapp.isHidden = true
        self.viewDeleteappPop.isHidden = true
    }
    
    
    //MARK:- Get Profile Details Api
    func getProfileDetails()
    {
        if(CommonFunction.isInternetAvailable())
        {
            var strUrl = String()
            
            strUrl = Constants.BASEURL + MethodName.profileState + "?access-token=\(UserDefaults.standard.value(forKey: "Access_Token") as! String)&expand=userStat"
            
            Alamofire.request(strUrl, method: .get, parameters: nil , encoding: JSONEncoding.default, headers:nil).responseJSON
                { response in
                    
                    if let result_dict = response.result.value as? [String:Any]
                    {
                        if let code = result_dict["code"] as? Int
                        {
                            if(code==200)
                            {
                                if let profile_info = result_dict["data"] as? [String:Any]
                                {
                                    if let img_str = profile_info["user_profile_pic"] as? String
                                    {
                                        self .user_profile_image.sd_setImage(with: URL(string: img_str), placeholderImage: UIImage(named: "user"))
                                    }
                                    
                                    if let name_str = profile_info["fullname"] as? String
                                    {
                                        self.lblusername.text = name_str
                                    }
                                    
                                    if let city_str = profile_info["city"] as? String
                                    {
                                        if let country_str = profile_info["country"] as? String
                                        {
                                            self.lblcity.text = city_str + "," + country_str
                                        }
                                    }
                                    
                                    if let is_Coach = profile_info["is_coach"] as? Int
                                    {
                                        if is_Coach == 1
                                        {
                                            self.imgCoach.isHidden = false
                                        }
                                        else
                                        {
                                            self.imgCoach.isHidden = true
                                        }
                                    }
                                }
                                
                                self.getUnreadNotificationCount()
                                
                            }
                            else if (code==401)
                            {
                                if let error_data = result_dict["errorData"] as? [[String:Any]]
                                {
                                    if let error_msg = error_data[0]["message"] as? String
                                    {
                                        let view = CongratulationView.instantiateFromNib()
                                        view.lblTitle.text = error_msg
                                        let modal = PathDynamicModal()
                                        modal.showMagnitude = 200.0
                                        modal.closeMagnitude = 130.0
                                        modal.closeByTapBackground = false
                                        modal.closeBySwipeBackground = false
                                        view.OkButtonHandler = {[weak modal] in
                                            
                                            let storyBoard = UIStoryboard(name: "Main", bundle: nil)
                                            let objHomeNav = storyBoard.instantiateViewController(withIdentifier: "HomeNav") as! UINavigationController
                                            UIApplication.shared.keyWindow?.rootViewController = objHomeNav
                                            
                                            modal?.closeWithLeansRandom()
                                            return
                                        }
                                        modal.show(modalView: view, inView: (UIApplication.shared.delegate?.window!)!)
                                    }
                                }
                                
                                
                            }
                            else
                            {
                                self.view.makeToast("Something Went Wrong!!")
                            }
                        }
                    }
            }
        }
        else
        {
            self.view.makeToast("Please Check Your Internet Connection!!")
        }
    }
    
    
    //MARK:- Unread Notification Count Api
    func getUnreadNotificationCount()
    {
        if(CommonFunction.isInternetAvailable())
        {
            MBProgressHUD.showAdded(to: self.view, animated: true)
            
            let headerString = ["Content-Type" : "application/json"]
            //let paramString = ["user_id":"\(UserDefaults.standard.value(forKey: Constants.USERID) as! Int)"]
            if let current_user = UserDefaults.standard.value(forKey: Constants.USERID) as? Int
            {
                Alamofire.request(Constants.BASEURL + MethodName.unreadNotificationCount + "?access-token=\(UserDefaults.standard.value(forKey: "Access_Token") as! String)&receiver_id=\(current_user)", method: .get, parameters: nil , encoding: JSONEncoding.default, headers:headerString).responseJSON
                    { response in
                        MBProgressHUD.hide(for: self.view, animated: true)
                        if let result_dict = response.result.value as? [String:Any]
                        {
                            if let code = result_dict["code"] as? Int
                            {
                                if(code==200)
                                {
                                    if let data_dict = result_dict["data"] as? [String:Any]
                                    {
                                        if let noti_count = data_dict["totalCount"] as? Int
                                        {
                                            self.notification_count = noti_count
                                            self.moreTbl.reloadData()
                                        }
                                    }
                                }
                                else
                                {
                                    if let error_arr = result_dict["errorData"] as? [[String:Any]]
                                    {
                                        if let error_msg = error_arr[0]["message"] as? String
                                        {
                                            self.view.makeToast(error_msg)
                                        }
                                    }
                                }
                            }
                        }
                }
            }
        }
        else
        {
            self.view.makeToast("Please Check Your Internet Connection!!")
        }
    }
    
}
extension MoreViewController : FBAdViewDelegate{
    func adViewDidLoad(_ adView: FBAdView) {
           bannerAdView.isHidden = false
       }
       
       
       func adView(_ adView: FBAdView, didFailWithError error: Error) {
           print(error)
       }
       
       func adViewDidClick(_ adView: FBAdView) {
           print("Did tap on ad view")
       }
}

extension MoreViewController: UITableViewDataSource,UITableViewDelegate
{
    
    //MARK:- TableView Delegate Methods
    func numberOfSections(in tableView: UITableView) -> Int
    {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return friendListArr.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell = tableView.dequeueReusableCell(withIdentifier: "moreCell") as! moreCell
        cell.lblTitle.font = UIFont.init(name:FONTS.Avenir_Light , size: 18)
        cell.lblTitle.text = friendListArr[indexPath.row]
        cell.lblTitle.textColor = UIColor.init(red: 252.0/255.0, green: 89.0/255.0, blue: 138.0/255.0, alpha: 1.0)
        cell.lblNotificationCount.font = UIFont.init(name:FONTS.Avenir_Light , size: 15)
        
        if indexPath.row == 2
        {
            
            if self.notification_count == 0
            {
                cell.lblNotificationCount.isHidden = true
            }
            else if self.notification_count > 99
            {
                cell.lblNotificationCount.isHidden = false
                cell.lblNotificationCount.text = "99+"
            }
            else
            {
                cell.lblNotificationCount.isHidden = false
                cell.lblNotificationCount.text = "\(self.notification_count)"
            }
        }
        else
        {
            cell.lblNotificationCount.isHidden = true
        }
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        if indexPath.row == 0
        {
            let storyBoard = UIStoryboard(name: "Main", bundle: nil)
            let objFriendsViewController = storyBoard.instantiateViewController(withIdentifier: "FriendsViewController") as! FriendsViewController
            objFriendsViewController.isTab = false
            self.navigationController?.pushViewController(objFriendsViewController, animated: true)
        }
        else if indexPath.row == 1
        {
            
            let storyBoard = UIStoryboard(name: "Main", bundle: nil)
            let objGroupListViewController = storyBoard.instantiateViewController(withIdentifier: "GroupListViewController") as! GroupListViewController
            self.navigationController?.pushViewController(objGroupListViewController, animated: true)

        }
        else if indexPath.row == 2
        {
            let objNotificationViewController = Constants.mainStoryboard.instantiateViewController(withIdentifier: "NotificationViewController") as! NotificationViewController
            self.navigationController?.pushViewController(objNotificationViewController, animated: true)
        }
        else if indexPath.row == 3
        {
            
            let objAboutViewController = Constants.mainStoryboard.instantiateViewController(withIdentifier: "AboutViewController") as! AboutViewController
            self.navigationController?.pushViewController(objAboutViewController, animated: true)
        }
        else if indexPath.row == 4
        {
            let storyBoard = UIStoryboard(name: "Main", bundle: nil)
            let objFriendsRequestViewController = storyBoard.instantiateViewController(withIdentifier: "FriendsRequestViewController") as! FriendsRequestViewController
            self.navigationController?.pushViewController(objFriendsRequestViewController, animated: true)
        }
        else
        {
            let storyBoard = UIStoryboard(name: "Main", bundle: nil)
            let objSettingsViewController = storyBoard.instantiateViewController(withIdentifier: "SettingsViewController") as! SettingsViewController
            self.navigationController?.pushViewController(objSettingsViewController, animated: true)
        }
        
        
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        return 60
    }
}
