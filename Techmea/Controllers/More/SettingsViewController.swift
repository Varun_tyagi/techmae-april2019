//
//  SettingsViewController.swift
//  Techmea
//
//  Created by Dhaval Panchani on 29/11/18.
//  Copyright © 2018 VISHAL SETH. All rights reserved.
//

import UIKit
import Alamofire
import MBProgressHUD
import Quickblox
import QMServices
import Toast_Swift

class SettingsViewController: UIViewController,QMAuthServiceDelegate,QMChatServiceDelegate,QMChatConnectionDelegate {

    //MARK:- Outlets
    var arrSettingList:[String] = []
    @IBOutlet weak var tblSettings: UITableView!
    @IBOutlet weak var signoutBtn: UIButton!
    
    @IBOutlet weak var btnBackgroundDeleteapp: UIButton!
    @IBOutlet weak var viewDeleteappPop: UIView!
    @IBOutlet weak var btnDeletePop: UIButton!
    @IBOutlet weak var btnCancelPop: UIButton!
    
    //MARK:- viewDidLoad
    override func viewDidLoad() {
        super.viewDidLoad()

        setupView()
        //popEmailForgotView.setBorder()
        self.navigationController?.setNavigationBarHidden(true, animated: false)
        tblSettings.separatorColor = UIColor.lightGray
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        self.tabBarController?.tabBar.isHidden = true
    }
    
    //MARK:- viewDidAppear
    override func viewDidAppear(_ animated: Bool)
    {
        super.viewDidAppear(true)
        self.navigationController?.setNavigationBarHidden(true, animated: false)
    }
    

    //MARK:- Setup View
    func setupView()
    {
        arrSettingList = ["Edit Profile","Blocked Users","Forgot Password","Deactivate Account","Report"]
        tblSettings.reloadData()
        //signout btn
        signoutBtn.layer.cornerRadius=22
        signoutBtn.clipsToBounds=true
        signoutBtn.layer.shadowColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.25).cgColor
        signoutBtn.layer.shadowOffset = CGSize(width: 0, height: 3)
        signoutBtn.layer.shadowOpacity = 1.0
        signoutBtn.layer.shadowRadius = 3.0
        signoutBtn.layer.masksToBounds = true
    }
    
    
    //MARK:- Button Signout Action
    @IBAction func signoutAction(_ sender: Any)
    {
        let view = ModalView.instantiateFromNib()
        view.updateModal("Sign Out", descMessage: "Are you sure you want to sign out?", firstTitle: "YES", secondTitle: "NO", type: .modalAlert)
        let modal = PathDynamicModal()
        modal.showMagnitude = 200.0
        modal.closeMagnitude = 130.0
        modal.closeByTapBackground = false
        modal.closeBySwipeBackground = false
        view.Cancel2ButtonHandler = {[weak modal] in
            modal?.closeWithLeansRandom()
            return
        }
        view.OkButtonHandler = {[weak modal] in
            
            Helper.resetDefaults()
            
            ServicesManager.instance().logoutUserWithCompletion { [weak self] (boolValue) -> () in
                
                guard let strongSelf = self else { return }
                if boolValue {
                    NotificationCenter.default.removeObserver(strongSelf)
                    
                    
                    ServicesManager.instance().chatService.removeDelegate(strongSelf)
                    ServicesManager.instance().authService.remove(strongSelf)
                    
                    ServicesManager.instance().lastActivityDate = nil;
                    
                    
                    // SVProgressHUD.showSuccess(withStatus: "SA_STR_COMPLETED".localized)
                }
            }
            
            let vc = Constants.mainStoryboard.instantiateViewController(withIdentifier:  "LoginRegisterViewController") as! LoginRegisterViewController
            let nav = UINavigationController.init(rootViewController: vc)
            nav.navigationBar.isHidden=true
            nav.setNavigationBarHidden(true, animated: true)
            Constants.appDelegate.window?.rootViewController = nav
            
            
            modal?.closeWithLeansRandom()
            return
        }
        modal.show(modalView: view, inView: (UIApplication.shared.delegate?.window!)!)
        
        
       
    }
    
    
    //MARK:- Deactivate Account Api
    func deactivateAccount()
    {
        if(CommonFunction.isInternetAvailable())
        {
            MBProgressHUD.showAdded(to: self.view, animated: true)
            
            let headerString = ["Token" : MethodName.Token,"Content-Type" : "application/json"]
            let paramString = ["user_id":"\(UserDefaults.standard.value(forKey: Constants.USERID) as! Int)"]
            
            Alamofire.request(Constants.BASEURL + "home/deactive-account" + "?access-token=\(UserDefaults.standard.value(forKey: "Access_Token") as! String)", method: .get, parameters: paramString , encoding: JSONEncoding.default, headers:headerString).responseJSON
                { response in
                    MBProgressHUD.hide(for: self.view, animated: true)
                    if let result_dict = response.result.value as? [String:Any]
                    {
                        
                        if let code = result_dict["code"] as? Int
                        {
                            if(code==200)
                            {
                                let alertController = UIAlertController(title: Constants.APP_NAME, message: "Your Account Successfully Deactivated!!", preferredStyle: .alert)
                                let okAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.default) {
                                    UIAlertAction in
                                   
                                    Helper.resetDefaults()
                                    let vc = Constants.mainStoryboard.instantiateViewController(withIdentifier:  "LoginRegisterViewController") as! LoginRegisterViewController
                                    let nav = UINavigationController.init(rootViewController: vc)
                                    nav.navigationBar.isHidden=true
                                    nav.setNavigationBarHidden(true, animated: true)
                                    Constants.appDelegate.window?.rootViewController = nav
                                    
                                }
                                alertController.addAction(okAction)
                                DispatchQueue.main.async {
                                self.present(alertController, animated: true, completion: nil)
                                }
                            }
                            else
                            {
                                if let error_arr = result_dict["errorData"] as? [[String:Any]]
                                {
                                    if let error_msg = error_arr[0]["message"] as? String
                                    {
                                        self.view.makeToast(error_msg)
                                    }
                                }
                            }
                        }
                    }
            }
        }
        else
        {
            self.view.makeToast("Please Check Your Internet Connection!!")
            
        }
    }
    
   
    
    //MARK:- Button Backgrount Delete
    @IBAction func btnBackgroundDeleteappTapped(_ sender: UIButton) {
        self.btnBackgroundDeleteapp.isHidden = true
        self.viewDeleteappPop.isHidden = true
    }
    
    //MARK:- Button Delete Pop
    @IBAction func btnDeletePopTapped(_ sender: UIButton) {
        
         self.deactivateAccount()
    }
    
    //MARK:- Button Cancel Pop
    @IBAction func btnCancelPopTapped(_ sender: UIButton) {
        
        self.btnBackgroundDeleteapp.isHidden = true
        self.viewDeleteappPop.isHidden = true
    }
    

    //MARK:- Back Action
    @IBAction func btnBackTapped(_ sender: UIButton) {
        
        self.navigationController?.popViewController(animated: true)
    }
    
}

//moreCell
extension SettingsViewController: UITableViewDataSource,UITableViewDelegate
{
    
    //MARK:- TableView Delegate Methods
    func numberOfSections(in tableView: UITableView) -> Int
    {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return arrSettingList.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell = tableView.dequeueReusableCell(withIdentifier: "settingsCell")
        cell?.textLabel?.font = UIFont.init(name:FONTS.Avenir_Light , size: 18)
        cell?.textLabel?.text = arrSettingList[indexPath.row]
        cell?.textLabel?.textColor = UIColor.init(red: 252.0/255.0, green: 89.0/255.0, blue: 138.0/255.0, alpha: 1.0)
        return cell!
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        if indexPath.row == 0
        {
            let storyBoard = UIStoryboard(name: "Main", bundle: nil)
            let objEditProfileViewController = storyBoard.instantiateViewController(withIdentifier: "EditProfileViewController") as! EditProfileViewController
            self.navigationController?.pushViewController(objEditProfileViewController, animated: true)
        }
       else     if indexPath.row == 1
            {
                let storyBoard = UIStoryboard(name: "Main", bundle: nil)
                let objEditProfileViewController = storyBoard.instantiateViewController(withIdentifier: "BlockListViewController") as! BlockListViewController
                objEditProfileViewController.isTab=false
                self.navigationController?.pushViewController(objEditProfileViewController, animated: true)
            }
              
        else if indexPath.row == 2
        {
            let storyBoard = UIStoryboard(name: "Main", bundle: nil)
            let objForgotPasswordViewController = storyBoard.instantiateViewController(withIdentifier: "ForgotPasswordViewController") as! ForgotPasswordViewController
            self.navigationController?.pushViewController(objForgotPasswordViewController, animated: true)
           
        }
        else if indexPath.row == 3
        {
            self.btnBackgroundDeleteapp.isHidden = false
            self.viewDeleteappPop.isHidden = false
        }
        else
        {
            let view = popDispute.instantiateFromNib()
            let modal = PathDynamicModal()
            modal.showMagnitude = 200.0
            modal.closeMagnitude = 130.0
            modal.closeByTapBackground = false
            modal.closeBySwipeBackground = false
            view.OkButtonHandler = {[weak modal] in
                
                if view.txtMessage.text != ""
                {
                    self.reportApiCall(txtMessage: view.txtMessage.text!)
                }
                else
                {
                    self.view.makeToast("Enter Message...")
                }
                
                modal?.closeWithLeansRandom()
                return
            }
            
            view.CancelButtonHandler = {[weak modal] in
                
                modal?.closeWithLeansRandom()
                return
            }
            modal.show(modalView: view, inView: (UIApplication.shared.delegate?.window!)!)
        }
        
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        return 60
    }
    
    
    func reportApiCall(txtMessage : String)
    {
        if(CommonFunction.isInternetAvailable())
        {
            MBProgressHUD.showAdded(to: self.view, animated: true)
            
            let headerString = ["Token" : MethodName.Token,"Content-Type" : "application/json"]
            let paramString = ["message" : txtMessage]
            
            Alamofire.request(Constants.BASEURL + MethodName.report  + "?access-token=\(UserDefaults.standard.value(forKey: "Access_Token") as! String)", method: .post, parameters: paramString , encoding: JSONEncoding.default, headers:headerString).responseJSON
                { response in
                    MBProgressHUD.hide(for: self.view, animated: true)
                    if let result_dict = response.result.value as? [String:Any]
                    {
                        
                        if let code = result_dict["code"] as? Int
                        {
                            if(code==200)
                            {
                               self.view.makeToast("Your Mesaage has been Reported!")
                            }
                            else
                            {
                                if let error_arr = result_dict["errorData"] as? [[String:Any]]
                                {
                                    if let error_msg = error_arr[0]["message"] as? String
                                    {
                                        self.view.makeToast(error_msg)
                                    }
                                }
                            }
                        }
                    }
            }
        }
        else
        {
            self.view.makeToast("Please Check Your Internet Connection!!")
            
        }
    }
}
