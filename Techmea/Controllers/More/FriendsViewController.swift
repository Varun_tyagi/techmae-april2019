    //
    //  FriendsViewController.swift
    //  Techmea
    //
    //  Created by Dhaval Panchani on 07/09/18.
    //  Copyright © 2018 VISHAL SETH. All rights reserved.
    //

    import UIKit
    import Alamofire
    import SDWebImage
    import MBProgressHUD
    import Quickblox
    import QMServices
    import Toast_Swift

    //MARK:- FriendsTableViewCell
    class FriendsTableViewCell : UITableViewCell
    {
        @IBOutlet weak var coachview_height_constraint: NSLayoutConstraint!
        @IBOutlet weak var coach_imageview: UIImageView!
        @IBOutlet weak var user_profile_imageview: UIImageView!
        @IBOutlet weak var user_namelbl: UILabel!
        @IBOutlet weak var proffesion_namelbl: UILabel!
        
        @IBOutlet weak var container_view: UIView!
        @IBOutlet weak var country_namelbl: UILabel!
        @IBOutlet weak var addFriend_btn: UIButton!
        
        @IBOutlet weak var hireme_btn: UIButton!
        @IBOutlet weak var hourlyRate_lbl: UILabel!
        @IBOutlet weak var like_btn: UIButton!
        @IBOutlet weak var message_btn: UIButton!
    }

    class FriendsViewController: UIViewController,UITableViewDelegate,UITableViewDataSource {
        
        
        //MARK:- Outlets
        var friendListArr = [UserModel]()
        var is_from_group = Bool()
        @IBOutlet weak var tblFriends: UITableView!
        @IBOutlet weak var ViewNoData: UIView!
        var selected_btn = UIButton()
        var image_str = String()
        var current_index = Int()
        var user_id_str = ""
        
        var isTab : Bool = true
        
        var profile_user_id = Int()
        
        var perpage_data_count = Int()
           var is_next_page_available = true
           var current_page = 1
        
        @IBOutlet weak var btnSearch: UIButton!
        @IBOutlet weak var back_white: UIImageView!
        @IBOutlet weak var btnBack: UIButton!
        
        //MARK:- viewDidLoad
        override func viewDidLoad() {
            super.viewDidLoad()
            getFriends()
            tblFriends.tableFooterView = UIView()
            
//            search_textfield.attributedPlaceholder = NSAttributedString(string: "Search Friend",
//                                                                        attributes: [NSAttributedStringKey.foregroundColor: UIColor.white])
            self.tblFriends.estimatedRowHeight = 123
            self.tblFriends.rowHeight = UITableViewAutomaticDimension
        }

        override func didReceiveMemoryWarning() {
            super.didReceiveMemoryWarning()
            // Dispose of any resources that can be recreated.
        }
        
        //MARK:- viewWillAppear
        override func viewWillAppear(_ animated: Bool) {
            
            if isTab == true
            {
                self.back_white.isHidden = true
                self.btnBack.isHidden = true
                self.tabBarController?.tabBar.frame.size.height = 49
                self.tabBarController?.tabBar .isHidden = false
            }
            else
            {
                self.back_white.isHidden = false
                self.btnBack.isHidden = false
                 self.tabBarController?.tabBar.isHidden = true
            }
        }
        

        //MARK:- TableView Delegate Methods
        func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
            return friendListArr.count
        }
        
        func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
            let cell = tableView.dequeueReusableCell(withIdentifier: "FriendsTableViewCell") as! FriendsTableViewCell
            
            CommonFunction.addshadow_view(view: cell.container_view, shadow_radius: 15.0, shadow_color: UIColor.lightGray, shadow_opecity: 0.5, shadow_offset: CGSize(width: 2.0, height: 2.0), is_clipBound: false, corner_radius: 10.0)
            
            let user_model = friendListArr[indexPath.row]
            cell.user_namelbl.text = user_model.userName
            self.add_tap_gester_chatlbl(cell.user_namelbl)
            cell.country_namelbl.text = "\(user_model.city) , \(user_model.country)"
            cell.proffesion_namelbl.text = user_model.aboutme.decodeEmoji
            
            if let curr_user = UserDefaults.standard.value(forKey: Constants.USERID) as? Int
            {
                if user_model.userId == "\(curr_user)"
                {
                    cell.addFriend_btn.isHidden = true
                    cell.message_btn.isHidden = true
                }
                else
                {
                    cell.addFriend_btn.isHidden = false
                    cell.message_btn.isHidden = false
                }
            }
            
            if(user_model.is_friend == 1)
            {
                cell.addFriend_btn.setBackgroundImage(UIImage(named: "accepticon"), for: .normal)
            }
            else if (user_model.is_friend == 0)
            {
                cell.addFriend_btn.setBackgroundImage(UIImage(named: "addFriend"), for: .normal)
            }
            else
            {
                cell.addFriend_btn.setBackgroundImage(UIImage(named: "pendingicon"), for: .normal)
            }
            if(user_model.is_like == true)
            {
                cell.like_btn.setBackgroundImage(UIImage(named: "likeSelect"), for: .normal)
            }
            else
            {
                cell.like_btn.setBackgroundImage(UIImage(named: "likeUnselect"), for: .normal)
            }
            if(user_model.hourly_rate != nil)
            {
                cell.hourlyRate_lbl.text = "Hourly Rate : \(user_model.hourly_rate!)"
            }
            if(user_model.is_coach == true)
            {
                cell.coach_imageview.isHidden = false
                //cell.coachview_height_constraint.constant = 20
                cell.coachview_height_constraint.constant = 0
            }
            else
            {
                cell.coach_imageview.isHidden = true
                cell.coachview_height_constraint.constant = 0
                
            }
            
            cell.user_profile_imageview.sd_setImage(with: URL(string: user_model.userImage), placeholderImage: UIImage(named: "userPlaceHolder"))
            self.add_tap_gester_chat(cell.user_profile_imageview)
           
            cell.addFriend_btn.addTarget(self, action: #selector(btnAddfriendAction(_:)), for: .touchUpInside)
            cell.message_btn.addTarget(self, action: #selector(btnMessageAction(_:)), for: .touchUpInside)
            cell.like_btn.addTarget(self, action: #selector(btnLikeAction(_:)), for: .touchUpInside)
            cell.hireme_btn.addTarget(self, action: #selector(btnHiremeAction(_:)), for: .touchUpInside)
            cell.contentView.layoutIfNeeded()
            return cell
        }
        
        //MARK:- Gestures
        func add_tap_gester_chat(_ imgview : UIImageView)
        {
            let tap = UITapGestureRecognizer(target: self, action: #selector(self.imageview_tapFunction))
            imgview.isUserInteractionEnabled = true
            imgview.addGestureRecognizer(tap)
        }
        
        func add_tap_gester_chatlbl(_ lbl : UILabel)
        {
            let tap = UITapGestureRecognizer(target: self, action: #selector(self.label_tapFunction))
            lbl.isUserInteractionEnabled = true
            lbl.addGestureRecognizer(tap)
        }
        
        @objc func imageview_tapFunction(sender:UITapGestureRecognizer)
        {
            guard let cell = sender.view?.superview?.superview?.superview as? FriendsTableViewCell else {
                return // or fatalError() or whatever
            }
            let selected_indexPath = tblFriends.indexPath(for: cell)
            self.open_profile(user_id: "\(self.friendListArr[(selected_indexPath?.row)!].userId)")
        }
        
        @objc func label_tapFunction(sender:UITapGestureRecognizer)
        {
            guard let cell = sender.view?.superview?.superview?.superview as? FriendsTableViewCell else {
                return // or fatalError() or whatever
            }
            let selected_indexPath = tblFriends.indexPath(for: cell)
            self.open_profile(user_id: "\(self.friendListArr[(selected_indexPath?.row)!].userId)")
        }
        
        
        //MARK:- Open Profile
        func open_profile(user_id:String)
        {
            let profile_page = Constants.mainStoryboard.instantiateViewController(withIdentifier: "FriendListViewController") as! FriendListViewController
            profile_page.user_id_str = user_id
            self.navigationController?.pushViewController(profile_page, animated: true)
        }
        
        //MARK:- Button Hireme Action
        @objc func btnHiremeAction(_ sender: UIButton)
        {
            guard let cell = sender.superview?.superview?.superview?.superview as? FriendsTableViewCell else {
                return // or fatalError() or whatever
            }
            let selected_indexPath = tblFriends.indexPath(for: cell)
            
            self.open_profile(user_id: "\(self.friendListArr[(selected_indexPath?.row)!].userId)")
        }
        
        
        //MARK:- Button Add Friend Action
        @objc func btnAddfriendAction(_ sender: UIButton)
        {
            guard let cell = sender.superview?.superview?.superview as? FriendsTableViewCell else {
                return // or fatalError() or whatever
            }
            let selected_indexPath = tblFriends.indexPath(for: cell)
            let user_model = friendListArr[(selected_indexPath?.row)!]
            current_index = (selected_indexPath?.row)!
            selected_btn = sender
            image_str = "unfriend"
            
            if sender.backgroundImage(for: .normal) == UIImage(named: "accepticon")
            {
                let view = ModalView.instantiateFromNib()
                view.updateModal("Remove Friend", descMessage: "Are you sure you want to Unfriend?", firstTitle: "YES", secondTitle: "NO", type: .modalAlert)
                
                let modal = PathDynamicModal()
                modal.showMagnitude = 200.0
                modal.closeMagnitude = 130.0
                modal.closeByTapBackground = false
                modal.closeBySwipeBackground = false
                view.Cancel2ButtonHandler = {[weak modal] in
                    
                    modal?.closeWithLeansRandom()
                    return
                }
                view.OkButtonHandler = {[weak modal] in
                    self.callUnfrienApi(user_id_str: "\(user_model.userId)")
                    modal?.closeWithLeansRandom()
                    return
                }
                modal.show(modalView: view, inView: (UIApplication.shared.delegate?.window!)!)
            }
            else if sender.backgroundImage(for: .normal) == UIImage(named: "addFriend")
            {
                self.callAddfriendApi(user_id_str: "\(user_model.userId)")
            }
            else if sender.backgroundImage(for: .normal) == UIImage(named: "pendingicon")
            {
                self.callCancelReqApi(user_id_str: "\(user_model.userId)")
            }
        }
        
        //MARK:- Button Message Action
        @objc func btnMessageAction(_ sender: UIButton)
        {
            
            guard let cell = sender.superview?.superview?.superview as? FriendsTableViewCell else {
                return // or fatalError() or whatever
            }
            let selected_indexPath = tblFriends.indexPath(for: cell)
            let user_model = friendListArr[(selected_indexPath?.row)!]
            let quickblox_data_dic = user_model.quickblox_data
            
            //Occupant
            var occupantID = Int()
            var occupantName = String()
            
            
            //Occupant Data
            if let occupantID_num = quickblox_data_dic["id"] as? Int
            {
                occupantID = occupantID_num
            }
            
            if let occupantName_str = quickblox_data_dic["full_name"] as? String
            {
                occupantName = occupantName_str
            }
            
            let chatDialog = QBChatDialog(dialogID: nil, type: QBChatDialogType.private)
            chatDialog.name = occupantName
            chatDialog.occupantIDs = [occupantID] as [NSNumber]
          //  chatDialog.data = ["class_name": "CustomData","custom_userId" : "\(UserDefaults.standard.value(forKey: Constants.USERID) as! Int),\(self.profile_user_id)"]
            chatDialog.data = ["class_name": "CustomData","custom_userId" : "\(UserDefaults.standard.value(forKey: Constants.USERID) as! Int),\(user_model.userId)"]

            
            QBRequest.createDialog(chatDialog, successBlock: { (response: QBResponse?, createdDialog : QBChatDialog?) -> Void in
                
                self.getDialogs(dialog : createdDialog!)
                
            }) { (response : QBResponse!) -> Void in
                
                print("Error while Creating Dialog!!")
                self.view.makeToast("Error while Creating Dialog!!")
            }
        }
        
        func getDialogs(dialog : QBChatDialog) {
            
            //        if let lastActivityDate = ServicesManager.instance().lastActivityDate {
            //
            //            ServicesManager.instance().chatService.fetchDialogsUpdated(from: lastActivityDate as Date, andPageLimit: kDialogsPageLimit, iterationBlock: { (response, dialogObjects, dialogsUsersIDs, stop) -> Void in
            //
            //                }, completionBlock: { (response) -> Void in
            //
            //                    if (response.isSuccess) {
            //
            //                        ServicesManager.instance().lastActivityDate = NSDate()
            //                    }
            //            })
            //        }
            //        else {
            
            
            ServicesManager.instance().chatService.allDialogs(withPageLimit: kDialogsPageLimit, extendedRequest: nil, iterationBlock: { (response: QBResponse?, dialogObjects: [QBChatDialog]?, dialogsUsersIDS: Set<NSNumber>?, stop: UnsafeMutablePointer<ObjCBool>) -> Void in
                
            }, completion: { (response: QBResponse?) -> Void in
                
                guard response != nil && response!.isSuccess else {
                    return
                }
                
                let chatVC = Constants.mainStoryboard.instantiateViewController(withIdentifier: "ChatViewController") as! ChatViewController
                chatVC.dialog = dialog
                self.navigationController?.pushViewController(chatVC, animated: true)
                
                ServicesManager.instance().lastActivityDate = NSDate()
            })
            //}
        }
        
        
        //MARK:- Button Like Friend Action
        @objc func btnLikeAction(_ sender: UIButton)
        {
            guard let cell = sender.superview?.superview?.superview as? FriendsTableViewCell else
            {
                return // or fatalError() or whatever
            }
            let selected_indexPath = tblFriends.indexPath(for: cell)
            let user_model = friendListArr[(selected_indexPath?.row)!]
            current_index = (selected_indexPath?.row)!
            selected_btn = sender
            if(sender.backgroundImage(for: .normal) == UIImage(named: "likeUnselect"))
            {
                image_str = "like"
            }
            else
            {
                image_str = "unlike"
            }
            calllikeApi(user_id_str: "\(user_model.userId)")
        }
        
        
        //MARK:- Back Action
        @IBAction func btnBackAction(_ sender: UIButton)
        {
            self.navigationController?.popViewController(animated: false)
        }
        
        
        //MARK:- Get Friend Api
        func getFriends()
        {
            
            if(CommonFunction.isInternetAvailable())
            {
                MBProgressHUD.showAdded(to: self.view, animated: true)
                var strUrl = String()
                
                if user_id_str == ""
                {
                    strUrl = Constants.BASEURL + MethodName.getFriendlist + "?access-token=\(UserDefaults.standard.value(forKey: "Access_Token") as! String)&page=\(current_page)"
                }
                else
                {
                    
                    if UserDefaults.standard.value(forKey: Constants.USERID) as? Int == Int(user_id_str)
                    {
                        
                        strUrl =  Constants.BASEURL + MethodName.getFriendlist + "?access-token=\(UserDefaults.standard.value(forKey: "Access_Token") as! String)&page=\(current_page)"
                    }
                    else
                    {
                        
                        strUrl = Constants.BASEURL + MethodName.getFriendlist + "?access-token=\(UserDefaults.standard.value(forKey: "Access_Token") as! String)&user_id=\(user_id_str)&page=\(current_page)"
                        
                    }
                }
                
                Alamofire.request(strUrl, method: .get, parameters: nil , encoding: URLEncoding.default, headers: ["Content-Type":"application/json"]).responseJSON { response in
                    MBProgressHUD.hide(for: self.view, animated: true)
                    if let result_dict = response.result.value as? [String:Any]
                    {
                        print(result_dict)
                        if(result_dict["code"] as! Int == 200)
                        {
                           // self.friendListArr.removeAll()
                            if let user_arr = result_dict ["data"] as? [[String:Any]]
                            {
                                
                                if user_arr.count < 20 {
                                    self.is_next_page_available=false
                                    }
                                for i in 0..<user_arr.count
                                {
                                    
                                    let user_model = UserModel()
                                    if let user_dict = user_arr[i] as? [String:Any]
                                    {
                                        if let about_str = user_dict["aboutme"] as? String
                                        {
                                            user_model.aboutme = about_str
                                        }
                                        if let name_str = user_dict["fullname"] as? String
                                        {
                                            user_model.userName = name_str
                                        }
                                        if let img_str = user_dict["user_profile_image"] as? String
                                        {
                                            user_model.userImage = img_str
                                        }
                                        if let city_str = user_dict["city"] as? String
                                        {
                                            user_model.city = city_str
                                        }
                                        if let country_str = user_dict["country"] as? String
                                        {
                                            user_model.country = country_str
                                        }
                                        if let userid_str = user_dict["user_id"] as? Int
                                        {
                                            user_model.userId = "\(userid_str)"
                                            self.profile_user_id = userid_str
                                        }
                                        if let friend_request_id = user_dict["friend_request_id"] as? Int
                                        {
                                            user_model.friend_request_id = friend_request_id
                                        }
                                        if let is_coach = user_dict["is_coach"] as? Bool
                                        {
                                            user_model.is_coach = is_coach
                                        }
                                        if let is_like = user_dict["is_like"] as? Bool
                                        {
                                            user_model.is_like = is_like
                                        }
                                        if let is_friend = user_dict["is_friend"] as? Int
                                        {
                                            user_model.is_friend = is_friend
                                        }
                                        if let hourly_rate = user_dict["hourly_rate"] as? Int
                                        {
                                            user_model.hourly_rate = hourly_rate
                                        }
                                        
                                        if let quickblox_data_dict = user_dict["quickblox_data"] as? [String:Any]
                                        {
                                            user_model.quickblox_data = quickblox_data_dict
                                        }
                                        
                                        self.friendListArr.append(user_model)
                                    }
                                }
                                
                            }
                            if self.friendListArr.count > 0
                            {
                                self.tblFriends.isHidden = false
                                self.ViewNoData.isHidden = true
                                
                            }
                            else
                            {
                                self.tblFriends.isHidden = true
                                self.ViewNoData.isHidden = false
                            }
                            self.ReloadTable()
                        }
                        else
                        {
                            self.is_next_page_available=false

                            if let error_arr = result_dict["errorData"] as? [[String:Any]]
                            {
                                if let error_msg = error_arr[0]["message"] as? String
                                {
                                    self.view.makeToast(error_msg)
                                }
                            }
                        }
                    }
                }
            }
            else
            {
                self.is_next_page_available=false

                self.view.makeToast("Please Check Your Internet Connection!!")
            }
            
        }
        
        //MARK:-  UnFriend Api
        func callUnfrienApi(user_id_str : String)
        {
            if(CommonFunction.isInternetAvailable())
            {
                MBProgressHUD.showAdded(to: self.view, animated: true)
                Alamofire.request(Constants.BASEURL + MethodName.Unfriend + "?access-token=\(UserDefaults.standard.value(forKey: "Access_Token") as! String)&id=\(user_id_str)" , method: .post, parameters: nil, encoding: JSONEncoding.default, headers: ["Content-Type":"application/json"]).responseJSON { response in
                    print(response.result.value!)
                    MBProgressHUD.hide(for: self.view, animated: true)
                    if let result_dict = response.result.value as? [String:Any]
                    {
                        if let code  = result_dict["code"] as? Int
                        {
                            if(code==200)
                            {
                                self.selected_btn.setBackgroundImage(UIImage(named: "addFriend"), for: .normal)
                                let user_model = self.friendListArr[self.current_index]
                                user_model.is_friend = 0
                                self.friendListArr.remove(at: self.current_index)
                                self.ReloadTable()
                                self.view.makeToast("Unfriend Successfully")
                                
                            }
                            else
                            {
                                if let error_arr = result_dict["errorData"] as? [[String:Any]]
                                {
                                    if let error_msg = error_arr[0]["message"] as? String
                                    {
                                        self.view.makeToast(error_msg)
                                    }
                                }
                            }
                        }
                    }
                }
            }
            else
            {
                self.view.makeToast("Please Check Your Internet Connection!!")
            }
        }
        
        //MARK:- Cancel Request Api
        func callCancelReqApi(user_id_str : String)
        {
            if(CommonFunction.isInternetAvailable())
            {
                Alamofire.request(Constants.BASEURL + MethodName.cancelFriendRequest + "?access-token=\(UserDefaults.standard.value(forKey: "Access_Token") as! String)&id=\(user_id_str)" , method: .post, parameters: nil, encoding: URLEncoding.default, headers: ["Content-Type":"application/json"]).responseJSON { response in
                    
                    if let result_dict = response.result.value as? [String:Any]
                    {
                        print(result_dict)
                        if let code = result_dict["code"] as? Int
                        {
                            if(code==200)
                            {
                                self.selected_btn.setBackgroundImage(UIImage(named: "addFriend"), for: .normal)
                                let user_model = self.friendListArr[self.current_index]
                                user_model.is_friend = 0
                                self.friendListArr[self.current_index] = user_model
                                self.ReloadTable()
                                self.displayAlert(msg: "Cancel request successfully", title_str: Constants.APP_NAME)
                                
                            }
                            else
                            {
                                if let error_arr = result_dict["errorData"] as? [[String:Any]]
                                {
                                    if let error_msg = error_arr[0]["message"] as? String
                                    {
                                        self.view.makeToast(error_msg)
                                    }
                                }
                            }
                        }
                    }
                    
                }
            }
            else
            {
                self.view.makeToast("Please Check Your Internet Connection!!")
            }
        }
        
        //MARK:- Add Friend Api
        func callAddfriendApi(user_id_str : String)
        {
            if(CommonFunction.isInternetAvailable())
            {
                let params = ["friend_user_id":user_id_str,
                              "user_id":"\(UserDefaults.standard.value(forKey: Constants.USERID) as! Int)"]
                let header_dict = ["Token":"31528198109743225ff9d0cf04d1fdd1",
                                   "user_id":"\(UserDefaults.standard.value(forKey: Constants.USERID) as! Int)"]
                
                Alamofire.request(Constants.BASEURL + MethodName.addFriendRequest + "?access-token=\(UserDefaults.standard.value(forKey: "Access_Token") as! String)" , method: .post, parameters: params, encoding: URLEncoding.default, headers: header_dict).responseJSON { response in
                    
                    if let result_dict = response.result.value as? [String:Any]
                    {
                        print(result_dict)
                        if(result_dict["code"] as! Int == 200)
                        {
                            self.selected_btn.setBackgroundImage(UIImage(named: "pending"), for: .normal)
                            let user_model = self.friendListArr[self.current_index]
                            user_model.is_friend = 2
                            self.friendListArr[self.current_index] = user_model
                            self.view.makeToast("Friend Request sent Successfully")
                            self.ReloadTable()
                        }
                        else
                        {
                            if let error_arr = result_dict["errorData"] as? [[String:Any]]
                            {
                                if let error_msg = error_arr[0]["message"] as? String
                                {
                                    self.view.makeToast(error_msg)
                                }
                            }
                        }
                    }
                }
            }
            else
            {
                self.view.makeToast("Please Check Your Internet Connection!!")
            }
        }

        //MARK:- Like Friend Api
        func calllikeApi(user_id_str : String)
        {
            MBProgressHUD.showAdded(to: self.view, animated: true)
            if(CommonFunction.isInternetAvailable())
            {
                let params = ["friend_id":user_id_str]
                Alamofire.request(Constants.BASEURL + MethodName.friendLike + "?access-token=\(UserDefaults.standard.value(forKey: "Access_Token") as! String)" , method: .post, parameters: params, encoding: URLEncoding.default, headers: nil).responseJSON { response in
                    MBProgressHUD.hide(for: self.view, animated: true)
                    if let result_dict = response.result.value as? [String:Any]
                    {
                        if(result_dict["code"] as! Int == 200)
                        {
                            if(self.image_str == "like")
                            {
                                self.selected_btn.setBackgroundImage(UIImage(named: "likeSelect"), for: .normal)
                                let user_model = self.friendListArr[self.current_index]
                                user_model.is_like = true
                                self.friendListArr[self.current_index] = user_model
                            }
                            else
                            {
                                self.selected_btn.setBackgroundImage(UIImage(named: "likeSelect"), for: .normal)
                                let user_model = self.friendListArr[self.current_index]
                                user_model.is_like = false
                                self.friendListArr[self.current_index] = user_model
                            }
                            self.ReloadTable()
                        }
                        else
                        {
                            if let error_arr = result_dict["errorData"] as? [[String:Any]]
                            {
                                if let error_msg = error_arr[0]["message"] as? String
                                {
                                    self.view.makeToast(error_msg)
                                }
                            }
                        }
                    }
                }
            }
            else
            {
                self.view.makeToast("Please Check Your Internet Connection!!")
            }
        }
        
        @IBAction func btnSearchTapped(_ sender: UIButton) {
            
            let searchPage = self.storyboard?.instantiateViewController(withIdentifier: "SearchFriendViewController") as! SearchFriendViewController
            self.navigationController?.pushViewController(searchPage, animated: false)
            
        }
        
        //MARK:- Reload Table
        func ReloadTable()
        {
            self.tblFriends.reloadData()
            if self.friendListArr.count > 0
            {
                self.tblFriends.isHidden = false
                self.ViewNoData.isHidden = true
                
            }
            else
            {
                self.tblFriends.isHidden = true
                self.ViewNoData.isHidden = false
            }
        }
}
    
    extension  FriendsViewController:UIScrollViewDelegate{
        func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
              if (self.tblFriends.contentOffset.y+10 >= (self.tblFriends.contentSize.height-10 - self.tblFriends.bounds.size.height))
                        {
                            if(is_next_page_available)
                            {
                                current_page = current_page + 1
                                getFriends()
                            }
                        }
           }
           
    }
