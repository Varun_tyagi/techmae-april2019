//
//  FriendsRequestViewController.swift
//  Techmea
//
//  Created by Dhaval Panchani on 07/09/18.
//  Copyright © 2018 VISHAL SETH. All rights reserved.
//

import UIKit
import Alamofire
import SDWebImage
import MBProgressHUD
import Toast_Swift
import GoogleMobileAds
//MARK:- FriendRequestCell
class FriendRequestCell : UITableViewCell
{
    
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var userProfile: UIImageView!
    @IBOutlet weak var userName: UILabel!
    @IBOutlet weak var userCountry: UILabel!
    @IBOutlet weak var userProffesion: UILabel!
    @IBOutlet weak var btnAccept: UIButton!
    @IBOutlet weak var btnCancel: UIButton!
    @IBOutlet weak var coach_image: UIImageView!
    
    @IBOutlet weak var coachview_height_constraint: NSLayoutConstraint!
    @IBOutlet weak var brnHireme: UIButton!
    @IBOutlet weak var hourlyRate: UILabel!
}

class FriendsRequestViewController: UIViewController,UITableViewDelegate,UITableViewDataSource,GADBannerViewDelegate {
    
    //MARK:- Outlets
    var friendListArr = [FriendRequestModel]()
    @IBOutlet weak var tblFriendRequests: UITableView!
    @IBOutlet weak var viewNoData: UIView!
    var deleted_index = Int()
    
    
    @IBOutlet weak var bannerView: DFPBannerView!
    @IBOutlet weak var btnSearch: UIButton!
    
    //MARK:- viewDidLoad
    override func viewDidLoad()
    {
        super.viewDidLoad()
        getFriendReuestList()
//        search_textfield.attributedPlaceholder = NSAttributedString(string: "Search Friend",
//                                                                    attributes: [NSAttributedStringKey.foregroundColor: UIColor.white])

        self.tblFriendRequests.estimatedRowHeight = 123
        self.tblFriendRequests.rowHeight = UITableViewAutomaticDimension
        
        addBannerViewToView(bannerView)
        bannerView.adUnitID = Constants.kGOOGLE_AD_ID
        bannerView.rootViewController = self
        bannerView.load(DFPRequest())
        bannerView.delegate = self
    }
    
    func addBannerViewToView(_ bannerView: UIView) {
        bannerView.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(bannerView)
        view.addConstraints(
            [NSLayoutConstraint(item: bannerView,
                                attribute: .bottom,
                                relatedBy: .equal,
                                toItem: bottomLayoutGuide,
                                attribute: .top,
                                multiplier: 1,
                                constant: 0),
             NSLayoutConstraint(item: bannerView,
                                attribute: .centerX,
                                relatedBy: .equal,
                                toItem: view,
                                attribute: .centerX,
                                multiplier: 1,
                                constant: 0)
            ])
    }
    
    /// Tells the delegate an ad request loaded an ad.
    func adViewDidReceiveAd(_ bannerView: GADBannerView) {
        print("adViewDidReceiveAd")
        addBannerViewToView(bannerView)
        bannerView.alpha = 0
        UIView.animate(withDuration: 1, animations: {
            bannerView.alpha = 1
        })
    }
    
    /// Tells the delegate an ad request failed.
    func adView(_ bannerView: GADBannerView,
                didFailToReceiveAdWithError error: GADRequestError) {
        print("adView:didFailToReceiveAdWithError: \(error.localizedDescription)")
    }
    
    /// Tells the delegate that a full-screen view will be presented in response
    /// to the user clicking on an ad.
    func adViewWillPresentScreen(_ bannerView: GADBannerView) {
        print("adViewWillPresentScreen")
    }
    
    /// Tells the delegate that the full-screen view will be dismissed.
    func adViewWillDismissScreen(_ bannerView: GADBannerView) {
        print("adViewWillDismissScreen")
    }
    
    /// Tells the delegate that the full-screen view has been dismissed.
    func adViewDidDismissScreen(_ bannerView: GADBannerView) {
        print("adViewDidDismissScreen")
    }
    
    /// Tells the delegate that a user click will open another app (such as
    /// the App Store), backgrounding the current app.
    func adViewWillLeaveApplication(_ bannerView: GADBannerView) {
        print("adViewWillLeaveApplication")
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        self.tabBarController?.tabBar.isHidden = true

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    //MARK:- TableView Delegate Methods
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return UITableViewAutomaticDimension
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.friendListArr.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "FriendRequestCell", for: indexPath) as! FriendRequestCell
        
        CommonFunction.addshadow_view(view: cell.containerView, shadow_radius: 15.0, shadow_color: UIColor.lightGray, shadow_opecity: 0.5, shadow_offset: CGSize(width: 2.0, height: 2.0), is_clipBound: false, corner_radius: 10.0)
        
        let user_request_model = friendListArr[indexPath.row]
        cell.userName.text = user_request_model.userName
        cell.userCountry.text = "\(user_request_model.city) , \(user_request_model.country)"
        cell.userProffesion.text = user_request_model.aboutme.decodeEmoji
        if(user_request_model.hourly_rate != nil)
        {
            cell.hourlyRate.text = "Hourly Rate : \(user_request_model.hourly_rate!)"
        }
//        if(user_request_model.is_coach == true)
//        {
//            cell.coach_image.isHidden = false
//            cell.coachview_height_constraint.constant = 20
//        }
//        else
//        {
            cell.coach_image.isHidden = true
            cell.coachview_height_constraint.constant = 0
       // }
        
        
        cell.userProfile.sd_setImage(with: URL(string: user_request_model.userImage), placeholderImage: UIImage(named: "userPlaceHolder"))            
     
        
        cell.btnAccept.addTarget(self, action: #selector(btnAcceptfriendAction(_:)), for: .touchUpInside)
        cell.btnCancel.addTarget(self, action: #selector(btnCancelAction(_:)), for: .touchUpInside)
        cell.brnHireme.addTarget(self, action: #selector(btnHiremeAction(_:)), for: .touchUpInside)
        cell.contentView.layoutIfNeeded()
        return cell
    }
    
    //MARK:- Button Hireme Action
    @objc func btnHiremeAction(_ sender: UIButton)
    {
        guard let cell = sender.superview?.superview?.superview?.superview as? FriendRequestCell else {
            return // or fatalError() or whatever
        }
        let selected_indexPath = tblFriendRequests.indexPath(for: cell)
        self.open_profile(user_id: "\(self.friendListArr[(selected_indexPath?.row)!].userId)")
    }
    
    
    //MARK:- Open Profile
    func open_profile(user_id:String)
    {
        let profile_page = Constants.mainStoryboard.instantiateViewController(withIdentifier: "FriendListViewController") as! FriendListViewController
        profile_page.user_id_str = user_id
        self.navigationController?.pushViewController(profile_page, animated: true)
    }
    
    //MARK:- Button Accept Friend Action
    @objc func btnAcceptfriendAction(_ sender: UIButton)
    {
        guard let cell = sender.superview?.superview?.superview as? FriendRequestCell else {
            return // or fatalError() or whatever
        }
        let selected_indexPath = tblFriendRequests.indexPath(for: cell)
        let user_request_model = friendListArr[(selected_indexPath?.row)!]
        deleted_index = (selected_indexPath?.row)!
        acceptFriendRequest(friend_request_id: "\(user_request_model.userId)", status: "1")
        
    }
    
    //MARK:- Button Cancel Friend Action
    @objc func btnCancelAction(_ sender: UIButton)
    {
        guard let cell = sender.superview?.superview?.superview as? FriendRequestCell else {
            return // or fatalError() or whatever
        }
        let selected_indexPath = tblFriendRequests.indexPath(for: cell)
        let user_request_model = friendListArr[(selected_indexPath?.row)!]
        let view = ModalView.instantiateFromNib()
        view.updateModal("Pending Request Cancel", descMessage: "Are you sure you want to cancel friend request?", firstTitle: "YES", secondTitle: "NO", type: .modalAlert)
        let modal = PathDynamicModal()
        modal.showMagnitude = 200.0
        modal.closeMagnitude = 130.0
        modal.closeByTapBackground = false
        modal.closeBySwipeBackground = false
        view.Cancel2ButtonHandler = {[weak modal] in
            modal?.closeWithLeansRandom()
            return
        }
        view.OkButtonHandler = {[weak modal] in
            self.cancelFriendRequest(friend_request_id: "\(user_request_model.userId)", status: "2")
            modal?.closeWithLeansRandom()
            return
        }
        modal.show(modalView: view, inView: (UIApplication.shared.delegate?.window!)!)
    }
    
    //MARK:- Cancel Friend Request Api
    func cancelFriendRequest(friend_request_id : String, status : String)
    {
        if(CommonFunction.isInternetAvailable())
        {
        MBProgressHUD.showAdded(to: self.view, animated: true)
        Alamofire.request(Constants.BASEURL+MethodName.cancelFriendRequest + "?access-token=\(UserDefaults.standard.value(forKey: "Access_Token") as! String)&id=\(friend_request_id)", method: .post, parameters: nil, encoding: URLEncoding.default, headers: nil).responseJSON
            { response in
                MBProgressHUD.hide(for: self.view, animated: true)
                if let result_dict = response.result.value as? [String:Any]
                {
                    if let code = result_dict["code"] as? Int
                    {
                        if(code==200)
                        {
                            self.friendListArr.remove(at: self.deleted_index)
                            self.ReloadTable()
                            if let data_dict = result_dict["data"] as? [String:Any]
                            {
                                if let msg = data_dict["message"] as? String
                                {
                                    self.view.makeToast(msg)
                                }
                            }
                        }
                        else
                        {
                            if let error_arr = result_dict["errorData"] as? [[String:Any]]
                            {
                                if let error_msg = error_arr[0]["message"] as? String
                                {
                                    self.view.makeToast(error_msg)
                                }
                            }
                        }
                    }
                }
            }
        }
        else
        {
            self.view.makeToast("Please Check Your Internet Connection!!")
        }
    }
    
    //MARK:- Accept Friend Request Api
    func acceptFriendRequest(friend_request_id : String, status : String)
    {        
        if(CommonFunction.isInternetAvailable())
        {
            let params = ["friend_user_id":friend_request_id, "status" : "0"]
            
            let headerString = ["Content-Type":"application/json"]
            MBProgressHUD.showAdded(to: self.view, animated: true)
            Alamofire.request(Constants.BASEURL+MethodName.acceptFriendRequest  + "?access-token=\(UserDefaults.standard.value(forKey: "Access_Token") as! String)", method: .post, parameters: params, encoding: JSONEncoding.default, headers: headerString).responseJSON { response in
                MBProgressHUD.hide(for: self.view, animated: true)
                if let result_dict = response.result.value as? [String:Any]
                {
                    if let code = result_dict["code"] as? Int
                    {
                        if(code==200)
                        {
                            self.friendListArr.remove(at: self.deleted_index)
                            self.ReloadTable()
                            if let data_dict = result_dict["data"] as? [String:Any]
                            {
                                if let msg = data_dict["message"] as? String
                                {
                                    self.view.makeToast("Friend Request Accepted")
                                }
                            }
                            
                        }
                        else
                        {
                            if let error_arr = result_dict["errorData"] as? [[String:Any]]
                            {
                                if let error_msg = error_arr[0]["message"] as? String
                                {
                                    self.view.makeToast(error_msg)
                                }
                            }
                        }
                    }
                }
            }
        }
        else
        {
            self.view.makeToast("Please Check Your Internet Connection!!")
        }
    }
    
    //MARK:- Get Friend Request List Api
    func getFriendReuestList()
    {
        
        if(CommonFunction.isInternetAvailable())
        {
            
            MBProgressHUD.showAdded(to: self.view, animated: true)
            Alamofire.request(Constants.BASEURL + MethodName.getPendingRequest + "?access-token=\(UserDefaults.standard.value(forKey: "Access_Token") as! String)", method: .get, parameters: nil , encoding: URLEncoding.default, headers: ["Content-Type":"application/json"]).responseJSON { response in
                MBProgressHUD.hide(for: self.view, animated: true)
                if let result_dict = response.result.value as? [String:Any]
                {
                    print(result_dict)
                    if(result_dict["code"] as! Int == 200)
                    {
                        self.friendListArr.removeAll()
                        if let user_arr = result_dict ["data"] as? [[String:Any]]
                        {
                            for i in 0..<user_arr.count
                            {
                                let user_model = FriendRequestModel()
                                if let user_dict = user_arr[i] as? [String:Any]
                                {
                                    if let about_str = user_dict["aboutme"] as? String
                                    {
                                        user_model.aboutme = about_str
                                    }
                                    if let name_str = user_dict["fullname"] as? String
                                    {
                                        user_model.userName = name_str
                                    }
                                    if let img_str = user_dict["user_profile_image"] as? String
                                    {
                                        user_model.userImage = img_str
                                    }
                                    if let city_str = user_dict["city"] as? String
                                    {
                                        user_model.city = city_str
                                    }
                                    if let country_str = user_dict["country"] as? String
                                    {
                                        user_model.country = country_str
                                    }
                                    
                                    if let userid_str = user_dict["user_id"] as? Int
                                    {
                                        user_model.userId = userid_str
                                    }
                                    if let friend_request_id = user_dict["friend_request_id"] as? Int
                                    {
                                        user_model.friend_request_id = friend_request_id
                                    }
                                    if let is_coach = user_dict["is_coach"] as? Bool
                                    {
                                        user_model.is_coach = is_coach
                                    }
                                    if let is_like = user_dict["is_like"] as? Bool
                                    {
                                        user_model.is_like = is_like
                                    }
                                    if let hourly_rate = user_dict["hourly_rate"] as? Int
                                    {
                                        user_model.hourly_rate = hourly_rate
                                    }
                                    
                                    self.friendListArr.append(user_model)
                                }
                            }
                            self.ReloadTable()
                        }
                        
                    }
                    else
                    {
                        if let error_arr = result_dict["errorData"] as? [[String:Any]]
                        {
                            if let error_msg = error_arr[0]["message"] as? String
                            {
                                self.view.makeToast(error_msg)
                            }
                        }
                    }
                }
            }
        }
        else
        {
            self.view.makeToast("Please Check Your Internet Connection!!")
        }
    }

    //MARK:- Back Action
    @IBAction func btnBackTapped(_ sender: UIButton) {
        
        self.navigationController?.popViewController(animated: true)
    }
    
    
    
    @IBAction func btnSearchTapped(_ sender: UIButton) {
        
        let searchPage = self.storyboard?.instantiateViewController(withIdentifier: "SearchFriendViewController") as! SearchFriendViewController
        self.navigationController?.pushViewController(searchPage, animated: false)
    }
    
    
    //MARK:- Reload Table
    func ReloadTable()
    {
        self.tblFriendRequests.reloadData()
        
        if self.friendListArr.count > 0
        {
            self.tblFriendRequests.isHidden = false
            self.viewNoData.isHidden = true
            
        }
        else
        {
            self.tblFriendRequests.isHidden = true
            self.viewNoData.isHidden = false
        }
    }
}
