//
//  popDispute.swift
//  Techmea
//
//  Created by Dhaval Panchani on 09/02/19.
//  Copyright © 2019 VISHAL SETH. All rights reserved.
//

import UIKit

class popDispute: UIView {

    var OkButtonHandler: (() -> Void)?
    var CancelButtonHandler: (() -> Void)?
    @IBOutlet weak var viewMain: UIView!
    @IBOutlet weak var viewMessage: UIView!
    @IBOutlet weak var txtMessage: UITextField!
    @IBOutlet weak var btnSubmit: UIButton!
    @IBOutlet weak var btnCancel: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.viewMain.layer.cornerRadius = 15.0
        self.layer.cornerRadius = 15.0
        self.txtMessage.placeHolderColor = UIColor.init(red: 252.0/255.0, green: 89.0/255.0, blue: 138.0/255.0, alpha: 1.0)
    }
    
    
    class func instantiateFromNib() -> popDispute {
        let view = UINib(nibName: "popDispute", bundle: nil).instantiate(withOwner: nil, options: nil).first as! popDispute
        
        return view
    }
    @IBAction func btnDoneTapped(_ sender: UIButton) {
        
        self.OkButtonHandler?()
    }

    @IBAction func btnCancelTapped(_ sender: UIButton) {
        
        self.CancelButtonHandler?()
    }
}
