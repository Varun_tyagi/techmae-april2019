//
//  ForgotPasswordViewController.swift
//  Techmea
//
//  Created by Dhaval Panchani on 12/01/19.
//  Copyright © 2019 VISHAL SETH. All rights reserved.
//

import UIKit
import Toast_Swift
import Alamofire
import MBProgressHUD
import Quickblox

class ForgotPasswordViewController: UIViewController,QMAuthServiceDelegate,QMChatServiceDelegate,QMChatConnectionDelegate {

    
    @IBOutlet weak var viewForgotPasswordEmail: UIView!
    @IBOutlet weak var txtForgotPasswordEmail: UITextField!
    @IBOutlet weak var btnSubmitForgotPasswordEmail: UIButton!
    
    
    @IBOutlet weak var btnBackgroundPopReset: UIButton!
   
    @IBOutlet weak var popViewResetPassword: UIView!
    @IBOutlet weak var viewResetPasswordCode: UIView!
    @IBOutlet weak var txtResetPasswordCode: UITextField!
    @IBOutlet weak var viewResetPassword: UIView!
    @IBOutlet weak var viewResetPassowrdConfirm: UIView!
    
    @IBOutlet weak var txtResetPassword: UITextField!
    @IBOutlet weak var txtResetPasswordConfirm: UITextField!
    @IBOutlet weak var btnSubmitResetPassword: UIButton!
    
    var resetCode = String()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    func ForgetPassword()
    {
        if(CommonFunction.isInternetAvailable())
        {
            let strParam = ["email" : txtForgotPasswordEmail.text!]
            MBProgressHUD.showAdded(to: self.view, animated: true)
            Alamofire.request(Constants.BASEURL + "user-info/forget-password", method: .post, parameters: strParam , encoding: JSONEncoding.default, headers: ["Content-Type":"application/json"]).responseJSON { response in
                MBProgressHUD.hide(for: self.view, animated: true)
                if let result_dict = response.result.value as? [String:Any]
                {
                    print(result_dict)
                    if result_dict["code"] as! Int == 200
                    {
                        if let data_dict = result_dict["data"] as? [String:Any]
                        {
                            if let reset_code = data_dict["message"] as? String
                            {
                                //self.resetCode = reset_code
                                self.popViewResetPassword.isHidden = false
                                self.btnBackgroundPopReset.isHidden = false
                                self.view.makeToast(reset_code)
                            }
                        }
                    }
                    else
                    {
                        if let error_arr = result_dict["errorData"] as? [[String:Any]]
                        {
                            if let error_msg = error_arr[0]["message"] as? String
                            {
                                self.view.makeToast(error_msg)
                            }
                        }
                    }
                }
            }
        }
        else
        {
            self.view.makeToast("Please Check Your Internet Connection!!")
        }
    }
    
    func ResetCode()
    {
        if(CommonFunction.isInternetAvailable())
        {
            if txtResetPassword.text == txtResetPasswordConfirm.text
            {
                
                
                let strParam = ["code" : txtResetPasswordCode.text! ,"password" : txtResetPassword.text!] as [String : Any]
                MBProgressHUD.showAdded(to: self.view, animated: true)
                Alamofire.request(Constants.BASEURL + "user-info/reset-password", method: .post, parameters: strParam , encoding: JSONEncoding.default, headers: ["Content-Type":"application/json"]).responseJSON { response in
                    MBProgressHUD.hide(for: self.view, animated: true)
                    if let result_dict = response.result.value as? [String:Any]
                    {
                        print(result_dict)
                        if result_dict["code"] as! Int == 200
                        {
                            
                            self.popViewResetPassword.isHidden = true
                            self.btnBackgroundPopReset.isHidden = true
                            Helper.resetDefaults()
                            
                            ServicesManager.instance().logoutUserWithCompletion { [weak self] (boolValue) -> () in
                                
                                guard let strongSelf = self else { return }
                                if boolValue {
                                    NotificationCenter.default.removeObserver(strongSelf)
                                    
                                    
                                    ServicesManager.instance().chatService.removeDelegate(strongSelf)
                                    ServicesManager.instance().authService.remove(strongSelf)
                                    
                                    ServicesManager.instance().lastActivityDate = nil;
                                    
                                    
                                    // SVProgressHUD.showSuccess(withStatus: "SA_STR_COMPLETED".localized)
                                }
                            }
                            
                            let vc = Constants.mainStoryboard.instantiateViewController(withIdentifier:  "LoginRegisterViewController") as! LoginRegisterViewController
                            let nav = UINavigationController.init(rootViewController: vc)
                            nav.navigationBar.isHidden=true
                            nav.setNavigationBarHidden(true, animated: true)
                            Constants.appDelegate.window?.rootViewController = nav
                            
                        }
                        else
                        {
                            if let error_arr = result_dict["errorData"] as? [[String:Any]]
                            {
                                if let error_msg = error_arr[0]["message"] as? String
                                {
                                    self.view.makeToast(error_msg)
                                }
                            }
                        }
                    }
                }
            }
            else
            {
                self.view.makeToast("Password Should be Same!!")
            }
        }
        else
        {
            self.view.makeToast("Please Check Your Internet Connection!!")
        }
        
    }
    
    
    @IBAction func btnBackTapped(_ sender: UIButton) {
        
        self.navigationController?.popViewController(animated: true)
    }
    
    
    @IBAction func btnSubmitForgotPasswordEmailTapped(_ sender: UIButton) {
        
        self.ForgetPassword()
    }
    
    @IBAction func btnBackgroundPopReset(_ sender: UIButton) {
        
        self.popViewResetPassword.isHidden = true
        self.btnBackgroundPopReset.isHidden = true
    }
    
    @IBAction func btnSubmitResetPasswordTapped(_ sender: UIButton) {
        
        self.ResetCode()
    }
    
    
}
