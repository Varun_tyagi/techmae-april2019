//
//  OnBoardingViewController.swift
//  Techmea
//
//  Created by varun on 21/12/19.
//  Copyright © 2019 varun tyagi. All rights reserved.
//

import UIKit



class OnBoardingCollectionViewCell:UICollectionViewCell{
    @IBOutlet weak var imgView: UIImageView!
    @IBOutlet weak var nextBtn: UIButton!
    @IBOutlet weak var skipBtn: UIButton!

}

class OnBoardingViewController: UIViewController {

    @IBOutlet weak var onBoardCollection: UICollectionView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        onBoardCollection.isHidden=false
        onBoardCollection.reloadData()
        
        self.navigationController?.setNavigationBarHidden(true, animated: false)
    }
    
  

}
extension OnBoardingViewController: UICollectionViewDataSource,UICollectionViewDelegateFlowLayout{
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 5
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 1
    }
    
//    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
//        return CGSize.init(width: view.frame.size.width, height: 500)
//    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "OnBoardingCollectionViewCell", for: indexPath) as! OnBoardingCollectionViewCell
        
        //Onboard_x_
        cell.imgView.image = UIImage.init(named: "Onboard_x_\(indexPath.section+1)")
        
        cell.nextBtn.tag = indexPath.section
        cell.skipBtn.tag = indexPath.section
        
        cell.nextBtn.addTarget(self, action: #selector(nextAction(_:)), for: .touchUpInside)
        cell.skipBtn.addTarget(self, action: #selector(skipAction(_:)), for: .touchUpInside)

        if indexPath.section == 0 {
            cell.skipBtn.isHidden=true
        }else{
            cell.skipBtn.isHidden=false
        }
        
        return cell
    }
    
    @objc func nextAction(_ sender:UIButton){
        if sender.tag == 4{
            self.skipAction(UIButton())
        }else{
            self.onBoardCollection.scrollToItem(at: IndexPath.init(row: 0, section: sender.tag+1), at: UICollectionViewScrollPosition.centeredHorizontally, animated: true)
        }
    }
    
    @objc func  skipAction(_ sender:UIButton){
        self.gotoLogin()
      }
    
    func gotoLogin(){
            
    let storyboard = UIStoryboard(name: "Main", bundle: nil)
    var navigationController = UINavigationController()
    let viewController = storyboard.instantiateViewController(withIdentifier: "LoginRegisterViewController") as! LoginRegisterViewController
    navigationController = UINavigationController.init(rootViewController: viewController)
    navigationController.setNavigationBarHidden(true, animated: false)
    UIApplication.shared.windows.first?.rootViewController = navigationController
        
    }
    
}
