//
//  ImageViewController.swift
//  Techmea
//
//  Created by Dhaval Panchani on 15/02/19.
//  Copyright © 2019 VISHAL SETH. All rights reserved.
//

import UIKit
import CameraManager


class ImageViewController: UIViewController,CropViewControllerDelegate {

    @IBOutlet weak var btnCancel: UIButton!
    @IBOutlet weak var btnDone: UIButton!
    
    var image: UIImage?
    var cameraManager: CameraManager?
    @IBOutlet weak var imageView: UIImageView!
    
    
    var current_selection = String()
    var parameter_dict = [String:Any]()
    var loginType_str = String()
    var already_added = Bool()
    
    var passport_image : UIImage?
    var passport_faceId =  NSString()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.navigationController?.navigationBar.isHidden = true
        
        guard let validImage = image else {
            return
        }
        
        self.imageView.image = validImage
        
//        if cameraManager?.cameraDevice == .front {
//            switch validImage.imageOrientation {
//            case .up, .down:
//                //self.imageView.transform = CGAffineTransform(rotationAngle: CGFloat(Double.pi))
//            default:
//                break
//            }
//        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func closeButtonTapped(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
    

    @IBAction func btnCancelTapped(_ sender: UIButton) {
        
        navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnDoneTapped(_ sender: UIButton) {
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5, execute: {
            let cropViewController = CropViewController(image:  UIImage(data: UIImageJPEGRepresentation(self.image!, 0.30)!)! )
            cropViewController.delegate = self
            self.present(cropViewController, animated: true, completion: nil)
        })   
    }
    
    
    //MARK:- didCropToImage
    func cropViewController(_ cropViewController: CropViewController, didCropToImage image: UIImage, withRect cropRect: CGRect, angle: Int)
    {
        cropViewController.dismiss(animated: true, completion: nil)
        
        
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.5, execute: {
                let verificationPage = self.storyboard?.instantiateViewController(withIdentifier: "FaceRecognizerCameraViewController") as! FaceRecognizerCameraViewController
                verificationPage.current_selection = self.current_selection
                verificationPage.param_dict = self.parameter_dict
                verificationPage.loginType = self.loginType_str
                verificationPage.already_register = self.already_added
               
                if(self.current_selection == "Photo")
                {
                    verificationPage.photo_image = image
                    if self.passport_image != nil
                    {
                        verificationPage.passport_image = self.passport_image
                    }
                    if self.passport_faceId != ""
                    {
                        verificationPage.passport_faceId = self.passport_faceId
                    }
                }
                else
                {
                    verificationPage.passport_image = image
                }
                self.navigationController?.pushViewController(verificationPage, animated: true)
            })
        
    }
    
    
    override var preferredInterfaceOrientationForPresentation: UIInterfaceOrientation {
        return .portrait
    }
    
    override var shouldAutorotate: Bool {
        return false
    }

}
