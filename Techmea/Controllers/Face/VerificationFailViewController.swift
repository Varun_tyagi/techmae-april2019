//
//  VerificationFailViewController.swift
//  Techmea
//
//  Created by Dhaval Panchani on 21/09/18.
//  Copyright © 2018 VISHAL SETH. All rights reserved.
//

import UIKit

class VerificationFailViewController: UIViewController {

    //MARK:- viewDidLoad
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK:- Back Action
    @IBAction func btnBackAction(_ sender: UIButton)
    {
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "ResetImage"), object: nil) 
        self.navigationController?.popViewController(animated: true)
    }
    
    //MARK:- Button Try Again Action
    @IBAction func brnTryagainAction(_ sender: UIButton)
    {
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "ResetImage"), object: nil)

        self.navigationController?.popViewController(animated: true)
        
    }
    

}
