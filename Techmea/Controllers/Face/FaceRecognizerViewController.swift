//
//  FaceRecognizerViewController.swift
//  Techmea
//
//  Created by Dhaval Panchani on 13/09/18.
//  Copyright © 2018 VISHAL SETH. All rights reserved.
//

import UIKit
import AVFoundation
import AVKit
import Photos
import Toast_Swift

class FaceRecognizerViewController: UIViewController,UIImagePickerControllerDelegate,UINavigationControllerDelegate,CropViewControllerDelegate {

    
    //MARK:- Outlets
    @IBOutlet weak var btn_photo: UIButton!
    @IBOutlet weak var btn_passport: UIButton!
    let picker = UIImagePickerController()
    var selected_image = UIImage()
    var current_selection = String()
    var parameter_dict = [String:Any]()
    var loginType_str = String()
    var already_added = Bool()
    var cropViewController = CropViewController.init(image: UIImage())

    //MARK:- viewDidLoad
    override func viewDidLoad()
    {
        super.viewDidLoad()
        CommonFunction.addshadow_button(btn: btn_photo, shadow_radius: 5.0, shadow_color: UIColor.lightGray, shadow_opecity: 0.5, shadow_offset: CGSize(width: 2.0, height: 2.0), is_clipBound: false, corner_radius: 20.0)
        CommonFunction.addshadow_button(btn: btn_passport, shadow_radius: 5.0, shadow_color: UIColor.lightGray, shadow_opecity: 0.5, shadow_offset: CGSize(width: 2.0, height: 2.0), is_clipBound: false, corner_radius: 20.0)
        
        print(loginType_str)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    //MARK:- Back Action
    @IBAction func btnBackAction(_ sender: UIButton)
    {
        self.navigationController?.popViewController(animated: true)
    }
    
    
    //MARK:- Button Passport Action
    @IBAction func btnPassportAction(_ sender: UIButton)
    {
        current_selection = "Passport"
        Select_image()
        //self.openCamera()
    }
    
    
    //MARK:- Button Photo Action
    @IBAction func btnPhotoAction(_ sender: UIButton)
    {
        current_selection = "Photo"
        openCustomCamera()
        //openCamera()
        //Select_image()
    }
    
    //MARK:- Custom Camera
    func openCustomCamera()
    {
        let storyBoard = UIStoryboard(name: "Main", bundle: nil)
        let objCustomCameraViewViewController = storyBoard.instantiateViewController(withIdentifier: "CustomCameraViewViewController") as! CustomCameraViewViewController
        objCustomCameraViewViewController.already_added = self.already_added
        objCustomCameraViewViewController.parameter_dict = self.parameter_dict
        objCustomCameraViewViewController.loginType_str = self.loginType_str
        objCustomCameraViewViewController.current_selection = self.current_selection
        self.navigationController?.pushViewController(objCustomCameraViewViewController, animated: true)
    }
    
    //MARK:- Select Image
    func Select_image()
    {
        let alert = UIAlertController(title: Constants.APP_NAME, message: "Browse Photo using", preferredStyle: UIAlertControllerStyle.actionSheet)
        
        let galleryBtn = UIAlertAction(title: "Gallery", style: UIAlertActionStyle.default) { (alert) -> Void in
            DispatchQueue.main.async{
            self.openGallery()
            }
        }
        
        let cameraBtn = UIAlertAction(title: "Camera", style: UIAlertActionStyle.default) { (alert) -> Void in
             DispatchQueue.main.async{
            self.openCamera()
            }
        }
        
        let cancelButton = UIAlertAction(title: "Cancel", style: UIAlertActionStyle.destructive) { (alert) -> Void in
            //self.txtUserType.text="Trader"
        }
        
        alert.addAction(galleryBtn)
        alert.addAction(cameraBtn)
        alert.addAction(cancelButton)
         DispatchQueue.main.async{
        self.present(alert, animated: true, completion: nil)
        }
    }
    
    //MARK:- Open Camera
    func openCamera()
    {
        if(Platform.isSimulator)
        {
             DispatchQueue.main.async{
                self.openGallery()
                
            //self.displayAlert(msg: "No camera Available", title_str: Constants.APP_NAME)
            self.view.makeToast("No camera Available")
        }
        
        }
        else
        {
            _ = AVCaptureDevice.authorizationStatus(for: AVMediaType.video)
            AVCaptureDevice.requestAccess(for: AVMediaType.video, completionHandler: { (granted :Bool) -> Void in
                if granted == true
                {
                    DispatchQueue.main.async{
                    self.picker.allowsEditing = false
                    self.picker.sourceType = UIImagePickerControllerSourceType.camera
                    self.picker.cameraCaptureMode = .photo
                    self.picker.cameraDevice = .front
                    self.picker.delegate = self
                    self.picker.modalPresentationStyle = .fullScreen
                     
                    self.present(self.picker,animated: true,completion: nil)
                    }
                }
                else
                {
                     DispatchQueue.main.async{
                    self.show_deny_alert(msg: "You deny to use camera.To allow camera you should allow camera permission from setting.")
                    }
                }
            })
        }
    }
    func show_deny_alert(msg:String)
    {
        let alert = UIAlertController(title: Constants.APP_NAME, message: msg, preferredStyle: UIAlertControllerStyle.alert)
        
        let okBtn = UIAlertAction(title: "OK", style: UIAlertActionStyle.default) { (alert) -> Void in
            guard let settingsUrl = URL(string: UIApplicationOpenSettingsURLString) else {
                return
            }
            
            if UIApplication.shared.canOpenURL(settingsUrl) {
                UIApplication.shared.open(settingsUrl, completionHandler: { (success) in
                    print("Settings opened: \(success)") // Prints true
                })
            }
        }
        alert.addAction(okBtn)
         DispatchQueue.main.async{
        self.present(alert, animated: true, completion: nil)
        }
    }
    
    //MARK:- Open Gallery
    func openGallery()
    {
        PHPhotoLibrary.requestAuthorization
        { (status) in
            let status = PHPhotoLibrary.authorizationStatus()
            if (status == PHAuthorizationStatus.authorized)
            {
                DispatchQueue.main.async{
                self.picker.allowsEditing = false
                self.picker.sourceType = .photoLibrary
                self.picker.delegate=self
                self.picker.mediaTypes = UIImagePickerController.availableMediaTypes(for: .photoLibrary)!
                 
                self.present(self.picker, animated: true, completion: nil)
                }
            }
            else
            {
                 DispatchQueue.main.async{
                self.show_deny_alert(msg: "You deny to use photo library.To allow photo library you should allow photos permission from setting.")
                }
            }
        }
    }
    
    //MARK:- didFinishPickingMediaWithInfo
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any])
    {
        let chosenImage = info[UIImagePickerControllerOriginalImage] as! UIImage
        dismiss(animated:true, completion: nil)
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5, execute: {
            self.cropViewController = CropViewController(image:  UIImage(data: UIImageJPEGRepresentation(chosenImage, 0.30)!)! )
            self.cropViewController.delegate = self
            self.cropViewController.modalTransitionStyle = .coverVertical
            self.present(self.cropViewController, animated: true, completion: nil)
        })        
    }
    
    //MARK:- imagePickerControllerDidCancel
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController)
    {
        picker.dismiss(animated: true, completion: nil)
    }
    
    
    //MARK:- didCropToImage
    func cropViewController(_ cropViewController: CropViewController, didCropToImage image: UIImage, withRect cropRect: CGRect, angle: Int)
    {
        self.cropViewController.dismiss(animated: true, completion: nil)
      //  dismiss(animated:true, completion: nil)
//        self.cropViewController.dismiss(animated: true) {
//            DispatchQueue.main.async {
//                let storyBoard = UIStoryboard(name: "Main", bundle: nil)
//
//                      let verificationPage = storyBoard.instantiateViewController(withIdentifier: "FaceRecognizerCameraViewController") as! FaceRecognizerCameraViewController
//                      verificationPage.current_selection = self.current_selection
//                      verificationPage.param_dict = self.parameter_dict
//                      verificationPage.loginType = self.loginType_str
//                      verificationPage.already_register = self.already_added
//                      if(self.current_selection == "Photo")
//                      {
//                          verificationPage.photo_image = image
//                      }
//                      else
//                      {
//                          verificationPage.passport_image = image
//                      }
//                      self.navigationController?.pushViewController(verificationPage, animated: true)
//                  }
//        }
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5, execute: {
            let verificationPage = self.storyboard?.instantiateViewController(withIdentifier: "FaceRecognizerCameraViewController") as! FaceRecognizerCameraViewController
            verificationPage.current_selection = self.current_selection
            verificationPage.param_dict = self.parameter_dict
            verificationPage.loginType = self.loginType_str
            verificationPage.already_register = self.already_added
            if(self.current_selection == "Photo")
            {
                verificationPage.photo_image = image
            }
            else
            {
                verificationPage.passport_image = image
            }
            self.navigationController?.pushViewController(verificationPage, animated: true)
        })
    }
}
