//
//  FaceRecognizerCameraViewController.swift
//  Techmea
//
//  Created by Dhaval Panchani on 13/09/18.
//  Copyright © 2018 VISHAL SETH. All rights reserved.
//

import UIKit
import MBProgressHUD
import Alamofire
import AVFoundation
import Photos
import Toast_Swift

class FaceRecognizerCameraViewController: UIViewController,UIImagePickerControllerDelegate,UINavigationControllerDelegate,CropViewControllerDelegate,CustomSliderViewDelegate {
    
    
    
    //MARK:- Outlets
    @IBOutlet weak var photo_img_btn: UIButton!
    @IBOutlet weak var passport_img_btn: UIButton!
    @IBOutlet weak var btn_passport: UIButton!
    @IBOutlet weak var btn_photo: UIButton!
    let picker = UIImagePickerController()
    var SelectedImage = UIImage()
    var current_selection = String()
    var passport_image : UIImage?
    var photo_image : UIImage?
    var passport_faceId =  NSString()
    var photo_faceId = NSString()
    
    @IBOutlet weak var viewSlideToOpen: UIView!
    var customSliderView: CustomSliderView?
    
    @IBOutlet weak var lblStatusapi: UILabel!
    @IBOutlet weak var imageview_photo: UIImageView!
    @IBOutlet weak var imageview_passport: UIImageView!
    var param_dict = [String:Any]()
    var loginType = String()
    var already_register = Bool()
    
    
    //MARK:- viewDidLoad
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        customSliderView = Bundle.main.loadNibNamed("NewSliderView", owner: self, options: nil)?.last as? CustomSliderView
        customSliderView?.delegate = self
        customSliderView?.sliderTitle.text = "Slide To Enter"
        var frame: CGRect = customSliderView!.frame
        frame.size.width = viewSlideToOpen.frame.size.width
        frame.size.height = viewSlideToOpen.frame.size.height
        customSliderView?.frame = frame
        viewSlideToOpen.addSubview(customSliderView!)
        
        CommonFunction.addshadow_button(btn: btn_photo, shadow_radius: 5.0, shadow_color: UIColor.lightGray, shadow_opecity: 0.5, shadow_offset: CGSize(width: 2.0, height: 2.0), is_clipBound: false, corner_radius: 20.0)
        CommonFunction.addshadow_button(btn: btn_passport, shadow_radius: 5.0, shadow_color: UIColor.lightGray, shadow_opecity: 0.5, shadow_offset: CGSize(width: 2.0, height: 2.0), is_clipBound: false, corner_radius: 20.0)
        if(current_selection=="Passport")
        {
            btn_passport.isHidden = true
            imageview_passport.image = passport_image
            imageview_photo.isHidden = true
            passport_img_btn.isHidden = false
            btn_photo.isHidden = false
            photo_img_btn.isHidden = true
            self.get_faceId(img: passport_image!)
        }
        else
        {
            btn_passport.isHidden = false
            passport_img_btn.isHidden = true
            imageview_photo.image = photo_image
            imageview_passport.isHidden = true
            photo_img_btn.isHidden = false
            btn_photo.isHidden = true
            photo_img_btn.isHidden = false
            if passport_image != nil
            {
                btn_passport.isHidden = true
                imageview_passport.isHidden = false
                imageview_passport.image = passport_image
                passport_img_btn.isHidden = false
            }
            self.get_faceId(img: photo_image!)
        }
        NotificationCenter.default.addObserver(self, selector: #selector(self.reset_photo), name: NSNotification.Name(rawValue: "ResetImage"), object: nil)
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
    
        customSliderView?.sliderImage.frame = CGRect(x: 5.0, y: 5.0, width: 40.0, height: 50.0)
    }
    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
    }
    
    //MARK:- Back Action
    @IBAction func btnBackAction(_ sender: UIButton)
    {
        //self.navigationController?.popViewController(animated: true)
        
        for vc in self.navigationController!.viewControllers
        {
            if vc is FaceRecognizerViewController
            {
                self.navigationController!.popToViewController(vc, animated: false)
                break
            }
        }
    }
    
    //MARK:- Button Passport Action
    @IBAction func btnPassportAction(_ sender: UIButton)
    {
        current_selection = "Passport"
        self.Select_image()
        //self.openCamera()
    }
    
    //MARK:- Button Photo Action
    @IBAction func btnPhotoAction(_ sender: UIButton)
    {
        current_selection = "Photo"
        //openCamera()
        //self.Select_image()
        openCustomCamera()
    }
    
    //MARK:- button Edit Passport Action
    @IBAction func btnEditPassportAction(_ sender: UIButton)
    {
        current_selection = "Passport"
        Select_image()
        //openCamera()
    }
    
    //MARK:- Button Edit Photo Action
    @IBAction func btnEditPhotoAction(_ sender: UIButton)
    {
        current_selection = "Photo"
       // openCamera()
        //Select_image()
        openCustomCamera()
    }
    
    //MARK:- Custom Camera
    func openCustomCamera()
    {
        let storyBoard = UIStoryboard(name: "Main", bundle: nil)
        let objCustomCameraViewViewController = storyBoard.instantiateViewController(withIdentifier: "CustomCameraViewViewController") as! CustomCameraViewViewController
        objCustomCameraViewViewController.already_added = self.already_register
        objCustomCameraViewViewController.parameter_dict = self.param_dict
        objCustomCameraViewViewController.loginType_str = self.loginType
        objCustomCameraViewViewController.current_selection = self.current_selection
        if self.passport_image != nil
        {
            objCustomCameraViewViewController.passport_image = self.passport_image!
        }
        objCustomCameraViewViewController.passport_faceId = self.passport_faceId
        self.navigationController?.pushViewController(objCustomCameraViewViewController, animated: true)
    }
    
    //MARK:- Select Image
    func Select_image()
    {
        let alert = UIAlertController(title: Constants.APP_NAME, message: "Browse Photo using", preferredStyle: UIAlertControllerStyle.actionSheet)
        
        let galleryBtn = UIAlertAction(title: "Gallery", style: UIAlertActionStyle.default) { (alert) -> Void in
            self.openGallery()
        }
        
        let cameraBtn = UIAlertAction(title: "Camera", style: UIAlertActionStyle.default) { (alert) -> Void in
            self.openCamera()
        }
        
        let cancelButton = UIAlertAction(title: "Cancel", style: UIAlertActionStyle.destructive) { (alert) -> Void in
            //self.txtUserType.text="Trader"
        }
        
        alert.addAction(galleryBtn)
        alert.addAction(cameraBtn)
        alert.addAction(cancelButton)
        DispatchQueue.main.async {
        self.present(alert, animated: true, completion: nil)
        }
    }
    
    //MARK:- Open Camera
    func openCamera()
    {
        if(Platform.isSimulator)
        {
            openGallery()
            //self.displayAlert(msg: "No camera Available", title_str: Constants.APP_NAME)
            self.view.makeToast("No camera Available")
        }
        else
        {
            //let authorizationStatus = AVCaptureDevice.authorizationStatus(for: AVMediaType.video)
            AVCaptureDevice.requestAccess(for: AVMediaType.video, completionHandler:
            { (granted :Bool) -> Void in
                if granted == true
                {
                    DispatchQueue.main.async {
                    self.picker.allowsEditing = false
                    self.picker.sourceType = UIImagePickerControllerSourceType.camera
                    self.picker.cameraCaptureMode = .photo
                    self.picker.delegate = self
                    self.picker.cameraDevice = .front
                    self.picker.modalPresentationStyle = .fullScreen
                    self.present(self.picker,animated: true,completion: nil)
                    }
                }
                else
                {
                    self.show_deny_alert(msg: "You deny to use camera.To allow camera you should allow camera permission from setting.")                    
                }
            })
        }
    }
    func show_deny_alert(msg:String)
    {
        let alert = UIAlertController(title: Constants.APP_NAME, message: msg, preferredStyle: UIAlertControllerStyle.alert)
        
        let okBtn = UIAlertAction(title: "OK", style: UIAlertActionStyle.default) { (alert) -> Void in
            guard let settingsUrl = URL(string: UIApplicationOpenSettingsURLString) else {
                return
            }
            
            if UIApplication.shared.canOpenURL(settingsUrl) {
                UIApplication.shared.open(settingsUrl, completionHandler: { (success) in
                    print("Settings opened: \(success)") // Prints true
                })
            }
        }
        alert.addAction(okBtn)
        DispatchQueue.main.async {
        self.present(alert, animated: true, completion: nil)
        }
    }
    
    //MARK:- Open Gallery
    func openGallery()
    {
        PHPhotoLibrary.requestAuthorization
        { (status) in
            let status = PHPhotoLibrary.authorizationStatus()
            if (status == PHAuthorizationStatus.authorized)
            {
                DispatchQueue.main.async {
                self.picker.allowsEditing = false
                self.picker.sourceType = .photoLibrary
                self.picker.delegate=self
                self.picker.mediaTypes = UIImagePickerController.availableMediaTypes(for: .photoLibrary)!
                self.present(self.picker, animated: true, completion: nil)
                }
            }
            else
            {
                self.show_deny_alert(msg: "You deny to use photo library.To allow photo library you should allow photos permission from setting.")
            }
        }
    }
    
    //MARK:- didFinishPickingMediaWithInfo
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any])
    {
        let chosenImage = info[UIImagePickerControllerOriginalImage] as! UIImage
        dismiss(animated:true, completion: nil)
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5, execute: {
        let cropViewController = CropViewController(image:  UIImage(data: UIImageJPEGRepresentation(chosenImage, 0.30)!)! )
        cropViewController.delegate = self
            DispatchQueue.main.async {
        self.present(cropViewController, animated: true, completion: nil)
            }
        })
//        if(current_selection=="Photo")
//        {
//            btn_photo.isHidden = true
//            photo_image = chosenImage
//            self.get_faceId(img: photo_image!)
//            self.imageview_photo.image = photo_image
//            self.imageview_photo.isHidden = false
//        }
//        else
//        {
//            btn_passport.isHidden = true
//            passport_image = chosenImage
//            self.get_faceId(img: passport_image!)
//            self.imageview_passport.image = passport_image
//            self.imageview_passport.isHidden = false
//        }
        
    }
    
    
    //MARK:- didCropToImage
    func cropViewController(_ cropViewController: CropViewController, didCropToImage image: UIImage, withRect cropRect: CGRect, angle: Int)
    {
        cropViewController.dismiss(animated: true, completion: nil)
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5, execute: {
            if(self.current_selection=="Photo")
                {
                    self.btn_photo.isHidden = true
                    self.photo_image = image
                    self.get_faceId(img: self.photo_image!)
                    self.imageview_photo.image = self.photo_image
                    self.imageview_photo.isHidden = false
                }
                else
                {
                    self.btn_passport.isHidden = true
                    self.passport_image = image
                    self.get_faceId(img: self.passport_image!)
                    self.imageview_passport.image = self.passport_image
                    self.imageview_passport.isHidden = false
                }
            })
    }
    
    //MARK:- imagePickerControllerDidCancel
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController)
    {
        picker.dismiss(animated: true, completion: nil)
    }
    
    //MARK:- Get Face Id
    func get_faceId(img:UIImage)
    {
        if(CommonFunction.isInternetAvailable())
        {
        self.lblStatusapi.text = "Face ID API Processing"
        MBProgressHUD.showAdded(to: self.view, animated: true)
        let data = UIImageJPEGRepresentation(img, 0.3)
            let client = MPOFaceServiceClient(endpointAndSubscriptionKey: "https://westus.api.cognitive.microsoft.com/face/v1.0", key: "fb4ee7cc7bd44da492883a9e0b550f7f")
        client?.detect(with: data, returnFaceId: true, returnFaceLandmarks: true, returnFaceAttributes: [NSInteger(MPOFaceAttributeTypeGender.rawValue)], completionBlock: { (collection, error) in
            MBProgressHUD.hide(for: self.view, animated: true)
            
            if(error == nil)
            {
                self.lblStatusapi.text = "Face ID API Success"
                if (collection?.count == 0)
                {
                    if(self.current_selection=="Passport")
                    {
                        self.imageview_passport.image = nil
                        self.imageview_passport.isHidden = true
                        self.btn_passport.isHidden = false
                    }
                    else
                    {
                        self.imageview_photo.image = nil
                        self.imageview_photo.isHidden = true
                        self.btn_photo.isHidden = false
                    }
                    self.displayAlert(msg: "No face detected", title_str: Constants.APP_NAME)
                }
                else
                {
                    
                    var res = [MPOFace]()
                    res = collection!
                    let gender = res[0].attributes.gender
                    
                    if gender == "female"
                    {
                        self.displayAlert(msg: "Face detected", title_str: Constants.APP_NAME)
                
                        if let faceid_str = collection![0].faceId as NSString?
                        {
                            if(self.current_selection=="Passport")
                            {
                                self.imageview_passport.isHidden = false
                                self.passport_faceId = faceid_str
                                self.imageview_passport.image = self.passport_image
                                self.passport_img_btn.isHidden = false
                                self.btn_passport.isHidden = true
                            }
                            else
                            {
                                self.imageview_photo.isHidden = false
                                self.photo_faceId = faceid_str
                                self.imageview_photo.image = self.photo_image
                                self.photo_img_btn.isHidden = false
                                self.btn_photo.isHidden = true
                            }
                        }
                    }
                    else
                    {
                        //self.reset_photo()
                        if(self.current_selection=="Passport")
                        {
                            self.passport_image = nil
                            self.passport_img_btn.isHidden = true
                            self.passport_faceId = NSString()
                            self.imageview_passport.image = nil
                            self.imageview_passport.isHidden = true
                            self.btn_passport.isHidden = false
                        }
                        else
                        {
                            self.imageview_photo.image = nil
                            self.imageview_photo.isHidden = true
                            self.photo_faceId = NSString()
                            self.btn_photo.isHidden = false
                            self.photo_image = nil
                            self.photo_img_btn.isHidden = true
                        }
                        self.displayAlert(msg: "Sorry we weren’t able to verify your identity. Please contact support@techmae.com if you have gotten this in error", title_str: Constants.APP_NAME)
                    }
                }
            }
            else
            {
                //print(error)
                self.lblStatusapi.text = "Face ID API Fail"
                if(self.current_selection=="Passport")
                {
                    self.imageview_passport.image = nil
                    self.imageview_passport.isHidden = true
                    self.btn_passport.isHidden = false
                }
                else
                {
                    self.imageview_photo.image = nil
                    self.imageview_photo.isHidden = true
                    self.btn_photo.isHidden = false
                }
                //self.displayAlert(msg: "Something went wrong. Please try again", title_str: Constants.APP_NAME)
                self.view.makeToast("Something went wrong. Please try again")
            }
        })
        }
        else
        {
            //self.displayAlert(msg: "Please Check Your Internet Connection!!", title_str: Constants.APP_NAME)
            self.view.makeToast("Please Check Your Internet Connection!!")
        }
    }
    
    
    func sliderAction() {
        
        if(CommonFunction.isInternetAvailable())
        {
            print(passport_faceId)
            self.lblStatusapi.text = "Face Verify API Proccessig"
            if(passport_image != nil && photo_image != nil)
            {
                MBProgressHUD.showAdded(to: self.view, animated: true)
                let client = MPOFaceServiceClient(endpointAndSubscriptionKey: "https://westus.api.cognitive.microsoft.com/face/v1.0", key: "fb4ee7cc7bd44da492883a9e0b550f7f")
                client?.verify(withFirstFaceId: passport_faceId as String, faceId2: photo_faceId as! String, completionBlock:{ (verifyResult, error) in
                    MBProgressHUD.hide(for: self.view, animated: true)
                    if(error==nil)
                    {
                        self.lblStatusapi.text = "Face Verify API Success"
                        if(verifyResult?.isIdentical)!
                        {
                            if(self.already_register == false)
                            {
                                self.Register_user()
                            }
                            else
                            {
                                MBProgressHUD.showAdded(to: self.view, animated: true)
                                self.Passport_image_upload()
                            }
                        }
                        else
                        {
                            
                            let failVc =  Constants.mainStoryboard.instantiateViewController(withIdentifier: "VerificationFailViewController") as! VerificationFailViewController
                            self.navigationController?.pushViewController(failVc, animated: true)
                        }
                    }
                    else
                    {
                        self.lblStatusapi.text = "Face Verify API Fail"
                    }
                })
            }
            else
            {
                if(passport_image==nil)
                {
                    //self.displayAlert(msg: "Please upload passport image", title_str: Constants.APP_NAME)
                    self.view.makeToast("Please upload passport image")
                }
                else if(photo_image==nil)
                {
                    //self.displayAlert(msg: "Please upload selfie", title_str: Constants.APP_NAME)
                    self.view.makeToast("Please upload selfie")
                }
            }
        }
        else
        {
            // self.displayAlert(msg: "Please Check Your Internet Connection!!", title_str: Constants.APP_NAME)
            self.view.makeToast("Please Check Your Internet Connection!!")
        }
    }
    

    //MARK:- Button Verify Action
    @IBAction func btnVerifyAction(_ sender: Any)
    {
        if(CommonFunction.isInternetAvailable())
        {
        print(passport_faceId)
        self.lblStatusapi.text = "Face Verify API Proccessig"
        if(passport_image != nil && photo_image != nil)
        {
            MBProgressHUD.showAdded(to: self.view, animated: true)
            let client = MPOFaceServiceClient(endpointAndSubscriptionKey: "https://westus.api.cognitive.microsoft.com/face/v1.0", key: "fb4ee7cc7bd44da492883a9e0b550f7f")
            client?.verify(withFirstFaceId: passport_faceId as String, faceId2: photo_faceId as! String, completionBlock:{ (verifyResult, error) in
                MBProgressHUD.hide(for: self.view, animated: true)
                if(error==nil)
                {
                    self.lblStatusapi.text = "Face Verify API Success"
                    if(verifyResult?.isIdentical)!
                    {
                        if(self.already_register == false)
                        {
                            self.Register_user()
                        }
                        else
                        {
                            MBProgressHUD.showAdded(to: self.view, animated: true)
                            self.Passport_image_upload()
                        }
                    }
                    else
                    {
                        
                        let failVc =  Constants.mainStoryboard.instantiateViewController(withIdentifier: "VerificationFailViewController") as! VerificationFailViewController
                        self.navigationController?.pushViewController(failVc, animated: true)
                    }
                }
                else
                {
                    self.lblStatusapi.text = "Face Verify API Fail"
                }
            })
        }
        else
        {
            if(passport_image==nil)
            {
                //self.displayAlert(msg: "Please upload passport image", title_str: Constants.APP_NAME)
                self.view.makeToast("Please upload passport image")
            }
            else if(photo_image==nil)
            {
                //self.displayAlert(msg: "Please upload selfie", title_str: Constants.APP_NAME)
                self.view.makeToast("Please upload selfie")
            }
        }
        }
        else
        {
           // self.displayAlert(msg: "Please Check Your Internet Connection!!", title_str: Constants.APP_NAME)
            self.view.makeToast("Please Check Your Internet Connection!!")
        }
    }
    
    //MARK:- Reset Photo
    @objc func reset_photo()
    {
        passport_image = nil
        passport_img_btn.isHidden = true
        passport_faceId = NSString()
        imageview_passport.image = nil
        imageview_passport.isHidden = true
        btn_passport.isHidden = false

        imageview_photo.image = nil
        imageview_photo.isHidden = true
        photo_faceId = NSString()
        btn_photo.isHidden = false
        photo_image = nil
        photo_img_btn.isHidden = true
    }
    
    
    //MARK:- Passport Image Upload Api
    func Passport_image_upload()
    {
        if(CommonFunction.isInternetAvailable())
        {
            
            let headers: HTTPHeaders = ["Content-type": "multipart/form-data"]
            self.lblStatusapi.text = "Pasport Image API Processing"
            Alamofire.upload(multipartFormData: { (multipartFormData) in
                let image_data = UIImageJPEGRepresentation(self.passport_image!, 0.3)
                multipartFormData.append(image_data!, withName: "user_passport_pic", fileName: "\(Date.timeIntervalSinceReferenceDate).png", mimeType: "image/png")
            }, usingThreshold: UInt64.init(), to: Constants.BASEURL + MethodName.uploadPassport + "?access-token=\(UserDefaults.standard.value(forKey: "Access_Token") as! String)&id=\(UserDefaults.standard.value(forKey: Constants.USERID) as! Int)", method: .post, headers: headers)
            { (result) in
                
                switch result
                {
                case .success(let upload, _, _):
                    upload.responseJSON
                        { response in
                            self.lblStatusapi.text = "Pasport Image API Success"
                            if let result_dict = response.result.value as? [String:Any]
                            {
                                if let code_val = result_dict["code"] as? Int
                                {
                                    
                                    if(code_val==200)
                                    {
                                        self.Profile_image_upload()
                                        if let userDict = result_dict["data"] as? [String:Any]
                                        {
                                            print(userDict)
                                            let userData: Data = NSKeyedArchiver.archivedData(withRootObject: userDict)
                                            UserDefaults.standard.set(userData, forKey: "userData")
                                        }
                                    }
                                    else
                                    {
                                        MBProgressHUD.hide(for: self.view, animated: true)
                                        if let error_array = result_dict["errorData"] as? NSArray
                                        {
                                            if let error_dict = error_array[0] as? [String : Any]
                                            {
                                                if let error_msg = error_dict["message"] as? String
                                                {
                                                    self.view.makeToast(error_msg)
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                    }
                case .failure(let error):
                     self.lblStatusapi.text = "Pasport Image API Fail"
                    MBProgressHUD.hide(for: self.view, animated: true)
                    print("Error in upload: \(error.localizedDescription)")
                    self.view.makeToast("\(error.localizedDescription)")
                    
                }
                
            }
            
        }
        else
        {
            MBProgressHUD.hide(for: self.view, animated: true)
            self.view.makeToast("Please Check Your Internet Connection!!")
        }
    }
    
    
    //MARK:- Profile Image Upload Api
    func Profile_image_upload()
    {
        if(CommonFunction.isInternetAvailable())
        {
            
            let headers: HTTPHeaders = ["Content-type": "multipart/form-data"]
            self.lblStatusapi.text = "Profile Image API Processing"
            Alamofire.upload(multipartFormData: { (multipartFormData) in
                let image_data = UIImageJPEGRepresentation(self.photo_image!, 0.3)
                multipartFormData.append(image_data!, withName: "image", fileName: "\(Date.timeIntervalSinceReferenceDate).png", mimeType: "image/png")
            }, usingThreshold: UInt64.init(), to: Constants.BASEURL + MethodName.uploadProfile + "?access-token=\(UserDefaults.standard.value(forKey: "Access_Token") as! String)&id=\(UserDefaults.standard.value(forKey: Constants.USERID) as! Int)", method: .post, headers: headers)
            { (result) in
                
                switch result
                {
                case .success(let upload, _, _):
                    upload.responseJSON
                        { response in
                            self.lblStatusapi.text = "Profile Image API Success"
                            if let result_dict = response.result.value as? [String:Any]
                            {
                                if let code_val = result_dict["code"] as? Int
                                {
                                    if(code_val==200)
                                    {
                                        if let userDict = result_dict["data"] as? [String:Any]
                                        {
                                            MBProgressHUD.hide(for: self.view, animated: true)
                                            print(userDict)
                                            let userData: Data = NSKeyedArchiver.archivedData(withRootObject: userDict)
                                            UserDefaults.standard.set(userData, forKey: "userData")
                                            
                                            if (UserDefaults.standard.object(forKey: Constants.KDeviceToken) as? String) != nil
                                            {
                                                
                                                if let fcm_token = UserDefaults.standard.object(forKey: Constants.KDeviceToken) as? String
                                                {
                                                    self.call_set_device_token(token_str: fcm_token)
                                                }
                                            }
                                            
//                                            let view = CongratulationView.instantiateFromNib()
//                                            let modal = PathDynamicModal()
//                                            modal.showMagnitude = 200.0
//                                            modal.closeMagnitude = 130.0
//                                            modal.closeByTapBackground = false
//                                            modal.closeBySwipeBackground = false
//                                            view.OkButtonHandler = {[weak modal] in
//
//                                                let vc = Constants.mainStoryboard.instantiateViewController(withIdentifier: "MainTabbarViewController")as! MainTabbarViewController
//                                                self.navigationController?.pushViewController(vc, animated: true)
//
//                                                modal?.closeWithLeansRandom()
//                                                return
//                                            }
//                                            modal.show(modalView: view, inView: (UIApplication.shared.delegate?.window!)!)
                                            
                                        }
                                    }
                                    else
                                    {
                                        MBProgressHUD.hide(for: self.view, animated: true)
                                        if let error_array = result_dict["errorData"] as? NSArray
                                        {
                                            if let error_dict = error_array[0] as? [String : Any]
                                            {
                                                if let error_msg = error_dict["message"] as? String
                                                {
                                                    self.view.makeToast(error_msg)
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                    }
                case .failure(let error):
                    self.lblStatusapi.text = "Profile Image API Fail"
                    MBProgressHUD.hide(for: self.view, animated: true)
                    print("Error in upload: \(error.localizedDescription)")
                    self.view.makeToast("\(error.localizedDescription)")
                }
                
            }
            
        }
        else
        {
            self.view.makeToast("Please Check Your Internet Connection!!")
        }
    }
    
    
    
    //MARK:- Call Device Token Api
    func call_set_device_token(token_str:String)
    {
        if(CommonFunction.isInternetAvailable())
        {
            if let auth_token = UserDefaults.standard.value(forKey: "Access_Token") as? String
            {
                let param = ["device_id":"\(token_str)",
                    "device_type":"ios","user_id": "\(UserDefaults.standard.value(forKey: Constants.USERID) as! Int)"]
                Alamofire.request(Constants.BASEURL + MethodName.registerToken + "?access-token=\(auth_token)&user_id=\(UserDefaults.standard.value(forKey: Constants.USERID) as! Int)" , method: .post, parameters: param, encoding: JSONEncoding.default, headers: ["Content-Type":"application/json"]).responseJSON { response in
                    
                    if let result_dict = response.result.value as? [String:Any]
                    {
                        if let code = result_dict["code"] as? Int
                        {
                            if(code==200)
                            {
                                self.checkAccessApi()
                            }
                            else if (code==401)
                            {
                                if let error_data = result_dict["errorData"] as? [String:Any]
                                {
                                    if let error_msg = error_data["message"] as? String
                                    {
                                        let view = CongratulationView.instantiateFromNib()
                                        view.lblTitle.text = error_msg
                                        let modal = PathDynamicModal()
                                        modal.showMagnitude = 200.0
                                        modal.closeMagnitude = 130.0
                                        modal.closeByTapBackground = false
                                        modal.closeBySwipeBackground = false
                                        view.OkButtonHandler = {[weak modal] in
                                            
                                            let storyBoard = UIStoryboard(name: "Main", bundle: nil)
                                            let objHomeNav = storyBoard.instantiateViewController(withIdentifier: "HomeNav") as! UINavigationController
                                            UIApplication.shared.keyWindow?.rootViewController = objHomeNav
                                            
                                            modal?.closeWithLeansRandom()
                                            return
                                        }
                                        modal.show(modalView: view, inView: (UIApplication.shared.delegate?.window!)!)
                                    }
                                }
                                
                                
                            }
                            else
                            {
                                self.view.makeToast("Something Went Wrong!!")
                            }
                        }
                    }
                }
            }
        }
        else
        {
            self.view.makeToast("Please Check Your Internet Connection!!")
        }
    }
    
    
    //MARK:- Check Accsess Api
    func checkAccessApi()
    {
        if(CommonFunction.isInternetAvailable())
        {
            if let user_id = UserDefaults.standard.value(forKey: Constants.USERID) as? Int
            {
                Alamofire.request(Constants.BASEURL + MethodName.checkaccess + "?user_id=\(user_id)" , method: .post, parameters: nil, encoding: JSONEncoding.default, headers: ["Content-Type":"application/json"]).responseJSON { response in
                    
                    if let result_dict = response.result.value as? [String:Any]
                    {
                        if let code = result_dict["code"] as? Int
                        {
                            if(code==200)
                            {
                                let view = CongratulationView.instantiateFromNib()
                                let modal = PathDynamicModal()
                                modal.showMagnitude = 200.0
                                modal.closeMagnitude = 130.0
                                modal.closeByTapBackground = false
                                modal.closeBySwipeBackground = false
                                view.OkButtonHandler = {[weak modal] in

                                    let vc = Constants.mainStoryboard.instantiateViewController(withIdentifier: "MainTabbarViewController")as! MainTabbarViewController
                                    self.navigationController?.pushViewController(vc, animated: true)

                                    modal?.closeWithLeansRandom()
                                    return
                                }
                                modal.show(modalView: view, inView: (UIApplication.shared.delegate?.window!)!)
                            }
                            else if (code==401)
                            {
                                if let error_data = result_dict["errorData"] as? [[String:Any]]
                                {
                                    if let error_msg = error_data[0]["message"] as? String
                                    {
                                        let view = CongratulationView.instantiateFromNib()
                                        view.lblTitle.text = error_msg
                                        let modal = PathDynamicModal()
                                        modal.showMagnitude = 200.0
                                        modal.closeMagnitude = 130.0
                                        modal.closeByTapBackground = false
                                        modal.closeBySwipeBackground = false
                                        view.OkButtonHandler = {[weak modal] in
                                            
                                            let storyBoard = UIStoryboard(name: "Main", bundle: nil)
                                            let objHomeNav = storyBoard.instantiateViewController(withIdentifier: "HomeNav") as! UINavigationController
                                            UIApplication.shared.keyWindow?.rootViewController = objHomeNav
                                            
                                            modal?.closeWithLeansRandom()
                                            return
                                        }
                                        modal.show(modalView: view, inView: (UIApplication.shared.delegate?.window!)!)
                                    }
                                }
                                
                                
                            }
                            else
                            {
                                self.view.makeToast("Something Went Wrong!!")
                            }
                        }
                    }
                }
            }
        }
        else
        {
            //self.displayAlert(msg: "Please Check Your Internet Connection!!", title_str: Constants.APP_NAME)
        }
    }
    
    //MARK:- Register User Api
    func Register_user()
    {
        var urlStr = String()
        if(self.loginType == "email")
        {
            urlStr = Constants.BASEURL+MethodName.register
        }
        else
        {
            param_dict["password"] = nil
            urlStr = Constants.BASEURL+MethodName.socialRegister
        }
        
        if(CommonFunction.isInternetAvailable())
        {            
            MBProgressHUD.showAdded(to: self.view, animated: true)
            self.lblStatusapi.text = "Register API Processing"
            Alamofire.request(urlStr, method: .post, parameters: param_dict , encoding: JSONEncoding.default, headers:["Content-Type":"application/json"]).responseJSON
                { response in
                    
                    if let result_dict = response.result.value as? [String:Any]
                    {
                        if let code = result_dict["code"] as? Int
                        {
                            if(code==200)
                            {
                                self.lblStatusapi.text = "Register API Success"
                                if let data_dict = result_dict["data"] as? [String:Any]
                                {
                                    if let auth_key = data_dict["auth_key"] as? String
                                    {
                                        UserDefaults.standard.set(auth_key, forKey: "Access_Token")
                                    }
                                    else
                                    {
                                        UserDefaults.standard.set("xyz", forKey: "Access_Token")
                                    }
                                    
                                    if let user_id = data_dict["userId"] as? Int
                                    {
                                        UserDefaults.standard.set(user_id, forKey: Constants.USERID)
                                    }
                                    else
                                    {
                                        UserDefaults.standard.set(0, forKey: Constants.USERID)
                                    }
                                    
                                    if let quickBlox_dict = data_dict["quickblox_data"] as? [String:Any]
                                    {
                                        let QuickBlData: Data = NSKeyedArchiver.archivedData(withRootObject: quickBlox_dict)
                                        UserDefaults.standard.set(QuickBlData, forKey: "quickBloxData")
                                    }
                                    self.Passport_image_upload()
                                    
                                }
                            }
                            else
                            {
                                self.lblStatusapi.text = "Register API Fail"
                                MBProgressHUD.hide(for: self.view, animated: true)
                                if let error_arr = result_dict["errorData"] as? [[String:Any]]
                                {
                                    if let error_msg = error_arr[0]["message"] as? String
                                    {
                                        self.view.makeToast(error_msg)
                                    }
                                }
                            }
                        }
                    }
            }
        }
        else
        {
            MBProgressHUD.hide(for: self.view, animated: true)
            self.view.makeToast("Please Check Your Internet Connection!!")
        }

    }
    
}

