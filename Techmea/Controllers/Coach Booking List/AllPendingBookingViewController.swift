//
//  AllPendingBookingViewController.swift
//  Techmea
//
//  Created by Dhaval Panchani on 24/10/18.
//  Copyright © 2018 VISHAL SETH. All rights reserved.
//

import UIKit
import Alamofire
import MBProgressHUD
import SDWebImage
import Toast_Swift

//MARK:- AllPendingTableViewCell
class AllPendingTableViewCell : UITableViewCell
{
    
    
    @IBOutlet weak var lblname: UILabel!
    @IBOutlet weak var lblbookingname: UILabel!
    @IBOutlet weak var lbltime: UILabel!
    @IBOutlet weak var lbldate: UILabel!
    @IBOutlet weak var btnRejectBooking: UIButton!
    @IBOutlet weak var btnAcceptBooking: UIButton!
    @IBOutlet weak var viewBackground: UIView!
    @IBOutlet weak var imgprofile: UIImageView!
}

class AllPendingBookingViewController: UIViewController,UITableViewDelegate,UITableViewDataSource {
    
    //MARK:- Outlets
    var arrPendingBookingList = [CoachBookingListModel]()
    @IBOutlet weak var tblAllPendingBooking: UITableView!
    var coach_booking_id = Int()

    @IBOutlet weak var popViewReasonRejectBooking: UIView!
    @IBOutlet weak var popReasonRejectTextView: UITextView!
    @IBOutlet weak var btnpopReasonRejectSubmit: UIButton!
    @IBOutlet weak var btnBackgroundReasonReject: UIButton!
    var refreshControl = UIRefreshControl()

    //MARK:- viewDidLoad
    override func viewDidLoad() {
        super.viewDidLoad()

        print(self.arrPendingBookingList)
        self.tblAllPendingBooking.tableFooterView = UIView()
        self.refreshControl.addTarget(self, action: #selector(NotificationViewController.pullto_reloadData),for: UIControlEvents.valueChanged)
        self.tblAllPendingBooking.addSubview(refreshControl)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK:- Pull To Reload
    @objc func pullto_reloadData()
    {
        self.tblAllPendingBooking.reloadData()
        self.refreshControl.endRefreshing()
    }
    

    //MARK:- TableView Delegate Methods
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return self.arrPendingBookingList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "AllPendingTableViewCell", for: indexPath) as! AllPendingTableViewCell
        self.setShadow(view: cell.viewBackground)
        
        if let coachname = self.arrPendingBookingList[indexPath.row].coach["fullname"] as? String
        {
            cell.lblname.text = coachname
        }
        
        if let username = self.arrPendingBookingList[indexPath.row].userData["fullname"] as? String
        {
            cell.lblbookingname.text = "You got booking from \(username) for 2 hours session"
        }
        
        let start_time = self.arrPendingBookingList[indexPath.row].start_time
        let end_time = self.arrPendingBookingList[indexPath.row].end_time
        
        cell.lbltime.text = "\(start_time)" + "-" + "\(end_time)"
        cell.lbldate.text = self.arrPendingBookingList[indexPath.row].booking_date
        if let profile_img = self.arrPendingBookingList[indexPath.row].coach["user_profile_image"] as? String
        {
            cell.imgprofile.sd_setImage(with: URL(string: profile_img), placeholderImage: UIImage(named: "user"))
        }
        
        cell.btnAcceptBooking.addTarget(self, action: #selector(self.btnBookingAcceptTapped(_:)) , for: .touchUpInside)
        cell.btnRejectBooking.addTarget(self, action: #selector(self.btnBookingRejectTapped(_:)) , for: .touchUpInside)
        
        return cell
    }
    
    //MARK:- Button Booking Accept Action
    @objc func btnBookingAcceptTapped(_ sender:UIButton)
    {
        guard let cell = sender.superview?.superview?.superview?.superview as? AllPendingTableViewCell else
        {
            return // or fatalError() or whatever
        }
        let selected_indexPath = tblAllPendingBooking.indexPath(for: cell)
        let booking_id_str = arrPendingBookingList[(selected_indexPath?.row)!].id
        
        self.AcceptBookingAPI(id_str: "\(booking_id_str)")
        
    }
    
    //MARK:- Button Booking Reject Action
    @objc func btnBookingRejectTapped(_ sender:UIButton)
    {
        guard let cell = sender.superview?.superview?.superview?.superview as? AllPendingTableViewCell else
        {
            return // or fatalError() or whatever
        }
        let selected_indexPath = tblAllPendingBooking.indexPath(for: cell)
        let booking_id_str = arrPendingBookingList[(selected_indexPath?.row)!].id
        
        self.btnBackgroundReasonReject.isHidden = false
        self.popViewReasonRejectBooking.isHidden = false
        
        self.coach_booking_id = booking_id_str
        
    }
    
    //MARK:- Accept booking Request Api
    func AcceptBookingAPI(id_str:String)
    {
        if(CommonFunction.isInternetAvailable())
        {
            MBProgressHUD.showAdded(to: self.view, animated: true)
            Alamofire.request(Constants.BASEURL + MethodName.acceptBooking + "?access-token=\(UserDefaults.standard.value(forKey: "Access_Token") as! String)&id=\(id_str)", method: .post, parameters: nil , encoding: URLEncoding.default, headers: ["Content-Type":"application/json"]).responseJSON { response in
                MBProgressHUD.hide(for: self.view, animated: true)
                if let result_dict = response.result.value as? [String:Any]
                {
                    print(result_dict)
                    if(result_dict["code"] as! Int == 200)
                    {
                        
                    }
                    else
                    {
                        if let error_arr = result_dict["errorData"] as? [[String:Any]]
                        {
                            if let error_msg = error_arr[0]["message"] as? String
                            {
                                self.view.makeToast(error_msg)
                            }
                        }
                    }
                }
            }
        }
        else
        {
             self.view.makeToast("Please Check Your Internet Connection!!")
        }
    }
    
    //MARK:- Reject booking Request Api
    func RejectBookingAPI(id_str:String, reject_txt: String)
    {
        if(CommonFunction.isInternetAvailable())
        {
            let paramString = ["reject_reason" : reject_txt]
            
            MBProgressHUD.showAdded(to: self.view, animated: true)
            Alamofire.request(Constants.BASEURL + MethodName.cancelBooking + "?access-token=\(UserDefaults.standard.value(forKey: "Access_Token") as! String)&id=\(id_str)&mode=reject", method: .post, parameters: paramString , encoding: URLEncoding.default, headers: ["Content-Type":"application/json"]).responseJSON { response in
                MBProgressHUD.hide(for: self.view, animated: true)
                if let result_dict = response.result.value as? [String:Any]
                {
                    print(result_dict)
                    if(result_dict["code"] as! Int == 200)
                    {
                        
                    }
                    else
                    {
                        if let error_arr = result_dict["errorData"] as? [[String:Any]]
                        {
                            if let error_msg = error_arr[0]["message"] as? String
                            {
                                self.view.makeToast(error_msg)
                            }
                        }
                    }
                }
            }
        }
        else
        {
            self.view.makeToast("Please Check Your Internet Connection!!")
        }
    }
    
    //MARK:- Set Shadow
    func setShadow(view: UIView)
    {
        view.layer.borderColor=UIColor.init(red: 249/255, green: 225/255, blue: 233/255, alpha: 1.0).cgColor
        view.layer.borderWidth = 1.0
        view.layer.cornerRadius = 10.0
        view.layer.shadowColor = UIColor(red: 253.0 / 255.0, green: 233.0 / 255.0, blue: 240.0 / 255.0, alpha: 1.0).cgColor
        view.layer.shadowOpacity = 1.0
        view.layer.shadowRadius = 15.0
        view.layer.shadowOffset = CGSize(width: 2.0, height: 2.0)
    }
    
    
    //MARK:- Back Action
    @IBAction func btnBackTapped(_ sender: UIButton) {
        
        self.navigationController?.popViewController(animated: true)
    }
    
    //MARK:- Button Background Reason Reject Action
    @IBAction func btnBackgroundReasonRejectTapped(_ sender: UIButton) {
        
        self.popViewReasonRejectBooking.isHidden = true
        self.btnBackgroundReasonReject.isHidden = true
    }
    
    //MARK:- Button Pop Reason Reject Submit Action
    @IBAction func btnpopReasonRejectSubmitTapped(_ sender: UIButton) {
        
        if popReasonRejectTextView.text != "" && popReasonRejectTextView.text != "Mention Reason To Reject The Booking."
        {
            self.RejectBookingAPI(id_str: "\(self.coach_booking_id)",reject_txt : self.popReasonRejectTextView.text)
        }
        else
        {
            //self.displayAlert(msg: "Please Enter Specific Reason!!", title_str: Constants.APP_NAME)
            self.view.makeToast("Please Enter Specific Reason!!")
        }
    }

}
