//
//  CoachBookingListViewController.swift
//  Techmea
//
//  Created by Dhaval Panchani on 12/10/18.
//  Copyright © 2018 VISHAL SETH. All rights reserved.
//

import UIKit
import Alamofire
import MBProgressHUD
import SDWebImage
import Toast_Swift

//MARK:- PendingTableViewCell
class PendingTableViewCell : UITableViewCell
{
    @IBOutlet weak var imgprofile: UIImageView!
    @IBOutlet weak var lblname: UILabel!
    @IBOutlet weak var lblbookingname: UILabel!
    @IBOutlet weak var lbltime: UILabel!
    @IBOutlet weak var lbldate: UILabel!
    @IBOutlet weak var btnRejectBooking: UIButton!
    @IBOutlet weak var btnAcceptBooking: UIButton!
    @IBOutlet weak var viewBackground: UIView!
    
    
}

//MARK:- CancelTableViewCell
class CancelTableViewCell : UITableViewCell
{
    @IBOutlet weak var imgprofile: UIImageView!
    @IBOutlet weak var lblname: UILabel!
    @IBOutlet weak var lblbookingname: UILabel!
    @IBOutlet weak var lbltime: UILabel!
    @IBOutlet weak var lbldate: UILabel!
    @IBOutlet weak var imgsad: UIImageView!
    @IBOutlet weak var btnReasonCancel: UIButton!
    @IBOutlet weak var viewBackground: UIView!
    
}

//MARK:- UpcomingTableViewCell
class UpcomingTableViewCell : UITableViewCell
{
    @IBOutlet weak var imgprofile: UIImageView!
    @IBOutlet weak var lblname: UILabel!
    @IBOutlet weak var lbltime: UILabel!
    @IBOutlet weak var lbldate: UILabel!
    @IBOutlet weak var lblbottomview: UILabel!
    @IBOutlet weak var btnStartSession: UIButton!
    @IBOutlet weak var viewBackground: UIView!
    
}

class CoachBookingListViewController: UIViewController,UITableViewDelegate,UITableViewDataSource {
    
    //MARK:- Outlets
    @IBOutlet weak var tblUpcomingCoachBooking: UITableView!
    @IBOutlet weak var tblPendingCoachBooking: UITableView!
    @IBOutlet weak var tblCancelCoachBooking: UITableView!
    
    
    let arrSections = ["Upcoming Booking","Pending Booking","Cancel Booking"]
    var arrCoachBookingList = [CoachBookingListModel]()
    var arrPendingList = [CoachBookingListModel]()
    var arrUpcomingList = [CoachBookingListModel]()
    var arrCanceledList = [CoachBookingListModel]()
    var coach_booking_id = Int()
    
    @IBOutlet weak var UpcomingTopView: UIView!
    @IBOutlet weak var PendingTopView: UIView!
    @IBOutlet weak var CancelTopView: UIView!
    
    @IBOutlet weak var UpcomingTableHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var PendingTableHeightConstraing: NSLayoutConstraint!
    @IBOutlet weak var CanceledTableHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var CanceledTableBottomConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var CanceledTopViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var PendingTopViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var UpcomingTopViewHeightConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var popViewReasonRejectBooking: UIView!
    @IBOutlet weak var popReasonRejectTextView: UITextView!
    @IBOutlet weak var btnpopReasonRejectSubmit: UIButton!
    @IBOutlet weak var btnBackgroundReasonReject: UIButton!
    var refreshControl = UIRefreshControl()
    
    @IBOutlet weak var CoachBookingScrollView: UIScrollView!
    
    //MARK:- viewDidLoad
    override func viewDidLoad() {
        super.viewDidLoad()

        self.tblUpcomingCoachBooking.tableFooterView = UIView()
        self.tblPendingCoachBooking.tableFooterView = UIView()
        self.tblCancelCoachBooking.tableFooterView = UIView()
        //self.tblCoachBooking.sectionHeaderHeight = 40
        self.refreshControl.addTarget(self, action: #selector(NotificationViewController.pullto_reloadData),for: UIControlEvents.valueChanged)
        self.CoachBookingScrollView.addSubview(refreshControl)
        
        self.getCoachBookingList()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
//    func numberOfSections(in tableView: UITableView) -> Int
//    {
//        return arrSections.count
//    }
    
//    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
//
//        let viewtop = UIView(frame: CGRect(x: 0, y: 0, width: tableView.frame.size.width, height: 40))
//        viewtop.backgroundColor = UIColor.white
//        let lbltitle = UILabel(frame: CGRect(x: 10, y: 0, width: 200, height: viewtop.frame.size.height))
//        lbltitle.text = arrSections[section]
//        lbltitle.textColor = UIColor.black
//        lbltitle.font = UIFont.systemFont(ofSize: 15.0)
//        viewtop.addSubview(lbltitle)
//
//        let viewallbtn = UIButton(frame: CGRect(x: tableView.frame.size.width - 50, y: 0, width: 50, height: viewtop.frame.size.height))
//        let attrs = [
//            NSAttributedStringKey.font : UIFont.systemFont(ofSize: 12.0),
//            NSAttributedStringKey.foregroundColor : UIColor.darkGray,
//            NSAttributedStringKey.underlineStyle : 1] as [NSAttributedStringKey : Any]
//        let attributedString = NSMutableAttributedString(string:"")
//        let buttonTitleStr = NSMutableAttributedString(string:"View All", attributes:attrs)
//        attributedString.append(buttonTitleStr)
//        viewallbtn.setAttributedTitle(attributedString, for: .normal)
//        viewallbtn.tag = section
//        viewallbtn.addTarget(self, action: #selector(self.btnviewAllTapped(_:)), for: .touchUpInside )
//        viewtop.addSubview(viewallbtn)
//
//        return viewtop
//    }
    
//    @objc func btnviewAllTapped(_ sender:UIButton)
//    {
//        if sender.tag == 0
//        {
//            let vc = Constants.mainStoryboard.instantiateViewController(withIdentifier: "AllUpcomingBookingListViewController") as! AllUpcomingBookingListViewController
//            vc.arrUpcomingBookingList = self.arrUpcomingList
//            self.navigationController?.pushViewController(vc, animated: true)
//        }
//        else if sender.tag == 1
//        {
//            let vc = Constants.mainStoryboard.instantiateViewController(withIdentifier: "AllPendingBookingViewController") as! AllPendingBookingViewController
//            vc.arrPendingBookingList = self.arrPendingList
//            self.navigationController?.pushViewController(vc, animated: true)
//        }
//        else
//        {
//            let vc = Constants.mainStoryboard.instantiateViewController(withIdentifier: "AllCanceledBookingViewController") as! AllCanceledBookingViewController
//            vc.arrCanceledBookingList = self.arrCanceledList
//            self.navigationController?.pushViewController(vc, animated: true)
//        }
//
//    }
    
//    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
//        return 40.0
//    }

    
    //MARK:- TableView Delegate Methods
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
                
        if tableView == tblUpcomingCoachBooking
        {
                if self.arrUpcomingList.count > 1
                {
                    return 2
                }
                else
                {
                    return self.arrUpcomingList.count
                }
        }
        else if tableView == tblPendingCoachBooking
        {
                if self.arrPendingList.count > 1
                {
                    return 2
                }
                else
                {
                    return self.arrPendingList.count
                }
        }
        else
        {
                if self.arrCanceledList.count > 1
                {
                    return 2
                }
                else
                {
                    return self.arrCanceledList.count
                }
        }
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        
        
        if tableView == tblUpcomingCoachBooking
        {
                let cell = tableView.dequeueReusableCell(withIdentifier: "UpcomingTableViewCell", for: indexPath) as! UpcomingTableViewCell
                self.setShadow(view: cell.viewBackground)
                
                if let username = self.arrUpcomingList[indexPath.row].coach["fullname"] as? String
                {
                    cell.lblname.text = username
                }
                let start_time = self.arrUpcomingList[indexPath.row].start_time
                let end_time = self.arrUpcomingList[indexPath.row].end_time
                
                cell.lbltime.text = "\(start_time)" + "-" + "\(end_time)"
                cell.lbldate.text = self.arrUpcomingList[indexPath.row].booking_date
                if let profile_img = self.arrUpcomingList[indexPath.row].coach["user_profile_image"] as? String
                {
                   cell.imgprofile.sd_setImage(with: URL(string: profile_img), placeholderImage: UIImage(named: "user"))
                }
                
                let totalamount = String(format: "%.2f", self.arrUpcomingList[indexPath.row].total_amount)
                let timeformatter = DateFormatter()
                timeformatter.dateFormat = "HH:mm:ss"
                
                let time1 = timeformatter.date(from: start_time)
                let time2 = timeformatter.date(from: end_time)
                
                //You can directly use from here if you have two dates
                
                let interval = time2?.timeIntervalSince(time1!)
                let hour = String(format: "%.2f", interval! / 3600)
                cell.lblbottomview.text = "$\(totalamount) Amount Paid for \(hour) Hours Session "
            
                cell.btnStartSession.addTarget(self, action: #selector(self.btnStartSessionTapped(_:)) , for: .touchUpInside)
                return cell
            
            
        }
        else if tableView == tblPendingCoachBooking
        {
                let cell = tableView.dequeueReusableCell(withIdentifier: "PendingTableViewCell", for: indexPath) as! PendingTableViewCell
                self.setShadow(view: cell.viewBackground)
                
                if let coachname = self.arrPendingList[indexPath.row].coach["fullname"] as? String
                {
                    cell.lblname.text = coachname
                }
                
                if let username = self.arrPendingList[indexPath.row].userData["fullname"] as? String
                {
                    cell.lblbookingname.text = "You got booking from \(username) for 2 hours session"
                }
                
                let start_time = self.arrPendingList[indexPath.row].start_time
                let end_time = self.arrPendingList[indexPath.row].end_time
                
                cell.lbltime.text = "\(start_time)" + "-" + "\(end_time)"
                cell.lbldate.text = self.arrPendingList[indexPath.row].booking_date
                if let profile_img = self.arrPendingList[indexPath.row].coach["user_profile_image"] as? String
                {
                    cell.imgprofile.sd_setImage(with: URL(string: profile_img), placeholderImage: UIImage(named: "user"))
                }
            
                cell.btnAcceptBooking.addTarget(self, action: #selector(self.btnBookingAcceptTapped(_:)) , for: .touchUpInside)
                cell.btnRejectBooking.addTarget(self, action: #selector(self.btnBookingRejectTapped(_:)) , for: .touchUpInside)
                return cell
    
                
        }
        else
        {
    
                let cell = tableView.dequeueReusableCell(withIdentifier: "CancelTableViewCell", for: indexPath) as! CancelTableViewCell
                self.setShadow(view: cell.viewBackground)
            
                return cell
    
           
        }
    
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 140.0
    }
    
    
    //MARK:- Set Shadow
    func setShadow(view: UIView)
    {
        view.layer.borderColor=UIColor.init(red: 249/255, green: 225/255, blue: 233/255, alpha: 1.0).cgColor
        view.layer.borderWidth = 1.0
        view.layer.cornerRadius = 10.0
        view.layer.shadowColor = UIColor(red: 253.0 / 255.0, green: 233.0 / 255.0, blue: 240.0 / 255.0, alpha: 1.0).cgColor
        view.layer.shadowOpacity = 1.0
        view.layer.shadowRadius = 15.0
        view.layer.shadowOffset = CGSize(width: 2.0, height: 2.0)
    }
    
    //MARK:- Pull To Reload
    @objc func pullto_reloadData()
    {
        self.getCoachBookingList()
    }
    
    
    //MARK:- Coach Booking List Api
    func getCoachBookingList()
    {
        if(CommonFunction.isInternetAvailable())
        {
            MBProgressHUD.showAdded(to: self.view, animated: true)
            Alamofire.request(Constants.BASEURL + MethodName.getCoachBookingList + "?access-token=\(UserDefaults.standard.value(forKey: "Access_Token") as! String)&expand=coach,userData,members,members.userData", method: .get, parameters: nil , encoding: URLEncoding.default, headers: ["Content-Type":"application/json"]).responseJSON { response in
                MBProgressHUD.hide(for: self.view, animated: true)
                if let result_dict = response.result.value as? [String:Any]
                {
                    print(result_dict)
                    self.arrCoachBookingList.removeAll()
                    self.arrCanceledList.removeAll()
                    self.arrUpcomingList.removeAll()
                    self.arrPendingList.removeAll()
                    if(result_dict["code"] as! Int == 200)
                    {
                        if let coachBookingList = result_dict["data"] as? NSArray
                        {
                            for dict in coachBookingList
                            {
                                let tempDict = dict as! NSDictionary
                                self.arrCoachBookingList.append(CoachBookingListModel.init(dict: tempDict))
                            }
                            for i in 0..<self.arrCoachBookingList.count
                            {
                                let status_str = self.arrCoachBookingList[i].status
                                
                                if status_str == 0
                                {
                                    self.arrPendingList.append(self.arrCoachBookingList[i])
                                }
                                else if status_str == 1
                                {
                                    self.arrUpcomingList.append(self.arrCoachBookingList[i])
                                }
                                else if status_str == 3
                                {
                                    self.arrCanceledList.append(self.arrCoachBookingList[i])
                                }
                            }
                            
                            if self.arrUpcomingList.count > 1
                            {
                                self.UpcomingTableHeightConstraint.constant = 290
                                self.UpcomingTopViewHeightConstraint.constant = 40
                                self.UpcomingTopView.isHidden = false
                            }
                            else if self.arrUpcomingList.count > 0
                            {
                                self.UpcomingTableHeightConstraint.constant = 140
                                self.UpcomingTopViewHeightConstraint.constant = 40
                                self.UpcomingTopView.isHidden = false
                            }
                            else
                            {
                                self.UpcomingTableHeightConstraint.constant = 0
                                self.UpcomingTopViewHeightConstraint.constant = 0
                                self.UpcomingTopView.isHidden = true
                            }
                            
                            if self.arrPendingList.count > 1
                            {
                                self.PendingTableHeightConstraing.constant = 290
                                self.PendingTopViewHeightConstraint.constant = 40
                                self.PendingTopView.isHidden = false
                            }
                            else if self.arrPendingList.count > 0
                            {
                                self.PendingTableHeightConstraing.constant = 140
                                self.PendingTopViewHeightConstraint.constant = 40
                                self.PendingTopView.isHidden = false
                            }
                            else
                            {
                                self.PendingTableHeightConstraing.constant = 0
                                self.PendingTopViewHeightConstraint.constant = 0
                                self.PendingTopView.isHidden = true
                            }
                            
                            if self.arrCanceledList.count > 1
                            {
                                self.CanceledTableHeightConstraint.constant = 290
                                self.CanceledTopViewHeightConstraint.constant = 40
                                self.CancelTopView.isHidden = false
                            }
                            else if self.arrCanceledList.count > 0
                            {
                                self.CanceledTableHeightConstraint.constant = 140
                                self.CanceledTopViewHeightConstraint.constant = 40
                                self.CancelTopView.isHidden = false
                            }
                            else
                            {
                                self.CanceledTableHeightConstraint.constant = 0
                                self.CanceledTopViewHeightConstraint.constant = 0
                                self.CancelTopView.isHidden = true
                            }
                            
                            self.tblUpcomingCoachBooking.reloadData()
                            self.tblPendingCoachBooking.reloadData()
                            self.tblCancelCoachBooking.reloadData()
                            self.refreshControl.endRefreshing()
                        }
                    }
                    else
                    {
                        if let error_arr = result_dict["errorData"] as? [[String:Any]]
                        {
                            if let error_msg = error_arr[0]["message"] as? String
                            {
                                self.view.makeToast(error_msg)
                            }
                        }
                    }
                }
            }
        }
        else
        {
            self.view.makeToast("Please Check Your Internet Connection!!")
        }
    }
    
    
    //MARK:- Button UpcomingAll Tapped
    @IBAction func btnUpcomingAllTapped(_ sender: UIButton) {
        
        let vc = Constants.mainStoryboard.instantiateViewController(withIdentifier: "AllUpcomingBookingListViewController") as! AllUpcomingBookingListViewController
        vc.arrUpcomingBookingList = self.arrUpcomingList
        self.navigationController?.pushViewController(vc, animated: true)
        
       
        
    }
    
    //MARK:- Button PendingAll Tapped
    @IBAction func btnPendingAllTapped(_ sender: UIButton) {
        
        let vc = Constants.mainStoryboard.instantiateViewController(withIdentifier: "AllPendingBookingViewController") as! AllPendingBookingViewController
        vc.arrPendingBookingList = self.arrPendingList
        self.navigationController?.pushViewController(vc, animated: true)
        
        
    }
    
    //MARK:- Button CancelAllTapped
    @IBAction func btnCancelAllTapped(_ sender: UIButton) {
        
        let vc = Constants.mainStoryboard.instantiateViewController(withIdentifier: "AllCanceledBookingViewController") as! AllCanceledBookingViewController
        vc.arrCanceledBookingList = self.arrCanceledList
        self.navigationController?.pushViewController(vc, animated: true)
       
    }
    
    
    //MARK:- Back Action
    @IBAction func btnBackTapped(_ sender: UIButton) {
        
        self.navigationController?.popViewController(animated: true)
    }
    
    
    //MARK:- Button Booking Accept Action
    @objc func btnBookingAcceptTapped(_ sender:UIButton)
    {
        guard let cell = sender.superview?.superview?.superview?.superview as? PendingTableViewCell else
        {
            return // or fatalError() or whatever
        }
        let selected_indexPath = tblPendingCoachBooking.indexPath(for: cell)
        let booking_id_str = arrPendingList[(selected_indexPath?.row)!].id
        
        self.AcceptBookingAPI(id_str: "\(booking_id_str)")
        
    }
    
    //MARK:- Button Booking Reject Action
    @objc func btnBookingRejectTapped(_ sender:UIButton)
    {
        guard let cell = sender.superview?.superview?.superview?.superview as? PendingTableViewCell else
        {
            return // or fatalError() or whatever
        }
        let selected_indexPath = tblPendingCoachBooking.indexPath(for: cell)
        let booking_id_str = arrPendingList[(selected_indexPath?.row)!].id
        
        self.btnBackgroundReasonReject.isHidden = false
        self.popViewReasonRejectBooking.isHidden = false
        
        self.coach_booking_id = booking_id_str
        
    }
    
    //MARK:- Button Start Session Action
    @objc func btnStartSessionTapped(_ sender:UIButton)
    {
//        guard let cell = sender.superview?.superview?.superview?.superview as? UpcomingTableViewCell else
//        {
//            return // or fatalError() or whatever
//        }
        //let selected_indexPath = tblUpcomingCoachBooking.indexPath(for: cell)
        
        let objLive = Constants.mainStoryboard.instantiateViewController(withIdentifier: "LiveStreamViewController") as! LiveStreamViewController
        self.navigationController?.pushViewController(objLive, animated: true)
    }
    
    //MARK:- Button Accept Booking Api
    func AcceptBookingAPI(id_str:String)
    {
        if(CommonFunction.isInternetAvailable())
        {
            MBProgressHUD.showAdded(to: self.view, animated: true)
            Alamofire.request(Constants.BASEURL + MethodName.acceptBooking + "?access-token=\(UserDefaults.standard.value(forKey: "Access_Token") as! String)&id=\(id_str)", method: .post, parameters: nil , encoding: URLEncoding.default, headers: ["Content-Type":"application/json"]).responseJSON { response in
                MBProgressHUD.hide(for: self.view, animated: true)
                if let result_dict = response.result.value as? [String:Any]
                {
                    print(result_dict)
                    if(result_dict["code"] as! Int == 200)
                    {
                        
                    }
                    else
                    {
                        if let error_arr = result_dict["errorData"] as? [[String:Any]]
                        {
                            if let error_msg = error_arr[0]["message"] as? String
                            {
                                self.view.makeToast(error_msg)
                            }
                        }
                    }
                }
            }
        }
        else
        {
            self.view.makeToast("Please Check Your Internet Connection!!")
        }
    }
    
    //MARK:- Button Reject Booking Api
    func RejectBookingAPI(id_str:String, reject_txt: String)
    {
        if(CommonFunction.isInternetAvailable())
        {
            let paramString = ["reject_reason" : reject_txt]
            MBProgressHUD.showAdded(to: self.view, animated: true)
            Alamofire.request(Constants.BASEURL + MethodName.cancelBooking + "?access-token=\(UserDefaults.standard.value(forKey: "Access_Token") as! String)&id=\(id_str)&mode=reject", method: .post, parameters: paramString , encoding: JSONEncoding.default, headers: ["Content-Type":"application/json"]).responseJSON { response in
                MBProgressHUD.hide(for: self.view, animated: true)
                if let result_dict = response.result.value as? [String:Any]
                {
                    print(result_dict)
                    if(result_dict["code"] as! Int == 200)
                    {
                        self.displayAlert(msg: (result_dict["data"] as! [String:Any])["message"] as! String, title_str: Constants.APP_NAME)
                        self.popViewReasonRejectBooking.isHidden = true
                        self.btnBackgroundReasonReject.isHidden = true
                    }
                    else
                    {
                        if let error_arr = result_dict["errorData"] as? [[String:Any]]
                        {
                            if let error_msg = error_arr[0]["message"] as? String
                            {
                                self.view.makeToast(error_msg)
                            }
                        }
                    }
                }
            }
        }
        else
        {
            self.view.makeToast("Please Check Your Internet Connection!!")
        }
    }
    
    //MARK:- Button Background Reason Reject Action
    @IBAction func btnBackgroundReasonRejectTapped(_ sender: UIButton) {
        
        self.popViewReasonRejectBooking.isHidden = true
        self.btnBackgroundReasonReject.isHidden = true
    }
    
    //MARK:- Button Pop Reason Reject Action
    @IBAction func btnpopReasonRejectSubmitTapped(_ sender: UIButton) {
        
        if popReasonRejectTextView.text != "" && popReasonRejectTextView.text != "Mention Reason To Reject The Booking."
        {
            self.RejectBookingAPI(id_str: "\(self.coach_booking_id)",reject_txt : self.popReasonRejectTextView.text)
        }
        else
        {
            self.view.makeToast("Please Enter Specific Reason!!")
        }
    }
}
