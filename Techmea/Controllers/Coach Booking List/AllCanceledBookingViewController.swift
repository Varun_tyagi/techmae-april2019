//
//  AllCanceledBookingViewController.swift
//  Techmea
//
//  Created by Dhaval Panchani on 24/10/18.
//  Copyright © 2018 VISHAL SETH. All rights reserved.
//

import UIKit

//MARK:- AllCancelTableViewCell
class AllCancelTableViewCell : UITableViewCell
{
    @IBOutlet weak var imgprofile: UIImageView!
    
    @IBOutlet weak var lblbookingname: UILabel!
    @IBOutlet weak var lbltime: UILabel!
    @IBOutlet weak var lbldate: UILabel!
    @IBOutlet weak var imgsad: UIImageView!
    @IBOutlet weak var btnReasonCancel: UIButton!
    @IBOutlet weak var viewBackground: UIView!
    @IBOutlet weak var lblname: UILabel!
}

class AllCanceledBookingViewController: UIViewController,UITableViewDelegate,UITableViewDataSource {
    
    //MARK:- Outlets
    var arrCanceledBookingList = [CoachBookingListModel]()
    @IBOutlet weak var tblAllCanceledBooking: UITableView!
    var refreshControl = UIRefreshControl()

    
    //MARK:- viewDidLoad
    override func viewDidLoad() {
        super.viewDidLoad()

        print(self.arrCanceledBookingList)
        self.tblAllCanceledBooking.tableFooterView = UIView()
        self.refreshControl.addTarget(self, action: #selector(NotificationViewController.pullto_reloadData),for: UIControlEvents.valueChanged)
        self.tblAllCanceledBooking.addSubview(refreshControl)
    }
    
    //MARK:- Pull To Reload
    @objc func pullto_reloadData()
    {
        self.tblAllCanceledBooking.reloadData()
        self.refreshControl.endRefreshing()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    //MARK:- TableView Delegate Methods
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return self.arrCanceledBookingList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "AllCancelTableViewCell", for: indexPath) as! AllCancelTableViewCell
        self.setShadow(view: cell.viewBackground)
        
        return cell
    }
    
    //MARK:- Set Shadow
    func setShadow(view: UIView)
    {
        view.layer.borderColor=UIColor.init(red: 249/255, green: 225/255, blue: 233/255, alpha: 1.0).cgColor
        view.layer.borderWidth = 1.0
        view.layer.cornerRadius = 10.0
        view.layer.shadowColor = UIColor(red: 253.0 / 255.0, green: 233.0 / 255.0, blue: 240.0 / 255.0, alpha: 1.0).cgColor
        view.layer.shadowOpacity = 1.0
        view.layer.shadowRadius = 15.0
        view.layer.shadowOffset = CGSize(width: 2.0, height: 2.0)
    }
    
    //MARK:- Back Action
    @IBAction func btnBackTapped(_ sender: UIButton) {
        
        self.navigationController?.popViewController(animated: true)
    }
}
