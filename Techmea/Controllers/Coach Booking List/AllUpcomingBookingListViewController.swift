//
//  AllUpcomingBookingListViewController.swift
//  Techmea
//
//  Created by Dhaval Panchani on 23/10/18.
//  Copyright © 2018 VISHAL SETH. All rights reserved.
//

import UIKit

//MARK:- AllUpcomingTableViewCell
class AllUpcomingTableViewCell : UITableViewCell
{
    @IBOutlet weak var viewBackground: UIView!
    @IBOutlet weak var imgprofile: UIImageView!
    @IBOutlet weak var lblname: UILabel!
    @IBOutlet weak var lbltime: UILabel!
    @IBOutlet weak var lbldate: UILabel!
    @IBOutlet weak var lblbottomview: UILabel!
    @IBOutlet weak var btnStartSession: UIButton!
    
}

class AllUpcomingBookingListViewController: UIViewController,UITableViewDelegate,UITableViewDataSource {
    
    
    //MARK:- Outlets
    var arrUpcomingBookingList = [CoachBookingListModel]()
    @IBOutlet weak var tblAllUpcomingBooking: UITableView!
    var refreshControl = UIRefreshControl()

    
    //MARK:- viewDidLoad
    override func viewDidLoad() {
        super.viewDidLoad()

        print(self.arrUpcomingBookingList)
        self.tblAllUpcomingBooking.tableFooterView = UIView()
        self.refreshControl.addTarget(self, action: #selector(NotificationViewController.pullto_reloadData),for: UIControlEvents.valueChanged)
        self.tblAllUpcomingBooking.addSubview(refreshControl)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK:- Pull To Reload
    @objc func pullto_reloadData()
    {
        self.tblAllUpcomingBooking.reloadData()
        self.refreshControl.endRefreshing()
    }
    
    
    //MARK:-  TableView Delegate Methods
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return self.arrUpcomingBookingList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "AllUpcomingTableViewCell", for: indexPath) as! AllUpcomingTableViewCell
        self.setShadow(view: cell.viewBackground)
        
        if let username = self.arrUpcomingBookingList[indexPath.row].coach["fullname"] as? String
        {
            cell.lblname.text = username
        }
        let start_time = self.arrUpcomingBookingList[indexPath.row].start_time
        let end_time = self.arrUpcomingBookingList[indexPath.row].end_time
        
        cell.lbltime.text = "\(start_time)" + "-" + "\(end_time)"
        cell.lbldate.text = self.arrUpcomingBookingList[indexPath.row].booking_date
        if let profile_img = self.arrUpcomingBookingList[indexPath.row].coach["user_profile_image"] as? String
        {
            cell.imgprofile.sd_setImage(with: URL(string: profile_img), placeholderImage: UIImage(named: "user"))
        }
        
        let totalamount = String(format: "%.2f", self.arrUpcomingBookingList[indexPath.row].total_amount)
        let timeformatter = DateFormatter()
        timeformatter.dateFormat = "HH:mm:ss"
        
        let time1 = timeformatter.date(from: start_time)
        let time2 = timeformatter.date(from: end_time)
        
        //You can directly use from here if you have two dates
        
        let interval = time2?.timeIntervalSince(time1!)
        let hour = String(format: "%.2f", interval! / 3600)
        cell.lblbottomview.text = "$\(totalamount) Amount Paid for \(hour) Hours Session "
        return cell
    }

    //MARK:-  Set Shadow
    func setShadow(view: UIView)
    {
        view.layer.borderColor=UIColor.init(red: 249/255, green: 225/255, blue: 233/255, alpha: 1.0).cgColor
        view.layer.borderWidth = 1.0
        view.layer.cornerRadius = 10.0
        view.layer.shadowColor = UIColor(red: 253.0 / 255.0, green: 233.0 / 255.0, blue: 240.0 / 255.0, alpha: 1.0).cgColor
        view.layer.shadowOpacity = 1.0
        view.layer.shadowRadius = 15.0
        view.layer.shadowOffset = CGSize(width: 2.0, height: 2.0)
    }
    
    //MARK:-  Back Action
    @IBAction func btnBackTapped(_ sender: UIButton) {
        
        self.navigationController?.popViewController(animated: true)
    }
    
   
}
