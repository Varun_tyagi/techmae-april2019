//
//  UserBookingListViewController.swift
//  Techmea
//
//  Created by Dhaval Panchani on 17/10/18.
//  Copyright © 2018 VISHAL SETH. All rights reserved.
//

import UIKit
import MBProgressHUD
import Alamofire
import Toast_Swift


//MARK:- BookingListCell
class BookingListCell : UITableViewCell
{
    @IBOutlet weak var mainView: UIView!
    @IBOutlet weak var imgBooking: UIImageView!
    @IBOutlet weak var lblusername: UILabel!
    @IBOutlet weak var lblmessage: UILabel!
    @IBOutlet weak var btnDetail: UIButton!
}

//MARK:- BookingRejectCell
class BookingListRejectCell : UITableViewCell
{    
    @IBOutlet weak var mainView: UIView!
    @IBOutlet weak var lblmessage: UILabel!
    @IBOutlet weak var btnDetail: UIButton!
    @IBOutlet weak var lblusername: UILabel!
    @IBOutlet weak var btnRebook: UIButton!
}
class UserBookingListViewController: UIViewController,UITableViewDelegate,UITableViewDataSource {
    
    //MARK:- Outlets

    @IBOutlet weak var imgUserProfile: UIImageView!
    @IBOutlet weak var tblBookingList: UITableView!
    @IBOutlet weak var lblBookingCount: UILabel!
    @IBOutlet weak var viewMainBackground: UIView!
    @IBOutlet weak var viewDetail: UIView!
    @IBOutlet weak var txtReason: UITextView!
    var arrUserBookingList = [UserBookingListModel]()
    
    
    //MARK:- viewDidLoad
    override func viewDidLoad() {
        super.viewDidLoad()

        CommonFunction.addshadow_view(view: self.viewMainBackground, shadow_radius: 2.0, shadow_color: UIColor.darkGray, shadow_opecity: 0.5, shadow_offset: CGSize(width: 0.0, height: 2.0), is_clipBound: false, corner_radius: 10.0)
        callUserBookingListAPI()
        
        tblBookingList.estimatedRowHeight = 120
        tblBookingList.rowHeight = UITableViewAutomaticDimension
        
        if let data = UserDefaults.standard.object(forKey: "userData") as? Data
        {
            let oD1: NSDictionary? = NSKeyedUnarchiver.unarchiveObject(with: data) as? NSDictionary
            
            if let imageurlstring = oD1?.value(forKey: "user_profile_image") as? String
            {
                imgUserProfile.sd_setImage(with: URL(string: imageurlstring), placeholderImage: UIImage(named: "userPlaceHolder"))
            }
            
            
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    //MARK:- TableView Delegate Methods
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.arrUserBookingList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let bookingstatus = arrUserBookingList[indexPath.row].status
        
        if bookingstatus == 1
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "BookingListCell", for: indexPath) as! BookingListCell
            
            if let userimg = arrUserBookingList[indexPath.row].userData["user_profile_image"] as? String
            {
                cell.imgBooking.sd_setImage(with: URL(string: userimg), placeholderImage: UIImage(named: "user") )
            }
            if let username = arrUserBookingList[indexPath.row].userData["fullname"] as? String
            {
                cell.lblusername.text = username
            }
            
            if let coachusername = arrUserBookingList[indexPath.row].coach["fullname"] as? String
            {
                cell.lblmessage.text = "Coach \(coachusername) has accepted your booking"
            }
            
            CommonFunction.addshadow_view(view: cell.mainView, shadow_radius: 5.0, shadow_color: UIColor.lightGray, shadow_opecity: 0.5, shadow_offset: CGSize(width: 0.0, height: 3.0), is_clipBound: false, corner_radius: 10.0)
            
            return cell
        }
        else
        {
        
            let cell = tableView.dequeueReusableCell(withIdentifier: "BookingListRejectCell", for: indexPath) as! BookingListRejectCell
            
            if let username = arrUserBookingList[indexPath.row].userData["fullname"] as? String
            {
                cell.lblusername.text = username
            }
            
            if let coachusername = arrUserBookingList[indexPath.row].coach["fullname"] as? String
            {
                cell.lblmessage.text = "Coach \(coachusername) has rejected your booking"
            }
            
            CommonFunction.addshadow_view(view: cell.mainView, shadow_radius: 5.0, shadow_color: UIColor.lightGray, shadow_opecity: 0.5, shadow_offset: CGSize(width: 0.0, height: 3.0), is_clipBound: false, corner_radius: 10.0)
            
            cell.btnDetail.addTarget(self, action: #selector(self.btnRejectDetailTapped(_:)) , for: .touchUpInside)
            cell.btnRebook.addTarget(self, action: #selector(self.btnRebookTapped(_:)) , for: .touchUpInside)
            
            return cell
        }
    }
    
    
    //MARK:- Button Reject Reason Detail Action
    @objc func btnRejectDetailTapped(_ sender:UIButton)
    {
        guard let cell = sender.superview?.superview?.superview as? BookingListRejectCell else
        {
            return // or fatalError() or whatever
        }
        let selected_indexPath = tblBookingList.indexPath(for: cell)
        let reason_str = arrUserBookingList[(selected_indexPath?.row)!].reject_reason
        
        txtReason.text = reason_str
        self.viewDetail.isHidden = false
    }
    
    //MARK:- Button Rebook Action
    @objc func btnRebookTapped(_ sender:UIButton)
    {
        guard let cell = sender.superview?.superview?.superview as? BookingListRejectCell else
        {
            return // or fatalError() or whatever
        }
        let selected_indexPath = tblBookingList.indexPath(for: cell)
        if let coach_user_id = arrUserBookingList[(selected_indexPath?.row)!].coach["user_id"] as? Int
        {
            let profile_page = Constants.mainStoryboard.instantiateViewController(withIdentifier: "FriendListViewController") as! FriendListViewController
            profile_page.user_id_str = "\(coach_user_id)"
            self.navigationController?.pushViewController(profile_page, animated: true)
        }
        
    }
    

    //MARK:- Back Action
    @IBAction func btnBackTapped(_ sender: UIButton) {
        
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func btnOkTapped(_ sender: UIButton) {
        
        self.viewDetail.isHidden = true
    }
    
    //MARK:- User Booking List Api
    func callUserBookingListAPI()
    {
        if(CommonFunction.isInternetAvailable())
        {
            MBProgressHUD.showAdded(to: self.view, animated: true)
            Alamofire.request(Constants.BASEURL + MethodName.getUserBookingList + "?access-token=\(UserDefaults.standard.value(forKey: "Access_Token") as! String)&expand=userData,coach", method: .get, parameters: nil , encoding: URLEncoding.default, headers: ["Content-Type":"application/json"]).responseJSON { response in
                MBProgressHUD.hide(for: self.view, animated: true)
                if let result_dict = response.result.value as? [String:Any]
                {
                    print(result_dict)
                    self.arrUserBookingList.removeAll()
                    if(result_dict["code"] as! Int == 200)
                    {
                        if let userBookingList = result_dict["data"] as? NSArray
                        {
                            for dict in userBookingList
                            {
                                let tempDict = dict as! NSDictionary
                                self.arrUserBookingList.append(UserBookingListModel.init(dict: tempDict))
                            }
                            self.lblBookingCount.text = "This Month(\(self.arrUserBookingList.count))"
                            self.tblBookingList.reloadData()
                        }
                    }
                    else
                    {
                        if let error_arr = result_dict["errorData"] as? [[String:Any]]
                        {
                            if let error_msg = error_arr[0]["message"] as? String
                            {
                                self.view.makeToast(error_msg)

                            }
                        }
                    }
                }
            }
        }
        else
        {
            self.view.makeToast("Please Check Your Internet Connection!!")
        }
    }
    
    @IBAction func btnHideDetailAction(_ sender: UIButton) {
        viewDetail.isHidden = true
    }
}
