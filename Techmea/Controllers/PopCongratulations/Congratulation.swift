//
//  Congratulation.swift
//  Techmea
//
//  Created by Dhaval Panchani on 17/01/19.
//  Copyright © 2019 VISHAL SETH. All rights reserved.
//

import UIKit

class CongratulationView: UIView {

    var OkButtonHandler: (() -> Void)?
    @IBOutlet weak var btnDone: UIButton!
    @IBOutlet weak var viewMain: UIView!
    @IBOutlet weak var lblTitle: UILabel!
        
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.viewMain.layer.cornerRadius = 15.0
        
    }
    
    
    class func instantiateFromNib() -> CongratulationView {
        let view = UINib(nibName: "CongratulationView", bundle: nil).instantiate(withOwner: nil, options: nil).first as! CongratulationView
        
        return view
    }
    @IBAction func btnDoneTapped(_ sender: UIButton) {
        
         self.OkButtonHandler?()
    }
}
