//
//  LiveStreamingSubscribersViewController.swift
//  Techmea
//
//  Created by Dhaval Panchani on 20/10/18.
//  Copyright © 2018 VISHAL SETH. All rights reserved.
//

import UIKit
import R5Streaming


enum AccessError: Error {
    case error(message: String)
}

extension AccessError: LocalizedError {
    public var errorDescription: String? {
        switch self {
        case .error:
            return NSLocalizedString("Unable to fetch stream for action subscribe.", comment: "Access Error")
        }
    }
}

class LiveStreamingSubscribersViewController: UIViewController,R5StreamDelegate {

    func onR5StreamStatus(_ stream: R5Stream!, withStatus statusCode: Int32, withMessage msg: String!) {
        NSLog("Status: %s ", r5_string_for_status(statusCode))
        let s =  String(format: "Status: %s (%@)",  r5_string_for_status(statusCode), msg)
        print(s)
        
        if (Int(statusCode) == Int(r5_status_disconnected.rawValue)) {
           // self.removeFromParentViewController()
        }
    
        if msg == "NetStream.Play.UnpublishNotify"
        {
            
            let alertController = UIAlertController(title: Constants.APP_NAME, message: "Live streaming has been stopped", preferredStyle: .alert)
            let okAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.default) {
                UIAlertAction in
                
                self.subscribeStream?.stop()
                self.subscribeStream?.delegate = nil
                self.navigationController?.popViewController(animated: true)
            }
            alertController.addAction(okAction)
            DispatchQueue.main.async {
            self.present(alertController, animated: true, completion: nil)
            }
        }
        
    }
    
    var shouldClose : Bool = true
    var currentView : R5VideoViewController? = nil
    var publishStream : R5Stream? = nil
    var subscribeStream : R5Stream? = nil
    
    var current_rotation = 0;
    @IBOutlet var test_view: UIView!
    @IBOutlet weak var userProfile_imgview: UIImageView!
    
    @IBOutlet weak var lbl_like: UILabel!
    @IBOutlet weak var lbl_username: UILabel!
    
    @IBOutlet weak var lbl_stream_comment_count: UILabel!
    
    @IBOutlet weak var lbl_stream_like_count: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        AVAudioSession.sharedInstance().requestRecordPermission { (gotPerm: Bool) -> Void in
            
        };
        
        r5_set_log_level((Int32)(r5_log_level_debug.rawValue))
        
        self.view.autoresizesSubviews = false
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    override func viewWillAppear(_ animated: Bool) {
        self.tabBarController?.tabBar.isHidden = true
    }
    override func viewWillDisappear(_ animated: Bool) {
        self.tabBarController?.tabBar.isHidden = false
    }

    override func viewDidAppear(_ animated: Bool) {
        
        setupDefaultR5VideoViewController()
        
        let config = getConfig()
        // Set up the connection and stream
        let connection = R5Connection(config: config)
        self.subscribeStream = R5Stream(connection: connection)
        self.subscribeStream!.delegate = self
        self.subscribeStream?.client = self
        
        currentView?.attach(subscribeStream)
        
        
        self.subscribeStream!.play("stream1")
    }
    
    func getConfig()->R5Configuration{
        // Set up the configuration
        let config = R5Configuration()
        config.host = "18.224.246.119"
        config.port = 8554
        config.contextName = "live"
        config.`protocol` = 1;
        config.buffer_time = 0.5
        config.licenseKey = "RZGX-3C4A-7YR5-V2UH"
        return config
    }
    
    func closeTest(){
        
        NSLog("closing view")
        
        if( self.publishStream != nil ){
            self.publishStream!.stop()
        }
        
        if( self.subscribeStream != nil ){
            self.subscribeStream!.stop()
        }
        
        // Moved to status disconnect, due to publisher emptying queue buffer on bad connections.
        //        self.removeFromParentViewController()
    }
    
    override func viewDidLayoutSubviews() {
        
        super.viewDidLayoutSubviews()
        
        if(currentView != nil){
            
            currentView?.setFrame(view.frame);
        }
        
    }
    
    
    func setupDefaultR5VideoViewController() -> R5VideoViewController{
        
        let r5View : R5VideoViewController = getNewR5VideoViewController(rect: self.view.frame);
        self.addChildViewController(r5View);
        
        
        view.addSubview(r5View.view)
        
        r5View.setFrame(self.view.bounds)
        
        r5View.showPreview(true)
        
        //r5View.showDebugInfo(true)
        
        currentView = r5View;
        test_view.frame = self.view.frame
        DispatchQueue.main.asyncAfter(deadline: .now() + 1.0) {
            self.currentView?.view.addSubview(self.test_view)
        }
        return currentView!
    }
    
    func setupPublisher(connection: R5Connection){
        
        self.publishStream = R5Stream(connection: connection)
        self.publishStream!.delegate = self
        
        
            // Attach the video from camera to stream
        let videoDevice = AVCaptureDevice.devices(for: AVMediaType.video).last as? AVCaptureDevice
            
            let camera = R5Camera(device: videoDevice, andBitRate: 750)
            
            camera?.width = 640
            camera?.height = 360
            camera?.orientation = 90
            self.publishStream!.attachVideo(camera)
        
        
            // Attach the audio from microphone to stream
            let audioDevice = AVCaptureDevice.default(for: AVMediaType.audio)
            let microphone = R5Microphone(device: audioDevice)
            microphone?.bitrate = 32
            microphone?.device = audioDevice;
            NSLog("Got device %@", String(describing: audioDevice?.localizedName))
            self.publishStream!.attachAudio(microphone)
        
       
        
    }
    
    
    func updateOrientation(value: Int) {
        
        if current_rotation == value {
            return
        }
        
        current_rotation = value
        currentView?.view.layer.transform = CATransform3DMakeRotation(CGFloat(value), 0.0, 0.0, 0.0);
        
    }
    
    func onMetaData(data : String) {
        
        let props = data.characters.split(separator: ";").map(String.init)
        props.forEach { (value: String) in
            let kv = value.characters.split(separator: "=").map(String.init)
            if (kv[0] == "orientation") {
                updateOrientation(value: Int(kv[1])!)
            }
        }
        
    }
    
    func getNewR5VideoViewController(rect : CGRect) -> R5VideoViewController{
        
        let view : UIView = UIView(frame: rect)
        
        let r5View : R5VideoViewController = R5VideoViewController();
        r5View.view = view;
        
        return r5View;
    }
    
   override var shouldAutorotate:Bool {
        get {
            return true
        }
    }
    
    override var supportedInterfaceOrientations: UIInterfaceOrientationMask {
        get {
            return [UIInterfaceOrientationMask.all]
        }
    }
    @IBAction func close_press(_ sender: UIButton)
    {
        publishStream?.stop()
        self.navigationController?.popViewController(animated: true)
    }
}
