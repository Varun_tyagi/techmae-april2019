//
//  LiveStreamViewController.swift
//  Techmea
//
//  Created by Dhaval Panchani on 16/10/18.
//  Copyright © 2018 VISHAL SETH. All rights reserved.
//

import UIKit
import R5Streaming

class LiveStreamViewController: UIViewController,R5StreamDelegate {

    //MARK:- Outlets
    @IBOutlet var test_view: UIView!
    var currentView : R5VideoViewController? = nil
    var publishStream : R5Stream? = nil
    
    @IBOutlet weak var userProfile_imgview: UIImageView!
    
    @IBOutlet weak var lbl_like: UILabel!
    @IBOutlet weak var lbl_username: UILabel!
    
    @IBOutlet weak var lbl_stream_comment_count: UILabel!
    
    @IBOutlet weak var lbl_stream_like_count: UILabel!
    
    //MARK:- viewDidLoad
    override func viewDidLoad()
    {
        super.viewDidLoad()
    }
    
    //MARK:- viewWillAppear
    override func viewWillAppear(_ animated: Bool) {
        self.tabBarController?.tabBar.isHidden = true
    }
    
    //MARK:- viewWillDisappear
    override func viewWillDisappear(_ animated: Bool) {
        self.tabBarController?.tabBar.isHidden = false
    }
    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
    }
    
    func onR5StreamStatus(_ stream: R5Stream!, withStatus statusCode: Int32, withMessage msg: String!) {
        NSLog("Status: %s ", r5_string_for_status(statusCode))
    }
    
    func getConfig()->R5Configuration{
        // Set up the configuration
        let config = R5Configuration()
        config.host = "18.224.246.119"
        config.port = 8554
        config.contextName = "live"
        config.`protocol` = 1;
        config.buffer_time = 0.5
        config.licenseKey = "RZGX-3C4A-7YR5-V2UH"
        return config
    }
    
    
    //MARK:- viewDidAppear
    override func viewDidAppear(_ animated: Bool) {
        
        AVAudioSession.sharedInstance().requestRecordPermission { (gotPerm: Bool) -> Void in
            
        };
        
        setupDefaultR5VideoViewController()
        
        
        // Set up the configuration
        let config = getConfig()
        // Set up the connection and stream
        let connection = R5Connection(config: config)
        
        setupPublisher(connection!)
        // show preview and debug info
        // self.publishStream?.getVideoSource().fps = 2;
        self.currentView!.attach(publishStream!)
        
        self.publishStream!.publish("stream1", type: R5RecordTypeLive)
    }
    
    func setupDefaultR5VideoViewController() -> R5VideoViewController{
        
        let r5View : R5VideoViewController = getNewR5VideoViewController(self.view.frame);
        self.addChildViewController(r5View);
        
        view.addSubview(r5View.view)
        r5View.setFrame(self.view.bounds)
        
        r5View.showPreview(true)
        
        //r5View.showDebugInfo(true)
        
        currentView = r5View;
        test_view.frame = self.view.frame
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
            self.currentView?.view.addSubview(self.test_view)
        }
        return currentView!
    }
    
    func getNewR5VideoViewController(_ rect : CGRect) -> R5VideoViewController{
        
        let view : UIView = UIView(frame: rect)
        
        let r5View : R5VideoViewController = R5VideoViewController();
        r5View.view = view;
        
        return r5View;
    }
    
    func setupPublisher(_ connection: R5Connection){
        
        self.publishStream = R5Stream(connection: connection)
        self.publishStream!.delegate = self
        
        // Attach the video from camera to stream
        let videoDevice = AVCaptureDevice.default(.builtInWideAngleCamera, for: AVMediaType.video, position: .front)
        
        let camera = R5Camera(device: videoDevice, andBitRate: 750)
        camera?.width = 640
        camera?.height = 360
        camera?.orientation = 90
        self.publishStream!.attachVideo(camera)
        
        // Attach the audio from microphone to stream
        let audioDevice = AVCaptureDevice.default(for: AVMediaType.audio)
        let microphone = R5Microphone(device: audioDevice)
        microphone?.bitrate = 32
        microphone?.device = audioDevice;
        NSLog("Got device %@", audioDevice!)
        self.publishStream!.attachAudio(microphone)
    }

    @IBAction func test_press(_ sender: UIButton) {
        print("test success")
    }
    @IBAction func close_press(_ sender: UIButton)
    {
        publishStream?.stop()
        self.navigationController?.popViewController(animated: true)
    }
}
