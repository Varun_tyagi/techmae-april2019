//
//  SearchFriendViewController.swift
//  Techmea
//
//  Created by Dhaval Panchani on 05/09/18.
//  Copyright © 2018 VISHAL SETH. All rights reserved.
//

import UIKit
import Alamofire
import MBProgressHUD
import Quickblox
import QMServices
import Toast_Swift

import GoogleMobileAds

class SearchFriendViewController: UIViewController,UITableViewDataSource,UITableViewDelegate,UITextFieldDelegate,GADBannerViewDelegate 
{
    
    //MARK:- Outlets
    
    @IBOutlet weak var viewBannerView: DFPBannerView!
    @IBOutlet weak var lbl_nodata: UILabel!
    @IBOutlet weak var secrchTextfeild: UITextField!
    @IBOutlet weak var topNavigation_view: UIView!
    @IBOutlet weak var friendListTbl: UITableView!
    var friendListArr = [UserModel]()
    var is_from_group = Bool()
    var selected_btn = UIButton()
    var image_str = String()
    var current_index = Int()
    var selected_view = UIView()
    var profile_user_id = Int()
    
    //MARK:- viewDidLoad
    override func viewDidLoad()
    {
        super.viewDidLoad()
        friendListTbl.tableFooterView = UIView()
        super.viewDidLoad()
        secrchTextfeild.becomeFirstResponder()
        friendListTbl.tableFooterView = UIView()
        secrchTextfeild.attributedPlaceholder = NSAttributedString(string: "Search Expert or Friend",
                                                                   attributes: [NSAttributedStringKey.foregroundColor: UIColor.white])
        self.friendListTbl.estimatedRowHeight = 123
        self.friendListTbl.rowHeight = UITableViewAutomaticDimension
        
        
        //viewBannerView = DFPBannerView(adSize: kGADAdSizeBanner)
        viewBannerView.adUnitID = Constants.kGOOGLE_AD_ID
        
        viewBannerView.rootViewController = self
        viewBannerView.load(DFPRequest())
    }
    
    func addBannerViewToView(_ bannerView: UIView) {
        bannerView.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(bannerView)
        view.addConstraints(
            [NSLayoutConstraint(item: bannerView,
                                attribute: .bottom,
                                relatedBy: .equal,
                                toItem: bottomLayoutGuide,
                                attribute: .top,
                                multiplier: 1,
                                constant: 0),
             NSLayoutConstraint(item: bannerView,
                                attribute: .centerX,
                                relatedBy: .equal,
                                toItem: view,
                                attribute: .centerX,
                                multiplier: 1,
                                constant: 0)
            ])
    }
    
    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        self.navigationController?.isNavigationBarHidden = true
        self.tabBarController?.tabBar.isHidden = true
        self.tabBarController?.tabBar.frame.size.height = 0
    }
    
    //MARK:- textFieldShouldReturn
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        searchFriends()
        return false
    }

    
    //MARK:- TableView Delegate Methods
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return friendListArr.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell = tableView.dequeueReusableCell(withIdentifier: "FriendListTableViewCell") as! FriendListTableViewCell
        CommonFunction.addshadow_view(view: cell.container_view, shadow_radius: 15.0, shadow_color: UIColor.lightGray, shadow_opecity: 0.5, shadow_offset: CGSize(width: 2.0, height: 2.0), is_clipBound: false, corner_radius: 10.0)
        let user_model = friendListArr[indexPath.row]
        cell.user_namelbl.text = user_model.userName
        self.add_tap_gester_lbl(cell.user_namelbl)
        cell.country_namelbl.text = "\(user_model.city) , \(user_model.country)"
        cell.proffesion_namelbl.text = user_model.aboutme.decodeEmoji
        if(user_model.is_friend == 1)
        {
            cell.addFriend_btn.setBackgroundImage(UIImage(named: "accepticon"), for: .normal)
        }
        else if(user_model.is_friend == 2)
        {
            cell.addFriend_btn.setBackgroundImage(UIImage(named: "pendingicon"), for: .normal)
        }
        else
        {
            cell.addFriend_btn.setBackgroundImage(UIImage(named: "addFriend"), for: .normal)
        }
        if(user_model.is_like == true)
        {
            cell.like_btn.setBackgroundImage(UIImage(named: "likeSelect"), for: .normal)
        }
        else
        {
            cell.like_btn.setBackgroundImage(UIImage(named: "likeUnselect"), for: .normal)
        }
//        if(user_model.is_coach == true)
//        {
//            cell.coach_imageview.isHidden = false
//            cell.coachview_height_constraint.constant = 20
//
//        }
//        else
//        {
            cell.coach_imageview.isHidden = true
            cell.coachview_height_constraint.constant = 0
            
      //  }
        if(user_model.hourly_rate != nil)
        {
            cell.hourlyrate_lbl.text = "Hourly Rate : \(user_model.hourly_rate!)" 
        }
        
        cell.user_profile_imageview.sd_setImage(with: URL(string:user_model.userImage), placeholderImage: UIImage(named: "userPlaceHolder"))
        self.add_tap_gester(cell.user_profile_imageview)
        
        if (user_model.friend_request_id != nil)
        {
            if(user_model.friend_request_id == Int(user_model.userId))
            {
                cell.view_AcceptCancel.isHidden = false
            }
            else
            {
                cell.view_AcceptCancel.isHidden = true
            }
        }
        else
        {
            cell.view_AcceptCancel.isHidden = true
        }
        
        cell.addFriend_btn.addTarget(self, action: #selector(btnAddfriendAction(_:)), for: .touchUpInside)
        cell.message_btn.addTarget(self, action: #selector(btnMessageAction(_:)), for: .touchUpInside)
        cell.like_btn.addTarget(self, action: #selector(btnLikeAction(_:)), for: .touchUpInside)
        cell.btnCancel.addTarget(self, action: #selector(btnCancelAction(_:)), for: .touchUpInside)
        cell.bntAccept.addTarget(self, action: #selector(btnAcceptAction(_:)), for: .touchUpInside)
        cell.hireme_btn.addTarget(self, action: #selector(btnHireme_action(_:)), for: .touchUpInside)
        cell.contentView.layoutIfNeeded()
        return cell
    }
    
    /// Tells the delegate an ad request failed.
    func adView(_ bannerView: GADBannerView,
                didFailToReceiveAdWithError error: GADRequestError) {
        print("adView:didFailToReceiveAdWithError: \(error.localizedDescription)")
    }
    
    /// Tells the delegate that a full-screen view will be presented in response
    /// to the user clicking on an ad.
    func adViewWillPresentScreen(_ bannerView: GADBannerView) {
        print("adViewWillPresentScreen")
    }
    
    /// Tells the delegate that the full-screen view will be dismissed.
    func adViewWillDismissScreen(_ bannerView: GADBannerView) {
        print("adViewWillDismissScreen")
    }
    
    /// Tells the delegate that the full-screen view has been dismissed.
    func adViewDidDismissScreen(_ bannerView: GADBannerView) {
        print("adViewDidDismissScreen")
    }
    
    /// Tells the delegate that a user click will open another app (such as
    /// the App Store), backgrounding the current app.
    func adViewWillLeaveApplication(_ bannerView: GADBannerView) {
        print("adViewWillLeaveApplication")
    }
    
    func adViewDidReceiveAd(_ bannerView: GADBannerView) {
        bannerView.alpha = 0
        UIView.animate(withDuration: 1, animations: {
            bannerView.alpha = 1
        })
    }
    
    //MARK:- Button Hireme Action
    @objc func btnHireme_action(_ sender: UIButton)
    {
        guard let cell = sender.superview?.superview?.superview?.superview as? FriendListTableViewCell else
        {
            return // or fatalError() or whatever
        }
        let selected_indexPath = friendListTbl.indexPath(for: cell)
        self.open_profile(user_id: "\(self.friendListArr[(selected_indexPath?.row)!].userId)")
    }
    
    //MARK:- Gestures
    func add_tap_gester_lbl(_ lbl : UILabel)
    {
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.tapFunction))
        lbl.isUserInteractionEnabled = true
        lbl.addGestureRecognizer(tap)
    }
    func add_tap_gester(_ imgview : UIImageView)
    {
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.tapFunction))
        imgview.isUserInteractionEnabled = true
        imgview.addGestureRecognizer(tap)
    }
    @objc func tapFunction(sender:UITapGestureRecognizer)
    {
        guard let cell = sender.view?.superview?.superview?.superview as? FriendListTableViewCell else
        {
            return
        }
        let selected_indexPath = friendListTbl.indexPath(for: cell)
        self.open_profile(user_id: "\(self.friendListArr[(selected_indexPath?.row)!].userId)")
    }
    
    //MARK:- Open Profile
    func open_profile(user_id:String)
    {
        let profile_page = Constants.mainStoryboard.instantiateViewController(withIdentifier: "FriendListViewController") as! FriendListViewController
        profile_page.user_id_str = user_id
        self.navigationController?.pushViewController(profile_page, animated: true)
    }
    
    //MARK:-  Button Add Friend Action
    @objc func btnAddfriendAction(_ sender: UIButton)
    {
        guard let cell = sender.superview?.superview?.superview as? FriendListTableViewCell else
        {
            return // or fatalError() or whatever
        }
        let selected_indexPath = friendListTbl.indexPath(for: cell)
        current_index = (selected_indexPath?.row)!
        let user_model = friendListArr[current_index]
        selected_btn = sender
        if(sender.backgroundImage(for: .normal) == UIImage(named: "addFriend"))
        {
            //sender.setBackgroundImage(UIImage(named: "pendingicon"), for: .normal)
            callAddfriendApi(user_id_str: "\(user_model.userId)")
        }
        else
        {
            var title_str = String()
            var msg_str = String()
            if(sender.backgroundImage(for: .normal) == UIImage(named: "pending"))
            {
                
                image_str = "pending cancel"
                title_str = "Pending Request Cancel"
                msg_str = "Are you sure you want to cancel friend request?"
            }
            else
            {
                image_str = "unfriend"
                title_str = "Remove Friend"
                msg_str = "Are you sure you want to Unfriend?"
            }
            let view = ModalView.instantiateFromNib()
            view.updateModal(title_str, descMessage: msg_str, firstTitle: "YES", secondTitle: "NO", type: .modalAlert)
            let modal = PathDynamicModal()
            modal.showMagnitude = 200.0
            modal.closeMagnitude = 130.0
            modal.closeByTapBackground = false
            modal.closeBySwipeBackground = false
            view.Cancel2ButtonHandler = {[weak modal] in
                modal?.closeWithLeansRandom()
                return
            }
            view.OkButtonHandler = {[weak modal] in
                
                if(sender.backgroundImage(for: .normal) == UIImage(named: "pending"))
                {
                    //sender.setBackgroundImage(UIImage(named: "addFriend"), for: .normal)
                    self.callCancelReqApi(user_id_str: "\(user_model.userId)")
                }
                else
                {
                    self.callUnfrienApi(user_id_str: "\(user_model.userId)")
                }
                modal?.closeWithLeansRandom()
                return
            }
            modal.show(modalView: view, inView: (UIApplication.shared.delegate?.window!)!)
            
        }
        
    }
    
    //MARK:- Button Message Action
    @objc func btnMessageAction(_ sender: UIButton)
    {
        
        
        guard let cell = sender.superview?.superview?.superview as? FriendListTableViewCell else
        {
            return // or fatalError() or whatever
        }
        let selected_indexPath = friendListTbl.indexPath(for: cell)
        let user_model = friendListArr[(selected_indexPath?.row)!]
        let quickblox_data_dic = user_model.quickblox_data
        
        //Occupant
        var occupantID = Int()
        var occupantName = String()
        
        
        //Occupant Data
        if let occupantID_num = quickblox_data_dic["id"] as? Int
        {
            occupantID = occupantID_num
        }
        
        if let occupantName_str = quickblox_data_dic["full_name"] as? String
        {
            occupantName = occupantName_str
        }
        
        let chatDialog = QBChatDialog(dialogID: nil, type: QBChatDialogType.private)
        chatDialog.name = occupantName
        chatDialog.occupantIDs = [occupantID] as [NSNumber]
       // chatDialog.data = ["class_name": "CustomData","custom_userId" : "\(UserDefaults.standard.value(forKey: Constants.USERID) as! Int),\(self.profile_user_id)"]
        chatDialog.data = ["class_name": "CustomData","custom_userId" : "\(UserDefaults.standard.value(forKey: Constants.USERID) as! Int),\(user_model.userId)"]

        
        QBRequest.createDialog(chatDialog, successBlock: { (response: QBResponse?, createdDialog : QBChatDialog?) -> Void in
            
            self.getDialogs(dialog : createdDialog!)
            
        }) { (response : QBResponse!) -> Void in
            
            print("Error while Creating Dialog!!")
            self.view.makeToast("Error while Creating Dialog!!")
        }
        
        
    }
    
    func getDialogs(dialog : QBChatDialog) {
        
        //        if let lastActivityDate = ServicesManager.instance().lastActivityDate {
        //
        //            ServicesManager.instance().chatService.fetchDialogsUpdated(from: lastActivityDate as Date, andPageLimit: kDialogsPageLimit, iterationBlock: { (response, dialogObjects, dialogsUsersIDs, stop) -> Void in
        //
        //                }, completionBlock: { (response) -> Void in
        //
        //                    if (response.isSuccess) {
        //
        //                        ServicesManager.instance().lastActivityDate = NSDate()
        //                    }
        //            })
        //        }
        //        else {
        
        
        ServicesManager.instance().chatService.allDialogs(withPageLimit: kDialogsPageLimit, extendedRequest: nil, iterationBlock: { (response: QBResponse?, dialogObjects: [QBChatDialog]?, dialogsUsersIDS: Set<NSNumber>?, stop: UnsafeMutablePointer<ObjCBool>) -> Void in
            
        }, completion: { (response: QBResponse?) -> Void in
            
            guard response != nil && response!.isSuccess else {
                return
            }
            
            let chatVC = Constants.mainStoryboard.instantiateViewController(withIdentifier: "ChatViewController") as! ChatViewController
            chatVC.dialog = dialog
            self.navigationController?.pushViewController(chatVC, animated: true)
            
            ServicesManager.instance().lastActivityDate = NSDate()
        })
        //}
    }
    
    //MARK:- Button Like Action
    @objc func btnLikeAction(_ sender: UIButton)
    {
        guard let cell = sender.superview?.superview?.superview as? FriendListTableViewCell else
        {
            return // or fatalError() or whatever
        }
        let selected_indexPath = friendListTbl.indexPath(for: cell)
        let user_model = friendListArr[(selected_indexPath?.row)!]
        current_index = (selected_indexPath?.row)!
        selected_btn = sender
        if(sender.backgroundImage(for: .normal) == UIImage(named: "likeUnselect"))
        {
            image_str = "like"
        }
        else
        {
            image_str = "unlike"
        }
         calllikeApi(user_id_str: "\(user_model.userId)")
    }
    
    //MARK:- Button Accept Request Action
    @objc func btnAcceptAction(_ sender: UIButton)
    {
        guard let cell = sender.superview?.superview?.superview?.superview as? FriendListTableViewCell else
        {
            return // or fatalError() or whatever
        }
        let selected_indexPath = friendListTbl.indexPath(for: cell)
        let user_model = friendListArr[(selected_indexPath?.row)!]
        current_index = (selected_indexPath?.row)!
        selected_view = cell.view_AcceptCancel
        cell.view_AcceptCancel.isHidden = true
        cell.addFriend_btn.setImage(UIImage(named: "accepticon"), for: .normal)
        self.callacceptFriendRequest(friend_request_id: "\(user_model.userId)")
        
    }
    
    //MARK:- Button Cancel Action
    @objc func btnCancelAction(_ sender: UIButton)
    {
        
    }
    
    
    //MARK:- Back Action
    @IBAction func btnBackAction(_ sender: UIButton)
    {
        self.navigationController?.popViewController(animated: false)
    }
    
    //MARK:- Search Action
    @IBAction func btnsearchAction(_ sender: UIButton)
    {
        searchFriends()
    }
    
    
    //MARK:- Search Friend Api
    func searchFriends()
    {
        self.view.endEditing(true)
        if(secrchTextfeild.text?.isEmpty)!
        {
            
        }
        else
        {
            if(CommonFunction.isInternetAvailable())
            {
                let params = ["searchKey":secrchTextfeild.text!]
                MBProgressHUD.showAdded(to: self.view, animated: true)
                
                Alamofire.request(Constants.BASEURL + MethodName.search + "?access-token=\(UserDefaults.standard.value(forKey: "Access_Token") as! String)", method: .get, parameters: params, encoding: URLEncoding.default, headers: ["Content-Type":"application/json"]).responseJSON { response in
                    
                    MBProgressHUD.hide(for: self.view, animated: true)
                    
                    
                    if let result_dict = response.result.value as? [String:Any]
                    {
                        print(result_dict)
                        if(result_dict["code"] as! Int == 200)
                        {
                            self.friendListArr.removeAll()
                            if let user_arr = result_dict ["data"] as? [[String:Any]]
                            {
                                for i in 0..<user_arr.count
                                {
                                    let user_model = UserModel()
                                    if let user_dict = user_arr[i] as? [String:Any]
                                    {
                                        if let about_str = user_dict["aboutme"] as? String
                                        {
                                            user_model.aboutme = about_str
                                        }
                                        if let name_str = user_dict["fullname"] as? String
                                        {
                                            user_model.userName = name_str
                                        }
                                        if let img_str = user_dict["user_profile_image"] as? String
                                        {
                                            user_model.userImage = img_str
                                        }
                                        if let city_str = user_dict["city"] as? String
                                        {
                                            user_model.city = city_str
                                        }
                                        if let country_str = user_dict["country"] as? String
                                        {
                                            user_model.country = country_str
                                        }
                                        
                                        if let userid_str = user_dict["user_id"] as? Int
                                        {
                                            user_model.userId = "\(userid_str)"
                                            self.profile_user_id = userid_str
                                        }
                                        if let friend_request_id = user_dict["friend_request_id"] as? Int
                                        {
                                            user_model.friend_request_id = friend_request_id
                                        }
                                        if let is_coach = user_dict["is_coach"] as? Bool
                                        {
                                            user_model.is_coach = is_coach
                                        }
                                        if let is_like = user_dict["is_like"] as? Bool
                                        {
                                            user_model.is_like = is_like
                                        }
                                        if let is_friend = user_dict["is_friend"] as? Int
                                        {
                                            user_model.is_friend = is_friend
                                        }
                                        if let hourly_rate = user_dict["hourly_rate"] as? Int
                                        {
                                            user_model.hourly_rate = hourly_rate
                                        }
                                        
                                        if let quickblox_data_dict = user_dict["quickblox_data"] as? [String:Any]
                                        {
                                            user_model.quickblox_data = quickblox_data_dict
                                        }
                                        self.friendListArr.append(user_model)
                                    }
                                }
                                
                            }
                            self.ReloadTable()
                        }
                        else
                        {
                            if let error_arr = result_dict["errorData"] as? [[String:Any]]
                            {
                                if let error_msg = error_arr[0]["message"] as? String
                                {
                                    self.view.makeToast(error_msg)
                                }
                            }
                        }
                    }
                }
            }
        }
    }
    
    
    //MARK:- UnFriend Api
    func callUnfrienApi(user_id_str : String)
    {
        if(CommonFunction.isInternetAvailable())
        {
            print(Constants.BASEURL + MethodName.Unfriend + "?access-token=\(UserDefaults.standard.value(forKey: "Access_Token") as! String)&id=\(user_id_str)")
            Alamofire.request(Constants.BASEURL + MethodName.Unfriend + "?access-token=\(UserDefaults.standard.value(forKey: "Access_Token") as! String)&id=\(user_id_str)" , method: .post, parameters: nil, encoding: JSONEncoding.default, headers: ["Content-Type":"application/json"]).responseJSON { response in
                
                if let result_dict = response.result.value as? [String:Any]
                {
                    if let code  = result_dict["code"] as? Int
                    {
                        if(code==200)
                        {
                            self.selected_btn.setBackgroundImage(UIImage(named: "addFriend"), for: .normal)
                            let user_model = self.friendListArr[self.current_index]
                            user_model.is_friend = 0
                            self.friendListArr[self.current_index] = user_model
                            self.friendListTbl.reloadData()
                            self.view.makeToast("Unfriend Successfully")
                        }
                        else
                        {
                            if let error_arr = result_dict["errorData"] as? [[String:Any]]
                            {
                                if let error_msg = error_arr[0]["message"] as? String
                                {
                                    self.view.makeToast(error_msg)
                                }
                            }
                        }
                    }
                }
            }
        }
        else
        {
             self.view.makeToast("Please Check Your Internet Connection!!")
        }
    }
    
    
    //MARK:- Cancel Request Api
    func callCancelReqApi(user_id_str : String)
    {
        if(CommonFunction.isInternetAvailable())
        {
            Alamofire.request(Constants.BASEURL + MethodName.cancelFriendRequest + "?access-token=\(UserDefaults.standard.value(forKey: "Access_Token") as! String)&id=\(user_id_str)" , method: .post, parameters: nil, encoding: URLEncoding.default, headers: ["Content-Type":"application/json"]).responseJSON { response in
                
                if let result_dict = response.result.value as? [String:Any]
                {
                    print(result_dict)
                    if let code = result_dict["code"] as? Int
                    {
                        if(code==200)
                        {
                            self.selected_btn.setBackgroundImage(UIImage(named: "addFriend"), for: .normal)
                            let user_model = self.friendListArr[self.current_index]
                            user_model.is_friend = 0
                            self.friendListArr[self.current_index] = user_model
                            self.friendListTbl.reloadData()
                            self.view.makeToast("Cancel request successfully")
                            
                        }
                        else
                        {
                            if let error_arr = result_dict["errorData"] as? [[String:Any]]
                            {
                                if let error_msg = error_arr[0]["message"] as? String
                                {
                                    self.view.makeToast(error_msg)
                                }
                            }
                        }
                    }
                }
                
            }
        }
        else
        {
            self.view.makeToast("Please Check Your Internet Connection!!")
        }
    }

    //MARK:- Add Friend Api
    func callAddfriendApi(user_id_str : String)
    {
        if(CommonFunction.isInternetAvailable())
        {
            let params = ["friend_user_id":user_id_str,
                          "user_id":"\(UserDefaults.standard.value(forKey: Constants.USERID) as! Int)"]
            let header_dict = ["Token":"31528198109743225ff9d0cf04d1fdd1",
                               "user_id":"\(UserDefaults.standard.value(forKey: Constants.USERID) as! Int)"]
            
            Alamofire.request(Constants.BASEURL + MethodName.addFriendRequest + "?access-token=\(UserDefaults.standard.value(forKey: "Access_Token") as! String)" , method: .post, parameters: params, encoding: URLEncoding.default, headers: header_dict).responseJSON { response in
                
                if let result_dict = response.result.value as? [String:Any]
                {
                    print(result_dict)
                    if(result_dict["code"] as! Int == 200)
                    {
                        self.selected_btn.setBackgroundImage(UIImage(named: "pendingicon"), for: .normal)
                        let user_model = self.friendListArr[self.current_index]
                        user_model.is_friend = 2
                        self.friendListArr[self.current_index] = user_model
                        self.view.makeToast("Friend Request sent Successfully")
                        self.ReloadTable()
                    }
                    else
                    {
                        if let error_arr = result_dict["errorData"] as? [[String:Any]]
                        {
                            if let error_msg = error_arr[0]["message"] as? String
                            {
                                self.view.makeToast(error_msg)
                            }
                        }
                    }
                }
            }
        }
        else
        {
            self.view.makeToast("Please Check Your Internet Connection!!")
        }
    }
    
    //MARK:- Like Frind Api
    func calllikeApi(user_id_str : String)
    {
        if(CommonFunction.isInternetAvailable())
        {
            let params = ["friend_id":user_id_str]
            Alamofire.request(Constants.BASEURL + MethodName.friendLike + "?access-token=\(UserDefaults.standard.value(forKey: "Access_Token") as! String)" , method: .post, parameters: params, encoding: URLEncoding.default, headers: nil).responseJSON { response in
                
                if let result_dict = response.result.value as? [String:Any]
                {
                    if(result_dict["code"] as! Int == 200)
                    {
                        if(self.image_str == "like")
                        {
                            self.selected_btn.setBackgroundImage(UIImage(named: "likeSelect"), for: .normal)
                            let user_model = self.friendListArr[self.current_index]
                            user_model.is_like = true
                            self.friendListArr[self.current_index] = user_model
                        }
                        else
                        {
                            self.selected_btn.setBackgroundImage(UIImage(named: "likeSelect"), for: .normal)
                            let user_model = self.friendListArr[self.current_index]
                            user_model.is_like = false
                            self.friendListArr[self.current_index] = user_model
                        }
                        self.ReloadTable()
                    }
                    else
                    {
                        if let error_arr = result_dict["errorData"] as? [[String:Any]]
                        {
                            if let error_msg = error_arr[0]["message"] as? String
                            {
                                self.view.makeToast(error_msg)
                            }
                        }
                    }
                }
            }
        }
        else
        {
            self.view.makeToast("Please Check Your Internet Connection!!")
        }
    }
    
    //MARK:- Accept Friend Request Api
    func callacceptFriendRequest(friend_request_id : String)
    {
        
        if(CommonFunction.isInternetAvailable())
        {
        let params = ["friend_user_id":friend_request_id, "status" : "0"]
        
        let headerString = ["Content-Type":"application/json"]
        MBProgressHUD.showAdded(to: self.view, animated: true)
        Alamofire.request(Constants.BASEURL+MethodName.acceptFriendRequest  + "?access-token=\(UserDefaults.standard.value(forKey: "Access_Token") as! String)", method: .post, parameters: params, encoding: JSONEncoding.default, headers: headerString).responseJSON { response in
            MBProgressHUD.hide(for: self.view, animated: true)
            if let result_dict = response.result.value as? [String:Any]
            {
                if let code = result_dict["code"] as? Int
                {
                    if(code==200)
                    {
                        self.selected_view.isHidden = true
                        let user_model = self.friendListArr[self.current_index]
                        user_model.is_friend = 0
                        user_model.friend_request_id = nil
                        self.friendListArr[self.current_index] = user_model
                        self.friendListTbl.reloadData()
                        
                        if let data_dict = result_dict["data"] as? [String:Any]
                        {
                            if let msg = data_dict["message"] as? String
                            {
                                self.view.makeToast("Friend Request Accepted")
                            }
                        }
                    }
                    else
                    {
                        self.selected_view.isHidden = false
                        if let error_arr = result_dict["errorData"] as? [[String:Any]]
                        {
                            if let error_msg = error_arr[0]["message"] as? String
                            {
                                self.view.makeToast(error_msg)
                            }
                        }
                    }
                }
            }
        }
        }
        else
        {
            self.view.makeToast("Please Check Your Internet Connection!!")
        }
    }
    
    //MARK:- Reload Table
    func ReloadTable()
    {
        self.friendListTbl.reloadData()
        if(friendListArr.count>0)
        {
            lbl_nodata.isHidden = true
        }
        else
        {
            lbl_nodata.isHidden = false
        }
    }
}

extension UIViewController
{
    
    func displayAlert(msg : String, title_str : String)
    {
        DispatchQueue.main.async {
        let alert = UIAlertController(title: title_str, message: msg, preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
        self.present(alert, animated: true, completion: nil)
        }
    }
}
