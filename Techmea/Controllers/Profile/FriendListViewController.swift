//
//  FriendListViewController.swift
//  Techmae
//
//  Created by varun tyagi on 22/08/18.
//  Copyright © 2018 VISHAL SETH. All rights reserved.
//

import UIKit
import MBProgressHUD
import SDWebImage
import Alamofire
import AVKit
import AVFoundation
import Photos
import Quickblox
import QMServices
import Toast_Swift
import SwiftyDrop
import ImageSlideshow
import SwiftLinkPreview
import SKPhotoBrowser
import ReadMoreTextView
import SafariServices


//MARK:- Profile, Photo, Video Cell
class profile_Photo_cell : UICollectionViewCell
{
    
    @IBOutlet weak var photo_imageview: UIImageView!
}

class profile_Video_cell : UICollectionViewCell
{
    
    @IBOutlet weak var photo_imageview: UIImageView!
    @IBOutlet weak var btnPlay: UIImageView!
}

//MARK:- Friend LikeList Cell
class likelist_cell : UICollectionViewCell
{
    @IBOutlet weak var like_user_imageview: UIImageView!
    
    @IBOutlet weak var lbl_friendName: UILabel!
    @IBOutlet weak var img_container: UIView!
    
}

//MARK:- Friend List Cell
class FriendListTableViewCell : UITableViewCell
{
    
    
    @IBOutlet weak var coachview_height_constraint: NSLayoutConstraint!
    @IBOutlet weak var btnCancel: UIButton!
    @IBOutlet weak var bntAccept: UIButton!
    @IBOutlet weak var view_AcceptCancel: UIView!
    @IBOutlet weak var coach_imageview: UIImageView!
    @IBOutlet weak var user_profile_imageview: UIImageView!
    @IBOutlet weak var user_namelbl: UILabel!
    @IBOutlet weak var proffesion_namelbl: UILabel!
    @IBOutlet weak var hourlyrate_lbl: UILabel!
    @IBOutlet weak var container_view: UIView!
    @IBOutlet weak var country_namelbl: UILabel!
    @IBOutlet weak var addFriend_btn: UIButton!
    
    @IBOutlet weak var like_btn: UIButton!
    @IBOutlet weak var message_btn: UIButton!
    
    @IBOutlet weak var hireme_btn: UIButton!
}

extension UILabel {
    func calculateMaxLines() -> Int {
        let maxSize = CGSize(width: frame.size.width, height: CGFloat(Float.infinity))
        let charSize = font.lineHeight
        let text = (self.text ?? "") as NSString
        let textSize = text.boundingRect(with: maxSize, options: .usesLineFragmentOrigin, attributes: [NSAttributedString.Key.font: font], context: nil)
        let linesRoundedUp = Int(ceil(textSize.height/charSize))
        return linesRoundedUp
    }
}

class FriendListViewController: UIViewController,UITableViewDelegate,UITableViewDataSource,UINavigationControllerDelegate,UIImagePickerControllerDelegate,UITextViewDelegate,UICollectionViewDelegate,UICollectionViewDataSource,JTCalendarDelegate,UICollectionViewDelegateFlowLayout,SambagTimePickerViewControllerDelegate,UITextFieldDelegate,SKPhotoBrowserDelegate,SFSafariViewControllerDelegate
{

    //MARK:- Home Outlets
    var data_arr = [[String:Any]]()
    var arrPostList = [PostListModel]()
    var collectionInitialized:Bool = false
    var post_select_index = Int()
    @IBOutlet weak var tblFeeds: UITableView!
    var total_no_of_page = Int()
    var current_page = 1
    var post_type = "text"
    
    var refreshControl = UIRefreshControl()
    var is_picker_open = false
    var perpage_data_count = Int()
    var is_next_page_available = true
    var profile_img_url = String()
    var aboutmestr = String()
    
    @IBOutlet weak var reportBtn: UIButton!

    @IBOutlet weak var timelineHeightContainer: NSLayoutConstraint!
    @IBOutlet weak var viewProfileHeader: UIView!
    
    @IBOutlet weak var photo_collectionview: UICollectionView!
    @IBOutlet weak var video_collectionview: UICollectionView!
    var photo_arr = [[String:Any]]()
    var video_arr = [[String:Any]]()
    var user_id_str = String()
    var request_status = String()
    var profile_user_type = String()
    var profile_user_id = Int()
    
    var singleDate: Date = Date()
    var multipleDates: [Date] = []
    var theme: SambagTheme = .light
    
    var totalAmountValue = CGFloat()
    
    @IBOutlet weak var viewProfileHeaderMain: UIView!
    @IBOutlet weak var morebtn_height_constraint: NSLayoutConstraint!
    
    @IBOutlet weak var more_btn: UIButton!
    @IBOutlet weak var morebtn_top_constraint: NSLayoutConstraint!
    
    @IBOutlet weak var user_profile_imageview: UIImageView!
    @IBOutlet weak var lbl_groupCount: UILabel!
    @IBOutlet weak var lbl_friendCount: UILabel!
    //@IBOutlet weak var lbl_likeCount: UILabel!
    @IBOutlet weak var view_group: UIView!
    @IBOutlet weak var view_friend: UIView!
    //@IBOutlet weak var view_like: UIView!
    @IBOutlet weak var lbl_city: UILabel!
    @IBOutlet weak var lbl_username: UILabel!
    
    @IBOutlet weak var coach_icon_imgview: UIImageView!
    
    var userHourlyRate = CGFloat()
    var userName = String()
    
    var quickBloxData = [String:Any]()
    
    
    //MARK:- View RML Outlets
    @IBOutlet weak var view_rml: UIView!
    @IBOutlet weak var view_rml_heightconstraint: NSLayoutConstraint!
    @IBOutlet weak var btnRequest: UIButton!
    @IBOutlet weak var btnDirectMessage: UIButton!
    @IBOutlet weak var btnLikeFriend: UIButton!
    
    //MARK:- View Hire Outlets
    @IBOutlet weak var view_hire: UIView!
    @IBOutlet weak var viewHireProfileImage: UIImageView!
    @IBOutlet weak var borderHireProfileImage: UIView!
    @IBOutlet weak var lblViewHireHourlyRate: UILabel!
    
    
    @IBOutlet weak var btn_videos: UIButton!
    @IBOutlet weak var btn_photos: UIButton!
    @IBOutlet weak var btn_timeline: UIButton!
    @IBOutlet weak var photo_view: UIView!
    @IBOutlet weak var video_view: UIView!
    @IBOutlet weak var timeline_view: UIView!
    @IBOutlet weak var calendarMenuView: JTCalendarMenuView!
    @IBOutlet weak var calendarContentView: JTHorizontalCalendarView!
    @IBOutlet weak var lblNoVideoFound: UILabel!
    @IBOutlet weak var lblNoPhotoFound: UILabel!
    @IBOutlet weak var lblNoDataFound: UILabel!
    
    var arr_like_friend = [UserModel]()
    var calendarManager = JTCalendarManager()
    var selecteddate : Date!
    var eventsByDate = NSMutableDictionary()
    var dateSelected: Date?
    
    @IBOutlet weak var Monthname_lbl: UILabel!
    
    //MARK:- Booking Confirmation View  Outlets
    @IBOutlet weak var viewBookingConfirmation: UIView!
    @IBOutlet weak var viewBookingDetailBookingConfirmation: UIView!
    @IBOutlet weak var btnHireMeBookingConfirmationView: UIButton!
    @IBOutlet weak var btnCancelBookingConfirmationView: UIButton!
    @IBOutlet weak var imgProfileBookingConfirmation: UIImageView!
    @IBOutlet weak var lblNameBookingConfirmation: UILabel!
    @IBOutlet weak var lblHourlyRateBookingConfirmation: UILabel!
    @IBOutlet weak var lblDateBookingConfirmation: UILabel!
    @IBOutlet weak var lblTimeBookingConfirmation: UILabel!
    @IBOutlet weak var lblTotalAmountBookingConfirmation: UILabel!
    @IBOutlet weak var lblTaxBookingConfirmation: UILabel!
    
    
    //MARK:- Time Picker View Outlets
    @IBOutlet weak var viewTimePicker: UIView!
    @IBOutlet weak var txt_endTime: UITextField!
    @IBOutlet weak var txt_startTime: UITextField!
    
    
    var tempText  = UITextField()
    
    //MARK:- Booking Payment View Outlets
    @IBOutlet weak var viewBookingPayment: UIView!
    @IBOutlet weak var viewPaymentEmail: UIView!
    @IBOutlet weak var viewPaymentCardNumber: UIView!
    @IBOutlet weak var viewPaymentMonthCVV: UIView!
    @IBOutlet weak var txtEmailAddress: UITextField!
    @IBOutlet weak var txtCardNumber: UITextField!
    @IBOutlet weak var txtExpiryDate: UITextField!
    @IBOutlet weak var txtCVV: UITextField!
    @IBOutlet weak var btnCheckPayment: UIButton!
    @IBOutlet weak var lblAmountPaymentView: UILabel!
    @IBOutlet weak var lblTaxPaymentView: UILabel!
    @IBOutlet weak var lblTotalAmuntPaymentView: UILabel!
    @IBOutlet weak var txtEmailPaymentAmount: UITextField!
    @IBOutlet weak var txtCardNumPaymentView: UITextField!
    @IBOutlet weak var txtExpiryDatePayment: UITextField!
    @IBOutlet weak var txtCVVPayment: UITextField!
    @IBOutlet weak var btnCheckBoxPayment: UIButton!
    @IBOutlet weak var btnPayPaymentView: UIButton!
    
    
    
    //MARK:- Booking Completed View Outlets
    @IBOutlet weak var viewBookingCompleted: UIView!
    @IBOutlet weak var lblBookingDateCompletedView: UILabel!
    @IBOutlet weak var lblUsernameBookingCompletedView: UILabel!
    
    
    //MARK:- Booking Final View Outlets
    @IBOutlet weak var viewBookingFinal: UIView!
    
    //MARK:- Coach Bottom view for user Outlets
    @IBOutlet weak var CoachBottomView: UIView!
    @IBOutlet weak var lblCoachHourRateUser: UILabel!
    @IBOutlet weak var btnHireMeUser: UIButton!
    
    
    //MARK:- Coach Bottom view for coach Outlets
    @IBOutlet weak var CoachBottomViewCoach: UIView!
    @IBOutlet weak var lblCoachHourRateCoach: UILabel!
    @IBOutlet weak var btnMyBookingCoach: UIButton!
    
    //MARK:- User Bottom View for User Outlets
    @IBOutlet weak var UserBottomViewUser: UIView!
    @IBOutlet weak var btnMyBookingUser: UIButton!
    
    
    //MARK:- Link Outlets
    var result = SwiftLinkPreview.Response()
    let placeholderImages = [ImageSource(image: UIImage(named: "Placeholder")!)]
    
    let slp = SwiftLinkPreview(cache: InMemoryCache())
    
    var linksProperties: [NSTextCheckingResult]?
    
    var expandedCells = Set<Int>()
    
    var headerView: UICollectionReusableView?
    let layout = UICollectionViewFlowLayout()
    
    //MARK:- viewDidLoad
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        //self.getProfileTimeline()
        
        
        
        self.navigationController?.setNavigationBarHidden(true, animated: false)
        //CommonFunction.addshadow_view(view: view_like, shadow_radius: 10.0, shadow_color: UIColor.lightGray, shadow_opecity: 0.5, shadow_offset: CGSize(width: 2.0, height: 2.0), is_clipBound: false, corner_radius: 4.0)
        CommonFunction.addshadow_view(view: view_group, shadow_radius: 10.0, shadow_color: UIColor.lightGray, shadow_opecity: 0.5, shadow_offset: CGSize(width: 2.0, height: 2.0), is_clipBound: false, corner_radius: 4.0)
        CommonFunction.addshadow_view(view: view_friend, shadow_radius: 10.0, shadow_color: UIColor.lightGray, shadow_opecity: 0.5, shadow_offset: CGSize(width: 2.0, height: 2.0), is_clipBound: false, corner_radius: 4.0)
        //CommonFunction.addshadow_view(view: view_likefriend_container, shadow_radius: 10.0, shadow_color: UIColor.lightGray, shadow_opecity: 0.5, shadow_offset: CGSize(width: 2.0, height: 2.0), is_clipBound: false, corner_radius: 20.0)

       // calendarManager.delegate = self
        //calendarMenuView.contentRatio = 0.75
       // calendarManager.settings.weekDayFormat = .single;
        //calendarManager.menuView = calendarMenuView
       // calendarManager.contentView = calendarContentView
        //calendarManager.setDate(Date())
        //Monthname_lbl.text = getMonthName(date_val: Date())
        
        //Home
        self.automaticallyAdjustsScrollViewInsets = false
        //self.tblFeeds.estimatedRowHeight = 1000
        //self.tblFeeds.rowHeight = UITableViewAutomaticDimension
        //self.refreshControl.addTarget(self, action: #selector(HomeViewController.pullto_reloadData),for: UIControlEvents.valueChanged)
        //self.tblFeeds.addSubview(refreshControl)
        
       // NotificationCenter.default.addObserver(self, selector: #selector(self.show_tab), name: NSNotification.Name(rawValue: "Hidetab"), object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.show_image(_:)), name: NSNotification.Name(rawValue: "Show_image"), object: nil)
        
        
        //self.viewPaymentEmail.setPaymentBorder()
        //self.viewPaymentCardNumber.setPaymentBorder()
        //self.viewPaymentMonthCVV.setPaymentBorder()
        //NotificationCenter.default.addObserver(self, selector: #selector(self.setMonthName(_:)), name: NSNotification.Name(rawValue: "MonthName"), object: nil)
        
        
        // Static setup
        SKPhotoBrowserOptions.displayStatusbar = true
        SKPhotoBrowserOptions.displayCounterLabel = false
        SKPhotoBrowserOptions.displayBackAndForwardButton = false
        SKPhotoBrowserOptions.displayCloseButton = false
        SKPhotoBrowserOptions.displayAction = false
    
        
    }
    @objc func setMonthName(_ notification: NSNotification)
    {
        if let dict = notification.userInfo as NSDictionary?
        {
            if let firstDate = dict["MonthDate"] as? Date
            {
                Monthname_lbl.text = getMonthName(date_val: firstDate)
            }
        }
    
    }

    
    @IBAction func moreBtnAction(_ sender: UIButton) {
        if(sender.title(for: .normal)=="More")
        {
            sender.setTitle("Less", for: .normal)
            self.lbl_city.numberOfLines = 0
        }
        else
        {
            sender.setTitle("More", for: .normal)
            self.lbl_city.numberOfLines = 1
        }
        
        if(photo_view.isHidden == false)
        {
            let height = headerView?.systemLayoutSizeFitting(UILayoutFittingCompressedSize).height
            var headerFramecol = viewProfileHeader.frame
            
            // If we don't have this check, viewDidLayoutSubviews() will get
            // repeatedly, causing the app to hang.
            if height != headerFramecol.size.height {
                headerFramecol.size.height = height!
                headerView?.frame = headerFramecol
            }

            self.photo_collectionview.reloadData()

        }
        if video_view.isHidden == false
        {
            
            let height = headerView?.systemLayoutSizeFitting(UILayoutFittingCompressedSize).height
            var headerFramecol = viewProfileHeader.frame
            
            // If we don't have this check, viewDidLayoutSubviews() will get
            // repeatedly, causing the app to hang.
            if height != headerFramecol.size.height {
                headerFramecol.size.height = height!
                headerView?.frame = headerFramecol
            }
            
            self.video_collectionview.reloadData()
        }
        if timeline_view.isHidden == false
        {
            self.tblFeeds.reloadData()

        }
    }
    
    //MARK:- viewWillAppear
    override func viewWillAppear(_ animated: Bool)
    {
        //self.callLikeFriendListApi()
        self.tabBarController?.tabBar.frame.size.height = 49
        self.tabBarController?.tabBar.isHidden = false
        
        self.lblNoDataFound.isHidden = true
        self.lblNoPhotoFound.isHidden = true
        self.lblNoVideoFound.isHidden = true
        
        getProfileDetails()
        self.tblFeeds.addSubview(self.viewProfileHeader)
        self.photo_collectionview.delegate = nil
        self.photo_collectionview.dataSource = nil
        self.video_collectionview.delegate = nil
        self.video_collectionview.dataSource = nil
        self.tblFeeds.delegate = self
        self.tblFeeds.dataSource = self
        //self.tblFeeds.layoutIfNeeded()
        self.tblFeeds.reloadData()
        timeline_view.isHidden = false
        photo_view.isHidden = true
        video_view.isHidden = true
        self.is_next_page_available=true
        self.arrPostList.removeAll()
        self.tblFeeds.reloadData()

        
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
    
        self.lbl_city.text = ""
        self.photo_collectionview.delegate = nil
        self.photo_collectionview.dataSource = nil
        self.video_collectionview.delegate = nil
        self.video_collectionview.dataSource = nil
        self.tblFeeds.delegate = nil
        self.tblFeeds.dataSource = nil
    }
    
    //MARK:- viewDidAppear
    override func viewDidAppear(_ animated: Bool)
    {
        super.viewDidAppear(true)
        self.navigationController?.setNavigationBarHidden(true, animated: false)
        if UserDefaults.standard.value(forKey: Constants.USERID) as? Int != Int(user_id_str) &&  user_id_str != ""
        {
          reportBtn.isHidden = false
        }
    }
    
    //MARK:- btnSearch Action
    
    
    @IBAction func blockUserAction(_ sender: UIButton) {
        let alert = UIAlertController(title: Constants.APP_NAME, message: "Perform action", preferredStyle: UIAlertControllerStyle.actionSheet)
        
            
            let reportBtn = UIAlertAction(title: "Report User", style: UIAlertActionStyle.default) { (alert) -> Void in
                
              ReportNetworkManager.BlockUser(message: "Blockk this user" , userId: self.user_id_str, controller: self) { (flag) in
                         if flag {
                             DispatchQueue.main.async {
                             self.navigationController?.popViewController(animated: true)
                         }
                }
                      }
        }

        let cancelButton = UIAlertAction(title: "Cancel", style: UIAlertActionStyle.cancel) { (alert) -> Void in
                
        }
            alert.addAction(reportBtn)
            alert.addAction(cancelButton)
            DispatchQueue.main.async {
            self.present(alert, animated: true, completion: nil)
            }
        
    }
    
    @IBAction func btnSearchAction(_ sender: UIButton)
    {
        let searchPage = self.storyboard?.instantiateViewController(withIdentifier: "SearchFriendViewController") as! SearchFriendViewController
        self.navigationController?.pushViewController(searchPage, animated: false)
    }
    
    
//    //MARK:- Likelist Popup
//    @IBAction func btnLikeACtion(_ sender: UIButton)
//    {
//        if(lbl_likeCount.text=="0")
//        {
//            //self.displayAlert(msg: "\(lbl_username.text!) dosen't get any like.", title_str: Constants.APP_NAME)
//            self.view.makeToast("\(lbl_username.text!) dosen't get any like.")
//        }
//        else
//        {
//            UIView.animate(withDuration: Double(0.35), animations: {
//            self.likelist_top_constaint.constant = 0
//            self.view.layoutIfNeeded()
//            })
//        }
//
//    }
    
    //MARK:- Add Friend
    @IBAction func btnFriendAction(_ sender: UIButton)
    {
        let friendvc = Constants.mainStoryboard.instantiateViewController(withIdentifier: "FriendsViewController") as! FriendsViewController
        friendvc.user_id_str = user_id_str
        friendvc.isTab = false
        self.navigationController?.pushViewController(friendvc, animated: true)
    }
    
    //MARK:- Groups
    @IBAction func btnGroupACtion(_ sender: UIButton)
    {
        let groupPage = self.storyboard?.instantiateViewController(withIdentifier: "GroupListViewController") as! GroupListViewController
        groupPage.user_id_str = user_id_str
        self.navigationController?.pushViewController(groupPage, animated: true)
    }
    
    //MARK:- LikeFriendList Api
    func callLikeFriendListApi()
    {
        if(CommonFunction.isInternetAvailable())
        {
        self.arr_like_friend.removeAll()
        MBProgressHUD.showAdded(to: self.view, animated: true)
            
        var strUrl = String()
        if user_id_str == ""
        {
            strUrl = Constants.BASEURL + MethodName.getLikeList + "?access-token=\(UserDefaults.standard.value(forKey: "Access_Token") as! String)"
        }
        else
        {
            
            if UserDefaults.standard.value(forKey: Constants.USERID) as? Int == Int(user_id_str)
            {
                
                strUrl =  Constants.BASEURL + MethodName.getLikeList + "?access-token=\(UserDefaults.standard.value(forKey: "Access_Token") as! String)"
            }
            else
            {
                
                strUrl = Constants.BASEURL + MethodName.getLikeList + "?access-token=\(UserDefaults.standard.value(forKey: "Access_Token") as! String)&user_id=\(user_id_str)"
                
            }
        }
        print(Constants.BASEURL + MethodName.getLikeList + "?access-token=\(UserDefaults.standard.value(forKey: "Access_Token") as! String)")
        Alamofire.request(strUrl, method: .get, parameters: nil , encoding: JSONEncoding.default, headers:nil).responseJSON
            { response in
                MBProgressHUD.hide(for: self.view, animated: true)
                if let result_dict = response.result.value as? [String:Any]
                {
                    if let code = result_dict["code"] as? Int
                    {
                        if(code==200)
                        {
                            if let user_arr = result_dict["data"] as? [[String:Any]]
                            {
                                for i in 0..<user_arr.count
                                {
                                    let user_model = UserModel()
                                    if let user_dict = user_arr[i] as? [String : Any]
                                    {
                                        if let about_str = user_dict["aboutme"] as? String
                                        {
                                            user_model.aboutme = about_str
                                        }
                                        if let name_str = user_dict["fullname"] as? String
                                        {
                                            user_model.userName = name_str
                                            self.userName = name_str
                                           
                                        }
                                        if let img_str = user_dict["user_profile_image"] as? String
                                        {
                                            user_model.userImage = img_str
                                           
                                        }
                                        if let city_str = user_dict["city"] as? String
                                        {
                                            user_model.city = city_str
                                        }
                                        if let country_str = user_dict["country"] as? String
                                        {
                                            user_model.country = country_str
                                        }
                                        
                                        if let userid_str = user_dict["user_id"] as? Int
                                        {
                                            user_model.userId = "\(userid_str)"
                                        }
                                        if let friend_request_id = user_dict["friend_request_id"] as? Int
                                        {
                                            user_model.friend_request_id = friend_request_id
                                        }
                                        if let is_coach = user_dict["is_coach"] as? Bool
                                        {
                                            user_model.is_coach = is_coach
                                        }
                                        if let is_like = user_dict["is_like"] as? Bool
                                        {
                                            user_model.is_like = is_like
                                        }
                                        if let is_friend = user_dict["is_friend"] as? Int
                                        {
                                            user_model.is_friend = is_friend
                                        }
                                        self.arr_like_friend.append(user_model)
                                    }
                                }
                                //self.lbl_likeCount.text = "\(self.arr_like_friend.count)"
                                //self.user_collectionview.reloadData()
                            }
                        }
                        else
                        {
                            if let error_arr = result_dict["errorData"] as? [[String:Any]]
                            {
                                if let error_msg = error_arr[0]["message"] as? String
                                {
                                    self.view.makeToast(error_msg)
                                }
                            }
                        }
                    }
                }
            }
            
        }
        else
        {
            self.view.makeToast("Please Check Your Internet Connection!!")
        }
    }
    
    //MARK:- CollectionView Delegate Methods
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        if(collectionView==video_collectionview)
        {
            return video_arr.count
        }
        else if(collectionView==photo_collectionview)
        {
            return photo_arr.count
        }
        else
        {
            return self.data_arr.count
        }
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell
    {
            if(collectionView == video_collectionview)
            {
                let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "profile_Video_cell", for: indexPath) as! profile_Video_cell
                
                cell.photo_imageview.image = nil
                
                if let img_str = video_arr[indexPath.item]["preview_image"] as? String
                {
                    cell.photo_imageview.sd_setImage(with: URL(string: img_str))
                    
                }
                else
                {
                    cell.photo_imageview.image = nil
                }
                
                return cell
            }
            else if (collectionView == photo_collectionview)
            {
                let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "profile_Photo_cell", for: indexPath) as! profile_Photo_cell
                
                cell.photo_imageview.image = nil
                
                if let img_str = photo_arr[indexPath.item]["url"] as? String
                {
                    //cell.photo_imageview.sd_setShowActivityIndicatorView(true)
                    //cell.photo_imageview.sd_setIndicatorStyle(.gray)
                    //cell.photo_imageview.startAnimating()
                    cell.photo_imageview.sd_setImage(with: URL(string: img_str))
                        
                        //cell.photo_imageview.stopAnimating()
                   
                }
                else
                {
                    cell.photo_imageview.image = nil
                }
                
                return cell
            }
            else
            {
                return UICollectionViewCell()
            }
        
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath)
    {
        if(collectionView == video_collectionview)
        {
            
            print(self.video_arr)
            var images = [SKPhotoProtocol]()
            for i in 0..<self.video_arr.count
            {
                if let img_dict = self.video_arr[i] as? [String: Any]
                {
                    let photo = SKPhoto.photoWithImageURL(img_dict["preview_image"] as! String)
                    photo.shouldCachePhotoURLImage = false // you can use image cache by true(NSCache)
                    let movie_url = NSURL(string: img_dict["url"] as! String)
                    photo.movieURL = movie_url
                    photo.isVideo = true
                    images.append(photo)
                }
            }
            
            let browser = SKPhotoBrowser(photos: images)
            browser.initializePageIndex(indexPath.row)
            present(browser, animated: true, completion: {})
            
            
        }
        else if(collectionView == photo_collectionview)
        {
            
            print(self.photo_arr)
            var images = [SKPhotoProtocol]()
            for i in 0..<self.photo_arr.count
            {
                if let img_dict = self.photo_arr[i] as? [String: Any]
                {
                    let photo = SKPhoto.photoWithImageURL(img_dict["url"] as! String)
                    photo.shouldCachePhotoURLImage = false // you can use image cache by true(NSCache)
                    photo.isVideo = false
                    images.append(photo)
                }
            }
            
            let browser = SKPhotoBrowser(photos: images)
            browser.initializePageIndex(indexPath.row)
            present(browser, animated: true, completion: {})
            
        }
        
        
        
    }

    
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView
    {
        
        
        if collectionView == photo_collectionview
        {
            if kind == UICollectionElementKindSectionHeader {
                headerView = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: "PhotoCollectionHeader", for: indexPath as IndexPath)
                
                
                headerView?.addSubview(self.viewProfileHeader)
                
                return headerView!
                
            }
            else
            {
                return UICollectionReusableView()
            }
        }
        else
        {
            if kind == UICollectionElementKindSectionHeader {
                 headerView = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: "VideoCollectionHeader", for: indexPath as IndexPath)
                
                headerView?.addSubview(self.viewProfileHeader)
                    
                return headerView!
                
            }
            else
            {
                return UICollectionReusableView()
            }
        }
    
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        
        return UIEdgeInsets(top: 10.0, left: 10.0, bottom: 10.0, right: 10.0)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize
    {
        if(collectionView == photo_collectionview || collectionView == video_collectionview)
        {
            let cellsAcross: CGFloat = 3
            let spaceBetweenCells: CGFloat = 10
            let dim = (collectionView.bounds.width - (cellsAcross - 1) * spaceBetweenCells) / cellsAcross
            return CGSize(width: (collectionView.bounds.width-40)/3, height: (collectionView.bounds.width-40)/3)
            //return CGSize(width: dim, height:dim)
        }

        else
        {
            return CGSize(width: (collectionView.bounds.width - 10)/2, height: (collectionView.bounds.width - 10)/2)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize{
        
        return CGSize(width:collectionView.bounds.width , height: self.viewProfileHeader.frame.size.height)
    }
    
//    @IBAction func btnCancelAction(_ sender: Any)
//    {
//        UIView.animate(withDuration: Double(0.35), animations: {
//            self.likelist_top_constaint.constant = self.view.bounds.size.height
//            self.view.layoutIfNeeded()
//        })
//        DispatchQueue.main.asyncAfter(deadline: .now() + 0.40) {
//            self.likelist_top_constaint.constant = -self.view.bounds.size.height
//            self.view.layoutIfNeeded()
//        }
//    }
    
    func getMonthName(date_val:Date) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "MMMM YYYY"
        let strMonth = dateFormatter.string(from: date_val)
        return strMonth
    }
    
    func calendar(_ calendar: JTCalendarManager!, prepareDayView dayView: (UIView & JTCalendarDay)!)
    {
        if let dView = dayView as? JTCalendarDayView
        {
            dView.isHidden = false
            if dView.isFromAnotherMonth
            {
                dView.isHidden = true
            }
            else if(calendarManager.dateHelper.date(Date(), isTheSameDayThan: dView.date))
            {
                if(dateSelected == nil)
                {
                    dView.circleView.isHidden = false
                }
                else
                {
                    if Calendar.current.isDateInToday(dateSelected!)
                    {
                        dView.circleView.isHidden = false
                    }
                    else
                    {
                        dView.circleView.isHidden = true
                    }
                }
                dView.circleView.backgroundColor = UIColor.red
                dView.dotView.backgroundColor = UIColor.white
                dView.textLabel?.textColor = UIColor.white
            }
            else if (dateSelected != nil) && calendarManager.dateHelper.date(dateSelected, isTheSameDayThan: dView.date)
            {
                dView.circleView.isHidden = false
                dView.circleView.backgroundColor = UIColor.red
                dView.dotView.backgroundColor = UIColor.white
                dView.textLabel?.textColor = UIColor.white
            }
            else
            {
                dView.circleView.isHidden = true
                dView.dotView.backgroundColor = UIColor.red
                dView.textLabel?.textColor = UIColor.white
            }
            
            if haveEvent(forDay: dView.date)
            {
                dView.dotView.isHidden = false
            }
            else
            {
                dView.dotView.isHidden = true
            }
        }
    }

    func calendar(_ calendar: JTCalendarManager!, didTouchDayView dayView: (UIView & JTCalendarDay)!)
    {
        if let dView = dayView as? JTCalendarDayView
        {
            
    
            dateSelected = dView.date
            lblDateBookingConfirmation.text = convert_date_string(dateval: dateSelected!, format_str: "dd MMMM YYYY")
            
            dView.circleView.transform = CGAffineTransform.identity.scaledBy(x: 0.1, y: 0.1)
            UIView.transition(with: dView, duration: 0.3, options: [], animations: {
                dView.circleView.transform = CGAffineTransform.identity
                self.calendarManager.reload()
            })
            if calendarManager.settings.weekModeEnabled
            {
                return
            }
            if !calendarManager.dateHelper.date(calendarContentView.date, isTheSameMonthThan: dView.date)
            {
                if calendarContentView.date.compare(dView.date) == .orderedAscending
                {
                    calendarContentView.loadNextPageWithAnimation()
                    
                }
                else
                {
                    calendarContentView.loadPreviousPageWithAnimation()
                }
                Monthname_lbl.text = getMonthName(date_val: calendarContentView.date)
            }
            
        }
    }
    func convert_date_string(dateval:Date , format_str:String) -> String
    {
        let dateformatter = DateFormatter()
        dateformatter.dateFormat = format_str
        return dateformatter.string(from: dateval)
    }
    
    func haveEvent(forDay date: Date?) -> Bool
    {
        var key: String? = nil
        if let aDate = date
        {
            key = dateFormatter().string(from: aDate)
        }
//        if eventsByDate[key ?? ""] && (eventsByDate[key ?? ""]).count() > 0
//        {
//            return true
//        }
        return false
    }
    func dateFormatter() -> DateFormatter
    {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd-MM-yyyy"
        return dateFormatter
    }
    
    //MARK:- Profile Photos Api
    
    func getProfilePhotos()
    {
        if(CommonFunction.isInternetAvailable())
        {
           // MBProgressHUD.showAdded(to: self.view, animated: true)
            
            var strUrl = String()
            
            if user_id_str == ""
            {
                strUrl = Constants.BASEURL + MethodName.profilePhotos + "?access-token=\(UserDefaults.standard.value(forKey: "Access_Token") as! String)"
            }
            else
            {
                
                if UserDefaults.standard.value(forKey: Constants.USERID) as? Int == Int(user_id_str)
                {
                    
                    strUrl = Constants.BASEURL + MethodName.profilePhotos + "?access-token=\(UserDefaults.standard.value(forKey: "Access_Token") as! String)"
                }
                else
                {
                    
                    strUrl = Constants.BASEURL + MethodName.profilePhotos + "?access-token=\(UserDefaults.standard.value(forKey: "Access_Token") as! String)&user_id=\(user_id_str)"
                    
                }
            }
            
            Alamofire.request(strUrl, method: .get, parameters: nil , encoding: JSONEncoding.default, headers:nil).responseJSON
                { response in
                    
                //    MBProgressHUD.hide(for: self.view, animated: true)
                    self.photo_arr.removeAll()
                    if let result_dict = response.result.value as? [String:Any]
                    {
                        if let code = result_dict["code"] as? Int
                        {
                            if(code==200)
                            {
                                if let arrData = result_dict["data"] as? [[String:Any]]
                                {
                                    self.photo_arr = arrData
                                    self.photo_collectionview.reloadData()
                                    
                                    if arrData.count > 0
                                    {
                                        self.lblNoPhotoFound.isHidden = true
                                    }
                                    else
                                    {
                                        self.lblNoPhotoFound.isHidden = false
                                    }
                                }
                                
                            }
                            else
                            {
                                if let error_arr = result_dict["errorData"] as? [[String:Any]]
                                {
                                    if let error_msg = error_arr[0]["message"] as? String
                                    {
                                        self.view.makeToast(error_msg)
                                    }
                                }
                                
                            }
                        }
                        
                    }
            }
        }
        else
        {
            self.view.makeToast("Please Check Your Internet Connection!!")
        }
    }

    //MARK:- Profile Videos Api
    
    func getProfileVideos()
    {
        if(CommonFunction.isInternetAvailable())
        {
            //MBProgressHUD.showAdded(to: self.view, animated: true)
            
            var strUrl = String()
            
            if user_id_str == ""
            {
                strUrl = Constants.BASEURL + MethodName.profileVideos + "?access-token=\(UserDefaults.standard.value(forKey: "Access_Token") as! String)"
            }
            else
            {
                
                if UserDefaults.standard.value(forKey: Constants.USERID) as? Int == Int(user_id_str)
                {
                    
                    strUrl = Constants.BASEURL + MethodName.profileVideos + "?access-token=\(UserDefaults.standard.value(forKey: "Access_Token") as! String)"
                }
                else
                {
                    
                    strUrl = Constants.BASEURL + MethodName.profileVideos + "?access-token=\(UserDefaults.standard.value(forKey: "Access_Token") as! String)&user_id=\(user_id_str)"
                    
                }
            }
            
            Alamofire.request(strUrl, method: .get, parameters: nil , encoding: JSONEncoding.default, headers:nil).responseJSON
                { response in
                    
               //     MBProgressHUD.hide(for: self.view, animated: true)
                    if let result_dict = response.result.value as? [String:Any]
                    {
                        if let code = result_dict["code"] as? Int
                        {
                            if(code==200)
                            {
                                if let arrData = result_dict["data"] as? [[String:Any]]
                                {
                                    self.video_arr = arrData
                                    self.video_collectionview.reloadData()
                                    
                                    if arrData.count > 0
                                    {
                                        self.lblNoVideoFound.isHidden = true
                                    }
                                    else
                                    {

                                        self.lblNoVideoFound.isHidden = false
                                    }
                                }
                            }
                            else
                            {
                                if let error_arr = result_dict["errorData"] as? [[String:Any]]
                                {
                                    if let error_msg = error_arr[0]["message"] as? String
                                    {
                                        self.view.makeToast(error_msg)
                                    }
                                }
                            }
                        }
                        
                    }
            }
        }
        else
        {
            self.view.makeToast("Please Check Your Internet Connection!!")
        }
    }
    
    //MARK:- Profile Timeline Api
    
    func getProfileTimeline()
    {
        if(CommonFunction.isInternetAvailable())
        {
          //  MBProgressHUD.showAdded(to: self.view, animated: true)
            
            var strUrl = String()
            
            if user_id_str == ""
            {
                strUrl = Constants.BASEURL + MethodName.profileTimeline + "?access-token=\(UserDefaults.standard.value(forKey: "Access_Token") as! String)&page=\(current_page)"
            }
            else
            {
                
                if UserDefaults.standard.value(forKey: Constants.USERID) as? Int == Int(user_id_str)
                {
                    
                    strUrl = Constants.BASEURL + MethodName.profileTimeline + "?access-token=\(UserDefaults.standard.value(forKey: "Access_Token") as! String)&page=\(current_page)"
                }
                else
                {
                    
                    strUrl = Constants.BASEURL + MethodName.profileTimeline + "?access-token=\(UserDefaults.standard.value(forKey: "Access_Token") as! String)&page=\(current_page)&user_id=\(user_id_str)"
                    
                }
            }
            
            self.is_next_page_available=true

            Alamofire.request(strUrl, method: .get, parameters: nil , encoding: JSONEncoding.default, headers:nil).responseJSON
                { response in
                    
                  //  MBProgressHUD.hide(for: self.view, animated: true)
                    
                    
                    if let result_dict = response.result.value as? [String:Any]
                    {
                        
                        let code:Int = result_dict["code"]as! Int
                        if (code == 200)
                        {
                            if let arrData = result_dict["data"] as? NSArray
                            {
                                print(arrData)
                               // self.arrPostList.removeAll()
                                if arrData.count < 20 {
                                    self.is_next_page_available=false
                                }
                                for dict in arrData
                                {
                                    let tempDict = dict as! NSDictionary
                                    self.arrPostList.append(PostListModel.init(dict: tempDict))
                                }
                                
                                self.tblFeeds.reloadData()
                                
                                //self.refreshControl.endRefreshing()
                                
                                if(self.arrPostList.count>0)
                                {
                                    self.lblNoDataFound.isHidden = true
                                }
                                else
                                {
                                    self.lblNoDataFound.isHidden = false
                                }
                                
                            }
                            
                        }
                        else
                        {
                            self.is_next_page_available=false
                            if let result_error = response.result.value as? [String:Any]
                            {
                                if let error_array = result_error["errorData"] as? NSArray
                                {
                                    if let error_dict = error_array[0] as? [String : Any]
                                    {
                                        if let error_msg = error_dict["message"] as? String
                                        {
                                            self.view.makeToast(error_msg)
                                        }
                                    }
                                }
                            }
                            
                        }
                    }
            }
        }
        else
        {
            self.is_next_page_available=false

            self.view.makeToast("Please Check Your Internet Connection!!")
        }
    }
    
    
    
    //MARK:- Profile Details Api
    
    func getProfileDetails()
    {
        if(CommonFunction.isInternetAvailable())
        {
            var strUrl = String()
            
            if user_id_str == ""
            {
                strUrl = Constants.BASEURL + MethodName.profileState + "?access-token=\(UserDefaults.standard.value(forKey: "Access_Token") as! String)&expand=userStat"
            }
            else
            {
            
                if UserDefaults.standard.value(forKey: Constants.USERID) as? Int == Int(user_id_str)
                {
                
                strUrl = Constants.BASEURL + MethodName.profileState + "?access-token=\(UserDefaults.standard.value(forKey: "Access_Token") as! String)&expand=userStat"
                }
                else
                {
                
                strUrl = Constants.BASEURL + MethodName.profileState + "?access-token=\(UserDefaults.standard.value(forKey: "Access_Token") as! String)&id=\(user_id_str)&expand=userStat"
                
                }
            }
            
            clearProfileData()
           // MBProgressHUD.showAdded(to: self.view, animated: true)
            Alamofire.request(strUrl, method: .get, parameters: nil , encoding: JSONEncoding.default, headers:nil).responseJSON
                { response in
                    
                //    MBProgressHUD.hide(for: self.view, animated: true)
                    if let result_dict = response.result.value as? [String:Any]
                    {
                        if let code = result_dict["code"] as? Int
                        {
                            if(code==200)
                            {
                                if let profile_info = result_dict["data"] as? [String:Any]
                                {
                                    if let state_dict = profile_info["userStat"] as? [String:Any]
                                    {
                                        if let friendCount = state_dict["friend_count"] as? String
                                        {
                                            self.lbl_friendCount.text = "\(friendCount)"
                                        }
                                        if let groupCount = state_dict["group_count"] as? String
                                        {
                                            self.lbl_groupCount.text = "\(groupCount)"
                                        }
                                        
                                    }
                                    
                                    
                                    if let quickBloxData_dict = profile_info["quickblox_data"] as? [String:Any]
                                    {
                                        self.quickBloxData = quickBloxData_dict
                                    }
                                    
                                    if let is_friend_str = profile_info["is_friend"] as? Int
                                    {
                                        self.request_status = "\(is_friend_str)"
                                        
                                        if is_friend_str == 0
                                        {
                                            self.btnRequest.setBackgroundImage(UIImage(named: "addFriend"), for: .normal)
                                        }
                                        else if is_friend_str == 1
                                        {
                                            self.btnRequest.setBackgroundImage(UIImage(named: "accepticon"), for: .normal)
                                        }
                                        else
                                        {
                                            self.btnRequest.setBackgroundImage(UIImage(named: "pendingicon"), for: .normal)
                                        }
                                        
                                    }
                                    
                                    
                                    if let type_val = profile_info["is_coach"] as? Int
                                    {
                                        if(type_val==0)
                                        {
                                            self.profile_user_type = "User"
                                            self.coach_icon_imgview.isHidden = true
                                        }
                                        else
                                        {
                                            self.profile_user_type = "Coach"
                                            self.coach_icon_imgview.isHidden = false
                                        }
                                    }
                                    if let img_str = profile_info["user_profile_pic"] as? String
                                    {
                                        self.user_profile_imageview.sd_setImage(with: URL(string: img_str), placeholderImage: UIImage(named: "userPlaceHolder"))
                                        self.profile_img_url = img_str
                                        let tap = UITapGestureRecognizer(target: self, action: #selector(self.tapFunction_img))
                                        self.user_profile_imageview.isUserInteractionEnabled = true
                                        self.user_profile_imageview.addGestureRecognizer(tap)
                                        
                                        self.viewHireProfileImage.sd_setImage(with: URL(string: img_str), placeholderImage: UIImage(named: "userPlaceHolder"))
                                        self.imgProfileBookingConfirmation.sd_setImage(with: URL(string: img_str), placeholderImage: UIImage(named: "userPlaceHolder"))
                                    
                                    }
                                    if let name_str = profile_info["fullname"] as? String
                                    {
                                        self.lbl_username.text = name_str
            if let country = profile_info["country"] as? String , let city = profile_info["city"] as? String{
                self.lbl_username.text = "\(name_str)\n\(country),\(city)"
                        }
                                        
                                        if let userStat = profile_info["userStat"] as? [String:Any]
                                        {
                                            if (userStat["like_friend_count"] as? String) != nil
                                            {
                                            }
                                        }
                                        
                                        self.lblNameBookingConfirmation.text = name_str
                                        self.lblUsernameBookingCompletedView.text = name_str
                                    }
                                    if let userID = profile_info["user_id"] as? Int
                                    {
                                        self.profile_user_id = userID
                                    }
                                    
                                    if let hourly_rate_str = profile_info["hourly_rate"] as? Int
                                    {
                                        self.lblCoachHourRateUser.text = "$\(hourly_rate_str) Per Hour"
                                        self.lblCoachHourRateCoach.text = "$\(hourly_rate_str) Per Hour"
                                        self.lblHourlyRateBookingConfirmation.text = "$\(hourly_rate_str) Per Hour"
                                        self.userHourlyRate = CGFloat(hourly_rate_str)
                                        if let hire_name_str = profile_info["fullname"] as? String
                                        {
                                            
                                            let hourly_rates = "$\(hourly_rate_str)"
                                            let attributedString = NSMutableAttributedString(string:hourly_rates)
                                            attributedString.addAttribute(NSAttributedStringKey.foregroundColor, value: UIColor.init(red: 252.0/255.0, green: 89.0/255.0, blue: 138.0/255.0, alpha: 1.0), range: NSRange(location: 0, length: (hourly_rates.count)))
                                            
                                            self.lblViewHireHourlyRate.text = "\(hire_name_str) Hourly rate is \(hourly_rates) Per Hour"
                                            
                                        }
                                    }
                                            if let about_me = profile_info["aboutme"] as? String
                                            {
                                                 self.lbl_city.text = "\(about_me.decodeEmoji)"
                                                
                                                let lbl_lines = self.lbl_city.calculateMaxLines()
                                                 if(lbl_lines == 1)
                                                 {
                                                    self.lbl_city.numberOfLines = 1
                                                    self.more_btn.isHidden = true
                                                    self.morebtn_top_constraint.constant = 0
                                                    self.morebtn_height_constraint.constant = 0
                                                 }
                                                 else
                                                 {
                                                    self.more_btn.setTitle("More", for: .normal)
                                                    self.lbl_city.numberOfLines = 1
                                                    self.more_btn.isHidden = false
                                                    self.morebtn_top_constraint.constant = -15
                                                    self.morebtn_height_constraint.constant = 7.5
                                                    
                                                 }
                                                
                                                self.aboutmestr = about_me.decodeEmoji
                                            }
                                    
                                    if CommonFunction.is_login_user_coach()
                                    {
                                        if let userIDstr = UserDefaults.standard.value(forKey: Constants.USERID) as? Int
                                        {
                                            if(self.profile_user_id == userIDstr)
                                            {
                                              //self.CoachBottomViewCoach.isHidden = false
                                              //self.CoachBottomView.isHidden = true
                                              //self.UserBottomViewUser.isHidden = true
                                                
                                                
                                                self.view_rml.isHidden = true
                                                self.view_rml_heightconstraint.constant = 0
                                                
                                                self.CoachBottomViewCoach.isHidden = true
                                                self.CoachBottomView.isHidden = true
                                                self.UserBottomViewUser.isHidden = true
                                            }
                                            else
                                            {
                                                if(self.profile_user_type == "Coach")
                                                {
                                                  //self.CoachBottomViewCoach.isHidden = true
                                                  //self.CoachBottomView.isHidden = true
                                                  //self.UserBottomViewUser.isHidden = true
                                                    
                                                    self.view_rml.isHidden = false
                                                    self.view_rml_heightconstraint.constant = 80
                                                    
                                                    self.CoachBottomViewCoach.isHidden = true
                                                    self.CoachBottomView.isHidden = true
                                                    self.UserBottomViewUser.isHidden = true
                                                }
                                                else
                                                {
                                                  //self.CoachBottomViewCoach.isHidden = true
                                                  //self.CoachBottomView.isHidden = true
                                                  //self.UserBottomViewUser.isHidden = true
                                                    
                                                    self.view_rml.isHidden = false
                                                    self.view_rml_heightconstraint.constant = 80
                                                    
                                                    self.CoachBottomViewCoach.isHidden = true
                                                    self.CoachBottomView.isHidden = true
                                                    self.UserBottomViewUser.isHidden = true
                                                }
                                            }
                                        }
                                        
                                    }
                                    else
                                    {
                                        if let userIDstr = UserDefaults.standard.value(forKey: Constants.USERID) as? Int
                                        {
                                            if(self.profile_user_id == userIDstr)
                                            {
                                              //self.CoachBottomViewCoach.isHidden = true
                                              //self.CoachBottomView.isHidden = true
                                              //self.UserBottomViewUser.isHidden = false
                                                
                                                self.view_rml.isHidden = true
                                                self.view_rml_heightconstraint.constant = 0
                                                
                                                self.CoachBottomViewCoach.isHidden = true
                                                self.CoachBottomView.isHidden = true
                                                self.UserBottomViewUser.isHidden = true
                                            }
                                            else
                                            {
                                                if(self.profile_user_type == "User")
                                                {
                                                  //self.CoachBottomViewCoach.isHidden = true
                                                  //self.CoachBottomView.isHidden = true
                                                  //self.UserBottomViewUser.isHidden = true
                                                    
                                                    self.view_rml.isHidden = false
                                                    self.view_rml_heightconstraint.constant = 80
                                                    
                                                    self.CoachBottomViewCoach.isHidden = true
                                                    self.CoachBottomView.isHidden = true
                                                    self.UserBottomViewUser.isHidden = true
                                                }
                                                else
                                                {
                                                  //self.CoachBottomViewCoach.isHidden = true
                                                  //self.CoachBottomView.isHidden = false
                                                  //self.UserBottomViewUser.isHidden = true
                                                    
                                                    self.view_rml.isHidden = false
                                                    self.view_rml_heightconstraint.constant = 80
                                                    
                                                    self.CoachBottomViewCoach.isHidden = true
                                                    self.CoachBottomView.isHidden = true
                                                    self.UserBottomViewUser.isHidden = true
                                                }
                                            }
                                        }
                                        
                                    }
                                }
                                
                                self.getProfileTimeline()
                                //self.getProfilePhotos()
                                //self.getProfileVideos()
                            }
                            else if (code==401)
                            {
                                if let error_data = result_dict["errorData"] as? [[String:Any]]
                                {
                                    if let error_msg = error_data[0]["message"] as? String
                                    {
                                        let view = CongratulationView.instantiateFromNib()
                                        view.lblTitle.text = error_msg
                                        let modal = PathDynamicModal()
                                        modal.showMagnitude = 200.0
                                        modal.closeMagnitude = 130.0
                                        modal.closeByTapBackground = false
                                        modal.closeBySwipeBackground = false
                                        view.OkButtonHandler = {[weak modal] in
                                            
                                            let storyBoard = UIStoryboard(name: "Main", bundle: nil)
                                            let objHomeNav = storyBoard.instantiateViewController(withIdentifier: "HomeNav") as! UINavigationController
                                            UIApplication.shared.keyWindow?.rootViewController = objHomeNav
                                            
                                            modal?.closeWithLeansRandom()
                                            return
                                        }
                                        modal.show(modalView: view, inView: (UIApplication.shared.delegate?.window!)!)
                                    }
                                }
                                
                                
                            }
                            else
                            {
                                self.view.makeToast("Something Went Wrong!!")
                            }
                        }
                        
                    }
            }
        }
        else
        {
            self.view.makeToast("Please Check Your Internet Connection!!")
        }
    }
    
    @objc func tapFunction_img(sender:UITapGestureRecognizer)
    {

        var images = [SKPhotoProtocol]()
        
        let photo = SKPhoto.photoWithImageURL(self.profile_img_url)
        photo.shouldCachePhotoURLImage = false
        photo.isVideo = false // you can use image cache by true(NSCache)
        images.append(photo)
        
        let browser = SKPhotoBrowser(photos: images)
        present(browser, animated: true, completion: {})
    }
    
    
    func lines(label: UILabel) -> Int {
        let textSize = CGSize(width: label.frame.size.width, height: CGFloat(Float.infinity))
        let rHeight = lroundf(Float(label.sizeThatFits(textSize).height))
        let charSize = lroundf(Float(label.font.lineHeight))
        let lineCount = rHeight/charSize
        return lineCount
    }
    
    func clearProfileData()
    {
        self.lbl_friendCount.text = "0"
        self.lbl_groupCount.text = "0"
        //self.lbl_likeCount.text = "0"
        self.user_profile_imageview.image = UIImage(named: "userPlaceHolder")
        self.lbl_username.text = ""
    }
    
    //MARK:- Button Timeline, Photos, Videos Actions
    
    @IBAction func btnTimelineAction(_ sender: UIButton)
    {
        self.photo_collectionview.delegate = nil
        self.photo_collectionview.dataSource = nil
        self.video_collectionview.delegate = nil
        self.video_collectionview.dataSource = nil
        self.tblFeeds.delegate = self
        self.tblFeeds.dataSource = self
        self.tblFeeds.addSubview(self.viewProfileHeader)
        self.getProfileTimeline()
        self.btn_timeline.setTitleColor(UIColor.black, for: .normal)
        self.btn_photos.setTitleColor(UIColor(red: 250.0/255.0, green: 132.0/255.0, blue: 133.0/255.0, alpha: 1.0), for: .normal)
        self.btn_videos.setTitleColor(UIColor(red: 250.0/255.0, green: 132.0/255.0, blue: 133.0/255.0, alpha: 1.0), for: .normal)
        self.timeline_view.isHidden = false
        self.video_view.isHidden = true
        self.photo_view.isHidden = true
    }
    
    @IBAction func btnPhotoAction(_ sender: UIButton)
    {
        self.getProfilePhotos()
        self.video_collectionview.delegate = nil
        self.video_collectionview.dataSource = nil
        self.tblFeeds.delegate = nil
        self.tblFeeds.dataSource = nil
        self.photo_collectionview.delegate = self
        self.photo_collectionview.dataSource = self
        self.btn_photos.setTitleColor(UIColor.black, for: .normal)
        self.btn_timeline.setTitleColor(UIColor(red: 250.0/255.0, green: 132.0/255.0, blue: 133.0/255.0, alpha: 1.0), for: .normal)
        self.btn_videos.setTitleColor(UIColor(red: 250.0/255.0, green: 132.0/255.0, blue: 133.0/255.0, alpha: 1.0), for: .normal)
        self.timeline_view.isHidden = true
        self.video_view.isHidden = true
        self.photo_view.isHidden = false

    }
    
    @IBAction func btnVideoAction(_ sender: UIButton)
    {
        self.getProfileVideos()
        self.photo_collectionview.delegate = nil
        self.photo_collectionview.dataSource = nil
        self.tblFeeds.delegate = nil
        self.tblFeeds.dataSource = nil
        self.video_collectionview.delegate = self
        self.video_collectionview.dataSource = self
        self.btn_videos.setTitleColor(UIColor.black, for: .normal)
        self.btn_timeline.setTitleColor(UIColor(red: 250.0/255.0, green: 132.0/255.0, blue: 133.0/255.0, alpha: 1.0), for: .normal)
        self.btn_photos.setTitleColor(UIColor(red: 250.0/255.0, green: 132.0/255.0, blue: 133.0/255.0, alpha: 1.0), for: .normal)
        self.timeline_view.isHidden = true
        self.video_view.isHidden = false
        self.photo_view.isHidden = true

    }
    
    
    //Home
    @objc func pullto_reloadData()
    {
        current_page = 1
        self.getProfileTimeline()
    }
    
    @objc func show_tab()
    {
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.3) {
            self.tabBarController?.tabBar.isHidden = false
        }
    }
    
    @objc func show_image(_ notification: NSNotification) {
        print(notification.object ?? "")
        
        let browser = SKPhotoBrowser(photos: notification.object as! [SKPhotoProtocol])

        if let startIndexDict = notification.userInfo as? [String:Int], let startIndex = startIndexDict[SKBrowserStartIndex] {
            browser.initializePageIndex(startIndex)
        }
        present(browser, animated: true, completion: {})
    }
    
    
    @objc func showToolTipMenu(_ sender: UIButton)
    {
        guard (sender.superview?.superview?.superview as? FeedCell) != nil else
        {
            return // or fatalError() or whatever
        }
       // let selected_indexPath =  //tblFeeds.indexPath(for: cell)
        post_select_index =  sender.tag //(selected_indexPath?.row)!
        self.showOptionAlertUserType(btn: sender)
        
    }
    
    
    func showOptionAlertUserType(btn:UIButton)
    {
        guard (btn.superview?.superview?.superview as? FeedCell) != nil else
        {
            return // or fatalError() or whatever
        }
        let selected_indexPath = IndexPath.init(row: btn.tag, section: 0)//tblFeeds.indexPath(for: cell)
        let alert = UIAlertController(title: Constants.APP_NAME, message: "Perform action", preferredStyle: UIAlertControllerStyle.actionSheet)
    
        
        let deletebtn = UIAlertAction(title: "Delete", style: UIAlertActionStyle.default) { (alert) -> Void in
            
            let view = ModalView.instantiateFromNib()
            view.updateModal("Delete Post", descMessage: "Are you sure you want to delete this post?", firstTitle: "YES", secondTitle: "NO", type: .modalAlert)
            let modal = PathDynamicModal()
            modal.showMagnitude = 200.0
            modal.closeMagnitude = 130.0
            modal.closeByTapBackground = false
            modal.closeBySwipeBackground = false
            view.Cancel2ButtonHandler = {[weak modal] in
                modal?.closeWithLeansRandom()
                return
            }
            view.OkButtonHandler = {[weak modal] in
                
                let list_model = self.arrPostList[self.post_select_index]
                if self.arrPostList[btn.tag].groupData != nil
                {
                    
                    self.callDeleteGroupPostAPI(delete_index: Int(list_model.post_id)!)
                }
                else
                {
                    self.callDeletePostAPI(delete_index: Int(list_model.post_id)!)
                }
                modal?.closeWithLeansRandom()
                return
            }
            modal.show(modalView: view, inView: (UIApplication.shared.delegate?.window!)!)
            
            
        }
        
        let cancelButton = UIAlertAction(title: "Cancel", style: UIAlertActionStyle.destructive) { (alert) -> Void in
            //self.txtUserType.text="Trader"
        }
        
        
        let editBtn = UIAlertAction.init(title: "Edit", style: UIAlertActionStyle.default) { (alert) in
            let reportVC = Constants.mainStoryboard.instantiateViewController(withIdentifier:  "EditPostViewController") as! EditPostViewController
                   
            let post = self.arrPostList[btn.tag]
               
                       reportVC.post = post
                       reportVC.indexPath = selected_indexPath
                       self.add(child: reportVC)
        }
        // now hide delete btn if not my post
        if btn.accessibilityElements != nil  {
            //  do not show  report btn as it is my post
            alert.addAction(deletebtn)
            alert.addAction(editBtn)

        }else{
            // show report btn and hide delete btn
            // show report btn
            let reportBtn = UIAlertAction.init(title: "Report", style: UIAlertActionStyle.default) { (alert) in
                self.showReportPostOptionAlert(btn)
            }
            alert.addAction(reportBtn)
        }
       
       // alert.addAction(deletebtn)
        alert.addAction(cancelButton)
        DispatchQueue.main.async {
        self.present(alert, animated: true, completion: nil)
        }
    }
    func showReportPostOptionAlert(_ sender:UIButton){
        let reportOptionAlert = UIAlertController.init(title: Constants.APP_NAME, message: "Report Post", preferredStyle: UIAlertControllerStyle.actionSheet)
        
        
        guard let cell = sender.superview?.superview?.superview as? FeedCell else
              {
                  return // or fatalError() or whatever
              }
              let selected_indexPath = tblFeeds.indexPath(for: cell)
        
        let post = self.arrPostList[selected_indexPath?.row ?? 0 ]
    
        
        
        reportOptionAlert.addAction(UIAlertAction.init(title: "Pretending To Be Someone", style: UIAlertActionStyle.destructive, handler: { (alert) in
            // hit service for report
            ReportNetworkManager.ReportPost(message:"Pretending To Be Someone",postType: post.groupData == nil ? "POST" : "GROUP" , postId: post.post_id, reportType: "1", reportImage: nil, controller: self ) { (flag) in
                // hide port if true
                if flag {
                    DispatchQueue.main.async {
                        if let indexPath = selected_indexPath {
            self.arrPostList.remove(at: indexPath.row  )
                        self.tblFeeds.deleteRows(at: [indexPath], with: UITableViewRowAnimation.automatic)
                    }
                    }
                }
            }
            
        }))
        reportOptionAlert.addAction(UIAlertAction.init(title: "Posted Inappropriate  Material", style: UIAlertActionStyle.destructive, handler: { (alert) in
                   // hit service for report
            ReportNetworkManager.ReportPost(message: "Posted Inappropriate  Material", postType: post.groupData == nil ? "POST" : "GROUP" , postId: post.post_id, reportType: "2", reportImage: nil, controller: self ) { (flag) in
                           // hide port if true
                           if flag {
                               DispatchQueue.main.async {
                                   if let indexPath = selected_indexPath {
                       self.arrPostList.remove(at: indexPath.row  )
                                   self.tblFeeds.deleteRows(at: [indexPath], with: UITableViewRowAnimation.automatic)
                               }
                               }
                           }
                       }
               }))
        reportOptionAlert.addAction(UIAlertAction.init(title: "Inappropriate Profile Picture", style: UIAlertActionStyle.destructive, handler: { (alert) in
                   // hit service for report
            ReportNetworkManager.ReportPost(message: "Inappropriate Profile Picture", postType: post.groupData == nil ? "POST" : "GROUP" , postId: post.post_id, reportType: "3", reportImage: nil, controller: self ) { (flag) in
                           // hide port if true
                           if flag {
                               DispatchQueue.main.async {
                                   if let indexPath = selected_indexPath {
                       self.arrPostList.remove(at: indexPath.row  )
                                   self.tblFeeds.deleteRows(at: [indexPath], with: UITableViewRowAnimation.automatic)
                               }
                               }
                           }
                       }
               }))
        reportOptionAlert.addAction(UIAlertAction.init(title: "Other", style: UIAlertActionStyle.destructive, handler: { (alert) in
                   // show popup to add comment and screenshot and hit service for report
            
            let reportVC = Constants.mainStoryboard.instantiateViewController(withIdentifier:  "ReportViewController") as! ReportViewController
            reportVC.post = post
            reportVC.indexPath = selected_indexPath
            self.add(child: reportVC)

               }))
        reportOptionAlert.addAction(UIAlertAction.init(title: "Cancel", style: UIAlertActionStyle.cancel, handler: { (alert) in
               // do nothing
        
           }))
        DispatchQueue.main.async {
        self.present(reportOptionAlert, animated: true, completion: nil)
        }
    }
    
    @objc func btnDeleteAction(btn:UIButton)
    {
        self.data_arr.remove(at: btn.tag)
        self.tblFeeds.reloadData()
    }
    
    @objc func press_like_btn(btn:UIButton)
    {
        guard let cell = btn.superview?.superview?.superview as? FeedCell else
        {
            return // or fatalError() or whatever
        }
        let selected_indexPath = tblFeeds.indexPath(for: cell)
        
        
        if self.arrPostList[(selected_indexPath?.row)!].groupData != nil
        {
            callLikeGroupPostAPI(post_id: self.arrPostList[(selected_indexPath?.row)!].post_id, select_index: (selected_indexPath?.row)!)
            
        }
        else
        {
            callLikePostAPI(post_id: self.arrPostList[(selected_indexPath?.row)!].post_id, select_index: (selected_indexPath?.row)!)
        }
    }
    
    //MARK:- LikeGroup Post Api
    
    func callLikeGroupPostAPI(post_id:String , select_index:Int)
    {
        let headerString = [
            "Content-Type": "application/json"
        ]
        
        let paramString = ["post_id" : post_id]
        
        Alamofire.request(Constants.BASEURL+MethodName.groupPostLike + "?access-token=\(UserDefaults.standard.value(forKey: "Access_Token") as! String)" , method: .post, parameters: paramString, encoding: JSONEncoding.default, headers: headerString).responseJSON { response in
            
            if let responseDict = response.result.value as? [String:Any]
            {
                if let code:Int = responseDict["code"] as? Int
                {
                    if code == 200
                    {
                        if let data = responseDict["data"] as? [String : Any]
                        {
                            if let msg = data["message"] as? String
                            {
                                //let like_counter = Int(self.arrPostList[select_index].likes) as! Int
                                if (msg=="Liked Successfully")
                                {
                                    let tempDict:PostListModel = self.arrPostList[select_index]
                                    tempDict.likes = "\(Int(self.arrPostList[select_index].likes)!+1)"
                                    tempDict.isLike="1"
                                    self.arrPostList[select_index] = tempDict
                                }
                                else if (msg=="Disliked Successfully")
                                {
                                    let tempDict:PostListModel = self.arrPostList[select_index]
                                    tempDict.likes = "\(Int(self.arrPostList[select_index].likes)!-1)"
                                    tempDict.isLike="0"
                                    self.arrPostList[select_index] = tempDict
                                    
                                }
                                self.tblFeeds.reloadData()
                            }
                        }
                    
                    }
                    else
                    {
                        if let result_error = response.result.value as? [String:Any]
                        {
                            if let error_array = result_error["errorData"] as? NSArray
                            {
                                if let error_dict = error_array[0] as? [String : Any]
                                {
                                    if let error_msg = error_dict["message"] as? String
                                    {
                                          self.view.makeToast(error_msg)
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        
    }
    
    
    //MARK:- LikeSimple Post Api
    
    func callLikePostAPI(post_id:String , select_index:Int)
    {
        if(CommonFunction.isInternetAvailable())
        {
            let headerString = [
                "Content-Type": "application/json"
            ]
            
            Alamofire.request(Constants.BASEURL+MethodName.likePost + "?access-token=\(UserDefaults.standard.value(forKey: "Access_Token") as! String)&id=\(post_id)" , method: .post, parameters: nil, encoding: JSONEncoding.default, headers: headerString).responseJSON { response in
                
                if let responseDict = response.result.value as? [String:Any]
                {
                    if let code:Int = responseDict["code"] as? Int
                    {
                        if code == 200
                        {
                            if let data = responseDict["data"] as? [String : Any]
                            {
                                if let msg = data["message"] as? String
                                {
                                    if (msg=="Liked Successfully")
                                    {
                                        let tempDict:PostListModel = self.arrPostList[select_index]
                                        tempDict.likes = "\(Int(self.arrPostList[select_index].likes)!+1)"
                                        tempDict.isLike="1"
                                        self.arrPostList[select_index] = tempDict
                                        self.tblFeeds.reloadData()
                                    }
                                    else if (msg=="Disliked Successfully")
                                    {
                                        let tempDict:PostListModel = self.arrPostList[select_index]
                                        tempDict.likes = "\(Int(self.arrPostList[select_index].likes)!-1)"
                                        tempDict.isLike="0"
                                        self.arrPostList[select_index] = tempDict
                                        self.tblFeeds.reloadData()
                                    }
                                }
                            }
                        
                        }
                        else
                        {
                            if let result_error = response.result.value as? [String:Any]
                            {
                                if let error_array = result_error["errorData"] as? NSArray
                                {
                                    if let error_dict = error_array[0] as? [String : Any]
                                    {
                                        if let error_msg = error_dict["message"] as? String
                                        {
                                            self.view.makeToast(error_msg)
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        else
        {
            self.view.makeToast("Please Check Your Internet Connection!!")
        }
        
    }
    
    
    @objc func loadMoreComments(btn:UIButton)
    {
        
        let vc = Constants.mainStoryboard2.instantiateViewController(withIdentifier: "CommentsViewController")as! CommentsViewController
        let pid:Int=Int(self.arrPostList[btn.tag].post_id)!
        vc.postId=String(pid)
        
        let comment_count = self.arrPostList[btn.tag].postcomments.count
        if(comment_count>0)
        {
            vc.last_comment_id = self.arrPostList[btn.tag].postcomments[comment_count-1].commentId
        }
        if self.arrPostList[btn.tag].groupData != nil
        {
            vc.comment_type = "Group"
            if let dictGroupDetails = self.arrPostList[btn.tag].groupData
            {
                vc.groupId = Int(dictGroupDetails["groupId"] as! String)!
            }
            
        }
        vc.arrComments=self.arrPostList[btn.tag].postcomments

        self.navigationController?.pushViewController(vc, animated: true)
        
    }
    
    
    //MARK: ------TEXTVIEW DELEGATES-------
    func textViewDidBeginEditing(_ textView: UITextView) {
        textView.text=""
    }
    func textViewDidEndEditing(_ textView: UITextView) {
        if textView.text.count==0
        {
            textView.text="Write something...."
        }
    }
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        if text=="\n"
        {
            textView.resignFirstResponder()
            
        }
        return true
    }
    
    //MARK:- Delete Post Api
    
    func callDeletePostAPI(delete_index : Int)
    {
        if(CommonFunction.isInternetAvailable())
        {
            MBProgressHUD.showAdded(to: self.view, animated: true)
            Alamofire.request(Constants.BASEURL + MethodName.deletePost + "?access-token=\(UserDefaults.standard.value(forKey: "Access_Token") as! String)&id=\(delete_index)", method: .delete, parameters: nil , encoding: JSONEncoding.default, headers:nil).responseJSON
                { response in
                    MBProgressHUD.hide(for: self.view, animated: true)
                    
                    if let result_dict = response.result.value as? [String:Any]
                    {
                        if let code = result_dict["code"] as? Int
                        {
                            if(code==200)
                            {
                                if let smessage = result_dict["status"] as? String
                                {
                                    self.view.makeToast(smessage)
                                }
                                
                                self.arrPostList.remove(at: self.post_select_index)
                                self.tblFeeds.reloadData()
                            }
                            else
                            {
                                if let error_arr = result_dict["errorData"] as? [[String:Any]]
                                {
                                    if let error_msg = error_arr[0]["message"] as? String
                                    {
                                        self.view.makeToast(error_msg)
                                    }
                                }
                            }
                        }
                    }
            }
        }
        else
        {
            self.view.makeToast("Please Check Your Internet Connection!!")
        }
    }
    
    //MARK:- Delete Group Post Api
    
    func callDeleteGroupPostAPI(delete_index : Int)
    {
        MBProgressHUD.showAdded(to: self.view, animated: true)
        Alamofire.request(Constants.BASEURL + MethodName.groupPostDelete + "?access-token=\(UserDefaults.standard.value(forKey: "Access_Token") as! String)&id=\(delete_index)", method: .delete, parameters: nil , encoding: JSONEncoding.default, headers:nil).responseJSON
            { response in
                MBProgressHUD.hide(for: self.view, animated: true)
                
                if let result_dict = response.result.value as? [String:Any]
                {
                    if let code = result_dict["code"] as? Int
                    {
                        if(code==200)
                        {
                            self.view.makeToast("Successfully Deleted")
                            self.arrPostList.remove(at: self.post_select_index)
                            self.tblFeeds.reloadData()
                        }
                        else
                        {
                            if let error_arr = result_dict["errorData"] as? [[String:Any]]
                            {
                                if let error_msg = error_arr[0]["message"] as? String
                                {
                                    self.view.makeToast(error_msg)
                                }
                            }
                            
                        }
                    }
                }
        }
    }
   
    
    //MARK:- View Hire
    
    @IBAction func btnCloseTapped(_ sender: UIButton) {
        
        self.view_hire.isHidden = true
        self.CoachBottomView.isHidden = false
    }
    
    @IBAction func btnAddTimeTapped(_ sender: UIButton)
    {
        
        self.view_hire.isHidden = true
        self.viewTimePicker.isHidden = false
    }
    
    
    //MARK:- Coach Bottom View for user
    
    @IBAction func btnHireMeUserTapped(_ sender: UIButton) {
        dateSelected = Date()
        self.view_hire.isHidden = false
        self.CoachBottomView.isHidden = true
    }
    
    
    //MARK:- Time Picker View
    
    @IBAction func btnTimePickerViewCancelTapped(_ sender: UIButton) {
        
        self.view_hire.isHidden = false
        self.viewTimePicker.isHidden = true
    }
    @IBAction func btnTimePickerViewNextTapped(_ sender: UIButton) {
        
        if !(txt_startTime.text?.isEmpty)! && !(txt_endTime.text?.isEmpty)!
        {
        
            if check_time()
            {
                self.lblTimeBookingConfirmation.text = "\(txt_startTime.text!) - \(txt_endTime.text!)"
                let date_start = stringtoDate(date_str: txt_startTime.text!)
                let date_end = stringtoDate(date_str: txt_endTime.text!)
                
                let difference = Calendar.current.dateComponents([.minute], from: date_start, to: date_end)
                let formattedString = String(format: "%ld", difference.minute!)
                print(formattedString)
                self.lblTotalAmountBookingConfirmation.text = "\(CommonFunction.get_unit(min: Int(formattedString)!) * self.userHourlyRate)"
                self.lblTaxBookingConfirmation.text = "\((CommonFunction.get_unit(min: Int(formattedString)!) * self.userHourlyRate)*0.3)"
                self.viewBookingConfirmation.isHidden = false
                self.viewTimePicker.isHidden = true
            }
            else
            {
                self.view.makeToast("End Date Must After Start Date")
            }
        }
        else
        {
            if (txt_startTime.text?.isEmpty)!
            {
                self.view.makeToast("Please Select Start Time")
            }
            else if (txt_endTime.text?.isEmpty)!
            {
                self.view.makeToast("Please Select End Time")
            }
        }
        
        
    }
    
    
    //MARK:- Booking Confirmation View Button Action
    @IBAction func btnHireMeBookingConfirmationViewTapped(_ sender: UIButton) {
        
        let date_start = stringtoDate(date_str: txt_startTime.text!)
        let date_end = stringtoDate(date_str: txt_endTime.text!)
        
        let difference = Calendar.current.dateComponents([.minute], from: date_start, to: date_end)
        let formattedString = String(format: "%ld", difference.minute!)
        print(formattedString)
        self.lblAmountPaymentView.text = "\(CommonFunction.get_unit(min: Int(formattedString)!) * self.userHourlyRate)"
        self.lblTaxPaymentView.text = "\((CommonFunction.get_unit(min: Int(formattedString)!) * self.userHourlyRate)*0.3)"
        self.totalAmountValue = (CommonFunction.get_unit(min: Int(formattedString)!) * self.userHourlyRate) + ((CommonFunction.get_unit(min: Int(formattedString)!) * self.userHourlyRate)*0.3)
        self.lblTotalAmuntPaymentView.text = "\(totalAmountValue)"
        self.btnPayPaymentView.setTitle("$\(totalAmountValue)", for: .normal)
        self.viewBookingConfirmation.isHidden = true
        self.viewBookingPayment.isHidden = false
        
    }
    @IBAction func btnCancelBookingConfirmationViewTapped(_ sender: UIButton) {
        
        self.viewBookingConfirmation.isHidden = true
        self.CoachBottomView.isHidden = false
    }
    @IBAction func btnBackBookingConfirmationTapped(_ sender: UIButton) {
        
        self.viewBookingConfirmation.isHidden = true
        self.viewTimePicker.isHidden = false
    }
    
    
    
    //MARK:- View Booking Payment
    @IBAction func btnBackgroundPaymentTapped(_ sender: UIButton) {
        
        self.viewBookingPayment.isHidden = true
        self.CoachBottomView.isHidden = false
    }
    @IBAction func btnPayTaped(_ sender: UIButton) {
        
        
        self.callBookCoachAPI()
    }
    
    //MARK:- View Booking Completed
    @IBAction func btnBackgroundBookingCompletedTapped(_ sender: UIButton) {
        
        self.viewBookingCompleted.isHidden = true
        self.CoachBottomView.isHidden = false
    }
    
    @IBAction func btnBookingHistoryCompletedViewTapped(_ sender: UIButton) {
        
        self.viewBookingCompleted.isHidden = true
        self.CoachBottomView.isHidden = false
        let vc = Constants.mainStoryboard.instantiateViewController(withIdentifier: "CoachBookingListViewController") as! CoachBookingListViewController
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    
    //MARK:- Coach Bottom View for Coach
    
    @IBAction func btnMyBookingCoachTapped(_ sender: UIButton) {
        
        let vc = Constants.mainStoryboard.instantiateViewController(withIdentifier: "CoachBookingListViewController") as! CoachBookingListViewController
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    //MARK:- User Bottom View for User
    
    @IBAction func btnMyBookingUserTapped(_ sender: UIButton) {
        
        let vc = Constants.mainStoryboard.instantiateViewController(withIdentifier: "UserBookingListViewController") as! UserBookingListViewController
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    
    func open_time_picker()
    {
        let vc = SambagTimePickerViewController()
        vc.theme = theme
        vc.delegate = self
        present(vc, animated: true, completion: nil)
    }
    func sambagTimePickerDidSet(_ viewController: SambagTimePickerViewController, result: SambagTimePickerResult)
    {
        
        var hour = String()
        var minute = String()
        if result.hour < 10
        {
            hour = "0\(result.hour)"
        }
        else
        {
            hour = "\(result.hour)"
        }
        if result.minute < 10
        {
            minute = "0\(result.minute)"
        }
        else
        {
            minute = "\(result.minute)"
        }
        
        let locale = NSLocale.current
        let formatter : String = DateFormatter.dateFormat(fromTemplate: "j", options:0, locale:locale)!
        if formatter.contains("a")
        {
            tempText.text = "\(hour):\(minute):00 \(result.meridian)"
            
        }
        else
        {
            tempText.text = "\(hour):\(minute):00"
        }
        if Calendar.current.isDateInToday(dateSelected!)
        {
            
            let date = Date()
            let dateFormatter = DateFormatter()
            if(is_24_hour())
            {
                dateFormatter.dateFormat = "HH:mm:ss"
            }
            else
            {
                dateFormatter.dateFormat = "hh:mm:ss a"
            }
            let today_time = dateFormatter.string(from: date).capitalized
            let date_start = stringtoDate(date_str: today_time)
            let date_end = stringtoDate(date_str: tempText.text!)
            if(date_start>date_end)
            {
                tempText.text = nil
                PKSAlertController.alert(Constants.APP_NAME, message: "You can't select past time")
                return
            }
        } 
        if tempText==txt_startTime
        {
            lblBookingDateCompletedView.text = "\(self.lblDateBookingConfirmation.text!) at \(hour):\(minute) \(TimeZone.current.abbreviation()!)"
        }
        self.dismiss(animated: true, completion: nil)
    }
    func is_24_hour()->Bool
    {
        let locale = NSLocale.current
        let formatter : String = DateFormatter.dateFormat(fromTemplate: "j", options:0, locale:locale)!
        if formatter.contains("a")
        {
            return false
            
        }
        else
        {
            return true
        }
    }
    
    func sambagTimePickerDidCancel(_ viewController: SambagTimePickerViewController) {
        viewController.dismiss(animated: true, completion: nil)
    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool
    {
        if(textField==txt_startTime || textField==txt_endTime)
        {
            tempText = textField
            open_time_picker()
            return false
        }
        return true
    }
    
    func check_time() -> Bool
    {
        let date_start = stringtoDate(date_str: txt_startTime.text!)
        let date_end = stringtoDate(date_str: txt_endTime.text!)
        
        let difference = Calendar.current.dateComponents([.minute], from: date_start, to: date_end)
        let formattedString = String(format: "%ld", difference.minute!)
        print(formattedString)
        
        
        switch date_start.compare(date_end) {
        case .orderedAscending     :   return true
        case .orderedDescending    :   return false
        case .orderedSame          :   return true
            
        }
        return false
    }
    func stringtoDate(date_str:String) -> Date
    {
        let dateFormatter = DateFormatter()
        let locale = NSLocale.current
        let formatter : String = DateFormatter.dateFormat(fromTemplate: "j", options:0, locale:locale)!
        if formatter.contains("a") {
            dateFormatter.dateFormat = "hh:mm:ss a"
            dateFormatter.amSymbol = "AM"
            dateFormatter.pmSymbol = "PM"
        } else {
            dateFormatter.dateFormat = "HH:mm:ss"
        }
        
        let dateString =  dateFormatter.date(from: date_str)!
        return dateString
    }
    func convert_24hr_format(dateAsString : String) -> String
    {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "h:mm:ss a"
        dateFormatter.amSymbol = "AM"
        dateFormatter.pmSymbol = "PM"
        
        let date = dateFormatter.date(from: dateAsString)
        dateFormatter.dateFormat = "HH:mm:ss"
        
        return dateFormatter.string(from: date!)
    }
    
    //MARK:- Book Coack Api
    
    func callBookCoachAPI()
    {
        if(CommonFunction.isInternetAvailable())
        {
            let locale = NSLocale.current
            let formatter : String = DateFormatter.dateFormat(fromTemplate: "j", options:0, locale:locale)!
            var param = [String:Any]()
            if formatter.contains("a")
            {
                param = ["coach_id":user_id_str,
                         "booking_date":self.convert_date_string(dateval: dateSelected!, format_str: "YYYY-MM-dd"),
                         "start_time": self.convert_24hr_format(dateAsString: self.txt_startTime.text!),
                         "end_time": self.convert_24hr_format(dateAsString: self.txt_endTime.text!),
                         "rate_per_hour" : "\(self.userHourlyRate)",
                         "service_tax_percentage" : 30,
                         "service_tax_amount" : self.lblTaxPaymentView.text!,
                         "total_amount" : "\(self.totalAmountValue)"]
            }
            else
            {
                 param = ["coach_id":user_id_str,
                             "booking_date":self.convert_date_string(dateval: dateSelected!, format_str: "YYYY-MM-dd"),
                             "start_time":self.txt_startTime.text!,
                             "end_time":self.txt_endTime.text!,
                             "rate_per_hour" : "\(self.userHourlyRate)",
                             "service_tax_percentage" : 30,
                             "service_tax_amount" : self.lblTaxPaymentView.text!,
                             "total_amount" : "\(self.totalAmountValue)"]
            }
            
            MBProgressHUD.showAdded(to: self.view, animated: true)
            Alamofire.request(Constants.BASEURL + MethodName.bookCoach + "?access-token=\(UserDefaults.standard.value(forKey: "Access_Token") as! String)", method: .post, parameters: param , encoding: JSONEncoding.default, headers:nil).responseJSON
                { response in
                    
                    print(Constants.BASEURL + MethodName.bookCoach + "?access-token=\(UserDefaults.standard.value(forKey: "Access_Token") as! String)")
                   
                    
                    MBProgressHUD.hide(for: self.view, animated: true)
                    
                    if let result_dict = response.result.value as? [String:Any]
                    {
                        if let code = result_dict["code"] as? Int
                        {
                            if(code==200)
                            {
                                self.viewBookingPayment.isHidden = true
                                self.viewBookingCompleted.isHidden = false
                            }
                            else
                            {
                                if let error_arr = result_dict["errorData"] as? [[String:Any]]
                                {
                                    if let error_msg = error_arr[0]["message"] as? String
                                    {
                                        self.view.makeToast(error_msg)
                                    }
                                }
                            }
                        }
                    }
            }
        }
        else
        {
            self.view.makeToast("Please Check Your Internet Connection!!")
        }
    }
    
    
    
    //MARK:- View RML Actions
    
    @IBAction func btnAddFriendTapped(_ sender: UIButton) {
        
        if self.request_status == "0"
        {
            self.AddfriendApi(user_id_str : self.user_id_str)
            self.request_status = "2"
        }
        else if self.request_status == "1"
        {
            self.UnfrienApi(user_id_str : self.user_id_str)
            self.request_status = "0"
        }
        else
        {
            self.CancelReqApi(user_id_str : self.user_id_str)
            self.request_status = "0"
        }
        
        
    }
    @IBAction func btnLikeFriendTapped(_ sender: UIButton) {
        
        //self.LikeApi(user_id_str : user_id_str)
        
         let view = AboutMeView.instantiateFromNib()
         view.lblAbout.text = self.aboutmestr.decodeEmoji
        let modal = PathDynamicModal()
        modal.showMagnitude = 200.0
        modal.closeMagnitude = 130.0
        modal.closeByTapBackground = false
        modal.closeBySwipeBackground = false
        view.OkButtonHandler = {[weak modal] in
            
            
            modal?.closeWithLeansRandom()
            return
        }
        modal.show(modalView: view, inView: (UIApplication.shared.delegate?.window!)!)
        
    }
    @IBAction func btnDirectMessageTapped(_ sender: UIButton) {
        
        //Occupant
        var occupantID = Int()
        var occupantName = String()
        
    
        //Occupant Data
        if let occupantID_num = self.quickBloxData["id"] as? Int
        {
            occupantID = occupantID_num
        }
        
        if let occupantName_str = self.quickBloxData["full_name"] as? String
        {
            occupantName = occupantName_str
        }
        
        let chatDialog = QBChatDialog(dialogID: nil, type: QBChatDialogType.private)
        chatDialog.name = occupantName
        chatDialog.occupantIDs = [occupantID] as [NSNumber]
        chatDialog.data = ["class_name": "CustomData","custom_userId" : "\(UserDefaults.standard.value(forKey: Constants.USERID) as! Int),\(self.profile_user_id)"]

        
        QBRequest.createDialog(chatDialog, successBlock: { (response: QBResponse?, createdDialog : QBChatDialog?) -> Void in
            
            self.getDialogs(dialog : createdDialog!)
            
        }) { (response : QBResponse!) -> Void in
            
            print("Error while Creating Dialog!!")
            self.view.makeToast("Error while Creating Dialog!!")
        }
    }
    
    func getDialogs(dialog : QBChatDialog) {
        
        ServicesManager.instance().chatService.allDialogs(withPageLimit: kDialogsPageLimit, extendedRequest: nil, iterationBlock: { (response: QBResponse?, dialogObjects: [QBChatDialog]?, dialogsUsersIDS: Set<NSNumber>?, stop: UnsafeMutablePointer<ObjCBool>) -> Void in
            
        }, completion: { (response: QBResponse?) -> Void in
            
            guard response != nil && response!.isSuccess else {
                return
            }
            
            let chatVC = Constants.mainStoryboard.instantiateViewController(withIdentifier: "ChatViewController") as! ChatViewController
            chatVC.dialog = dialog
            self.navigationController?.pushViewController(chatVC, animated: true)
            
            ServicesManager.instance().lastActivityDate = NSDate()
        })
        //}
    }
    
    
    //MARK:- Add Friend Api
    func AddfriendApi(user_id_str : String)
    {
        if(CommonFunction.isInternetAvailable())
        {
            let params = ["friend_user_id":user_id_str,
                          "user_id":"\(UserDefaults.standard.value(forKey: Constants.USERID) as! Int)"]
            let header_dict = ["Token":"31528198109743225ff9d0cf04d1fdd1",
                               "user_id":"\(UserDefaults.standard.value(forKey: Constants.USERID) as! Int)"]

            Alamofire.request(Constants.BASEURL + MethodName.addFriendRequest + "?access-token=\(UserDefaults.standard.value(forKey: "Access_Token") as! String)" , method: .post, parameters: params, encoding: URLEncoding.default, headers: header_dict).responseJSON { response in

                if let result_dict = response.result.value as? [String:Any]
                {
                    print(result_dict)
                    if(result_dict["code"] as! Int == 200)
                    {
                        self.btnRequest.setBackgroundImage(UIImage(named: "pendingicon"), for: .normal)
                        self.displayAlert(msg: "Friend Request sent Successfully", title_str: Constants.APP_NAME)
                    }
                    else
                    {
                        if let error_arr = result_dict["errorData"] as? [[String:Any]]
                        {
                            if let error_msg = error_arr[0]["message"] as? String
                            {
                                self.view.makeToast(error_msg)
                            }
                        }
                    }
                }
            }
        }
        else
        {
            self.view.makeToast("Please Check Your Internet Connection!!")
        }
    }

    //MARK:- Cancel Request Api
    func CancelReqApi(user_id_str : String)
    {
        if(CommonFunction.isInternetAvailable())
        {
            Alamofire.request(Constants.BASEURL + MethodName.cancelFriendRequest + "?access-token=\(UserDefaults.standard.value(forKey: "Access_Token") as! String)&id=\(user_id_str)" , method: .post, parameters: nil, encoding: URLEncoding.default, headers: ["Content-Type":"application/json"]).responseJSON { response in

                if let result_dict = response.result.value as? [String:Any]
                {
                    print(result_dict)
                    if let code = result_dict["code"] as? Int
                    {
                        if(code==200)
                        {
                            self.btnRequest.setBackgroundImage(UIImage(named: "addFriend"), for: .normal)
                            self.view.makeToast("Cancel request successfully")

                        }
                        else
                        {
                            if let error_arr = result_dict["errorData"] as? [[String:Any]]
                            {
                                if let error_msg = error_arr[0]["message"] as? String
                                {
                                    self.view.makeToast(error_msg)
                                }
                            }
                        }
                    }
                }

            }
        }
        else
        {
            self.view.makeToast("Please Check Your Internet Connection!!")
        }
    }


    //MARK:- Unfriend Api
    func UnfrienApi(user_id_str : String)
    {
        if(CommonFunction.isInternetAvailable())
        {
            print(Constants.BASEURL + MethodName.Unfriend + "?access-token=\(UserDefaults.standard.value(forKey: "Access_Token") as! String)&id=\(user_id_str)")
            Alamofire.request(Constants.BASEURL + MethodName.Unfriend + "?access-token=\(UserDefaults.standard.value(forKey: "Access_Token") as! String)&id=\(user_id_str)" , method: .post, parameters: nil, encoding: JSONEncoding.default, headers: ["Content-Type":"application/json"]).responseJSON { response in

                if let result_dict = response.result.value as? [String:Any]
                {
                    if let code  = result_dict["code"] as? Int
                    {
                        if(code==200)
                        {
                            self.btnRequest.setBackgroundImage(UIImage(named: "addFriend"), for: .normal)
                            self.view.makeToast("Unfriend Successfully")

                        }
                        else
                        {
                            if let error_arr = result_dict["errorData"] as? [[String:Any]]
                            {
                                if let error_msg = error_arr[0]["message"] as? String
                                {
                                    self.view.makeToast(error_msg)
                                }
                            }
                        }
                    }
                }
            }
        }
        else
        {
            self.view.makeToast("Please Check Your Internet Connection!!")
        }
    }


    //MARK:- Like Friend Api
    func LikeApi(user_id_str : String)
    {
        if(CommonFunction.isInternetAvailable())
        {
            let params = ["friend_id":user_id_str]
            Alamofire.request(Constants.BASEURL + MethodName.friendLike + "?access-token=\(UserDefaults.standard.value(forKey: "Access_Token") as! String)" , method: .post, parameters: params, encoding: URLEncoding.default, headers: nil).responseJSON { response in

                if let result_dict = response.result.value as? [String:Any]
                {
                    if(result_dict["code"] as! Int == 200)
                    {
                        if let data_dict = result_dict["data"] as? [String:Any]
                        {
                            if let message_str = data_dict["message"] as? String
                            {
                                if message_str == "Disliked Successfully"
                                {
                                    self.btnLikeFriend.setBackgroundImage(UIImage(named: "likeUnselect"), for: .normal)
                                }
                                else
                                {
                                    self.btnLikeFriend.setBackgroundImage(UIImage(named: "likeSelect"), for: .normal)
                                }
                            }
                        }
                    }
                    else
                    {
                        if let error_arr = result_dict["errorData"] as? [[String:Any]]
                        {
                            if let error_msg = error_arr[0]["message"] as? String
                            {
                                self.view.makeToast(error_msg)
                            }
                        }
                    }
                }
            }
        }
    }
}
    
extension FriendListViewController
{
    
    //MARK:- TableView Delegate Methods
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return self.arrPostList.count
        
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        var cell = FeedCell()
        
            cell=tableView.dequeueReusableCell(withIdentifier: "FeedCell")as! FeedCell
            
            
            //------------
            cell.btnLoadMoreCmnt.tag=indexPath.row
            cell.btnLoadMoreCmnt.addTarget(self, action: #selector(self.loadMoreComments(btn:)), for: .touchUpInside)
            cell.btnComment.tag=indexPath.row
            cell.btnComment.addTarget(self, action: #selector(self.loadMoreComments(btn:)), for: .touchUpInside)
            
            cell.viewLblComment1.layer.cornerRadius=30
            cell.viewLblComment2.layer.cornerRadius=30
            cell.viewLblComment1.clipsToBounds=true
            cell.viewLblComment2.clipsToBounds=true
        
        //more btn
        cell.btnOption.tag=indexPath.row

        cell.btnOption.addTarget(self, action: #selector(showToolTipMenu(_:)), for: .touchUpInside)
        if let cuserid = UserDefaults.standard.value(forKey: Constants.USERID) as? Int
                   {
                       if self.arrPostList[indexPath.row].user_id == cuserid
                       {
                               cell.btnOption.isHidden = false
                           cell.btnOption.accessibilityElements = [true]

                       }
                       else
                       {
                           cell.btnOption.isHidden = false
                           cell.btnOption.accessibilityElements = nil

                       }
                   }
        
           // var isText : Bool = true
            if self.arrPostList[indexPath.row].postcontent.count>0
            {
                for i in 0..<self.arrPostList[indexPath.row].postcontent.count
                {
                    let post : PostContent = self.arrPostList[indexPath.row].postcontent[i]
                    
                    cell.txtPostTitle.text = self.arrPostList[indexPath.row].postTitle?.decodeEmoji
                    let readMoreTextAttributes: [NSAttributedStringKey: Any] = [
                        NSAttributedStringKey.foregroundColor: UIColor(red: 252.0/255.0, green: 89.0/255.0, blue: 138.0/255.0, alpha: 1.0),
                        NSAttributedStringKey.font: UIFont.systemFont(ofSize: 17)
                    ]
                    _ = [
                        NSAttributedStringKey.foregroundColor: UIColor(red: 252.0/255.0, green: 89.0/255.0, blue: 138.0/255.0, alpha: 1.0),
                        NSAttributedStringKey.font: UIFont.systemFont(ofSize: 17)
                    ]
                    cell.txtPostTitle.textColor = UIColor.black
                    cell.txtPostTitle.attributedReadMoreText = NSAttributedString(string: "...more", attributes: readMoreTextAttributes)
                    //cell.txtPostTitle.attributedReadLessText = NSAttributedString(string: "less", attributes: readLessTextAttributes)
                    cell.txtPostTitle.shouldTrim = !expandedCells.contains(indexPath.row)
                    cell.txtPostTitle.setNeedsUpdateTrim()
                    cell.txtPostTitle.layoutIfNeeded()
                    
                    if post.postType == "text"
                    {
                        
                        let result = self.arrPostList[indexPath.row].postTitle?.decodeEmoji.getAllClickableLinks()
                        if let links = result {
                            linksProperties = links

                            if links.count < 1
                            {
                                cell.media_pagecontrol.isHidden = true
                                cell.multimediaCollectionView.isHidden = true
                                cell.previewArea?.isHidden = true
                                cell.btnPreviewLink.isHidden = true
                                cell.previewHeight.constant = 0
                                cell.imagePagerHeight.constant=0
                            }
                            else
                            {
                                if self.arrPostList[indexPath.row].postLink != ""
                                {
                                    cell.media_pagecontrol.isHidden = true
                                    cell.multimediaCollectionView.isHidden = true
                                    cell.previewArea?.isHidden = false
                                    cell.btnPreviewLink.isHidden = false
                                    cell.previewHeight.constant = 171
                                    cell.imagePagerHeight.constant=171
                                    cell.slideshow.image = nil
                                    
                                    cell.btnPreviewLink.addTarget(self, action: #selector(btnPreviewLinkTapped(sender:)), for: .touchUpInside)
                                    
                                    cell.detailedView?.isHidden = true
                                    cell.loaderspinner.beginRefreshing()

                                    if let url = self.slp.extractURL(text: self.arrPostList[indexPath.row].postLink),
                                        let cached = self.slp.cache.slp_getCachedResponse(url: url.absoluteString) {

                                        self.result = cached

                                        if let value: [String] = self.result[.images] as? [String] {

                                            cell.slideshow.image = nil
                                            cell.slideshow.sd_showActivityIndicatorView()

                                            if let img_str = self.result[.image] as? String
                                            {
                                                cell.slideshow.sd_setImage(with: URL(string: img_str), placeholderImage: UIImage.init(),completed: { (image, error, type, url) in
                                                    cell.slideshow.sd_removeActivityIndicator()
                                                })
                                            }
                                            else
                                            {
                                                cell.slideshow.image = UIImage(named: "Placeholder")
                                            }

                                        } else {

                                            cell.slideshow.image = nil
                                            cell.slideshow.sd_showActivityIndicatorView()
                                            if let img_str = self.result[.image] as? String
                                            {
                                                cell.slideshow.sd_setImage(with: URL(string: img_str), placeholderImage: UIImage.init(),completed: { (image, error, type, url) in
                                                    cell.slideshow.sd_removeActivityIndicator()
                                                })
                                            }
                                            else
                                            {
                                                cell.slideshow.image = UIImage(named: "Placeholder")
                                            }
                                        }

                                        if let value: String = self.result[.title] as? String {

                                            cell.previewTitle?.text = value.isEmpty ? "" : value

                                        } else {

                                            cell.previewTitle?.text = ""

                                        }

                                        if let value: String = self.result[.canonicalUrl] as? String {

                                            cell.previewCanonicalUrl?.text = value

                                        }

                                        if let value: String = self.result[.description] as? String {

                                            cell.previewDescription?.text = value.isEmpty ? "" : value

                                        } else {

                                            cell.previewTitle?.text = ""

                                        }

                                        if let value: String = self.result[.icon] as? String, let url = URL(string: value) {
                                            cell.favicon?.af_setImage(withURL: url)
                                        }
                                        else
                                        {
                                            cell.favicon?.image = UIImage(named: "Placeholder")
                                        }

                                        cell.detailedView?.isHidden = false
                                        cell.loaderspinner.endRefreshing()

                                    } else {
                                        self.slp.preview(
                                            self.arrPostList[indexPath.row].postLink,
                                            onSuccess: { result in

                                                result.forEach { print("\($0):", $1) }
                                                self.result = result

                                                if let value: [String] = self.result[.images] as? [String] {

                                                    cell.slideshow.image = nil
                                                    cell.slideshow.sd_showActivityIndicatorView()

                                                    if let img_str = self.result[.image] as? String
                                                    {
                                                        cell.slideshow.sd_setImage(with: URL(string: img_str), placeholderImage: UIImage.init(),completed: { (image, error, type, url) in
                                                            cell.slideshow.sd_removeActivityIndicator()
                                                        })
                                                    }
                                                    else
                                                    {
                                                        cell.slideshow.image = UIImage(named: "Placeholder")
                                                    }

                                                } else {

                                                    cell.slideshow.image = nil

                                                    cell.slideshow.sd_showActivityIndicatorView()
                                                    if let img_str = self.result[.image] as? String
                                                    {
                                                        cell.slideshow.sd_setImage(with: URL(string: img_str), placeholderImage: UIImage.init(),completed: { (image, error, type, url) in
                                                            cell.slideshow.sd_removeActivityIndicator()
                                                        })
                                                    }
                                                    else
                                                    {
                                                        cell.slideshow.image = UIImage(named: "Placeholder")
                                                    }

                                                }

                                                if let value: String = self.result[.title] as? String {

                                                    cell.previewTitle?.text = value.isEmpty ? "" : value

                                                } else {

                                                    cell.previewTitle?.text = ""

                                                }

                                                if let value: String = self.result[.canonicalUrl] as? String {

                                                    cell.previewCanonicalUrl?.text = value

                                                }

                                                if let value: String = self.result[.description] as? String {

                                                    cell.previewDescription?.text = value.isEmpty ? "" : value

                                                } else {

                                                    cell.previewTitle?.text = ""

                                                }

                                                if let value: String = self.result[.icon] as? String, let url = URL(string: value) {
                                                    cell.favicon?.af_setImage(withURL: url)
                                                }
                                                else
                                                {
                                                    cell.favicon?.image = UIImage(named: "Placeholder")
                                                }

                                                cell.detailedView?.isHidden = false
                                                cell.loaderspinner.endRefreshing()
                                        },
                                            onError: { error in

                                                print(error)

                                                Drop.down(error.description, state: .error)

                                        }
                                        )
                                    }
                                }
                                else
                                {
                                    cell.media_pagecontrol.isHidden = true
                                    cell.multimediaCollectionView.isHidden = true
                                    cell.previewArea?.isHidden = true
                                    cell.btnPreviewLink.isHidden = true
                                    cell.previewHeight.constant = 0
                                    cell.imagePagerHeight.constant=0
                                }
                            }
                        }
                        
                    }
                    else if post.postType == "image" || post.postType == "video"
                    {
                        //cell.txtMsg.text = self.arrPostList[indexPath.row].postTitle?.decodeEmoji
                        if self.arrPostList[indexPath.row].postcontent.count > 1
                        {
                            cell.media_pagecontrol.isHidden = false
                        }
                        else
                        {
                            cell.media_pagecontrol.isHidden = true
                        }
                        cell.multimediaCollectionView.isHidden = false
                        cell.previewArea?.isHidden = true
                        cell.btnPreviewLink.isHidden = true
                        cell.previewHeight.constant = 171
                        cell.imagePagerHeight.constant=171
                        cell.layoutIfNeeded()
                        
                        cell.slideshow.image = nil
                    }
                    else
                    {
                        print("")
                    }
                }
            }
            
            let colorPink = UIColor(red: 255.0 / 255.0, green: 235.0 / 255.0, blue: 242.0 / 255.0, alpha: 1.0)
            let colorGray = UIColor(red: 244.0 / 255.0, green: 244.0 / 255.0, blue: 244.0 / 255.0, alpha: 1.0)
            let myUserID : Int = UserDefaults.standard.value(forKey: Constants.USERID) as! Int
            
            if self.arrPostList[indexPath.row].postcomments.count>=2
            {
                cell.viewComment1.isHidden = false
                cell.viewComment2.isHidden = false
                cell.heightViewComment1.constant = 70.0
                cell.heightViewComment2.constant = 70.0
                
                if self.arrPostList[indexPath.row].postcomments.count>2
                {
                    cell.btnLoadMoreCmnt.isHidden = false
                    cell.btnHeightCell.constant = 30
                }
                else
                {
                    cell.btnLoadMoreCmnt.isHidden = true
                    cell.btnHeightCell.constant = 0
                }
                
                
                if Int(self.arrPostList[indexPath.row].postcomments[0].userId)! == myUserID
                {
                    cell.widthCommenter1.constant = 40.0
                    cell.widthCommenter2.constant = 0.0
                    cell.imgSender.sd_setImage(with: URL(string: self.arrPostList[indexPath.row].postcomments[0].userImage), placeholderImage: UIImage(named: "userPlaceHolder"))
                    cell.imgSender.tag = 0
                    self.add_tap_gester_chat(cell.imgSender)
                    cell.viewLblComment1.backgroundColor = colorPink
                }
                else
                {
                    cell.widthCommenter1.constant = 0.0
                    cell.widthCommenter2.constant = 40.0
                    cell.viewLblComment1.backgroundColor = colorGray
                    cell.imgSender2.sd_setImage(with: URL(string: self.arrPostList[indexPath.row].postcomments[0].userImage), placeholderImage: UIImage(named: "userPlaceHolder"))
                    cell.imgSender2.tag = 0
                    self.add_tap_gester_chat(cell.imgSender2)
                    
                }
                
                if Int(self.arrPostList[indexPath.row].postcomments[1].userId)! == myUserID
                {
                    cell.widthCommenter3.constant = 40.0
                    cell.widthCommenter4.constant = 0.0
                    cell.imgRecv2.sd_setImage(with: URL(string: self.arrPostList[indexPath.row].postcomments[1].userImage), placeholderImage: UIImage(named: "userPlaceHolder"))
                    cell.imgRecv2.tag = 1
                    self.add_tap_gester_chat(cell.imgRecv2)
                    cell.viewLblComment2.backgroundColor = colorPink
                }
                else
                {
                    cell.widthCommenter3.constant = 0.0
                    cell.widthCommenter4.constant = 40.0
                    cell.imgRecv.sd_setImage(with: URL(string: self.arrPostList[indexPath.row].postcomments[1].userImage), placeholderImage: UIImage(named: "userPlaceHolder"))
                    cell.imgRecv.tag = 1
                    self.add_tap_gester_chat(cell.imgRecv)
                    cell.viewLblComment2.backgroundColor = colorGray
                }
                
                
                cell.lblSenderMsg.text=self.arrPostList[indexPath.row].postcomments[0].comment.decodeEmoji
                cell.lblSenderName.text = self.arrPostList[indexPath.row].postcomments[0].userName
                cell.lblReciever.text=self.arrPostList[indexPath.row].postcomments[1].comment.decodeEmoji
                cell.lblReciverName.text = self.arrPostList[indexPath.row].postcomments[1].userName
                cell.lblSenderName.tag = 0
                cell.lblReciverName.tag = 1
                self.add_tap_gester_chatlbl(cell.lblSenderName)
                self.add_tap_gester_chatlbl(cell.lblReciverName)
            }
            else if self.arrPostList[indexPath.row].postcomments.count>=1
            {
                cell.viewComment1.isHidden = false
                cell.viewComment2.isHidden = true
                cell.heightViewComment1.constant = 70.0
                cell.heightViewComment2.constant = 0.0
                
                cell.btnLoadMoreCmnt.isHidden = true
                cell.btnHeightCell.constant = 0
                
                if Int(self.arrPostList[indexPath.row].postcomments[0].userId)! == myUserID
                {
                    cell.widthCommenter1.constant = 40.0
                    cell.widthCommenter2.constant = 0.0
                    cell.imgSender.sd_setImage(with: URL(string: self.arrPostList[indexPath.row].postcomments[0].userImage), placeholderImage: UIImage(named: "userPlaceHolder"))
                    cell.imgSender.tag = 0
                    self.add_tap_gester_chat(cell.imgSender)
                    cell.viewLblComment1.backgroundColor = colorPink
                }
                else
                {
                    cell.widthCommenter1.constant = 0.0
                    cell.widthCommenter2.constant = 40.0
                    cell.imgSender2.sd_setImage(with: URL(string: self.arrPostList[indexPath.row].postcomments[0].userImage), placeholderImage: UIImage(named: "userPlaceHolder"))
                    cell.imgSender2.tag = 0
                    self.add_tap_gester_chat(cell.imgSender2)
                    cell.viewLblComment1.backgroundColor = colorGray
                }
                
                cell.lblSenderMsg.text=self.arrPostList[indexPath.row].postcomments[0].comment.decodeEmoji
                cell.lblSenderName.text = self.arrPostList[indexPath.row].postcomments[0].userName
                cell.lblSenderName.tag = 0
                self.add_tap_gester_chatlbl(cell.lblSenderName)
            }
            else
            {
                cell.viewComment1.isHidden = true
                cell.viewComment2.isHidden = true
                cell.heightViewComment1.constant = 0.0
                cell.heightViewComment2.constant = 0.0
                
                cell.btnLoadMoreCmnt.isHidden = true
                cell.btnHeightCell.constant = 0
            }
            cell.btnComment.tag=indexPath.row
            cell.btnLike.tag=indexPath.row
            
            if self.arrPostList[indexPath.row].isLike == "1"
            {
                cell.btnHeart.setImage(UIImage(named: "heart_fill"), for: .normal)
            }
            else
            {
                cell.btnHeart.setImage(UIImage(named: "like"), for: .normal)
            }
        
        
        cell.lblName.text = self.arrPostList[indexPath.row].username
        cell.lblName2.text = self.arrPostList[indexPath.row].username
        
        if self.arrPostList[indexPath.row].groupData != nil
        {
            cell.imgTriangle.isHidden = false
            cell.lblGroupName.isHidden = false
            cell.lblGroupName2.isHidden = false
            if let dictGroupDetails = self.arrPostList[indexPath.row].groupData
            {
                cell.groupNameLabelWidth.constant = (cell.name_view.frame.width - 30)/2
                cell.groupNameLabel2Width.constant = (cell.name_view.frame.width - 30)/2
                cell.lblNameWidth.constant = (cell.name_view.frame.width - 30)/2
                cell.lblName2Width.constant = (cell.name_view.frame.width - 30)/2
                cell.imgTriangleWidth.constant = 20.0
                cell.lblGroupName.layoutIfNeeded()
                cell.lblGroupName2.layoutIfNeeded()
                cell.lblName.layoutIfNeeded()
                cell.lblName2.layoutIfNeeded()
                cell.imgTriangle.layoutIfNeeded()
                
                cell.lblGroupName.text = dictGroupDetails["groupName"] as? String
                cell.lblGroupName2.text = dictGroupDetails["groupName"] as? String
                self.add_tap_gester_Grouplbl(cell.lblGroupName)
                self.add_tap_gester_Grouplbl(cell.lblGroupName2)
                
                if cell.lblName.frame.size.height >  cell.lblGroupName.frame.size.height
                {
                    cell.lblGroupName.isHidden = true
                    cell.lblGroupName2.isHidden = false
                    cell.lblName.isHidden = false
                    cell.lblName2.isHidden = true

                }
                else if cell.lblGroupName.frame.size.height >   cell.lblName.frame.size.height
                {
                    cell.lblName.isHidden = true
                    cell.lblName2.isHidden = false
                    cell.lblGroupName.isHidden = false
                    cell.lblGroupName2.isHidden = true
                }
                else
                {
                    cell.lblGroupName.isHidden = true
                    cell.lblGroupName2.isHidden = false
                    cell.lblName.isHidden = true
                    cell.lblName2.isHidden = false
                }
                
            }
            
        }
        else
        {
            cell.imgTriangle.isHidden = true
            cell.lblGroupName.isHidden = true
            cell.lblGroupName2.isHidden = true
            cell.lblName.isHidden = true
            cell.lblName2.isHidden = false
            cell.groupNameLabelWidth.constant = 0
            cell.groupNameLabel2Width.constant = 0
            cell.lblGroupName.layoutIfNeeded()
            cell.lblGroupName2.layoutIfNeeded()
            cell.imgTriangleWidth.constant = 0
            cell.imgTriangle.layoutIfNeeded()
        }
        
        
        
 
        
//            if self.arrPostList[indexPath.row].groupData != nil
//            {
//                cell.imgTriangle.isHidden = false
//                cell.lblGroupName.isHidden = false
//
//                if let dictGroupDetails = self.arrPostList[indexPath.row].groupData
//                {
//
//                    cell.name_view.layoutIfNeeded()
//
//                    cell.lblNameWidth.constant = (cell.name_view.frame.width - 30)/2
//                    cell.imgTriangleWidth.constant = 20.0
//                    cell.groupNameLabelWidth.constant = (cell.name_view.frame.width - 30)/2
//                    cell.lblGroupName.layoutIfNeeded()
//
//                    cell.lblGroupName.text = "fsdghsj jkbgjkg jbgskdjgb jkbvksdjg kjbdgkjsdgb kjbkjsdgbskjgbkjgbsgjgsgks" //dictGroupDetails["groupName"] as? String
//
//                    cell.lblName.layoutIfNeeded()
//                    cell.imgTriangle.layoutIfNeeded()
//                    cell.lblGroupName.layoutIfNeeded()
//
//                    self.add_tap_gester_Grouplbl(cell.lblGroupName)
//
//                    if cell.lblName.frame.size.height >  cell.lblGroupName.frame.size.height
//                    {
//                        cell.name_view_height.constant = cell.lblName.frame.size.height
//                        cell.name_view.layoutIfNeeded()
//
//                    }
//                    else
//                    {
//                        cell.name_view_height.constant = cell.lblGroupName.frame.size.height
//                        cell.name_view.layoutIfNeeded()
//                    }
//                }
//
//            }
//            else
//            {
//                cell.imgTriangle.isHidden = true
//                cell.lblGroupName.isHidden = true
//                cell.lblGroupName.text = ""
//
//                cell.groupNameLabelWidth.constant = 0.0
//                cell.lblGroupName.layoutIfNeeded()
//
//                cell.imgTriangleWidth.constant = 0.0
//                cell.imgTriangle.layoutIfNeeded()
//
//                cell.lblNameWidth.constant = cell.name_view.frame.width
//                cell.lblName.layoutIfNeeded()
//
//                cell.name_view_height.constant = cell.lblName.frame.size.height
//                cell.name_view.layoutIfNeeded()
//
//            }
        
        
        
        
        
            cell.btnLike.addTarget(self, action: #selector(self.press_like_btn(btn:)), for: .touchUpInside)
        
            cell.setData(data: self.arrPostList[indexPath.row])
            cell.media_pagecontrol.numberOfPages = self.arrPostList[indexPath.row].postcontent.count
            cell.lblTOtalCmnt.text = "(\(self.arrPostList[indexPath.row].comments_count))"
            cell.lblTOtalLike.text = "(\(self.arrPostList[indexPath.row].likes))"
            cell.multimediaCollectionView.reloadData()
        
//            if self.arrPostList[indexPath.row].time != ""
//            {
//
//                let fullDate = "\(self.arrPostList[indexPath.row].date)" + " " + "\(self.arrPostList[indexPath.row].time)"
//
//                let dateFormatter = DateFormatter()
//                dateFormatter.dateFormat = "MM/dd/yyyy HH:mm:ss"
//                if let anAbbreviation = NSTimeZone(abbreviation: "UTC") {
//                    dateFormatter.timeZone = anAbbreviation as TimeZone
//                }
//                let date: Date? = dateFormatter.date(from: fullDate) // create date from string
//
//                // change to a readable time format and change to local time zone
//                dateFormatter.dateFormat = "dd MMM yyyy 'at' h:mm a"
//                dateFormatter.timeZone = NSTimeZone.local
//                var timestamp: String? = nil
//                if let aDate = date {
//                    timestamp = dateFormatter.string(from: aDate)
//                }
//
//                let finaldate = "\(timestamp!)"
//                //print(finaldate)
//
//                cell.lblAuthor.text = finaldate
//            }
        
        
            let dateCreated = self.arrPostList[indexPath.row].created_At
            
            if dateCreated == 0
            {
                cell.lblAuthor.text = ""
            }
            else
            {
                let date = Date(timeIntervalSince1970: Double(dateCreated))
                let dateFormatter = DateFormatter()
                dateFormatter.timeZone = NSTimeZone.local //Set timezone that you want
                dateFormatter.dateFormat = "dd MMM yyyy 'at' h:mm a" //Specify your format that you want
                let strDate = dateFormatter.string(from: date)
                cell.lblAuthor.text = strDate
            }
        
            let imageURL:String = self.arrPostList[indexPath.row].user_image
            cell.imgPoster.sd_showActivityIndicatorView()
            
            self.add_tap_gester_lbl(cell.lblName)
            self.add_tap_gester_lbl(cell.lblName2)
            self.add_tap_gester(cell.imgPoster)
            
            cell.imgPoster.sd_setImage(with: URL.init(string: imageURL), placeholderImage: UIImage.init(named: "user"))
            { (image, error, type, url) in
                cell.imgPoster?.sd_removeActivityIndicator()
            }
            
            
        
        cell.selectionStyle=UITableViewCellSelectionStyle.none
        
        cell.viewMain.layer.borderColor=UIColor.init(red: 249/255, green: 225/255, blue: 233/255, alpha: 1.0).cgColor
        cell.viewMain.layer.borderWidth = 1.0
        cell.viewMain.layer.cornerRadius = 10.0
        cell.viewMain.layer.shadowColor = UIColor(red: 253.0 / 255.0, green: 233.0 / 255.0, blue: 240.0 / 255.0, alpha: 1.0).cgColor
        cell.viewMain.layer.shadowOpacity = 1.0
        cell.viewMain.layer.shadowRadius = 15.0
        cell.viewMain.layer.shadowOffset = CGSize(width: 2.0, height: 2.0)
        return cell
    }

    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        
        let cell:FeedCell = cell as! FeedCell
        
        cell.txtPostTitle.onSizeChange = { [unowned tableView, unowned self] r in
            let point = self.tblFeeds.convert(r.bounds.origin, from: r)
            guard let indexPath = self.tblFeeds.indexPathForRow(at: point) else { return }
            if r.shouldTrim {
                self.expandedCells.remove(indexPath.row)
            } else {
                self.expandedCells.insert(indexPath.row)
            }
            self.tblFeeds.reloadData()
        }
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return 1000.0
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        if(timeline_view.isHidden == false)
        {
            // Dynamic sizing for the header view
            if let headerView = tblFeeds.tableHeaderView {
                let height = headerView.systemLayoutSizeFitting(UILayoutFittingCompressedSize).height
                var headerFrame = viewProfileHeaderMain.frame
                
                // If we don't have this check, viewDidLayoutSubviews() will get
                // repeatedly, causing the app to hang.
                if height != headerFrame.size.height {
                    headerFrame.size.height = height
                    headerView.frame = headerFrame
                    tblFeeds.tableHeaderView = headerView
                }
            }
        }
        
    }
    
    
    @objc func btnPreviewLinkTapped(sender : UIButton)
    {
        guard let cell = sender.superview?.superview?.superview as? FeedCell else
        {
            return
        }
        let selected_indexPath = tblFeeds.indexPath(for: cell)
        
        let safariVC = SFSafariViewController(url: URL(string: self.arrPostList[(selected_indexPath?.row)!].postLink)!)
        present(safariVC, animated: true, completion: nil)
        
    }
    
    func safariViewControllerDidFinish(_ controller: SFSafariViewController) {
        controller.dismiss(animated: true, completion: nil)
    }
    
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
       if (self.tblFeeds.contentOffset.y+10 >= (self.tblFeeds.contentSize.height-10 - self.tblFeeds.bounds.size.height))
                 {
                     if(is_next_page_available)
                     {
                         current_page = current_page + 1
                         getProfileTimeline()
                     }
                 }
    }
    
    
    //MARK:- Gestures Methods
    
    func add_tap_gester_lbl(_ lbl : UILabel)
    {
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.tapFunction))
        lbl.isUserInteractionEnabled = true
        lbl.addGestureRecognizer(tap)
    }
    func add_tap_gester(_ imgview : UIImageView)
    {
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.tapFunction))
        imgview.isUserInteractionEnabled = true
        imgview.addGestureRecognizer(tap)
    }
    
    func add_tap_gester_chatlbl(_ lbl : UILabel)
    {
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.label_tapFunction))
        lbl.isUserInteractionEnabled = true
        lbl.addGestureRecognizer(tap)
    }
    
    func add_tap_gester_Grouplbl(_ lbl : UILabel)
    {
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.label_tapGroupFunction))
        lbl.isUserInteractionEnabled = true
        lbl.addGestureRecognizer(tap)
    }
    
    func add_tap_gester_chat(_ imgview : UIImageView)
    {
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.imageview_tapFunction))
        imgview.isUserInteractionEnabled = true
        imgview.addGestureRecognizer(tap)
    }
    @objc func imageview_tapFunction(sender:UITapGestureRecognizer)
    {
        guard let cell = sender.view?.superview?.superview?.superview?.superview as? FeedCell else
        {
            return
        }
        let selected_indexPath = tblFeeds.indexPath(for: cell)
        self.open_profile(user_id: "\(self.arrPostList[(selected_indexPath?.row)!].postcomments[(sender.view?.tag)!].userId)")
    }
    @objc func tapFunction(sender:UITapGestureRecognizer)
    {
        guard let cell = sender.view?.superview?.superview?.superview?.superview as? FeedCell else
        {
            return
        }
        let selected_indexPath = tblFeeds.indexPath(for: cell)
        self.open_profile(user_id: "\(self.arrPostList[(selected_indexPath?.row)!].user_id!)")
    }
    @objc func label_tapFunction(sender:UITapGestureRecognizer)
    {
        guard let cell = sender.view?.superview?.superview?.superview?.superview?.superview as? FeedCell else
        {
            return // or fatalError() or whatever
        }
        let selected_indexPath = tblFeeds.indexPath(for: cell)
        self.open_profile(user_id: "\(self.arrPostList[(selected_indexPath?.row)!].postcomments[(sender.view?.tag)!].userId)")
    }
    
    
    @objc func label_tapGroupFunction(sender:UITapGestureRecognizer)
    {
        guard let cell = sender.view?.superview?.superview?.superview?.superview as? FeedCell else
        {
            return // or fatalError() or whatever
        }
        let selected_indexPath = tblFeeds.indexPath(for: cell)
        
        //cell.lblGroupName.textColor = UIColor.red
        if let dictGroupDetails = self.arrPostList[(selected_indexPath?.row)!].groupData
        {
            if let group_id = dictGroupDetails["groupId"] as? String
            {
                // Fade out to set the text
                UIView.animate(withDuration: 0.1, delay: 0.0, options: UIViewAnimationOptions.curveEaseOut, animations: {
                    cell.lblGroupName.alpha = 0.0
                    cell.lblGroupName2.alpha = 0.0
                    cell.lblGroupName.textColor = UIColor.red
                    cell.lblGroupName2.textColor = UIColor.red

                }, completion: {
                    (finished: Bool) -> Void in
                    
                    //Once the label is completely invisible, set the text and fade it back in
                    cell.lblGroupName.text = dictGroupDetails["groupName"] as? String
                    cell.lblGroupName2.text = dictGroupDetails["groupName"] as? String
                    
                    // Fade in
                    UIView.animate(withDuration: 0.1, delay: 0.0, options: UIViewAnimationOptions.curveEaseIn, animations: {
                        cell.lblGroupName.alpha = 1.0
                        cell.lblGroupName2.alpha = 1.0
                        cell.lblGroupName.textColor = UIColor.red
                        cell.lblGroupName2.textColor = UIColor.red
                    }, completion: {
                        (value: Bool) in
                        
                        cell.lblGroupName.textColor = UIColor.black
                        cell.lblGroupName2.textColor = UIColor.black
                        let storyBoard = UIStoryboard(name: "Main", bundle: nil)
                        let objGroupDetailViewController = storyBoard.instantiateViewController(withIdentifier: "GroupDetailViewController") as! GroupDetailViewController
                        objGroupDetailViewController.groupID = Int(group_id)!
                        //cell.lblGroupName.textColor = UIColor.red
                        self.navigationController?.pushViewController(objGroupDetailViewController, animated: true)
                        
                        })
                    
                })
        
                
            }
                
        }
    }
    
    func open_profile(user_id:String)
    {
        let profile_page = Constants.mainStoryboard.instantiateViewController(withIdentifier: "FriendListViewController") as! FriendListViewController
        profile_page.user_id_str = user_id
        self.navigationController?.pushViewController(profile_page, animated: true)
    }
    
    
    func convertStringToDictionary(text: String) -> [String: Any]? {
        if let data = text.data(using: .utf8) {
            do {
                return try JSONSerialization.jsonObject(with: data, options: []) as? [String: Any]
            } catch {
                print(error.localizedDescription)
            }
        }
        return nil
    }
    
   
}

