//
//  AboutMeView.swift
//  Techmea
//
//  Created by Dhaval Panchani on 17/01/19.
//  Copyright © 2019 VISHAL SETH. All rights reserved.
//

import UIKit

class AboutMeView: UIView {
     var OkButtonHandler: (() -> Void)?
    
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblAbout: UITextView!
    @IBOutlet weak var innerView: UIView!
    @IBOutlet weak var btnClose: UIButton!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.innerView.layer.cornerRadius = 15
        
    }
    
    class func instantiateFromNib() -> AboutMeView {
        let view = UINib(nibName: "AboutMeView", bundle: nil).instantiate(withOwner: nil, options: nil).first as! AboutMeView
        
        return view
    }
    
    
    @IBAction func btnCloseTapped(_ sender: UIButton) {
        
        self.OkButtonHandler?()
    }
}
