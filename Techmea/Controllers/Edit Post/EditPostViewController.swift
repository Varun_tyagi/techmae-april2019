//
//  EditPostViewController.swift
//  Techmea
//
//  Created by varun tyagi on 08/12/19.
//  Copyright © 2019 VISHAL SETH. All rights reserved.
//

import UIKit
import MobileCoreServices
import MBProgressHUD
import SDWebImage
import Alamofire
import AVKit
import AVFoundation
import Photos
import Toast_Swift
import SwiftyDrop
import ImageSlideshow
import SwiftLinkPreview
import JTMaterialSpinner
import SKPhotoBrowser
import ReadMoreTextView
import SafariServices
import FirebaseMessaging


let isOlderPostImage = "olderPostImage"
class EditPostViewController: UIViewController,UINavigationControllerDelegate,UIImagePickerControllerDelegate,UITextViewDelegate,UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout,SKPhotoBrowserDelegate,SFSafariViewControllerDelegate,QMChatServiceDelegate, QMChatConnectionDelegate, QMAuthServiceDelegate
{
    
    //MARK:- Home Outlets
    var data_arr = [[String:Any]]()
    var arrPostList = [PostListModel]()
    var collectionInitialized:Bool = false
    var post_select_index = Int()
    @IBOutlet weak var tblFeeds: UITableView!
    var total_no_of_page = Int()
    var current_page = 1
    var SelectedImage:UIImage? = nil
    var selectedIndex:Int=0
    var post_type = "text"
    
    var refreshControl = UIRefreshControl()
    var is_picker_open = false
    var perpage_data_count = Int()
    var is_next_page_available = Bool()
    
    var  singleTap:UITapGestureRecognizer!
    
    var post:PostListModel?
    var indexPath:IndexPath?
    var isGroupPost = false
    var postMediaView:[[String:Any]] = []
    var dataToDelIdArr:[Int] = []
    //Link Outlets
     var result = SwiftLinkPreview.Response()
     let placeholderImages = [ImageSource(image: UIImage(named: "Placeholder")!)]
    
     let slp = SwiftLinkPreview(cache: InMemoryCache())
    
    var finalMessage = String()

    //----OUTLETS OF FIRST CELL
    @IBOutlet weak var btnSend: UIButton!
    @IBOutlet weak var btnVideo: UIButton!
    @IBOutlet weak var btnCamera: UIButton!
    @IBOutlet weak var viewMain: UIView!
    @IBOutlet weak var txtMessage: UITextView!
    @IBOutlet weak var collectionPageHeight: NSLayoutConstraint!
    
    @IBOutlet weak var previewView: UIView!
    @IBOutlet weak var previewImageView: UIImageView!
    @IBOutlet weak var previewTitle_lbl: UILabel!
    @IBOutlet weak var previewDesc_lbl: UILabel!
    @IBOutlet weak var previewView_height: NSLayoutConstraint!
    @IBOutlet weak var centerActivityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var collectionPostFeed: UICollectionView!
    @IBOutlet weak var previewloader: JTMaterialSpinner!
    
    @IBOutlet weak var viewParent: UIView!
    @IBOutlet weak var btnPreviewClose: UIButton!
    
    @IBOutlet weak var innerPreviewView: UIView!
    
    var isFirstTime = true
    var linkPreviewVisible = false
    var postLink = String()
    
    var isDeteced = false
    var linksProperties: [NSTextCheckingResult]?
    
    @IBOutlet weak var lblNotificationCount: UILabel!
    
    var expandedCells = Set<Int>()
    
    //Heart
    private struct HeartAttributes {
        static let heartSize: CGFloat = 36
        static let burstDelay: TimeInterval = 0.1
    }
    private var observer: NSObjectProtocol?

    var burstTimer: Timer?

    func getDialogs() {
        
        ServicesManager.instance().chatService.allDialogs(withPageLimit: kDialogsPageLimit, extendedRequest: nil, iterationBlock: { (response: QBResponse?, dialogObjects: [QBChatDialog]?, dialogsUsersIDS: Set<NSNumber>?, stop: UnsafeMutablePointer<ObjCBool>) -> Void in
            
           
        }, completion: { (response: QBResponse?) -> Void in
            
            guard response != nil && response!.isSuccess else {
                return
            }
           let _ =   self.dialogs()
            ServicesManager.instance().lastActivityDate = NSDate()
        })
        //}
    }
    
    // MARK: - DataSource
    
    func dialogs() -> [QBChatDialog]? {
        
        
        let dialogsArray = ServicesManager.instance().chatService.dialogsMemoryStorage.dialogsSortByUpdatedAt(withAscending: false)
        
        var unreadCount=0
        for messageDialog in dialogsArray{
            if messageDialog.unreadMessagesCount>0{
                unreadCount = unreadCount + 1
            }
        }
        
        if let tabItems = tabBarController?.tabBar.items {
            // In this case we want to modify the badge number of the third tab:
            let tabItem = tabItems[3]
            if unreadCount > 0 {
                tabItem.badgeValue = "\(unreadCount)"
            }else{
                tabItem.badgeValue = nil
            }
        }
        
        return dialogsArray
    }
    
    //MARK:- viewDidLoads
    override func viewDidLoad()
    {
        super.viewDidLoad()
       
        
        if self.post?.groupData != nil{
            self.isGroupPost=true
        }
        self.callPostDataApi()
        // Static setup
        SKPhotoBrowserOptions.displayAction = true
        SKPhotoBrowserOptions.displayStatusbar = true
        SKPhotoBrowserOptions.displayCounterLabel = true
        SKPhotoBrowserOptions.displayBackAndForwardButton = true
        SKPhotoBrowserOptions.displayCloseButton = false
        SKPhotoBrowserOptions.displayAction = false
                
        self.collectionPostFeed.dataSource=self
        self.collectionPostFeed.delegate=self
        self.viewMain.layer.borderColor=UIColor.init(red: 249/255, green: 225/255, blue: 233/255, alpha: 1.0).cgColor
        self.viewMain.layer.borderWidth = 1.0
        self.viewMain.layer.cornerRadius = 10.0
        self.viewMain.layer.shadowColor = UIColor(red: 253.0 / 255.0, green: 233.0 / 255.0, blue: 240.0 / 255.0, alpha: 1.0).cgColor
        self.viewMain.layer.shadowOpacity = 1.0
        self.viewMain.layer.shadowRadius = 15.0
        self.viewMain.layer.shadowOffset = CGSize(width: 2.0, height: 2.0)
        
        
        self.innerPreviewView.layer.borderColor=UIColor.init(red: 249/255, green: 225/255, blue: 233/255, alpha: 1.0).cgColor
        self.innerPreviewView.layer.borderWidth = 1.0
        self.innerPreviewView.layer.cornerRadius = 10.0

        
        self.txtMessage.text =  self.post?.postTitle ?? "" + " "
        self.CheckURL(self.txtMessage)
        self.checkforImagesVideo()
        self.setupPostData()
        
       
    
    }
    
    func checkforImagesVideo(){
        if self.post!.postcontent.count > 0 {
           
        for contentPost in self.post!.postcontent{
                                   var dataDict = [String:Any]()
                                   dataDict["media_type"]  = contentPost.postType
            dataDict["Preview"] = ""
            dataDict["previewImageUrl"] = contentPost.post
                                   
                                   if self.data_arr.count < 5 &&  contentPost.postType != "text"
                                   {
                                    dataDict[isOlderPostImage] = true
                                       self.data_arr.append(dataDict)
                                   }
                                   else
                                   {
                                       self.view.makeToast("Maximum five images/videos you can upload at a time")
                                   }
        }
    }
        self.collectionPostFeed.reloadData()
    }
    
    func CheckURL(_ textView:UITextView){
        let text = textView.text ?? " "
        if  !linkPreviewVisible
         {
            if text.contains(" ") || text.contains("http") || text.contains("://") || text.contains("\n") || text.contains("\t")
            {
                self.finalMessage = textView.text
                if text.contains("http") || text.contains("://") || text.contains("\n") || text.contains("\t"){
                    self.finalMessage = textView.text + text

                    self.finalMessage = " " + self.finalMessage + " "

                }
                
                 let textInputContent = self.finalMessage
                
                let result = textInputContent.getAllClickableLinks()
                if let links = result {
                    linksProperties = links
                    for link in links {
                        let content = textInputContent as NSString
                        let value = content.substring(with: link.range)
                        
                        if isFirstTime == true
                        {
                            if link == links.first
                            {
                                self.finalMessage = completeURL(url : "\(value)")
                                self.postLink = completeURL(url : "\(value)")
                                self.setupPostData()
                                break
                            }
                        }
                        else
                        {
                            if link == links.last
                            {
                                self.finalMessage = completeURL(url : "\(value)")
                                self.postLink = completeURL(url : "\(value)")
                                self.setupPostData()
                                break
                            }
                        }
                        
                    }
                }
                
                
                
            }
            else
            {
                self.finalMessage = textView.text
            }
        }
    }
    
    func callPostDataApi()
    {
        if(CommonFunction.isInternetAvailable())
        {
            //let params = ["user_id":"\(UserDefaults.standard.value(forKey: Constants.USERID))"]
            let apiURl  = "\(Constants.BASEURL)\(self.isGroupPost==false ? MethodName.getPostDetailView(postId: self.post?.post_id ?? "0") : MethodName.getGroupPostDetailView(postId: self.post?.post_id ?? "0"))"
            MBProgressHUD.showAdded(to: self.view, animated: true)
            Alamofire.request(apiURl, method: .get, parameters: nil, encoding: JSONEncoding.default, headers: nil).responseJSON { response in
                MBProgressHUD.hide(for: self.view, animated: true)
                if let result_dict = response.result.value as? [String:Any]
                {
                    if(result_dict["code"] as! Int == 200)
                    {
                        if let user_arr = result_dict["data"] as? [String:Any]
                        {
                           // self.friendListArr = user_arr
                            print(user_arr)
                            if let postMediaViewArr = user_arr["postMediaView"] as? [[String:Any]]{
                                self.postMediaView = postMediaViewArr
                            }
                        }
                       // self.tblUser.reloadData()
                    }
                    else
                    {
                        if let error_arr = result_dict["errorData"] as? [[String:Any]]
                        {
                            if let error_msg = error_arr[0]["message"] as? String
                            {
                                self.view.makeToast(error_msg)
                            }
                        }
                    }
                }
            }
        }
    }
    
    //MARK: Button Actions
    
    @IBAction func closeAction(_ sender: Any) {
        //self.navigationController?.popViewController(animated: false)
        self.remove()
    }
    
    func initialSetup(collectionSlider:UICollectionView)
    {
        if self.collectionInitialized==false
        {
            self.collectionInitialized=true
            let cellWidth : CGFloat = 80
            let cellheight : CGFloat = 80
            let cellSize = CGSize(width: cellWidth , height:cellheight)
            
            let layout = UICollectionViewFlowLayout()
            layout.scrollDirection = .horizontal
            layout.itemSize = cellSize
            layout.sectionInset = UIEdgeInsets(top: 1, left: 1, bottom: 1, right: 2)
            layout.minimumLineSpacing = 1.0
            layout.minimumInteritemSpacing = 1.0
            collectionSlider.setCollectionViewLayout(layout, animated: true)
            collectionSlider.reloadData()
            collectionSlider.isPagingEnabled=true
        }
        else
        {
            collectionSlider.reloadData()
        }
        
    }
    
    
   
   
   
    
    func showAlertUserType(media:String)
    {
        
        let alert = UIAlertController(title: Constants.APP_NAME, message: "Browse \(media) using", preferredStyle: UIAlertControllerStyle.actionSheet)
        
        let galleryBtn = UIAlertAction(title: "Gallery", style: UIAlertActionStyle.default) { (alert) -> Void in
            self.openGallery(type: media)
            self.btnCamera.setImage(UIImage(named: "camera"), for: .normal)
            self.btnVideo.setImage(UIImage(named: "video_camera"), for: .normal)
        }
        
        let cameraBtn = UIAlertAction(title: "Camera", style: UIAlertActionStyle.default) { (alert) -> Void in
            self.openCamera(type: media)
            self.btnCamera.setImage(UIImage(named: "camera"), for: .normal)
            self.btnVideo.setImage(UIImage(named: "video_camera"), for: .normal)
        }
        
        let cancelButton = UIAlertAction(title: "Cancel", style: UIAlertActionStyle.destructive) { (alert) -> Void in
            //self.txtUserType.text="Trader"
            self.btnCamera.setImage(UIImage(named: "camera"), for: .normal)
            self.btnVideo.setImage(UIImage(named: "video_camera"), for: .normal)
        }
        
        alert.addAction(galleryBtn)
        alert.addAction(cameraBtn)
        alert.addAction(cancelButton)
        DispatchQueue.main.async {
        self.present(alert, animated: true, completion: nil)
        }
    }
    
    
    //MARK:- Camera Video Action
    @IBAction func btnCameraAction(_ sender: Any)
    {
        self.view.endEditing(true)
        self.btnCamera.setImage(UIImage(named: "ic_camera"), for: .normal)
        post_type = "image"
        self.showAlertUserType(media: "Photo")
    }
    @IBAction func btnVideoAction(_ sender: Any)
    {
        self.view.endEditing(true)
        self.btnVideo.setImage(UIImage(named: "ic_videocam"), for: .normal)
        post_type = "video"
        showAlertUserType(media: "Video")
    }
    
    //MARK:- Open Camera
    func openCamera(type:String)
    {
        if(Platform.isSimulator)
        {
            //self.displayAlert(msg: "No camera available", title_str: Constants.APP_NAME)
            self.view.makeToast("No camera available")
        }
        else
        {
            //let authorizationStatus = AVCaptureDevice.authorizationStatus(for: AVMediaType.video)
            AVCaptureDevice.requestAccess(for: AVMediaType.video, completionHandler: { (granted :Bool) -> Void in
                if granted == true
                {
                    self.is_picker_open = true
                    let cameraController = UIImagePickerController()
                    cameraController.sourceType = UIImagePickerControllerSourceType.camera
                    cameraController.delegate = self
                    if type == "Photo"
                    {
                        cameraController.mediaTypes = ["public.image"]
                    }
                    else
                    {
                        cameraController.mediaTypes = ["public.movie"]
                    }
                    cameraController.isEditing = true
                    cameraController.videoMaximumDuration = 60
                    DispatchQueue.main.async {
                    self.present(cameraController, animated: true, completion: nil)
                    }
                }
                else
                {
                    self.show_deny_alert(msg: "You deny to use camera.To allow camera you should allow camera permission from setting.")
                    
                }
            });
        }
    }
    func show_deny_alert(msg:String)
    {
        let alert = UIAlertController(title: Constants.APP_NAME, message: msg, preferredStyle: UIAlertControllerStyle.alert)
        
        let okBtn = UIAlertAction(title: "OK", style: UIAlertActionStyle.default) { (alert) -> Void in
            guard let settingsUrl = URL(string: UIApplicationOpenSettingsURLString) else {
                return
            }
            
            if UIApplication.shared.canOpenURL(settingsUrl) {
                UIApplication.shared.open(settingsUrl, completionHandler: { (success) in
                    print("Settings opened: \(success)") // Prints true
                })
            }
        }
        alert.addAction(okBtn)
        DispatchQueue.main.async {

        self.present(alert, animated: true, completion: nil)
        }
    }
    
    //MARK:- Open Gallery
    func openGallery(type:String)
    {
        PHPhotoLibrary.requestAuthorization { (status) in
            let status = PHPhotoLibrary.authorizationStatus()
            print(status)
            if (status == PHAuthorizationStatus.authorized)
            {
                self.is_picker_open = true
                var config = TatsiConfig.default
                config.showCameraOption = false
                config.supportedMediaTypes = [ .image]
                config.firstView = .userLibrary
                config.maxNumberOfSelections = 1

                
                let pickerViewController = TatsiPickerViewController(config: config)
                pickerViewController.pickerDelegate = self
                DispatchQueue.main.async {
                self.present(pickerViewController, animated: true, completion: nil)
                }
            }
            else
            {
                self.show_deny_alert(msg: "You deny to use photo library .To allow photo library you should allow photos permission from setting.")
            }
        }
        
        
    }
    
    
    //MARK:- didFinishPickingMediaWithInfo
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any])
    {
        if let chosenImage = info[UIImagePickerControllerOriginalImage] as? UIImage
        {
            var dataDict = [String:Any]()
            dataDict["media_type"]  = "image"
            dataDict["data_str"]  = UIImageJPEGRepresentation(chosenImage, 0.5)
            dataDict["Preview"] = UIImage(data: UIImageJPEGRepresentation(chosenImage, 0.3)!)
            dataDict["URLPath"] = ""
            dataDict["previewImageUrl"] =  ""

            if self.data_arr.count < 5
            {
                
                dataDict[isOlderPostImage] = false
                self.data_arr.append(dataDict)
            }
            else
            {
                self.view.makeToast("Maximum five images/videos you can upload at a time")
            }
            
        }
        else
        {
            if let videofile = info[UIImagePickerControllerMediaURL] as? URL
            {
                var dataDict = [String:Any]()
                dataDict["media_type"]  = "video"
                dataDict["Preview"] = getThumbnailImage(forUrl: videofile)
                if let videoData = try? Data(contentsOf: videofile)
                {
                    dataDict["data_str"] =  videoData
                }
                dataDict["URLPath"] = videofile
                dataDict["previewImageUrl"] =  ""

                if self.data_arr.count < 5
                {
                    dataDict[isOlderPostImage] = false

                    self.data_arr.append(dataDict)
                }
                else
                {
                   self.view.makeToast("Maximum five images/videos you can upload at a time")
                }
            }
        }
        picker.dismiss(animated: true, completion: {
            
            self.collectionPostFeed.reloadData()
            
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.1, execute: {
                self.setupPostData()
            })
        })
        
       
    }
    
    //MARK:- encodeVideo
    func encodeVideo(at videoURL: URL, completionHandler: ((URL?, Error?) -> Void)?)  {
        let avAsset = AVURLAsset(url: videoURL, options: nil)
        
        let startDate = Date()
        
        //Create Export session
        guard let exportSession = AVAssetExportSession(asset: avAsset, presetName: AVAssetExportPresetPassthrough) else {
            completionHandler?(nil, nil)
            return
        }
        
        //Creating temp path to save the converted video
        let documentsDirectory = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)[0] as URL
        let filePath = documentsDirectory.appendingPathComponent("rendered-Video.mp4")
        
        //Check if the file already exists then remove the previous file
        if FileManager.default.fileExists(atPath: filePath.path) {
            do {
                try FileManager.default.removeItem(at: filePath)
            } catch {
                completionHandler?(nil, error)
            }
        }
        
        exportSession.outputURL = filePath
        exportSession.outputFileType = AVFileType.mp4
        exportSession.shouldOptimizeForNetworkUse = true
        let start = CMTimeMakeWithSeconds(0.0, 0)
        let range = CMTimeRangeMake(start, avAsset.duration)
        exportSession.timeRange = range
        
        exportSession.exportAsynchronously(completionHandler: {() -> Void in
            switch exportSession.status {
            case .failed:
                print(exportSession.error ?? "NO ERROR")
                completionHandler?(nil, exportSession.error)
            case .cancelled:
                print("Export canceled")
                completionHandler?(nil, nil)
            case .completed:
                //Video conversion finished
                let endDate = Date()
                
                let time = endDate.timeIntervalSince(startDate)
                print(time)
                print("Successful!")
                print(exportSession.outputURL ?? "NO OUTPUT URL")
                completionHandler?(exportSession.outputURL, nil)
                if (exportSession.outputURL as? URL) != nil
                {
                    self.getThumbnailImage(forUrl: exportSession.outputURL!)
                }
                
            default: break
            }
            
        })
    }
    func getThumbnailImage(forUrl url: URL) -> UIImage
    {
        let img = UIImage()
        
        let asset: AVAsset = AVAsset(url: url)
        let imageGenerator = AVAssetImageGenerator(asset: asset)
        imageGenerator.appliesPreferredTrackTransform = true
        do {
            
            let thumbnailImage = try imageGenerator.copyCGImage(at: CMTimeMake(1, 2) , actualTime: nil)
            DispatchQueue.main.async
            {
                self.collectionPostFeed.reloadData()
            }
            return UIImage.init(cgImage: thumbnailImage)
            
        }
        catch let error
        {
            print(error)
            return img
        }
        return img
       
    }
    
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        picker.dismiss(animated: true, completion: nil)
    }
    
    //MARK: TextView delegates
    func textViewDidBeginEditing(_ textView: UITextView) {
        if textView.text == "Write Something...."
        {
            textView.text=""
        }
    }
    func textViewDidEndEditing(_ textView: UITextView) {
        if textView.text.count==0
        {
            textView.text="Write Something...."
        }
        
        

    
    }
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {

        
     if  !linkPreviewVisible
     {
        if text.contains(" ") || text.contains("http") || text.contains("://") || text.contains("\n") || text.contains("\t")
        {
            self.finalMessage = textView.text
            if text.contains("http") || text.contains("://") || text.contains("\n") || text.contains("\t"){
                self.finalMessage = textView.text + text

                self.finalMessage = " " + self.finalMessage + " "

            }
            
             let textInputContent = self.finalMessage
            
            let result = textInputContent.getAllClickableLinks()
            if let links = result {
                linksProperties = links
                for link in links {
                    let content = textInputContent as NSString
                    let value = content.substring(with: link.range)
                    
                    if isFirstTime == true
                    {
                        if link == links.first
                        {
                            self.finalMessage = completeURL(url : "\(value)")
                            self.postLink = completeURL(url : "\(value)")
                            self.setupPostData()
                            break
                        }
                    }
                    else
                    {
                        if link == links.last
                        {
                            self.finalMessage = completeURL(url : "\(value)")
                            self.postLink = completeURL(url : "\(value)")
                            self.setupPostData()
                            break
                        }
                    }
                    
                }
            }
            
            
            
        }
        else
        {
            self.finalMessage = textView.text
        }
    }
        
        //tblFeeds.reloadRows(at: [IndexPath.init(row: 0, section: 0)], with: .automatic)
        return true
    }
    
    
    func completeURL(url : String) -> String
    {
        var newUrl = url
        
        
        if (newUrl.lowercased().hasPrefix("https://www.") || newUrl.lowercased().hasPrefix("http://www.") || newUrl.lowercased().hasPrefix("http://") || newUrl.lowercased().hasPrefix("https://")){
            
            return newUrl
        }
        else if (newUrl.lowercased().hasPrefix("www.") || newUrl.lowercased().hasPrefix("www."))
        {
            newUrl = "https://" + newUrl
            
            return newUrl
        }
        else
        {
            newUrl = "https://www." + newUrl
            return newUrl
        }
    
        //return newUrl
    }
    @IBAction func btnPreviewCloseTapped(_ sender: UIButton) {
        
        isFirstTime = false
        linkPreviewVisible = false
        self.postLink = ""
        self.previewView.isHidden = true
        self.collectionPostFeed.isHidden = true
        self.viewParent.frame.size.height = 148
        self.previewView_height.constant = 0
        self.collectionPageHeight.constant = 0
        self.tblFeeds.reloadData()
        self.view.layoutIfNeeded()
    }
    
    //MARK: Button Search Action
    @IBAction func btnSearchAction(_ sender: UIButton) {
        let searchPage = self.storyboard?.instantiateViewController(withIdentifier: "SearchFriendViewController") as! SearchFriendViewController
        self.navigationController?.pushViewController(searchPage, animated: false)
    }
    
    
    //MARK: Delete Post Api
    func callDeletePostAPI(delete_index : Int)
    {
        if(CommonFunction.isInternetAvailable())
        {
            MBProgressHUD.showAdded(to: self.view, animated: true)
            Alamofire.request(Constants.BASEURL + MethodName.deletePost + "?access-token=\(UserDefaults.standard.value(forKey: "Access_Token") as! String)&id=\(delete_index)", method: .delete, parameters: nil , encoding: JSONEncoding.default, headers:nil).responseJSON
            { response in
                MBProgressHUD.hide(for: self.view, animated: true)
                
                if let result_dict = response.result.value as? [String:Any]
                {
                    if let code = result_dict["code"] as? Int
                    {
                        if(code==200)
                        {
                            self.displayAlert(msg: (result_dict["data"] as! [String:Any])["message"] as! String, title_str: Constants.APP_NAME)
                            self.arrPostList.remove(at: self.post_select_index)
                            self.tblFeeds.reloadData()
                        }
                        else
                        {
                            if let error_arr = result_dict["errorData"] as? [[String:Any]]
                            {
                                if let error_msg = error_arr[0]["message"] as? String
                                {
                                    self.view.makeToast(error_msg)
                                }
                            }
                        }
                    }
                }
            }
        }
        else
        {
            self.view.makeToast("Please Check Your Internet Connection!!")
        }
    }
    
    //MARK: Delete Group Post Api
    func callDeleteGroupPostAPI(delete_index : Int)
    {
        MBProgressHUD.showAdded(to: self.view, animated: true)
        Alamofire.request(Constants.BASEURL + MethodName.groupPostDelete + "?access-token=\(UserDefaults.standard.value(forKey: "Access_Token") as! String)&id=\(delete_index)", method: .delete, parameters: nil , encoding: JSONEncoding.default, headers:nil).responseJSON
            { response in
                MBProgressHUD.hide(for: self.view, animated: true)
                
                if let result_dict = response.result.value as? [String:Any]
                {
                    if let code = result_dict["code"] as? Int
                    {
                        if(code==200)
                        {
                            self.displayAlert(msg: (result_dict["data"] as! [String:Any])["message"] as! String, title_str: Constants.APP_NAME)
                            self.arrPostList.remove(at: self.post_select_index)
                            self.tblFeeds.reloadData()
                        }
                        else
                        {
                            if let error_arr = result_dict["errorData"] as? [[String:Any]]
                            {
                                if let error_msg = error_arr[0]["message"] as? String
                                {
                                    self.view.makeToast(error_msg)
                                }
                            }
                            
                        }
                    }
                }
        }
    }
    
    
    func setupPostData()
    {
       
        //self.initialSetup(collectionSlider: self.collectionPostFeed)
        
        if self.data_arr.count>0
        {
            self.postLink = ""
            self.linkPreviewVisible = false
            self.previewView.isHidden = true
            self.collectionPostFeed.isHidden = false
            self.viewParent.frame.size.height = 170 + self.collectionPostFeed.collectionViewLayout.collectionViewContentSize.height
            self.previewView_height.constant = self.collectionPostFeed.collectionViewLayout.collectionViewContentSize.height
            //self.collectionPageHeight.constant = 87

            self.viewMain.layoutIfNeeded()
            self.viewParent.layoutIfNeeded()
            self.collectionPostFeed.layoutIfNeeded()
            self.view.layoutIfNeeded()
            self.collectionPostFeed.reloadData()
            
            
            self.collectionPageHeight.constant = self.collectionPostFeed.collectionViewLayout.collectionViewContentSize.height
            self.view.setNeedsLayout() //Or self.view.layoutIfNeeded()
            
            self.tblFeeds.layoutIfNeeded()
            self.tblFeeds.reloadData()

        }
        else
        {
            if (self.finalMessage.lowercased().range(of: "https://")) == nil
            {
                self.previewView.isHidden = true
                self.collectionPostFeed.isHidden = true
                self.viewParent.frame.size.height = 148
                self.previewView_height.constant = 0
                self.collectionPageHeight.constant = 0
                self.tblFeeds.layoutIfNeeded()
                self.tblFeeds.reloadData()
                self.view.layoutIfNeeded()
            }
            else
            {
                linkPreviewVisible = true
                self.previewView.isHidden = false
                self.collectionPostFeed.isHidden = true
                self.viewParent.frame.size.height = 235
                self.previewView_height.constant = 87
                self.collectionPageHeight.constant = 87
                self.view.layoutIfNeeded()
                self.tblFeeds.layoutIfNeeded()
                self.tblFeeds.reloadData()
                
                
                //self.centerActivityIndicator?.isHidden = false
                //self.centerActivityIndicator.startAnimating()
                self.previewloader.beginRefreshing()
                
                if let url = self.slp.extractURL(text: self.finalMessage),
                    let cached = self.slp.cache.slp_getCachedResponse(url: url.absoluteString) {
                    
                    self.result = cached
                    
                    if let value: [String] = self.result[.images] as? [String] {
                        
                        self.previewImageView.image = nil
                        if let img_str = self.result[.image] as? String
                        {
                            self.previewImageView.sd_setImage(with: URL(string: (self.result[.image] as? String)!), placeholderImage: UIImage.init(),completed: nil)
                        }
                        else
                        {
                            self.previewImageView.image = UIImage(named: "Placeholder")
                        }
                        
                        
                    } else {
                        
                        self.previewImageView.image = nil
                        
                        if let img_str = self.result[.image] as? String
                        {
                            self.previewImageView.sd_setImage(with: URL(string: (self.result[.image] as? String)!), placeholderImage: UIImage.init(),completed: nil)
                        }
                        else
                        {
                            self.previewImageView.image = UIImage(named: "Placeholder")
                        }
                    }
                    
                    if let value: String = self.result[.title] as? String {
                        
                        self.previewTitle_lbl?.text = value.isEmpty ? "" : value
                        
                    } else {
                        
                        self.previewTitle_lbl?.text = ""
                        
                    }
                    
                    
                    if let value: String = self.result[.description] as? String {
                        
                        self.previewDesc_lbl?.text = value.isEmpty ? "" : value
                        
                    } else {
                        
                        self.previewTitle_lbl?.text = ""
                        
                    }
                    
                    //self.centerActivityIndicator.stopAnimating()
                    //self.centerActivityIndicator.isHidden = true
                    self.previewloader.endRefreshing()
                    
                    result.forEach { print("\($0):", $1) }
                    
                } else {
                    self.slp.preview(
                        self.finalMessage,
                        onSuccess: { result in
                            
                            result.forEach { print("\($0):", $1) }
                            self.result = result
                            
                            if let value: [String] = self.result[.images] as? [String] {
                                
                                self.previewImageView.image = nil
                                if let img_str = self.result[.image] as? String
                                {
                                    self.previewImageView.sd_setImage(with: URL(string: (self.result[.image] as? String)!), placeholderImage: UIImage.init(),completed: nil)
                                }
                                else
                                {
                                    self.previewImageView.image = UIImage(named: "Placeholder")
                                }
                                
                                // }
                                
                            } else {
                                
                                self.previewImageView.image = nil
                                
                                if let img_str = self.result[.image] as? String
                                {
                                    self.previewImageView.sd_setImage(with: URL(string: (self.result[.image] as? String)!), placeholderImage: UIImage.init(),completed: nil)
                                }
                                else
                                {
                                    self.previewImageView.image = UIImage(named: "Placeholder")
                                }
                                
                            }
                            
                            if let value: String = self.result[.title] as? String {
                                
                                self.previewTitle_lbl?.text = value.isEmpty ? "" : value
                                
                            } else {
                                
                                self.previewTitle_lbl?.text = ""
                                
                            }
                            
                            if let value: String = self.result[.description] as? String {
                                
                                self.previewDesc_lbl?.text = value.isEmpty ? "" : value
                                
                            } else {
                                
                                self.previewTitle_lbl?.text = ""
                                
                            }
                            //self.centerActivityIndicator.stopAnimating()
                            //self.centerActivityIndicator.isHidden = true
                            self.previewloader.endRefreshing()
                    },
                        onError: { error in
                            
                            print(error)
                            
                            Drop.down(error.description, state: .error)
                            
                    }
                    )
                }
            }
            
        }
    }
    
    @IBAction func btnSendAction(_ sender: UIButton) {
        
        
        self.view.endEditing(true)
            var params = [String:Any]()
            
            if data_arr.count > 0
            {
                if data_arr.contains(where: { $0["media_type"] as! String == "video" }) {
                    post_type = "video"
                } else {
                    post_type = "image"
                }
            }
            else
            {
                post_type = "text"
            }
            params.updateValue(post_type, forKey: "post_type")
        
            if self.txtMessage.text=="Write Something...."
            {
                
            }
            else
            {
                params.updateValue(self.txtMessage.text.encodeEmoji, forKey: "post_title")
            }
            params.updateValue("\(UserDefaults.standard.value(forKey: Constants.USERID) as! Int)", forKey: "user_id")
            params.updateValue(MethodName.Token, forKey: "Token")
            params.updateValue(self.postLink, forKey: "url")
        
      //  params["deletedMedia"] = self.dataToDelIdArr
            
            if post_type=="text"
            {
                
                if(CommonFunction.isInternetAvailable())
                {
                    MBProgressHUD.showAdded(to: self.view, animated: true)
                    let headerString = [
                        "Content-Type": "application/json"
                    ]
                    
    Alamofire.request(Constants.BASEURL + "\(self.isGroupPost ? MethodName.editGroupPost : MethodName.editPost)" + "?access-token=\(UserDefaults.standard.value(forKey: "Access_Token") as! String)&id=\(self.post?.post_id ?? "0")", method: .post, parameters: params, encoding: JSONEncoding.default, headers: headerString).responseJSON { response in
                        DispatchQueue.main.async{
                        MBProgressHUD.hide(for: self.view, animated: true)
                        switch response.result {
                        case .success:
                            
                            if let result_dict = response.result.value as? [String:Any]
                            {
                                let code:Int = result_dict["code"]as! Int
                                if  (code == 200)
                                {
                                    self.post_type = "text"
                                    self.finalMessage = ""
                                    self.txtMessage.text = "Write Something...."
                                    self.previewDesc_lbl.text = ""
                                    self.previewTitle_lbl.text = ""
                                    self.postLink = ""
                                    self.linkPreviewVisible = false
                                    self.isFirstTime = true
                                    self.previewImageView.image = nil
                                    self.previewView.isHidden = true
                                    self.collectionPostFeed.isHidden = true
                                    self.viewParent.frame.size.height = 148
                                    self.previewView_height.constant = 0
                                    self.collectionPageHeight.constant = 0
                                    //self.getPost()
                                    #warning("Go Back to home and reload from start")
                                    NotificationCenter.default.post(name: NSNotification.Name.init(HomePAgePodtEditNotification), object: nil)
                                    self.remove()
                                }
                                else
                                {
                                    if let result_error = response.result.value as? [String:Any]
                                    {
                                        if let error_array = result_error["errorData"] as? NSArray
                                        {
                                            if let error_dict = error_array[0] as? [String : Any]
                                            {
                                                
                                                 MBProgressHUD.hide(for: self.view, animated: true)
                                                
                                                if let error_msg = error_dict["message"] as? String
                                                {
                                                    self.view.makeToast(error_msg)
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                            
                        case .failure(let error):
                            print(error)
                             MBProgressHUD.hide(for: self.view, animated: true)
                        }
                    }
                }
                }
                else
                {
                     MBProgressHUD.hide(for: self.view, animated: true)
                    self.view.makeToast("Please Check Your Internet Connection!!")
                }
            }
            else
            {
                if(CommonFunction.isInternetAvailable())
                {
                    MBProgressHUD.showAdded(to: self.view, animated: true)
                    Alamofire.upload( multipartFormData: { multipartFormData in
                        for (key, value) in params {
                            multipartFormData.append("\(value)".data(using: String.Encoding.utf8)!, withName: key as String)
                        }
                        //        params["deletedMedia"] = self.dataToDelIdArr
                        
                        if self.dataToDelIdArr.count > 0 {
                      //  var delArrStr = ""
                            for delId in self.dataToDelIdArr{
                              //  delArrStr.append("\(delId),")
                    multipartFormData.append("\(delId)".data(using: String.Encoding.utf8)!, withName:  "deletedMedia[]")

                            }
                           // delArrStr.removeLast()
                            //multipartFormData.append(delArrStr.data(using: String.Encoding.utf8)!, withName:  "deletedMedia[]")

                            
                        }

                        for data_val in self.data_arr
                        {
                            if data_val[isOlderPostImage] as? Bool == false{

                            let type_str = data_val["media_type"] as! String
                            if(type_str=="video")
                            {
                                 multipartFormData.append(data_val["data_str"] as? Data ?? Data(), withName: "image[]", fileName: "\(arc4random()).mp4", mimeType: "video/mp4")
                                let img = data_val["Preview"] as! UIImage
                                multipartFormData.append(UIImagePNGRepresentation(img)!, withName: "image[]", fileName: "\(arc4random()).png", mimeType: "image/png")
                                
                            }
                            else
                            {
                                multipartFormData.append(data_val["data_str"] as! Data, withName: "image[]", fileName: "\(arc4random()).png", mimeType: "image/png")
                                
                            }
                        }
                            
                        }
                        
                    }, to: Constants.BASEURL + "\(self.isGroupPost ? MethodName.editGroupPost : MethodName.editPost)" + "?access-token=\(UserDefaults.standard.value(forKey: "Access_Token") as! String)&id=\(self.post?.post_id ?? "0")", encodingCompletion: { encodingResult in
                        
                        switch encodingResult
                        {
                            
                        case .success(let upload, _, _):
                            
                            upload.responseJSON { response in
                                DispatchQueue.main.async{
                                MBProgressHUD.hide(for: self.view, animated: true)
                                if let JSON = response.result.value as? NSDictionary {
                                    if(JSON.value(forKey: "code") as! Int == 200)
                                    {
                                        
                                        print(JSON)
                                        self.data_arr.removeAll()
                                        self.post_type = "text"
                                        self.finalMessage = ""
                                        self.txtMessage.text = "Write Something...."
                                        self.collectionPostFeed.reloadData()
                                        self.previewDesc_lbl.text = ""
                                        self.previewTitle_lbl.text = ""
                                        self.postLink = ""
                                        self.linkPreviewVisible = false
                                        self.isFirstTime = true
                                        self.previewImageView.image = nil
                                        self.previewView.isHidden = true
                                        self.collectionPostFeed.isHidden = true
                                        self.viewParent.frame.size.height = 148
                                        self.previewView_height.constant = 0
                                        self.collectionPageHeight.constant = 0
                                        //self.getPost()
                                        NotificationCenter.default.post(name: NSNotification.Name.init(HomePAgePodtEditNotification), object: nil)
                                                                           self.remove()
                                        
                                    }
                                    else
                                    {
                                        if let error_array = JSON.value(forKey: "errorData") as? NSArray
                                        {
                                            if let error_dict = error_array[0] as? [String : Any]
                                            {
                                                 MBProgressHUD.hide(for: self.view, animated: true)
                                                
                                                if let error_msg = error_dict["message"] as? String
                                                {
                                                    self.view.makeToast(error_msg)
                                                }
                                            }
                                        }
                                    }
                                }
                                else
                                {
                                    
                                    print(response)
                                }
                            }
                        }
                        case .failure(let encodingError):
                            MBProgressHUD.hide(for: self.view, animated: true)
                            print(encodingError)
                            
                        }
                    })
                }
                else
                {
                     MBProgressHUD.hide(for: self.view, animated: true)
                    self.view.makeToast("Please Check Your Internet Connection!!")
                }
            }
        
    }
}

extension EditPostViewController
{
    
    override func viewDidLayoutSubviews() {
        tblFeeds.reloadData()
    }
    
     
    
    
    func safariViewControllerDidFinish(_ controller: SFSafariViewController) {
        controller.dismiss(animated: true, completion: nil)
    }
    
    
    


    
    //MARK:- Gestures
    func add_tap_gester_lbl(_ lbl : UILabel)
    {
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.tapFunction))
        lbl.isUserInteractionEnabled = true
        lbl.addGestureRecognizer(tap)
    }
    
    
    func add_tap_gester(_ imgview : UIImageView)
    {
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.tapFunctionImg))
        imgview.isUserInteractionEnabled = true
        imgview.addGestureRecognizer(tap)
    }
    
    func add_tap_gester_chatlbl(_ lbl : UILabel)
    {
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.label_tapFunction))
        lbl.isUserInteractionEnabled = true
        lbl.addGestureRecognizer(tap)
    }
    
    func add_tap_gester_Grouplbl(_ lbl : UILabel)
    {
        //lbl.textColor = UIColor.red
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.label_tapGroupFunction))
        lbl.isUserInteractionEnabled = true
        lbl.addGestureRecognizer(tap)
    }
    
    func add_tap_gester_chat(_ imgview : UIImageView)
    {
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.imageview_tapFunction))
        imgview.isUserInteractionEnabled = true
        imgview.addGestureRecognizer(tap)
    }
    @objc func imageview_tapFunction(sender:UITapGestureRecognizer)
    {
        guard let cell = sender.view?.superview?.superview?.superview?.superview as? FeedCell else
        {
            return
        }
        let selected_indexPath = tblFeeds.indexPath(for: cell)
        self.open_profile(user_id: "\(self.arrPostList[(selected_indexPath?.row)!].postcomments[(sender.view?.tag)!].userId)")
    }
    @objc func tapFunctionImg(sender:UITapGestureRecognizer)
    {
        guard let cell = sender.view?.superview?.superview?.superview as? FeedCell else
        {
            return
        }
        let selected_indexPath = tblFeeds.indexPath(for: cell)
        self.open_profile(user_id: "\(self.arrPostList[(selected_indexPath?.row)!].user_id!)")
    }
    @objc func tapFunction(sender:UITapGestureRecognizer)
    {
        guard let cell = sender.view?.superview?.superview?.superview?.superview as? FeedCell else
        {
            return
        }
        let selected_indexPath = tblFeeds.indexPath(for: cell)
        self.open_profile(user_id: "\(self.arrPostList[(selected_indexPath?.row)!].user_id!)")
    }
    @objc func label_tapFunction(sender:UITapGestureRecognizer)
    {
        guard let cell = sender.view?.superview?.superview?.superview?.superview?.superview as? FeedCell else
        {
            return // or fatalError() or whatever
        }
        let selected_indexPath = tblFeeds.indexPath(for: cell)
        self.open_profile(user_id: "\(self.arrPostList[(selected_indexPath?.row)!].postcomments[(sender.view?.tag)!].userId)")
    }
    
    @objc func label_tapGroupFunction(sender:UITapGestureRecognizer)
    {
        guard let cell = sender.view?.superview?.superview?.superview?.superview as? FeedCell else
        {
            return // or fatalError() or whatever
        }
        let selected_indexPath = tblFeeds.indexPath(for: cell)
        
//        if let dictGroupDetails = self.arrPostList[(selected_indexPath?.row)!].groupData
//        {
//            if let group_id = dictGroupDetails["groupId"] as? String
//            {
//                let storyBoard = UIStoryboard(name: "Main", bundle: nil)
//                let objGroupDetailViewController = storyBoard.instantiateViewController(withIdentifier: "GroupDetailViewController") as! GroupDetailViewController
//                objGroupDetailViewController.groupID = Int(group_id)!
//                self.navigationController?.pushViewController(objGroupDetailViewController, animated: true)
//            }
//        }
        
        //cell.lblGroupName.textColor = UIColor.red
        if let dictGroupDetails = self.arrPostList[(selected_indexPath?.row)!].groupData
        {
            if let group_id = dictGroupDetails["groupId"] as? String
            {
                // Fade out to set the text
                UIView.animate(withDuration: 0.1, delay: 0.0, options: UIViewAnimationOptions.curveEaseOut, animations: {
                    cell.lblGroupName.alpha = 0.0
                    cell.lblGroupName2.alpha = 0.0
                    cell.lblGroupName.textColor = UIColor.red
                    cell.lblGroupName2.textColor = UIColor.red
                }, completion: {
                    (finished: Bool) -> Void in
                    
                    //Once the label is completely invisible, set the text and fade it back in
                    cell.lblGroupName.text = dictGroupDetails["groupName"] as? String
                    cell.lblGroupName2.text = dictGroupDetails["groupName"] as? String
                    // Fade in
                    UIView.animate(withDuration: 0.1, delay: 0.0, options: UIViewAnimationOptions.curveEaseIn, animations: {
                        cell.lblGroupName.alpha = 1.0
                        cell.lblGroupName2.alpha = 1.0
                        cell.lblGroupName.textColor = UIColor.red
                        cell.lblGroupName2.textColor = UIColor.red

                    }, completion: {
                        (value: Bool) in
                        
                        cell.lblGroupName.textColor = UIColor.black
                        cell.lblGroupName2.textColor = UIColor.black
                        
                        let storyBoard = UIStoryboard(name: "Main", bundle: nil)
                        let objGroupDetailViewController = storyBoard.instantiateViewController(withIdentifier: "GroupDetailViewController") as! GroupDetailViewController
                        objGroupDetailViewController.groupID = Int(group_id)!
                        //cell.lblGroupName.textColor = UIColor.red
                        self.navigationController?.pushViewController(objGroupDetailViewController, animated: true)
                        
                    })
                    
                })
                
                
            }
            
        }
    }
    
    
    
    //MARK:- Open Profile
    func open_profile(user_id:String)
    {
        let profile_page = Constants.mainStoryboard.instantiateViewController(withIdentifier: "FriendListViewController") as! FriendListViewController
        profile_page.user_id_str = user_id
        self.navigationController?.pushViewController(profile_page, animated: true)
    }
    
    
    //MARK:- scrollViewDidEndDecelerating
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {

        if (self.tblFeeds.contentOffset.y >= (self.tblFeeds.contentSize.height - self.tblFeeds.bounds.size.height))
        {
            if(is_next_page_available)
            {
                current_page = current_page + 1
            }
        }
    }
    
    func convertStringToDictionary(text: String) -> [String: Any]? {
        if let data = text.data(using: .utf8) {
            do {
                return try JSONSerialization.jsonObject(with: data, options: []) as? [String: Any]
            } catch {
                print(error.localizedDescription)
            }
        }
        return nil
    }
    
     func endCrawling(centerLoadingActivityIndicatorView : UIActivityIndicatorView? ,indicator : UIActivityIndicatorView?, detailedView : UIView? ) {
        
        indicator?.isHidden = true
        indicator?.stopAnimating()
        
    }
    

    
    //MARK:- Collection View Delegate Methods
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.data_arr.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell
    {
        let cell:PostFeedImageCell = collectionView.dequeueReusableCell(withReuseIdentifier: "PostFeedImageCell", for: indexPath) as! PostFeedImageCell
        let img_dict = self.data_arr[indexPath.item]
        if(img_dict["media_type"] as! String == "image")
        {
            //cell.centerIndicator.startAnimating()
            cell.ImageSpinner.beginRefreshing()
            cell.imgPostFeed.image = img_dict["Preview"] as? UIImage
            if let imgURl = img_dict["previewImageUrl"] as? String{
                if imgURl.count > 0{
                cell.imgPostFeed.sd_setImage(with: URL.init(string: imgURl)!) { (img, err, cash, uri) in
                    if let imageFinal = img{
                    var dataDict = self.data_arr[indexPath.row]
                    dataDict["data_str"] = UIImageJPEGRepresentation(imageFinal, 0.3)
                        self.data_arr[indexPath.row] = dataDict
                    }
                }
                }
            }
            
            cell.ImageSpinner.endRefreshing()
            //cell.centerIndicator.stopAnimating()
            
        }
        else
        {
            //cell.centerIndicator.startAnimating()
            cell.ImageSpinner.beginRefreshing()
            cell.imgPostFeed.image = img_dict["Preview"] as? UIImage
            if let imgURl = img_dict["previewImageUrl"] as? String{
                if imgURl.count > 0{
                cell.imgPostFeed.image = self.getThumbnailImage(forUrl: URL.init(string: imgURl)!)
                    DispatchQueue.global().async {
                        let data =  try? Data.init(contentsOf: URL.init(string: imgURl)!)
                        DispatchQueue.main.async{
                            if let imageFinal = data{
 var dataDict = self.data_arr[indexPath.row]
 dataDict["data_str"] = imageFinal
 self.data_arr[indexPath.row] = dataDict
 }
                        }
                    }
                }
            }
            cell.ImageSpinner.endRefreshing()
            //cell.centerIndicator.stopAnimating()
            
        }
        cell.imgPostFeed.contentMode = .scaleAspectFill
        cell.imgPostFeed.clipsToBounds = true
        cell.imgPostFeed.layer.borderWidth=1.0
        cell.imgPostFeed.layer.cornerRadius=5
        cell.imgPostFeed.clipsToBounds=true
        cell.imgPostFeed.layer.borderColor=UIColor.red.cgColor
        cell.btnClose.tag=indexPath.item
        cell.btnClose.addTarget(self, action: #selector(self.btnDeleteAction(btn:)), for: .touchUpInside)
        return cell
    }
    @objc func btnDeleteAction(btn:UIButton)
    {
        
        guard let cell = btn.superview?.superview as? PostFeedImageCell else
        {
            return // or fatalError() or whatever
        }
        let selected_indexPath = collectionPostFeed.indexPath(for: cell)
        let dictToDel = self.data_arr[(selected_indexPath?.item)!]
        print(dictToDel)
        for dict in self.postMediaView{
            if dict["post"] as? String == dictToDel["previewImageUrl"] as? String{
                self.dataToDelIdArr.append((dict["id"] as? Int ?? 0 ))
               }
    }
        self.data_arr.remove(at: (selected_indexPath?.item)!)
       
        self.collectionPostFeed.reloadData()
        setupPostData()
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        //return CGSize(width: 85.0, height: 85.0)
        
        return CGSize(width: (self.collectionPostFeed.frame.size.width - 20)/4, height: (self.collectionPostFeed.frame.size.width - 20)/4)

    }
    
    
    
}

extension EditPostViewController: TatsiPickerViewControllerDelegate
{
    
    //MARK:- TatsiPickerViewControllerDelegate
    func pickerViewController(_ pickerViewController: TatsiPickerViewController, didPickAssets assets: [PHAsset])
    {
        _ = PHPhotoLibrary.authorizationStatus()

        for asset in assets
        {
            if(asset.mediaType == .video)
            {
                PHImageManager.default().requestAVAsset(forVideo: asset, options: nil, resultHandler: { (asset, mix, nil) in
                    let myAsset = asset as? AVURLAsset
                    do
                    {
                        let videoData = try Data(contentsOf: (myAsset?.url)!)
                        var dataDict = [String:Any]()
                        dataDict["media_type"]  = "video"
                        dataDict["data_str"]  = videoData
                        dataDict["Preview"] = self.getThumbnailImage(forUrl: (myAsset?.url)!)
                        dataDict["URLPath"] = (myAsset?.url)!
                         dataDict["previewImageUrl"] =  ""
                        if self.data_arr.count < 5
                        {
                            dataDict[isOlderPostImage]=false
                            self.data_arr.append(dataDict)
                        }
                        else
                        {
                            self.view.makeToast("Maximum five images/videos you can upload at a time")
                        }
                    }
                    catch
                    {
                        print("exception catch at block - while uploading video")
                    }
                })
            }
            else
            {
                let manager = PHImageManager.default()
                let options = PHImageRequestOptions()
                options.version = .original
                options.isSynchronous = true
                manager.requestImageData(for: asset, options: options)
                { data, _, _, _ in
                    if let imgdata = data
                    {
                        var dataDict = [String:Any]()
                        dataDict["media_type"]  = "image"
                        dataDict["data_str"]  = imgdata
                        //dataDict["Preview"] = UIImage(named: "buzz")
                        dataDict["Preview"] = UIImage(data: imgdata)
                        dataDict["previewImageUrl"] =  ""

                        dataDict["URLPath"] = ""
                        if self.data_arr.count < 5
                        {
                            dataDict[isOlderPostImage]=false

                            self.data_arr.append(dataDict)
                        }
                        else
                        {
                            self.view.makeToast("Maximum five images/videos you can upload at a time")
                        }
            
                    }
                }
            }
        }
        print(self.data_arr)
        pickerViewController.dismiss(animated: true, completion: {
        
                self.collectionPostFeed.reloadData()
                
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.1, execute: {
                    self.setupPostData()
                })
        })
    }
    
}


