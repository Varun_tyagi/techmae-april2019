//
//  DialogsTableViewController.swift
//  sample-chat-swift
//
//  Created by Anton Sokolchenko on 4/1/15.
//  Copyright (c) 2015 quickblox. All rights reserved.
//

import UIKit
import GoogleMobileAds

class DialogTableViewCellModel: NSObject {
    

    var detailTextLabelText: String = ""
    var textLabelText: String = ""
    var unreadMessagesCounterLabelText : String?
    var unreadMessagesCounterHiden = true
    var dialogIcon : UIImage?

    
    init(dialog: QBChatDialog) {
        super.init()
       
		switch (dialog.type){
           
		case .publicGroup:
			self.detailTextLabelText = "SA_STR_PUBLIC_GROUP".localized
		case .group:
			self.detailTextLabelText = "SA_STR_GROUP".localized
		case .private:
			self.detailTextLabelText = "SA_STR_PRIVATE".localized
			
			if dialog.recipientID == -1 {
				return
			}
			
			// Getting recipient from users service.
			if let recipient = ServicesManager.instance().usersService.usersMemoryStorage.user(withID: UInt(dialog.recipientID)) {
				self.textLabelText = recipient.login ?? recipient.email!
			}
		}
        
        if self.textLabelText.isEmpty {
            // group chat
            
            if let dialogName = dialog.name {
                self.textLabelText = dialogName
            }
        }
        
        // Unread messages counter label
        
        if (dialog.unreadMessagesCount > 0) {
            
            var trimmedUnreadMessageCount : String
            
            if dialog.unreadMessagesCount > 99 {
                trimmedUnreadMessageCount = "99+"
            } else {
                trimmedUnreadMessageCount = String(format: "%d", dialog.unreadMessagesCount)
            }
            
            self.unreadMessagesCounterLabelText = trimmedUnreadMessageCount
            self.unreadMessagesCounterHiden = false
            
        }
        else {
            
            self.unreadMessagesCounterLabelText = nil
            self.unreadMessagesCounterHiden = true
        }
        
        // Dialog icon
        
        if dialog.type == .private {
            self.dialogIcon = UIImage(named: "user")
        }
        else {
            self.dialogIcon = UIImage(named: "group")
        }
    }
}

class DialogsViewController: UIViewController,UITableViewDelegate,UITableViewDataSource, QMChatServiceDelegate, QMChatConnectionDelegate, QMAuthServiceDelegate {
    private var didEnterBackgroundDate: NSDate?
    private var observer: NSObjectProtocol?
    @IBOutlet weak var tblDialogs: UITableView!
    @IBOutlet weak var noDialogsView: UIView!
    
    @IBOutlet weak var bannerView: DFPBannerView!

    // MARK: - ViewController overrides
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationController?.navigationBar.barTintColor = UIColor(red: 253.0/255.0, green: 80.0/255.0, blue: 108.0/255.0, alpha: 1.0)
        self.tblDialogs.tableFooterView = UIView()
        // calling awakeFromNib due to viewDidLoad not being called by instantiateViewControllerWithIdentifier
        // self.navigationItem.title = ServicesManager.instance().currentUser.login!
        
        //self.navigationItem.leftBarButtonItem = self.createLogoutButton()
        
        ServicesManager.instance().chatService.addDelegate(self)
        
        ServicesManager.instance().authService.add(self)
        
        self.observer = NotificationCenter.default.addObserver(forName: NSNotification.Name.UIApplicationDidBecomeActive, object: nil, queue: OperationQueue.main) { (notification) -> Void in
            
            if !QBChat.instance.isConnected {
            }
        }
        
        NotificationCenter.default.addObserver(self, selector: #selector(DialogsViewController.didEnterBackgroundNotification), name: NSNotification.Name.UIApplicationDidEnterBackground, object: nil)
        
        if (QBChat.instance.isConnected) {
            self.getDialogs()
        }
        
        self.ConfigureBanner()
    }

    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.tabBarController?.tabBar.frame.size.height = 49
        self.tabBarController?.tabBar .isHidden = false
        getDialogs()
        self.tblDialogs.reloadData()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        
        if segue.identifier == "SA_STR_SEGUE_GO_TO_CHAT".localized {
            if let chatVC = segue.destination as? ChatViewController {
                chatVC.dialog = sender as? QBChatDialog
            }
        }
    }
    
    // MARK: - Notification handling
    
    @objc func didEnterBackgroundNotification() {
        self.didEnterBackgroundDate = NSDate()
    }
    
    // MARK: - Actions
    
    func createLogoutButton() -> UIBarButtonItem {
        
        let logoutButton = UIBarButtonItem(title: "SA_STR_LOGOUT".localized, style: UIBarButtonItemStyle.plain, target: self, action: #selector(DialogsViewController.logoutAction))
        return logoutButton
    }
    
    @IBAction func logoutAction() {
        
        if !QBChat.instance.isConnected {

            return
        }
        
        
        ServicesManager.instance().logoutUserWithCompletion { [weak self] (boolValue) -> () in
            
            guard let strongSelf = self else { return }
            if boolValue {
                NotificationCenter.default.removeObserver(strongSelf)
                
                if strongSelf.observer != nil {
                    NotificationCenter.default.removeObserver(strongSelf.observer!)
                    strongSelf.observer = nil
                }
                
                ServicesManager.instance().chatService.removeDelegate(strongSelf)
                ServicesManager.instance().authService.remove(strongSelf)
                
                ServicesManager.instance().lastActivityDate = nil;
                
                let _ = strongSelf.navigationController?.popToRootViewController(animated: true)
                
            }
        }
    }
	
    // MARK: - DataSource Action
	
    func getDialogs() {
		
			ServicesManager.instance().chatService.allDialogs(withPageLimit: kDialogsPageLimit, extendedRequest: nil, iterationBlock: { (response: QBResponse?, dialogObjects: [QBChatDialog]?, dialogsUsersIDS: Set<NSNumber>?, stop: UnsafeMutablePointer<ObjCBool>) -> Void in
				
                    if (dialogObjects?.count)! > 0
                    {
                        self.noDialogsView.isHidden = true
                    }
                    else
                    {
                        self.noDialogsView.isHidden = false
                    }
				}, completion: { (response: QBResponse?) -> Void in
					
					guard response != nil && response!.isSuccess else {
						return
					}
                
					ServicesManager.instance().lastActivityDate = NSDate()
			})
        //}
    }

    // MARK: - DataSource
    
	func dialogs() -> [QBChatDialog]? {
        
        
    let dialogsArray = ServicesManager.instance().chatService.dialogsMemoryStorage.dialogsSortByUpdatedAt(withAscending: false)
    
        var unreadCount=0
        for messageDialog in dialogsArray{
            if messageDialog.unreadMessagesCount>0{
                unreadCount = unreadCount + 1
            }
        }
        
        if let tabItems = tabBarController?.tabBar.items {
            // In this case we want to modify the badge number of the third tab:
            let tabItem = tabItems[3]
            if unreadCount > 0 {
            tabItem.badgeValue = "\(unreadCount)"
            }else{
                tabItem.badgeValue = nil
            }
        }
        
    return dialogsArray
    }
    
    // MARK: - UITableViewDataSource
    
     func numberOfSections(in tableView: UITableView) -> Int {
        
        return 1
    }
    
     func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
		if let dialogs = self.dialogs() {
			return dialogs.count
		}
        return 0
    }
    
     func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return 64.0
    }
    
     func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "dialogcell", for: indexPath) as! DialogTableViewCell
        
        if ((self.dialogs()?.count)! < indexPath.row) {
            return cell
        }
        
        guard let chatDialog = self.dialogs()?[indexPath.row] else {
            return cell
        }
        if let custom_data = chatDialog.data as? [String : Any]
        {
            if let opp_user_id = custom_data["custom_userId"] as? String
            {
                let opp_userid_arr = opp_user_id.components(separatedBy: ",")
                let first_userid_str = opp_userid_arr[0]
                let second_userid_str = opp_userid_arr[1]
                
                if first_userid_str == "\(UserDefaults.standard.value(forKey: Constants.USERID) as! Int)"
                {
                    cell.dialogTypeImage.sd_setImage(with: URL(string: Constants.imageBaseURL + "id=\(second_userid_str)"), placeholderImage: UIImage(named: "user"), options: SDWebImageOptions.refreshCached)
                    cell.dialogTypeImage.layer.masksToBounds = false
                    cell.dialogTypeImage.layer.cornerRadius = cell.dialogTypeImage.frame.size.height/2
                    cell.dialogTypeImage.clipsToBounds = true
                    cell.dialogTypeImage.contentMode = UIViewContentMode.scaleAspectFill
                }
                else
                {
                    cell.dialogTypeImage.sd_setImage(with: URL(string: Constants.imageBaseURL + "id=\(first_userid_str)"), placeholderImage: UIImage(named: "user"), options: SDWebImageOptions.refreshCached)
                    cell.dialogTypeImage.layer.masksToBounds = false
                    cell.dialogTypeImage.layer.cornerRadius = cell.dialogTypeImage.frame.size.height/2
                    cell.dialogTypeImage.clipsToBounds = true
                    cell.dialogTypeImage.contentMode = UIViewContentMode.scaleAspectFill
                }
                
                
            }
        }
        cell.isExclusiveTouch = true
        cell.contentView.isExclusiveTouch = true
        
        cell.tag = indexPath.row
        cell.dialogID = chatDialog.id!
        
        let cellModel = DialogTableViewCellModel(dialog: chatDialog)
        
        cell.dialogLastMessage?.text = chatDialog.lastMessageText
        cell.dialogName?.text = cellModel.textLabelText
        cell.unreadMessageCounterLabel.text = cellModel.unreadMessagesCounterLabelText
        cell.unreadMessageCounterHolder.isHidden = cellModel.unreadMessagesCounterHiden
        
        return cell
    }
    
    // MARK: - UITableViewDelegate
    
     func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        tableView.deselectRow(at: indexPath, animated: true)
        
        if (ServicesManager.instance().isProcessingLogOut!) {
            return
        }
        
        guard let dialog = self.dialogs()?[indexPath.row] else {
            return
        }
        
        
        let chatVC = Constants.mainStoryboard.instantiateViewController(withIdentifier: "ChatViewController") as! ChatViewController
        chatVC.dialog = dialog
        self.navigationController?.pushViewController(chatVC, animated: true)
    }

     func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        
        return true
    }
    
     func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        
        guard editingStyle == UITableViewCellEditingStyle.delete else {
            return
        }
        
        
        guard let dialog = self.dialogs()?[indexPath.row] else {
            return
        }
        
        
        
        
        let view = ModalView.instantiateFromNib()
        view.updateModal("SA_STR_WARNING".localized, descMessage: "Do you really want to leave this chat?".localized , firstTitle: "SA_STR_DELETE".localized, secondTitle: "SA_STR_CANCEL".localized, type: .modalAlert)
        let modal = PathDynamicModal()
        modal.showMagnitude = 200.0
        modal.closeMagnitude = 130.0
        modal.closeByTapBackground = false
        modal.closeBySwipeBackground = false
        view.Cancel2ButtonHandler = {[weak modal] in
            modal?.closeWithLeansRandom()
            return
        }
        view.OkButtonHandler = {[weak modal] in
            
            let deleteDialogBlock = { (dialog: QBChatDialog!) -> Void in
                
                // Deletes dialog from server and cache.
                ServicesManager.instance().chatService.deleteDialog(withID: dialog.id!, completion: { (response) -> Void in
                    
                    guard response.isSuccess else {
                        print(response.error!.error ?? "")
                        return
                    }
                    
                })
            }
            
            if dialog.type == QBChatDialogType.private {
                
                deleteDialogBlock(dialog)
                
            }
            else {
                // group
                let occupantIDs = dialog.occupantIDs!.filter({ (number) -> Bool in
                    
                    return number.uintValue != ServicesManager.instance().currentUser.id
                })
                
                dialog.occupantIDs = occupantIDs
                let userLogin = ServicesManager.instance().currentUser.login ?? ""
                let notificationMessage = "User \(userLogin) " + "SA_STR_USER_HAS_LEFT".localized
                // Notifies occupants that user left the dialog.
                ServicesManager.instance().chatService.sendNotificationMessageAboutLeaving(dialog, withNotificationText: notificationMessage, completion: { (error) -> Void in
                    deleteDialogBlock(dialog)
                })
            }
            
            
            modal?.closeWithLeansRandom()
            return
        }
        modal.show(modalView: view, inView: (UIApplication.shared.delegate?.window!)!)
        
        
        
        
        
        
        
        
        
        
        
        
        
//
//        _ = AlertView(title:"SA_STR_WARNING".localized , message:"SA_STR_DO_YOU_REALLY_WANT_TO_DELETE_SELECTED_DIALOG".localized , cancelButtonTitle: "SA_STR_CANCEL".localized, otherButtonTitle: ["SA_STR_DELETE".localized], didClick:{ (buttonIndex) -> Void in
//
//            guard buttonIndex == 1 else {
//                return
//            }
//
//
//            let deleteDialogBlock = { (dialog: QBChatDialog!) -> Void in
//
//                // Deletes dialog from server and cache.
//                ServicesManager.instance().chatService.deleteDialog(withID: dialog.id!, completion: { (response) -> Void in
//
//                    guard response.isSuccess else {
//                        print(response.error!.error ?? "")
//                        return
//                    }
//
//                })
//            }
//
//            if dialog.type == QBChatDialogType.private {
//
//                deleteDialogBlock(dialog)
//
//            }
//            else {
//                // group
//                let occupantIDs = dialog.occupantIDs!.filter({ (number) -> Bool in
//
//                    return number.uintValue != ServicesManager.instance().currentUser.id
//                })
//
//                dialog.occupantIDs = occupantIDs
//                let userLogin = ServicesManager.instance().currentUser.login ?? ""
//                let notificationMessage = "User \(userLogin) " + "SA_STR_USER_HAS_LEFT".localized
//                // Notifies occupants that user left the dialog.
//                ServicesManager.instance().chatService.sendNotificationMessageAboutLeaving(dialog, withNotificationText: notificationMessage, completion: { (error) -> Void in
//                    deleteDialogBlock(dialog)
//                })
//            }
//        })
    }
	
     func tableView(_ tableView: UITableView, titleForDeleteConfirmationButtonForRowAt indexPath: IndexPath) -> String? {
        
        return "SA_STR_DELETE".localized
    }
    
    // MARK: - QMChatServiceDelegate
	
    func chatService(_ chatService: QMChatService, didUpdateChatDialogInMemoryStorage chatDialog: QBChatDialog) {
		
        self.reloadTableViewIfNeeded()
    }
	
    func chatService(_ chatService: QMChatService,didUpdateChatDialogsInMemoryStorage dialogs: [QBChatDialog]){
		
        self.reloadTableViewIfNeeded()
    }
	
    func chatService(_ chatService: QMChatService, didAddChatDialogsToMemoryStorage chatDialogs: [QBChatDialog]) {
        
        self.reloadTableViewIfNeeded()
    }
    
    func chatService(_ chatService: QMChatService, didAddChatDialogToMemoryStorage chatDialog: QBChatDialog) {
        
        self.reloadTableViewIfNeeded()
    }
    
    func chatService(_ chatService: QMChatService, didDeleteChatDialogWithIDFromMemoryStorage chatDialogID: String) {
        
        self.reloadTableViewIfNeeded()
    }
    
    func chatService(_ chatService: QMChatService, didAddMessagesToMemoryStorage messages: [QBChatMessage], forDialogID dialogID: String) {
        
        self.reloadTableViewIfNeeded()
    }
    
    func chatService(_ chatService: QMChatService, didAddMessageToMemoryStorage message: QBChatMessage, forDialogID dialogID: String){
        
        self.reloadTableViewIfNeeded()
    }

    // MARK: QMChatConnectionDelegate
    
    func chatServiceChatDidFail(withStreamError error: Error) {
    }
    
    func chatServiceChatDidAccidentallyDisconnect(_ chatService: QMChatService) {
    }
    
    func chatServiceChatDidConnect(_ chatService: QMChatService) {
        if !ServicesManager.instance().isProcessingLogOut! {
            self.getDialogs()
        }
    }
    
    func chatService(_ chatService: QMChatService,chatDidNotConnectWithError error: Error){
    }
	
	
    func chatServiceChatDidReconnect(_ chatService: QMChatService) {
        if !ServicesManager.instance().isProcessingLogOut! {
            self.getDialogs()
        }
    }
    
    // MARK: - Helpers
    func reloadTableViewIfNeeded() {
        if !ServicesManager.instance().isProcessingLogOut! {
            self.tblDialogs.reloadData()
        }
    }
}
extension DialogsViewController: GADBannerViewDelegate{
    func ConfigureBanner(){
        addBannerViewToView(bannerView)
        bannerView.adUnitID = Constants.kGOOGLE_AD_ID
        bannerView.rootViewController = self
        bannerView.load(DFPRequest())
        bannerView.delegate = self
    }
    
    func addBannerViewToView(_ bannerView: UIView) {
        bannerView.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(bannerView)
        view.addConstraints(
            [NSLayoutConstraint(item: bannerView,
                                attribute: .bottom,
                                relatedBy: .equal,
                                toItem: bottomLayoutGuide,
                                attribute: .top,
                                multiplier: 1,
                                constant: 0),
             NSLayoutConstraint(item: bannerView,
                                attribute: .centerX,
                                relatedBy: .equal,
                                toItem: view,
                                attribute: .centerX,
                                multiplier: 1,
                                constant: 0)
            ])
    }
    
    /// Tells the delegate an ad request loaded an ad.
    func adViewDidReceiveAd(_ bannerView: GADBannerView) {
        print("adViewDidReceiveAd")
        addBannerViewToView(bannerView)
        bannerView.alpha = 0
        UIView.animate(withDuration: 1, animations: {
            bannerView.alpha = 1
        })
    }
    
    /// Tells the delegate an ad request failed.
    func adView(_ bannerView: GADBannerView,
                didFailToReceiveAdWithError error: GADRequestError) {
        print("adView:didFailToReceiveAdWithError: \(error.localizedDescription)")
    }
    
    /// Tells the delegate that a full-screen view will be presented in response
    /// to the user clicking on an ad.
    func adViewWillPresentScreen(_ bannerView: GADBannerView) {
        print("adViewWillPresentScreen")
    }
    
    /// Tells the delegate that the full-screen view will be dismissed.
    func adViewWillDismissScreen(_ bannerView: GADBannerView) {
        print("adViewWillDismissScreen")
    }
    
    /// Tells the delegate that the full-screen view has been dismissed.
    func adViewDidDismissScreen(_ bannerView: GADBannerView) {
        print("adViewDidDismissScreen")
    }
    
    /// Tells the delegate that a user click will open another app (such as
    /// the App Store), backgrounding the current app.
    func adViewWillLeaveApplication(_ bannerView: GADBannerView) {
        print("adViewWillLeaveApplication")
    }
}
