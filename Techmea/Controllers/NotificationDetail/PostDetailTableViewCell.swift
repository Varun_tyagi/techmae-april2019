//
//  PostDetailTableViewCell.swift
//  Techmea
//
//  Created by varun on 28/12/19.
//  Copyright © 2019 varun tyagi. All rights reserved.
//

import Foundation
import JTMaterialSpinner



class PostDetailTableViewCell : UITableViewCell
{
   
    @IBOutlet weak var heightImgMultimedia: NSLayoutConstraint!
    @IBOutlet weak var play_btn_image: UIImageView!
    @IBOutlet weak var imgMultimedia: UIImageView!
    @IBOutlet weak var spinner: JTMaterialSpinner!
    
    
    
   
}
class PostDetailTextTableViewCell : UITableViewCell
{
   
    @IBOutlet weak var postTextLbl: UILabel!
     
   
}
