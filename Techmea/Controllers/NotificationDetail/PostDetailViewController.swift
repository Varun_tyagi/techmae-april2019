//
//  PostDetailViewController.swift
//  Techmea
//
//  Created by varun on 28/12/19.
//  Copyright © 2019 varun tyagi. All rights reserved.
//

import UIKit
import Alamofire
import SDWebImage
import MBProgressHUD
import Toast_Swift
import AVFoundation
import MobileCoreServices
import SDWebImage
import Alamofire
import AVKit
import AVFoundation
import Photos
import Toast_Swift
import SwiftyDrop
import ImageSlideshow
import SwiftLinkPreview
import JTMaterialSpinner
import SKPhotoBrowser
import ReadMoreTextView
import SafariServices
import FirebaseMessaging


let KPlayVideoPostDetail  = "playVidePostDetail"
let KShowImagePostDetail = "showImagePostDetail"
class PostDetailViewController: UIViewController {

    
    @IBOutlet weak var tblpostDetail: UITableView!
    var imgArray = [PostContent]()

    var postModel:PostListModel?
    var currentPostId = ""
    var isGroup = false
    
    override func viewDidLoad() {
        super.viewDidLoad()

    NotificationCenter.default.addObserver(self, selector: #selector(self.show_image(_:)), name: NSNotification.Name(rawValue: KShowImagePostDetail), object: nil)
    NotificationCenter.default.addObserver(self, selector: #selector(self.play_video(_:)), name: NSNotification.Name(rawValue: KPlayVideoPostDetail), object: nil)
        self.tblpostDetail.tableFooterView = UIView()
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.getPost()
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(true)
        
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: KShowImagePostDetail), object: nil)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: KPlayVideoPostDetail), object: nil)

    }
    //MARK: Get Post
    func getPost()
    {
        if(CommonFunction.isInternetAvailable())
        {
            MBProgressHUD.showAdded(to: self.view, animated: true)
        
            Alamofire.request(Constants.BASEURL + ( self.isGroup ? MethodName.singleGroupPostDetail(postId: self.currentPostId) : MethodName.singlePostDetail(postId: self.currentPostId) ) , method: .get, parameters: nil, encoding: URLEncoding.default, headers: auth_header).responseJSON { response in
            
            MBProgressHUD.hide(for: self.view, animated: true)
            
            switch response.result {
            case .success:
                
                if let result_dict = response.result.value as? [String:Any]
                {
                   
                    
                    let code:Int = result_dict["code"]as! Int
                    if (code == 200)
                    {
                       if let dictPost = result_dict["data"] as? NSDictionary
                       {
                           print(dictPost)
                        self.postModel = PostListModel.init(dict: dictPost)
                        
                        for postContent in self.postModel?.postcontent ?? [] {
                            if postContent.postType !=  "text"{
                                self.imgArray.append(postContent)
                            }
                        }
                        
                            self.tblpostDetail.reloadData()
                       }else{
                        self.view.makeToast("No data to show!")
                        self.tblpostDetail.isHidden=true
                        }
                    }
                    else if (code==401)
                    {
                        if let error_data = result_dict["errorData"] as? [[String:Any]]
                        {
                            if let error_msg = error_data[0]["message"] as? String
                            {
                                let view = CongratulationView.instantiateFromNib()
                                view.lblTitle.text = error_msg
                                let modal = PathDynamicModal()
                                modal.showMagnitude = 200.0
                                modal.closeMagnitude = 130.0
                                modal.closeByTapBackground = false
                                modal.closeBySwipeBackground = false
                                view.OkButtonHandler = {[weak modal] in
                                    
                                    let storyBoard = UIStoryboard(name: "Main", bundle: nil)
                                    let objHomeNav = storyBoard.instantiateViewController(withIdentifier: "HomeNav") as! UINavigationController
                                    UIApplication.shared.keyWindow?.rootViewController = objHomeNav
                                    
                                    modal?.closeWithLeansRandom()
                                    return
                                }
                                modal.show(modalView: view, inView: (UIApplication.shared.delegate?.window!)!)
                            }
                        }
                        
                        
                    }
                    else
                    {
                        self.view.makeToast("Something Went Wrong!!")
                    }
                }

            case .failure(let error):
                print(error)

                }
        }
        }
        else
        {
            self.view.makeToast("Please Check Your Internet Connection!!")
        }
        
    }
    
    //MARK:- Back Action
           @IBAction func btnBackAction(_ sender: UIButton)
           {
               self.navigationController?.popViewController(animated: false)
           }
    
    
 @objc func play_video(_ notification: NSNotification) {
        print(notification.userInfo ?? "")
        if let dict = notification.userInfo as NSDictionary? {
            if let video_str = dict["video_url"] as? String
            {
                let player = AVPlayer(url: URL(string: video_str)!)
                let vc = AVPlayerViewController()
                vc.player = player
                
                present(vc, animated: true) {
                    vc.player?.play()
                }
            }
        }
    }
    
    @objc func show_image(_ notification: NSNotification) {
           print(notification.userInfo ?? "")
           print(notification.object ?? "")
           let browser = SKPhotoBrowser(photos: notification.object as! [SKPhotoProtocol])
           if let startIndexDict = notification.userInfo as? [String:Int], let startIndex = startIndexDict[SKBrowserStartIndex] {
               browser.initializePageIndex(startIndex)
           }
           present(browser, animated: true, completion: {})
       }
       

}
extension PostDetailViewController: UITableViewDataSource,UITableViewDelegate{
    func numberOfSections(in tableView: UITableView) -> Int {
        

        return self.imgArray.count + 1
       
    }
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 5
    }
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        return UIView()
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

        
        if  indexPath.section == 0
        {
            let postCell = tableView.dequeueReusableCell(withIdentifier: "PostDetailTextTableViewCell") as! PostDetailTextTableViewCell
            postCell.postTextLbl.text = self.postModel?.postTitle?.decodeEmoji ?? ""
            postCell.contentView.layer.cornerRadius = 5
            postCell.contentView.layer.masksToBounds = true
            return postCell
        }
        let cell = tableView.dequeueReusableCell(withIdentifier: "PostDetailTableViewCell") as! PostDetailTableViewCell

        
        cell.imgMultimedia.contentMode = .scaleAspectFit
               cell.imgMultimedia.layer.borderColor = UIColor.lightGray.cgColor
               cell.imgMultimedia.layer.borderWidth = 0.5
               cell.imgMultimedia.contentMode = .scaleAspectFill

               
               let postimg = imgArray[indexPath.section-1]
               
               let imageURL:String = postimg.post
               let imgURL = URL.init(string: imageURL)
               
               cell.spinner.circleLayer.lineWidth = 2.0
               cell.spinner.circleLayer.strokeColor = UIColor.orange.cgColor
               
               cell.imgMultimedia.image = nil
               
               if let type_str = imgURL?.lastPathComponent
               {
                   
                   if type_str.lowercased().range(of:"mp4") != nil
                   {
                       

                    cell.play_btn_image.isHidden = true
                       cell.spinner.beginRefreshing()
                       cell.imgMultimedia.sd_setImage(with: URL(string: postimg.preview_image), placeholderImage: UIImage.init(), completed: { (image, error, type, url) in
                        cell.spinner.endRefreshing()
                        cell.play_btn_image.isHidden = false
                        })

                   }
                   else
                   {

                    cell.play_btn_image.isHidden = true
                       cell.spinner.beginRefreshing()
                       cell.imgMultimedia.sd_setImage(with: URL(string: postimg.post), placeholderImage: UIImage.init(), completed: { (image, error, type, url) in

                        if let finalImage = image{
                            cell.heightImgMultimedia.constant = finalImage.getHeightAcordingToWidth(ScreenSize.SCREEN_WIDTH-40)
                            cell.layoutIfNeeded()
                            cell.reloadInputViews()
                            self.tblpostDetail.reloadRows(at: [indexPath], with: UITableViewRowAnimation.automatic)
                        }
                        cell.spinner.endRefreshing()
                           if error != nil
                           {
                               print(error.debugDescription)
                           }
                       })
                       
                   }
                   
               }
               else
               {

                cell.spinner.endRefreshing()
                   cell.play_btn_image.isHidden = true
                   cell.imgMultimedia.image = nil
               }
        
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.section == 0 {
            return
        }
         let postimg = imgArray[indexPath.section-1]
            
                let imageURL:String = postimg.post
                let imgURL = URL.init(string: imageURL)
                var data_dict = [String:String]()
                var images = [SKPhotoProtocol]()

            
                
                if let type_str = imgURL?.lastPathComponent
                {
                    if type_str.lowercased().range(of:"mp4") != nil
                    {
                        data_dict["video_url"] = postimg.post
                        data_dict["postType"] = postimg.postType
                        data_dict["preview_image"] = postimg.preview_image
                         NotificationCenter.default.post(name: NSNotification.Name(rawValue: KPlayVideoPostDetail), object: nil, userInfo: data_dict)
                    }
                    else
                    {
                        data_dict["image_url"] = postimg.post
                        data_dict["postType"] = postimg.postType
                        data_dict["preview_image"] = postimg.preview_image
                        for imgPost in self.imgArray{
                            let imgURL = URL.init(string: imgPost.post)

                            if let type_str = imgURL?.lastPathComponent
                                   {
                                       if type_str.lowercased().range(of:"mp4") == nil
                                       {
                                        let photo = SKPhoto.photoWithImageURL(imgPost.post)
                                    
                                        photo.shouldCachePhotoURLImage = false
                                                       photo.isVideo = false
                                                       images.append(photo)
                                    }
                                    
                            }
                                    }
       
                        
                        NotificationCenter.default.post(name: NSNotification.Name(rawValue: KShowImagePostDetail), object: images, userInfo: [SKBrowserStartIndex:indexPath.section-1])
                    }
                    
                }
                else
                {
                    let img_url = Bundle.main.url(forResource: "noimage", withExtension: "png")
                    
                    let photo = SKPhoto.photoWithImageURL("\(String(describing: img_url))")
                    photo.shouldCachePhotoURLImage = false // you can use image cache by true(NSCache)
                    photo.isVideo = false
                    images.append(photo)
                    
                    data_dict["image_url"] = "\(String(describing: img_url))"
                    data_dict["postType"] = "image"
                    data_dict["preview_image"] = ""
                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: KShowImagePostDetail), object: images, userInfo: nil)
                }
    }
    
    
    
}
