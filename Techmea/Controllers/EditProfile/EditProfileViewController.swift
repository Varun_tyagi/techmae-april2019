//
//  EditProfileViewController.swift
//  Techmea
//
//  Created by Dhaval Panchani on 28/09/18.
//  Copyright © 2018 VISHAL SETH. All rights reserved.
//

import UIKit
import Alamofire
import SDWebImage
import AVFoundation
import Photos
import MBProgressHUD
import Toast_Swift

class EditProfileViewController: UIViewController,UIImagePickerControllerDelegate,UINavigationControllerDelegate,TLTagsControlDelegate,UIScrollViewDelegate,UITableViewDelegate,UITableViewDataSource, UIGestureRecognizerDelegate {
    
    //MARK:- Outlets
    @IBOutlet weak var view_name: UIView!
    @IBOutlet weak var view_lname: UIView!
    @IBOutlet weak var view_email: UIView!
    @IBOutlet weak var view_birthday: UIView!
    @IBOutlet weak var view_about: UIView!
    
    @IBOutlet weak var txtAbout: UITextView!
    @IBOutlet weak var txtBirthday: VSTextField!
    @IBOutlet weak var txtName: UITextField!
    @IBOutlet weak var txtLname: UITextField!
    @IBOutlet weak var txtEmail: UITextField!
    @IBOutlet weak var profileImage: UIImageView!
    @IBOutlet weak var btnUpdatePhoto: UIButton!
    
    let picker = UIImagePickerController()
    var SelectedImage : UIImage?
    var SelectedImgUrl : URL?
    var pimage : UIImage?
    
    
    @IBOutlet weak var btnUpdateBottom: NSLayoutConstraint!
    @IBOutlet weak var viewProfession: UIView!
    @IBOutlet weak var tagsControlProfession: TLTagsControl!
    @IBOutlet weak var tblProfession: UITableView!
    @IBOutlet weak var tblProfessionHeight: NSLayoutConstraint!
    @IBOutlet weak var btnInterests: UIButton!
    
    var arrProfessions =  NSMutableArray()
    var arrProfesstionsID = [String]()
    var arrProfessionSelected = [NSDictionary]()
    var arrSelectedP = NSMutableArray()
    var SortArray =  NSMutableArray()
    
    var arrInterestSelected = [String]()
    
    //MARK:- viewDidLoad
    override func viewDidLoad() {
        super.viewDidLoad()
        getProfileDetails()
        getProfessions()
        view_name.setBorder()
        view_lname.setBorder()
        view_email.setBorder()
        view_birthday.setBorder()
        view_about.setBorder()
        viewProfession.setBorder()
        btnInterests.setBorder()
        
        self.btnUpdateBottom.constant = 10
        
        self.btnUpdatePhoto.isUserInteractionEnabled = false
        self.btnUpdatePhoto.tintColor = UIColor.gray
        self.btnUpdatePhoto.setTitleColor(UIColor.gray, for: .normal)
        self.txtBirthday.formatting = .socialSecurityNumber
        
        
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(self.showTableView),
            name: NSNotification.Name(rawValue: "showTable"),
            object: nil)
        
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(self.removeTags),
            name: NSNotification.Name(rawValue: "removeTags"),
            object: nil)
        
        
        NotificationCenter.default.post(name: Notification.Name("ChangePlaceHolder"), object: nil)
        
        tagsControlProfession.tagPlaceholder = "Professions"
        tagsControlProfession.placeholderColor = UIColor(red: 252.0/255.0, green: 135.0/255.0, blue: 166.0/255.0, alpha: 1.0)
        tagsControlProfession.placeholderFont = UIFont(name: "Avenir Next", size: 13.0)
        
        tagsControlProfession.delegate = self
        tagsControlProfession.mode = .edit
        self.tagsControlProfession.tags = self.arrSelectedP
        tagsControlProfession.reloadTagSubviews()
        tagsControlProfession.tagsBackgroundColor = UIColor.white
        tagsControlProfession.tagsTextColor = UIColor.red
        tagsControlProfession.tagsDeleteButtonColor = UIColor.red
        tagsControlProfession.tagsBorderWidth = 1.0
        tagsControlProfession.tagsBorderColor = UIColor.red
        tagsControlProfession.txtfieldTextColor = UIColor.red
        
        tblProfession.tableFooterView = UIView()
        tblProfession.layer.masksToBounds = false
        tblProfession.layer.shadowColor = UIColor.lightGray.cgColor
        tblProfession.layer.shadowOpacity = 1
        tblProfession.layer.shadowOffset = CGSize(width: -1, height: 1)
        tblProfession.layer.shadowRadius = 3
        
        tblProfession.layer.shouldRasterize = true
        tblProfession.layer.rasterizationScale = true ? UIScreen.main.scale : 1
        
        
    }
    
    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        self.tabBarController?.tabBar.isHidden = true
        
        if let arrSInterests = UserDefaults.standard.value(forKey: "SelectedInterests") as? [String]
        {
            self.arrInterestSelected = arrSInterests
        }
    }
    
    //MARK:- Button Photo Action
    @IBAction func btnAddPhotoTapped(_ sender: UIButton) {
        
        let alert = UIAlertController(title: Constants.APP_NAME, message: "Browse Photo using", preferredStyle: UIAlertControllerStyle.actionSheet)
        
        let galleryBtn = UIAlertAction(title: "Gallery", style: UIAlertActionStyle.default) { (alert) -> Void in
            self.openGallery()
        }
        
        let cameraBtn = UIAlertAction(title: "Camera", style: UIAlertActionStyle.default) { (alert) -> Void in
            self.openCamera()
        }
        
        let cancelButton = UIAlertAction(title: "Cancel", style: UIAlertActionStyle.destructive) { (alert) -> Void in
        }
        
        alert.addAction(galleryBtn)
        alert.addAction(cameraBtn)
        alert.addAction(cancelButton)
        DispatchQueue.main.async {
        self.present(alert, animated: true, completion: nil)
        }
    }
    
    //MARK:- Open Camera
    func openCamera()
    {
        if(Platform.isSimulator)
        {
            //self.displayAlert(msg: "No camera available", title_str: Constants.APP_NAME)
            self.view.makeToast("No camera available")
        }
        else
        {
            //let authorizationStatus = AVCaptureDevice.authorizationStatus(for: AVMediaType.video)
            AVCaptureDevice.requestAccess(for: AVMediaType.video, completionHandler: { (granted :Bool) -> Void in
                if granted == true
                {
                    DispatchQueue.main.async {
                    self.picker.allowsEditing = true
                    self.picker.sourceType = UIImagePickerControllerSourceType.camera
                    self.picker.cameraCaptureMode = .photo
                    self.picker.mediaTypes = ["public.image"]
                    self.picker.delegate = self
                    self.picker.modalPresentationStyle = .fullScreen
                    self.present(self.picker,animated: true,completion: nil)
                    }
                }
                else
                {
                    self.show_deny_alert(msg: "You deny to use camera.To allow camera you should allow camera permission from setting.")
                }
            })
        }
    }
    func show_deny_alert(msg:String)
    {
        let alert = UIAlertController(title: Constants.APP_NAME, message: msg, preferredStyle: UIAlertControllerStyle.alert)
        
        let okBtn = UIAlertAction(title: "OK", style: UIAlertActionStyle.default) { (alert) -> Void in
            guard let settingsUrl = URL(string: UIApplicationOpenSettingsURLString) else {
                return
            }
            
            if UIApplication.shared.canOpenURL(settingsUrl) {
                UIApplication.shared.open(settingsUrl, completionHandler: { (success) in
                    print("Settings opened: \(success)") // Prints true
                })
            }
        }
        alert.addAction(okBtn)
        DispatchQueue.main.async {
        self.present(alert, animated: true, completion: nil)
        }
    }
    
    //MARK:- Open Gallery
    func openGallery()
    {
        PHPhotoLibrary.requestAuthorization
            { (status) in
                let status = PHPhotoLibrary.authorizationStatus()
                if (status == PHAuthorizationStatus.authorized)
                {
                    DispatchQueue.main.async {
                    self.picker.allowsEditing = true
                    self.picker.sourceType = .photoLibrary
                    self.picker.delegate=self
                    self.picker.mediaTypes = UIImagePickerController.availableMediaTypes(for: .photoLibrary)!
                    self.present(self.picker, animated: true, completion: nil)
                    }
                }
                else
                {
                    self.show_deny_alert(msg: "You deny to use photo library.To allow photo library you should allow photos permission from setting.")
                }
        }
    }
    
    
    //MARK:- didFinishPickingMediaWithInfo
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any])
    {
        self.btnUpdatePhoto.isUserInteractionEnabled = true
        self.btnUpdatePhoto.setTitleColor(UIColor.red, for: .normal)
        let chosenImage = info[UIImagePickerControllerOriginalImage] as! UIImage
        SelectedImage = chosenImage
        
        self.profileImage.image = SelectedImage
        self.pimage = SelectedImage
        dismiss(animated:true, completion: nil)
    }
    
    
    //MARK:- imagePickerControllerDidCancel
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        picker.dismiss(animated: true, completion: nil)
    }
    
    
    //MARK:- Button Upload Photo Action
    @IBAction func btnUploadPhotoAction(_ sender: UIButton)
    {
        if(SelectedImage != nil || self.profileImage != nil)
        {
            updateProfileImage()
        }
        else
        {
            //self.displayAlert(msg: "Please add profile image", title_str: Constants.APP_NAME)
            self.view.makeToast("Please add profile image")
        }
    }
    
    //MARK:- Button Reset Password Action
    @IBAction func btnResetPasswordAction(_ sender: UIButton)
    {
    }
    
    //MARK:- Button Update Photo Action
    @IBAction func btnUpdateAction(_ sender: UIButton)
    {
        editProfile()
    }
    
    
    //MARK:- Get Profile Details Api
    func getProfileDetails()
    {
        if(CommonFunction.isInternetAvailable())
        {
            var strUrl = String()
            
            strUrl = Constants.BASEURL + MethodName.profileState + "?access-token=\(UserDefaults.standard.value(forKey: "Access_Token") as! String)&expand=userStat,interestData"
            
            MBProgressHUD.showAdded(to: self.view, animated: true)
            Alamofire.request(strUrl, method: .get, parameters: nil , encoding: JSONEncoding.default, headers:nil).responseJSON
                { response in
                    
                    MBProgressHUD.hide(for: self.view, animated: true)
                    if let result_dict = response.result.value as? [String:Any]
                    {
                        if let code = result_dict["code"] as? Int
                        {
                            if(code==200)
                            {
                                if let profile_info = result_dict["data"] as? [String:Any]
                                {
                                    if let img_str = profile_info["user_profile_pic"] as? String
                                    {
                                        self .profileImage.sd_setImage(with: URL(string: img_str), placeholderImage: UIImage(named: "user"))
//                                        if let urlstr = URL(string:img_str)
//                                        {
//                                            if let imgdata = try? Data(contentsOf: urlstr)
//                                            {
//                                                self.pimage = UIImage(data: imgdata)
//                                            }
//                                        }
                                    }
                                    if let name_str = profile_info["fullname"] as? String
                                    {
                                        let fullNameArr = name_str.components(separatedBy: " ")
                                        let firstName: String = fullNameArr[0]
                                        let lastName: String? = fullNameArr.count > 1 ? fullNameArr[1] : nil
                                        self.txtName.text = firstName
                                        self.txtLname.text = lastName
                                    }
                                    if let about_me = profile_info["aboutme"] as? String
                                    {
                                        self.txtAbout.text = about_me.decodeEmoji
                                    }
                                    
                                    if let email_str = profile_info["email"] as? String
                                    {
                                        self.txtEmail.text = email_str
                                    }
                                    
                                    if let dob_str = profile_info["dob"] as? String
                                    {
                                        self.txtBirthday.text = dob_str
                                    }
                                    
                                    if let professions_list = profile_info["profession"] as? [[String:Any]]
                                    {
                                        for i in 0..<professions_list.count
                                        {
                                            if let profession_dict = professions_list[i] as? NSDictionary
                                            {
                                            
                                                self.arrProfessionSelected.append(profession_dict)
                                                if let name = profession_dict.value(forKey: "name") as? String
                                                {
                                                    self.arrSelectedP.add(name)
                                                }
                                            }
                                        }
                                        
                                        self.tagsControlProfession.tags = self.arrSelectedP
                                        self.tagsControlProfession.reloadTagSubviews()

                                    }
                                    
                                    if let interest_list = profile_info["interestData"] as? [[String:Any]]
                                    {
                                        for i in 0..<interest_list.count
                                        {
                                            if let interests_dict = interest_list[i] as? [String:Any]
                                            {
                                                if let interest_id = interests_dict["id"] as? String
                                                {
                                                     self.arrInterestSelected.append(interest_id)
                                                }
                                            }
                                        }
                                        
                                    }
                                }
                            }
                            else
                            {
                                if let error_arr = result_dict["errorData"] as? [[String:Any]]
                                {
                                        if let error_msg = error_arr[0]["message"] as? String
                                        {
                                            self.view.makeToast(error_msg)
                                        }
                                }
                            }
                        }
                    }
            }
        }
        else
        {
            self.view.makeToast("Please Check Your Internet Connection!!")
        }
    }
    
    //MARK:- Update Profile Image Api
    func updateProfileImage()
    {
        if(CommonFunction.isInternetAvailable())
        {
            MBProgressHUD.showAdded(to: self.view, animated: true)
            let headers: HTTPHeaders = ["Content-type": "multipart/form-data"]
            Alamofire.upload(multipartFormData: { (multipartFormData) in
                let image_data = UIImageJPEGRepresentation(self.pimage!, 0.3)
                multipartFormData.append(image_data!, withName: "image", fileName: "\(Date.timeIntervalSinceReferenceDate).png", mimeType: "image/png")
            }, usingThreshold: UInt64.init(), to: Constants.BASEURL + MethodName.uploadProfile + "?access-token=\(UserDefaults.standard.value(forKey: "Access_Token") as! String)&id=\(UserDefaults.standard.value(forKey: Constants.USERID) as! Int)", method: .post, headers: headers)
            { (result) in
                
                switch result
                {
                case .success(let upload, _, _):
                    upload.responseJSON
                        { response in
                            if let result_dict = response.result.value as? [String:Any]
                            {
                                if let code_val = result_dict["code"] as? Int
                                {
                                    if(code_val==200)
                                    {
                                        //self.displayAlert(msg: "Profile Image Updated.", title_str: Constants.APP_NAME)
                                        self.view.makeToast("Profile Image Updated.")
                                        MBProgressHUD.hide(for: self.view, animated: true)
                                    }
                                    else
                                    {
                                        MBProgressHUD.hide(for: self.view, animated: true)
                                        if let error_array = result_dict["errorData"] as? [[String:Any]]
                                        {
                                            if let error_dict = error_array[0]["message"] as? String
                                            {
                                                self.view.makeToast(error_dict)
                                            }
                                        }
                                    }
                                }
                            }
                    }
                case .failure(let error):
                    MBProgressHUD.hide(for: self.view, animated: true)
                    print("Error in upload: \(error.localizedDescription)")
                    self.view.makeToast("\(error.localizedDescription)")
                }
            }
        }
        else
        {
            self.view.makeToast("Please Check Your Internet Connection!!")
        }
    }
    
    
    //MARK:- Edit Profile Api
    func editProfile()
    {
        if(CommonFunction.isInternetAvailable())
        {
            if validate()
            {
                
                var bday = txtBirthday.text!
                bday.insert("/", at: bday.index(bday.startIndex, offsetBy: 2))
                bday.insert("/", at: bday.index(bday.startIndex, offsetBy: 5))
                
                let params = ["first_name":"\(txtName.text!)",
                              "last_name":"\(txtLname.text!)",
                              "email":"\(txtEmail.text!)",
                              "dob":bday,
                              "aboutme":"\(txtAbout.text.encodeEmoji)",
                              "user_id":"\(UserDefaults.standard.value(forKey: Constants.USERID) as! Int)",
                              "profession": self.arrProfessionSelected,
                              "interest": self.arrInterestSelected] as [String : Any]
                Alamofire.request(Constants.BASEURL + "home/edit-profile" + "?access-token=\(UserDefaults.standard.value(forKey: "Access_Token") as! String)" , method: .post, parameters: params, encoding: JSONEncoding.default, headers: nil).responseJSON { response in
                    
                    if let result_dict = response.result.value as? [String:Any]
                    {
                        if let status_code = result_dict["code"] as? Int
                        {
                            if status_code == 200
                            {
                                self.view.makeToast("Profile has been updated.")
                            }
                            else
                            {
                                if let error_array = result_dict["errorData"] as? [[String:Any]]
                                {
                                    if let error_msg = error_array[0]["message"] as? String
                                    {
                                        self.view.makeToast(error_msg)
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        else
        {
            self.view.makeToast("Please Check Your Internet Connection!!")
        }
    }
    
    //MARK:- TextField Validation
    fileprivate  func validate() -> Bool
    {
        if (txtName.text?.isEmpty)!{
            txtName.becomeFirstResponder()
            
            PKSAlertController.alert(Constants.APP_NAME, message: AlertConstant.EnterFirstName)
            return false
        }
        else if (txtLname.text?.isEmpty)!{
            txtLname.becomeFirstResponder()
            
            PKSAlertController.alert(Constants.APP_NAME, message: AlertConstant.EnterLastName)
            return false
        }
        else if (txtBirthday.text?.isEmpty)!{
            txtBirthday.becomeFirstResponder()
            PKSAlertController.alert(Constants.APP_NAME, message: AlertConstant.EnterBirthday)
            return false
        }
        else if !Helper.isValidEmail(txtEmail.text!){
            txtEmail.becomeFirstResponder()
            PKSAlertController.alert(Constants.APP_NAME, message: AlertConstant.EnterValidEmail)
            return false
        }
        else if (txtAbout.text?.isEmpty)!
        {
            txtAbout.becomeFirstResponder()
            PKSAlertController.alert(Constants.APP_NAME, message: AlertConstant.EnterAbout)
            return false
        }
        
        if(!(txtBirthday.text?.isEmpty)!)
        {
            if((txtBirthday.text?.count)!<8)
            {
                txtBirthday.becomeFirstResponder()
                self.displayAlert(msg: "Please enter valid birthday", title_str: Constants.APP_NAME)
                return false
            }
        }
        return true
    }
    
    //MARK:- Back Action
    @IBAction func btnBackTapped(_ sender: UIButton) {
        
        self.navigationController?.popViewController(animated: true)
    }
    
    //MARK:- Interest Action
    @IBAction func btnInterestTapped(_ sender: UIButton) {
        
        let storyBoard = UIStoryboard(name: "Main", bundle: nil)
        let objEditInterestsViewController = storyBoard.instantiateViewController(withIdentifier: "EditInterestsViewController") as! EditInterestsViewController
        objEditInterestsViewController.arrInterestsID = self.arrInterestSelected
        self.navigationController?.pushViewController(objEditInterestsViewController, animated: true)
    }
    
    
    
    //MARK:- Professions Data
 
    @objc func showTableView(notification: NSNotification){
        
        
        if let newDict = notification.object as? NSDictionary
        {
            print(newDict)
            
            if let place = newDict.value(forKey: "place") as? String
            {
                
                if place == "Professions"
                {
                    SortArray =  NSMutableArray()
                    if let newString = newDict.value(forKey: "name") as? String
                    {
                        let predicate = NSPredicate(format: "(name BEGINSWITH[c] %@)", newString)
                        let arr : NSArray = arrProfessions.filtered(using: predicate) as NSArray
                        SortArray = NSMutableArray(array: arr)
                        print(arrProfessions)
                        print(SortArray)
                        
                        self.tblProfession.isHidden = false
                        self.tblProfession.reloadData()
                        
                        if SortArray.count == 1
                        {
                            self.tblProfessionHeight.constant = 50.0
                        }
                        else if SortArray.count == 2
                        {
                            self.tblProfessionHeight.constant = 100.0
                        }
                        else if SortArray.count > 3
                        {
                            self.tblProfessionHeight.constant = 196.0
                        }
                        else
                        {
                            self.tblProfessionHeight.constant = 0.0
                        }
                    }
                }
            }
            
        }
    }
    
    @objc func removeTags(notification: NSNotification){
        
        if let dict = notification.object as? NSDictionary
        {
            if let place = dict.value(forKey: "place") as? String
            {
                if place == "Professions"
                {
                    if let newString = dict.value(forKey: "name") as? String
                    {
                        
                        let filteredArray = arrProfessions.filter { ($0 as! NSDictionary)["name"] as! String == newString }
                        
                        if let dict_filter = filteredArray[0] as? NSDictionary
                        {
                            
                            if let indexd = self.arrProfessionSelected.index(of: dict_filter)
                            {
                                self.arrProfessionSelected.remove(at: indexd)
                            }
                        }
                    }
                    
                }
                
            }
        }
        
    }
    
    func getProfessions()
    {
        if(CommonFunction.isInternetAvailable())
        {
            MBProgressHUD.showAdded(to: self.view, animated: true)
            
            Alamofire.request(Constants.BASEURL + "profession", method: .get, parameters: nil , encoding: JSONEncoding.default, headers:nil).responseJSON
                { response in
                    
                    MBProgressHUD.hide(for: self.view, animated: true)
                    self.arrProfessions.removeAllObjects()
                    if let result_dict = response.result.value as? [String:Any]
                    {
                        if let rcode = result_dict["code"] as? Int
                        {
                            if rcode == 200
                            {
                                if let data_dict = result_dict["data"] as? [[String:Any]]
                                {
                                    for i in 0..<data_dict.count
                                    {
                                        let profession_dict = data_dict[i]
                                        if let profession_name = profession_dict["name"] as? String
                                        {
                                            var name_dict = [String:Any]()
                                            name_dict["name"] = profession_name
                                            if let profession_id = profession_dict["id"] as? Int
                                            {
                                                name_dict["id"] = profession_id
                                            }
                                            self.arrProfessions.add(name_dict)
                                        }
                                    }
                                }
                            }
                            else
                            {
                                if let error_array = result_dict["errorData"] as? [[String:Any]]
                                {
                                    if let error_msg = error_array[0]["message"] as? String
                                    {
                                        self.view.makeToast(error_msg)
                                    }
                                }
                            }
                        }
                    }
            }
        }
        else
        {
            self.view.makeToast("Please Check Your Internet Connections!!")
        }
    }
    
    
    func tagsControl(_ tagsControl: TLTagsControl!, tappedAt index: Int) {
        
        if tagsControl == tagsControlProfession
        {
            print(index)
        }
    }
    
    func tagsControl(_ tagsControl: TLTagsControl!, removedAt index: Int) {
        
        print(index)
    }
    
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return SortArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "ProfesstionsCell", for: indexPath)
        
        if let dict = SortArray[indexPath.row] as? [String : Any]
        {
            if let pname = dict["name"] as? String
            {
                cell.textLabel?.text = pname
            }
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        
        
        if let dict = SortArray[indexPath.row] as? [String : Any]
        {
            if let professiontag = dict["name"] as? String
            {
                if let professionid = dict["id"] as? Int
                {
                    self.arrProfesstionsID.append("\(professionid)")
                }
                
                if let dict_p = SortArray[indexPath.row] as? NSDictionary
                {
                    self.arrProfessionSelected.append(dict_p)
                }
                tagsControlProfession.addTag("\(professiontag)")
                self.tblProfession.isHidden = true
            }
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return 40.0
    }
}
