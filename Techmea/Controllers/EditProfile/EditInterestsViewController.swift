//
//  EditInterestsViewController.swift
//  Techmea
//
//  Created by Dhaval Panchani on 06/02/19.
//  Copyright © 2019 VISHAL SETH. All rights reserved.
//

import UIKit
import Alamofire
import MBProgressHUD

class EditInterestsViewController: UIViewController,UIScrollViewDelegate,UITextFieldDelegate, UITableViewDelegate, UITableViewDataSource {
    
    var arrInterests =  NSMutableArray()
    var arrInterestsID = [String]()
    
    @IBOutlet weak var tblInterests: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        getInterests()
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        self.tabBarController?.tabBar.isHidden = true
    }
    

    func getInterests()
    {
        if(CommonFunction.isInternetAvailable())
        {
            MBProgressHUD.showAdded(to: self.view, animated: true)
            
            Alamofire.request(Constants.BASEURL + "interest", method: .get, parameters: nil , encoding: JSONEncoding.default, headers:nil).responseJSON
                { response in
                    
                    MBProgressHUD.hide(for: self.view, animated: true)
                    self.arrInterests.removeAllObjects()
                    if let result_dict = response.result.value as? [String:Any]
                    {
                        if let rcode = result_dict["code"] as? Int
                        {
                            if rcode == 200
                            {
                                if let data_dict = result_dict["data"] as? [[String:Any]]
                                {
                                    for i in 0..<data_dict.count
                                    {
                                        let interest_dict = data_dict[i]
                                        self.arrInterests
                                            .add(interest_dict)
                                    }
                                    
                                    self.tblInterests.reloadData()
                                }
                            }
                            else
                            {
                                if let error_array = result_dict["errorData"] as? NSArray
                                {
                                    if let error_dict = error_array[0] as? [String : Any]
                                    {
                                        if let error_msg = error_dict["message"] as? String
                                        {
                                            self.view.makeToast(error_msg)
                                        }
                                    }
                                }
                            }
                        }
                    }
            }
        }
        else
        {
            self.view.makeToast("Please Check Your Internet Connections!!")
        }
    }
    
    
    @IBAction func btnBackTapped(_ sender: UIButton) {
        
        self.navigationController?.popViewController(animated: true)
    }

    
    @IBAction func btnConfirmTapped(_ sender: UIButton) {
        
        self.navigationController?.popViewController(animated: true)
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.arrInterests.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "interestsTableCell", for: indexPath) as! interestsTableCell
        
        if let interest_dict = self.arrInterests[indexPath.row] as? [String:Any]
        {
            if let interest_name = interest_dict["name"] as? String
            {
                cell.lbl_interest.text = interest_name
            }
        }
        
        if let interest_dict = self.arrInterests[indexPath.row] as? [String:Any]
        {
            if let interest_id = interest_dict["id"] as? Int
            {
                if self.arrInterestsID.contains("\(interest_id)")
                {
                    cell.bg_image.image = UIImage(named: "box-red")
                    cell.lbl_interest.textColor = UIColor.white
                    if let active_img = interest_dict["selected_icon"] as? String
                    {
                        cell.icon_image.sd_setImage(with: URL(string: active_img), placeholderImage: nil)
                    }
                }
                else
                {
                    cell.bg_image.image = UIImage(named: "box-normal")
                    cell.lbl_interest.textColor = UIColor.black
                    
                    if let active_img = interest_dict["icon"] as? String
                    {
                        cell.icon_image.sd_setImage(with: URL(string: active_img), placeholderImage: nil)
                    }
                }
            }
        }
        cell.mainView.cornerRadius = 10
        cell.btnSelect.addTarget(self, action: #selector(self.btnSelectTapped(_ :)) , for: .touchUpInside)
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return 100.0
    }
    
    
    
    
    @objc func btnSelectTapped(_ sender: UIButton)
    {
        guard let cell = sender.superview?.superview?.superview as? interestsTableCell else
        {
            return
        }
        let selected_indexPath = tblInterests.indexPath(for: cell)
        
        if let interest_dict = self.arrInterests[(selected_indexPath?.row)!] as? [String:Any]
        {
            if let interest_id = interest_dict["id"] as? Int
            {
                if self.arrInterestsID.contains("\(interest_id)")
                {
                    let ind = self.arrInterestsID.index(of: "\(interest_id)")
                    self.arrInterestsID.remove(at: ind!)
                    self.tblInterests.reloadData()
                }
                else
                {
                    self.arrInterestsID.append("\(interest_id)")
                    self.tblInterests.reloadData()
                }
                
                UserDefaults.standard.set(self.arrInterestsID, forKey: "SelectedInterests")
            }
        }
    }

}
