//
//  UIImageExtention.swift
//  Techmea
//
//  Created by VISHAL SETH on 8/5/18.
//  Copyright © 2018 VISHAL SETH. All rights reserved.
//

import Foundation
import UIKit

extension UIImageView
{
    func cornorRadius()
    {
        self.layer.cornerRadius=self.frame.size.width/2
        self.clipsToBounds=true
    }
    
}

extension UIImage{
    func getHeightAcordingToWidth(_ width: CGFloat) -> CGFloat {
        let size = self.size
        let aspectRatio =  size.width/size.height
        return width/aspectRatio
        
    }
}
