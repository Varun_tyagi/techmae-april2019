//
//  Image+Extension.swift
//  TapDoc
//
//  Created by Hitesh Dhawan on 10/08/18.
//  Copyright © 2018 Varun Tyagi. All rights reserved.
//

import Foundation
import UIKit
import SDWebImage

extension UIImageView{
    func set_ImageFromSdImage(_ imgUrl : String) {
        self.sd_showActivityIndicatorView()
        self.sd_setShowActivityIndicatorView(true)
        self.sd_setIndicatorStyle(UIActivityIndicatorViewStyle.whiteLarge)
        self.sd_setImage(with: URL.init(string: imgUrl), placeholderImage: #imageLiteral(resourceName: "placeholder"), options: SDWebImageOptions.continueInBackground) { (img, err, cash, url) in
            self.sd_removeActivityIndicator()
            self.sd_setShowActivityIndicatorView(false)
            if img == nil {
                self.image = #imageLiteral(resourceName: "placeholder")
            }
        }
    }
}
