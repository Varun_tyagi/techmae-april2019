//
//  UIViewExtentions.swift
//  Techmea
//
//  Created by VISHAL SETH on 8/4/18.
//  Copyright © 2018 VISHAL SETH. All rights reserved.
//

import Foundation
import UIKit

extension UITextField{
    @IBInspectable var placeHolderColor: UIColor? {
        get {
            return self.placeHolderColor
        }
        set {
            self.attributedPlaceholder =
                
                NSAttributedString(string:self.placeholder != nil ? self.placeholder! : "", attributes:[NSAttributedStringKey.foregroundColor : newValue!])
        }
    }
}
//extension UIColor {
//    convenience init(red: Int, green: Int, blue: Int) {
//        assert(red >= 0 && red <= 255, "Invalid red component")
//        assert(green >= 0 && green <= 255, "Invalid green component")
//        assert(blue >= 0 && blue <= 255, "Invalid blue component")
//        
//        self.init(red: CGFloat(red) / 255.0, green: CGFloat(green) / 255.0, blue: CGFloat(blue) / 255.0, alpha: 1.0)
//    }
//    
//    convenience init(rgb: Int) {
//        self.init(
//            red: (rgb >> 16) & 0xFF,
//            green: (rgb >> 8) & 0xFF,
//            blue: rgb & 0xFF
//        )
//    }
//}
extension UIView
{
    func setBorder()
    {
        //FFBECC
        self.layer.borderWidth=1.0
        self.layer.borderColor=UIColor.init(rgb: 0xFFBECC).cgColor
        self.layer.cornerRadius=22
        self.clipsToBounds=true
    }
    
    func setPaymentBorder()
    {
        //FFBECC
        self.layer.borderWidth=1.0
        self.layer.borderColor=UIColor.init(rgb: 0xFFBECC).cgColor
        self.layer.cornerRadius=15
        self.clipsToBounds=true
    }
}



