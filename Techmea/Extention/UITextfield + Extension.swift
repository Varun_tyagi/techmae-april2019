//
//  UITextfield + Extension.swift
//  CircleK
//
//  Created by Hitesh Dhawan on 20/07/18.
//  Copyright © 2018 Varun Tyagi. All rights reserved.
//

import Foundation
import UIKit

extension UITextField{
    func addEditAccessery()  {
        let imgView = UIImageView.init(frame: CGRect.init(x: 0, y: 0, width: 15, height: 15))
        imgView.image = #imageLiteral(resourceName: "editPen")
        self.rightViewMode = .always
        self.rightView = imgView
    }
    func removeEditAccessory()  {
        let imgView = UIImageView.init(frame: CGRect.init(x: 0, y: 0, width: 0 , height: 0))
        self.rightViewMode = .always
        self.rightView = imgView
    }

    func setAttributedPlaceholder(_ placeholderStr: String)  {
        self.attributedPlaceholder = NSAttributedString.init(string: placeholderStr, attributes: [NSAttributedStringKey.font : UIFont.init(name: FONTS.Helvetica_Regular, size: 16.0)!, NSAttributedStringKey.foregroundColor : UIColor.init(red: 49.0/255.0, green: 49.0/255.0, blue: 49.0/255.0, alpha: 1.0)])
        
    }
    func setLeftPaddingPoints(_ amount:CGFloat=10){
        let paddingView = UIView(frame: CGRect(x: 0, y: 0, width: amount, height: self.frame.size.height))
        self.leftView = paddingView
        self.leftViewMode = .always
    }
    func setRightPaddingPoints(_ amount:CGFloat=10) {
        let paddingView = UIView(frame: CGRect(x: 0, y: 0, width: amount, height: self.frame.size.height))
        self.rightView = paddingView
        self.rightViewMode = .always
    }
}
class UIShowHideTextField: UITextField {

    let rightButton  = UIButton(type: .custom)

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }

    required override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }

    func commonInit() {
        rightButton.setImage(UIImage(named: "password_show") , for: .normal)
        rightButton.addTarget(self, action: #selector(toggleShowHide), for: .touchUpInside)
        rightButton.frame = CGRect(x:0, y:0, width:30, height:30)

        rightViewMode = .always
        rightView = rightButton
        isSecureTextEntry = true
    }

    @objc
    func toggleShowHide(button: UIButton) {
        toggle()
    }

    func toggle() {
        isSecureTextEntry = !isSecureTextEntry
        if isSecureTextEntry {
            rightButton.setImage(UIImage(named: "password_show") , for: .normal)
        } else {
            rightButton.setImage(UIImage(named: "password_hide") , for: .normal)
        }
    }

}
