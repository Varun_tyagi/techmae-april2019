//
//  UIButtonExtention.swift
//  Techmea
//
//  Created by VISHAL SETH on 8/4/18.
//  Copyright © 2018 VISHAL SETH. All rights reserved.
//

import Foundation
import UIKit

extension   UIButton
{
    func cornorRadius()
    {
        self.layer.cornerRadius=22
        self.clipsToBounds=true
    }
}
class ButtonWithShadow: UIButton {
    
    override func draw(_ rect: CGRect) {
        updateLayerProperties()
    }
    
    func updateLayerProperties() {
        self.layer.shadowColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.25).cgColor
        self.layer.shadowOffset = CGSize(width: 0, height: 3)
        self.layer.shadowOpacity = 1.0
        self.layer.shadowRadius = 10.0
        self.layer.masksToBounds = false
    }
    
}
