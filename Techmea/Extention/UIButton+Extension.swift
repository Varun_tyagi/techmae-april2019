//
//  UIButton+Extension.swift
//  CircleK
//
//  Created by Hitesh Dhawan on 20/07/18.
//  Copyright © 2018 Varun Tyagi. All rights reserved.
//

import Foundation
import UIKit

extension UIButton {
    class func backButtonTarget(_ target: Any, action: Selector) -> UIBarButtonItem {
        let backButton = UIButton(frame: CGRect(x: 0, y: 0, width: 35, height: 35))
        backButton.setImage(#imageLiteral(resourceName: "back"), for: .normal)
        backButton.contentHorizontalAlignment = .left
        let barBackButtonItem = UIBarButtonItem(customView: backButton)
        backButton.addTarget(target, action: action, for: .touchUpInside)
        return barBackButtonItem
    }
    class func refreshButtonTarget(_ target: Any, action: Selector) -> UIBarButtonItem {
        let backButton = UIButton(frame: CGRect(x: 0, y: 0, width: 35, height: 35))
        backButton.setImage(#imageLiteral(resourceName: "refreshIcon"), for: .normal)
        backButton.contentHorizontalAlignment = .left
        let barBackButtonItem = UIBarButtonItem(customView: backButton)
        backButton.addTarget(target, action: action, for: .touchUpInside)
        return barBackButtonItem
    }
   
}
