

import UIKit

open class CustomPickerBarButtonItem: UIBarButtonItem {

    /**
        A bar button to close McPicker with selections.
     
        - parameter mcPicker: Target instance
        - parameter title: Optionally set a custom title
        - parameter barButtonSystemItem: Optionally set UIBarButtonSystemItem or omit for default: .done. NOTE: This option is ignored when title is non-nil.
     
        - returns: McPickerBarButtonItem
     */
    public class func done(mcPicker: CustomPicker, title: String? = nil, barButtonSystemItem: UIBarButtonSystemItem = .done) -> CustomPickerBarButtonItem {

        if let buttonTitle = title {
            return self.init(title: buttonTitle, style: .plain, target: mcPicker, action: #selector(CustomPicker.done))
        }

        return self.init(barButtonSystemItem: barButtonSystemItem, target: mcPicker, action: #selector(CustomPicker.done))
    }

    /**
         A bar button to close McPicker with out selections.
         
         - parameter mcPicker: Target instance
         - parameter title: Optionally set a custom title
         - parameter barButtonSystemItem: Optionally set UIBarButtonSystemItem or omit for default: .done. NOTE: This option is ignored when title is non-nil.
         
         - returns: McPickerBarButtonItem
     */
    public class func cancel(mcPicker: CustomPicker, title: String? = nil, barButtonSystemItem: UIBarButtonSystemItem = .cancel) -> CustomPickerBarButtonItem {

        if let buttonTitle = title {
            return self.init(title: buttonTitle, style: .plain, target: mcPicker, action: #selector(CustomPicker.cancel))
        }

        return self.init(barButtonSystemItem: barButtonSystemItem, target: mcPicker, action: #selector(CustomPicker.cancel))
    }

    public class func flexibleSpace() -> CustomPickerBarButtonItem {
        return self.init(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
    }

    public class func fixedSpace(width: CGFloat) -> CustomPickerBarButtonItem {
        let fixedSpace =  self.init(barButtonSystemItem: .fixedSpace, target: nil, action: nil)
        fixedSpace.width = width
        return fixedSpace
    }
}
