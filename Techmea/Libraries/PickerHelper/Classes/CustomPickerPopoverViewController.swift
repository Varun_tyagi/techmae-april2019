

import UIKit

internal class CustomPickerPopoverViewController: UIViewController {

    weak var myPicker: CustomPicker?

    internal convenience init(mcPicker: CustomPicker) {
        self.init(nibName: nil, bundle: nil)
        self.myPicker = mcPicker
    }

    internal required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }

    private override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
        super.init(nibName: nil, bundle: nil)
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        myPicker!.sizeViews()
        myPicker!.addAllSubviews()
        self.view.addSubview(myPicker!)
        self.preferredContentSize = myPicker!.popOverContentSize
    }
}
