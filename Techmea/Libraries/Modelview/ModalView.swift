//
//  ModalView.swift
//  PathDynamicModal-Demo
//
//  Created by Ryo Aoyama on 2/11/15.
//  Copyright (c) 2015 Ryo Aoyama. All rights reserved.
//

import UIKit

extension String {
    var byWords: [String] {
        var byWords:[String] = []
        enumerateSubstrings(in: startIndex..<endIndex, options: .byWords) {
            guard let word = $0 else { return }
            print($1,$2,$3)
            byWords.append(word)
        }
        return byWords
    }
    func firstWords(_ max: Int) -> [String] {
        return Array(byWords.prefix(max))
    }
    var firstWord: String {
        return byWords.first ?? ""
    }
    func lastWords(_ max: Int) -> [String] {
        return Array(byWords.suffix(max))
    }
    var lastWord: String {
        return byWords.last ?? ""
    }

}

extension StringProtocol where Index == String.Index {
    func index<T: StringProtocol>(of string: T, options: String.CompareOptions = []) -> Index? {
        return range(of: string, options: options)?.lowerBound
    }
    func endIndex<T: StringProtocol>(of string: T, options: String.CompareOptions = []) -> Index? {
        return range(of: string, options: options)?.upperBound
    }
    func indexes<T: StringProtocol>(of string: T, options: String.CompareOptions = []) -> [Index] {
        var result: [Index] = []
        var start = startIndex
        while start < endIndex, let range = range(of: string, options: options, range: start..<endIndex) {
            result.append(range.lowerBound)
            start = range.lowerBound < range.upperBound ? range.upperBound : index(range.lowerBound, offsetBy: 1, limitedBy: endIndex) ?? endIndex
        }
        return result
    }
    func ranges<T: StringProtocol>(of string: T, options: String.CompareOptions = []) -> [Range<Index>] {
        var result: [Range<Index>] = []
        var start = startIndex
        while start < endIndex, let range = range(of: string, options: options, range: start..<endIndex) {
            result.append(range)
            start = range.lowerBound < range.upperBound  ? range.upperBound : index(range.lowerBound, offsetBy: 1, limitedBy: endIndex) ?? endIndex
        }
        return result
    }
}

func getLinesArrayOfString(in label: UILabel) -> [String] {
    
    /// An empty string's array
    var linesArray = [String]()
    
    guard let text = label.text, let font = label.font else {return linesArray}
    
    let rect = label.frame
    
    let myFont: CTFont = CTFontCreateWithName(font.fontName as CFString, font.pointSize, nil)
    let attStr = NSMutableAttributedString(string: text)
    attStr.addAttribute((kCTFontAttributeName as NSAttributedStringKey) as String, value: myFont, range: NSRange(location: 0, length: attStr.length))
    
    let frameSetter: CTFramesetter = CTFramesetterCreateWithAttributedString(attStr as CFAttributedString)
    let path: CGMutablePath = CGMutablePath()
    path.addRect(CGRect(x: 0, y: 0, width: rect.size.width, height: 100000), transform: .identity)
    
    let frame: CTFrame = CTFramesetterCreateFrame(frameSetter, CFRangeMake(0, 0), path, nil)
    guard let lines = CTFrameGetLines(frame) as? [Any] else {return linesArray}
    
    for line in lines {
        let lineRef = line as! CTLine
        let lineRange: CFRange = CTLineGetStringRange(lineRef)
        let range = NSRange(location: lineRange.location, length: lineRange.length)
        let lineString: String = (text as NSString).substring(with: range)
        linesArray.append(lineString)
    }
    return linesArray
}

class ModalView: UIView {
    var OkButtonHandler: (() -> Void)?
    var Cancel2ButtonHandler: (() -> Void)?
    var CameraButtonHandler: (() -> Void)?
    var GalleryButtonHandler: (() -> Void)?
    var ChatButtonHandler: (() -> Void)?

    @IBOutlet weak var contentView: UIView!
    @IBOutlet weak var pickerView: UIView!
    
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblMessage: UILabel!
    
    @IBOutlet weak var heightPop: NSLayoutConstraint!
    
    @IBOutlet weak var btnOk: UIButton!
    @IBOutlet weak var btnCancel2: UIButton!
    @IBOutlet weak var btnChat: UIButton!
    
    @IBOutlet weak var lblCamera: UILabel!
    @IBOutlet weak var lblGallery: UILabel!
    @IBOutlet weak var viewSepartor: UIView!
    
    @IBOutlet weak var widthBtnCancel: NSLayoutConstraint!
    
    @IBOutlet weak var viewChat: UIView!
    @IBOutlet weak var widthViewChat: NSLayoutConstraint!
    @IBOutlet weak var lblChatCount: UILabel!
    @IBOutlet weak var titleBtnChat: UILabel!
    
    var chatBadge : String = ""
    
    class func instantiateFromNib() -> ModalView {
        let view = UINib(nibName: "ModalView", bundle: nil).instantiate(withOwner: nil, options: nil).first as! ModalView
        
        return view
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.configure()
    }
    
    fileprivate func configure() {
        self.contentView.layer.cornerRadius = AppData.sharedInstance.cornerradius
        self.contentView.clipsToBounds = true
        
        self.layer.shouldRasterize = true
        self.layer.rasterizationScale = UIScreen.main.scale
    }

    func updateButtonToOrange(_ objButton: UIButton!)
    {
        objButton.backgroundColor = UIColor(red: 242.0/255.0, green: 126.0/255.0, blue: 103.0/255.0, alpha: 1.0)
//        objButton.layer.cornerRadius = 8
//        objButton.clipsToBounds = true
//        objButton.layer.borderWidth = 1
        objButton.setTitleColor(UIColor.white, for: UIControlState())
//        objButton.layer.borderColor = UIColor(red: 242.0/255.0, green: 126.0/255.0, blue: 103.0/255.0, alpha: 1.0).cgColor
    }

    func updateButtonToGray(_ objButton: UIButton!)
    {
        objButton.backgroundColor = UIColor(red: 180.0/255.0, green: 184.0/255.0, blue: 195.0/255.0, alpha: 1.0)
//        objButton.layer.cornerRadius = 8
//        objButton.clipsToBounds = true
//        objButton.layer.borderWidth = 1
        objButton.setTitleColor(UIColor(red: 73.0/255.0, green: 82.0/255.0, blue: 93.0/255.0, alpha: 1.0), for: UIControlState())
//        objButton.layer.borderColor = UIColor(red: 109.0/255.0, green: 109.0/255.0, blue: 109.0/255.0, alpha: 1.0).cgColor
    }
    
    func updateButtonToGreen(_ objView: UIView!)
    {
        objView.backgroundColor = UIColor(red: 4.0/255.0, green: 181.0/255.0, blue: 0.0/255.0, alpha: 1.0)
    }
    
    func updateButtonToBlue(_ objView: UIView!)
    {
        objView.backgroundColor = UIColor(red: 0.0/255.0, green: 124.0/255.0, blue: 255.0/255.0, alpha: 1.0)
    }
    
    func updateMessages(_ titleMessage: String, descMessage: String, okTitle: String, type: ModalAlertType) {
        
        viewChat.isHidden = true
        let attri = NSAttributedString(string: okTitle, attributes: [NSForegroundColorAttributeName : UIColor.white])
        btnOk.setAttributedTitle(attri, for:  UIControlState())
        updateButtonToOrange(btnOk)
        
        switch type {
       
        case .modalAlert:
            break
        case .modalAlertPicker:
            break
        case .modalAlertSaved:
            btnCancel2.isHidden = true
            break
        case .modalAlertSelectImageSource:
            pickerView.isHidden = false
            btnCancel2.isHidden = true
            break
        }

        let paragraphStyle = NSMutableParagraphStyle()
        paragraphStyle.alignment = NSTextAlignment.center
        paragraphStyle.lineHeightMultiple = 20.0
        paragraphStyle.maximumLineHeight = 20.0
        paragraphStyle.minimumLineHeight = 20.0
        
        let string: String = SnapTaskUtlity.updateTextsToRemoveOrphansForErrorAlert(descMessage,lblMessage: lblMessage)!
        
        let ats = [NSFontAttributeName: UIFont(name: "Arial", size: 16.0)!, NSParagraphStyleAttributeName : paragraphStyle] as [String : Any]
        self.lblMessage.attributedText = NSAttributedString(string: string, attributes: ats)
        
        lblTitle.text = titleMessage
        
        let height = lblMessage.sizeThatFits(CGSize(width: lblMessage.frame.size.width, height: CGFloat.leastNormalMagnitude)).height
        
        heightPop.constant = height > 36 ? (165+height) : 201
        self.frame = CGRect.init(x: 0, y: 0, width: 292.0, height: heightPop.constant)
        
        self.layoutIfNeeded()
    }
 
    func updateModal(_ titleMessage: String, descMessage: String, firstTitle: String, secondTitle: String, type: ModalAlertType) {
        
        viewChat.isHidden = true

        switch type {
            
        case .modalAlert:
            let attri = NSAttributedString(string: "\(firstTitle)", attributes: [NSForegroundColorAttributeName : UIColor.white])
            btnOk.setAttributedTitle(attri, for:  UIControlState())
            btnCancel2.setTitle("  \(secondTitle)  ", for: UIControlState())
            widthBtnCancel.constant = self.frame.width / 2
            updateButtonToGray(btnCancel2)
            updateButtonToOrange(btnOk)
            break
            
        case .modalAlertPicker:
            pickerView.isHidden = false
            lblCamera.text = firstTitle
            lblGallery.text = secondTitle
            
            btnCancel2.isHidden = true
            let attri = NSAttributedString(string: "CANCEL", attributes: [NSForegroundColorAttributeName : UIColor.white])
            btnOk.setAttributedTitle(attri, for:  UIControlState())
            updateButtonToOrange(btnOk)
            break
            
        case .modalAlertSaved:
            break
        case .modalAlertSelectImageSource:
            break
        }
        
        let paragraphStyle = NSMutableParagraphStyle()
        paragraphStyle.alignment = NSTextAlignment.center
        paragraphStyle.lineHeightMultiple = 20.0
        paragraphStyle.maximumLineHeight = 20.0
        paragraphStyle.minimumLineHeight = 20.0
        let string: String = SnapTaskUtlity.updateTextsToRemoveOrphansForErrorAlert(descMessage,lblMessage: lblMessage)!
        let ats = [NSFontAttributeName: UIFont(name: "Arial", size: 16.0)!, NSParagraphStyleAttributeName : paragraphStyle] as [String : Any]
        self.lblMessage.attributedText = NSAttributedString(string: string, attributes: ats)
        
        lblTitle.text = titleMessage
        
        let height = lblMessage.sizeThatFits(CGSize(width: lblMessage.frame.size.width, height: CGFloat.leastNormalMagnitude)).height
        
        heightPop.constant = height > 36 ? (165+height) : 201
        self.frame = CGRect.init(x: 0, y: 0, width: 292.0, height: heightPop.constant)
        
        self.layoutIfNeeded()
    }
    
    func updateModalDeleting(_ titleMessage: String, descMessage: String, firstTitle: String, secondTitle: String, thirdTitle: String) {
        
        viewChat.isHidden = false
        lblChatCount.text = chatBadge
        
        let attri = NSAttributedString(string: "\(firstTitle)", attributes: [NSForegroundColorAttributeName : UIColor.white])
        btnOk.setAttributedTitle(attri, for:  UIControlState())
        titleBtnChat.text = "\(secondTitle)"
        btnCancel2.setTitle("  \(thirdTitle)  ", for: UIControlState())
        
        updateButtonToGray(btnCancel2)
        updateButtonToOrange(btnOk)
        
        if secondTitle == "CHAT"
        {
            updateButtonToGreen(viewChat)
            lblChatCount.textColor = UIColor(red: 4.0/255.0, green: 181.0/255.0, blue: 0.0/255.0, alpha: 1.0)
            widthBtnCancel.constant = self.frame.width / 3
            widthViewChat.constant = self.frame.width / 3
        }
        else
        {
            updateButtonToBlue(viewChat)
            lblChatCount.textColor = UIColor(red: 0.0/255.0, green: 124.0/255.0, blue: 255.0/255.0, alpha: 1.0)
            widthBtnCancel.constant = self.frame.width*0.29
            widthViewChat.constant = self.frame.width*0.42
        }
        
        let paragraphStyle = NSMutableParagraphStyle()
        paragraphStyle.alignment = NSTextAlignment.center
        paragraphStyle.lineHeightMultiple = 20.0
        paragraphStyle.maximumLineHeight = 20.0
        paragraphStyle.minimumLineHeight = 20.0
        let string: String = SnapTaskUtlity.updateTextsToRemoveOrphansForErrorAlert(descMessage,lblMessage: lblMessage)!
        let ats = [NSFontAttributeName: UIFont(name: "Arial", size: 16.0)!, NSParagraphStyleAttributeName : paragraphStyle] as [String : Any]
        self.lblMessage.attributedText = NSAttributedString(string: string, attributes: ats)
        
        lblTitle.text = titleMessage
        
        let height = lblMessage.sizeThatFits(CGSize(width: lblMessage.frame.size.width, height: CGFloat.leastNormalMagnitude)).height
        
        heightPop.constant = height > 36 ? (165+height) : 201
        self.frame = CGRect.init(x: 0, y: 0, width: 292.0, height: heightPop.constant)
        
        self.layoutIfNeeded()
    }
    
    @IBAction func btn_clkCamera(_ sender: AnyObject) {
        self.CameraButtonHandler?()
    }

    @IBAction func btn_clkGallery(_ sender: AnyObject) {
        self.GalleryButtonHandler?()
    }

    @IBAction func btn_clkCancel2(_ sender: AnyObject) {
        self.Cancel2ButtonHandler?()
    }
    
    @IBAction func btnChatTapped(_ sender: UIButton) {
        self.ChatButtonHandler?()
    }
    
    @IBAction func btn_clkOk(_ sender: AnyObject) {
        self.OkButtonHandler?()
    }
}
