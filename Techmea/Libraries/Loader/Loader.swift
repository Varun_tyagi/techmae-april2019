//
//  Loader.swift
//  CircleK
//
//  Created by Hitesh Dhawan on 18/07/18.
//  Copyright © 2018 Varun Tyagi. All rights reserved.
//

import Foundation
import UIKit


class Loader: UIView  {
    
    var activityView1 = UIView()
    var imageForR = UIImageView()
    var logoView = UIView()

    class var shared: Loader {
        struct Static {
            static let instance: Loader = Loader()
        }
        return Static.instance
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        activityView1 = UIView(frame: CGRect(x: 0, y: 0, width: ScreenSize.SCREEN_WIDTH , height: ScreenSize.SCREEN_HEIGHT))
        activityView1.backgroundColor = UIColor.clear
        let back = UIImageView(frame: CGRect(x: 0, y: 0, width: ScreenSize.SCREEN_WIDTH, height: ScreenSize.SCREEN_HEIGHT))
        back.backgroundColor = UIColor.black
        back.alpha = 0.5
        activityView1.addSubview(back)
        logoView = UIView(frame: CGRect(x: (ScreenSize.SCREEN_WIDTH - 75) / 2, y: (ScreenSize.SCREEN_HEIGHT - 75) / 2, width: 75, height: 75))
        logoView.layer.cornerRadius = 37.5
        logoView.backgroundColor = UIColor.white
        imageForR = UIImageView(frame: CGRect(x: 10, y: 10, width: 55, height: 55))
        imageForR.image = UIImage(named: "marker")
        imageForR.backgroundColor = UIColor.clear
        imageForR.contentMode = .scaleAspectFit
        logoView.addSubview(imageForR)
        activityView1.addSubview(logoView)
        logoView.addSubview(imageForR)
        activityView1.addSubview(logoView)
        Constants.appDelegate.window?.addSubview(activityView1)
        Constants.appDelegate.window?.sendSubview(toBack: activityView1)
    }
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    
    func startIndicator() {
        DispatchQueue.main.async(execute: {
            Constants.appDelegate.window?.addSubview(self.activityView1)
            self.rotate360Degrees()
            Constants.appDelegate.window?.bringSubview(toFront: self.activityView1)
        })
    }
    
    func stopIndicator() {
        DispatchQueue.main.async(execute: {
            self.imageForR.layer.removeAllAnimations()
            Constants.appDelegate.window?.willRemoveSubview(self.activityView1)
            self.activityView1.removeFromSuperview()
        })
    }
    
    func rotate360Degrees() {
        let rotate = CABasicAnimation(keyPath: "transform.rotation.y")
        rotate.fromValue = nil
        rotate.toValue = 180
        rotate.duration = 50
        rotate.repeatCount = 100.0
        imageForR.layer.add(rotate, forKey: "myRotationAnimation")
    }
    
}



