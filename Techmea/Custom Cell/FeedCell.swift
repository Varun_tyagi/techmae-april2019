
//  FeedCell.swift
//  Techmae
//
//  Created by VISHAL SETH on 8/28/18.
//  Copyright © 2018 VISHAL SETH. All rights reserved.
//

import UIKit
import AVKit
import AVFoundation
import SwiftyDrop
import ImageSlideshow
import SwiftLinkPreview
import JTMaterialSpinner
import SKPhotoBrowser
import ReadMoreTextView


let SKBrowserStartIndex = "skBeowserStartIndex"
class multimediacell: UICollectionViewCell
{
    @IBOutlet weak var play_btn_image: UIImageView!
    @IBOutlet weak var imgMultimedia: UIImageView!
    @IBOutlet weak var spinner: JTMaterialSpinner!
    
    @IBOutlet weak var imageHeight: NSLayoutConstraint!
}

class FeedCell: UITableViewCell,UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout,UIScrollViewDelegate {
    
    
    @IBOutlet weak var viewMain: UIView!
    
    //----OUTLETS OF SECOND CELL
    
    
    //Link Outlets
    @IBOutlet weak var centerLoadingActivityIndicatorView: UIActivityIndicatorView?
    @IBOutlet  weak var indicator: UIActivityIndicatorView?
    @IBOutlet weak var loaderspinner: JTMaterialSpinner!
    
    @IBOutlet weak var LikeListBtn: UIButton!
    @IBOutlet weak var previewArea: UIView?
    @IBOutlet weak var btnPreviewLink: UIButton!
    
    @IBOutlet weak var slideshow: UIImageView!
    @IBOutlet weak var previewTitle: UILabel?
    @IBOutlet weak var previewCanonicalUrl: UILabel?
    @IBOutlet weak var previewDescription: UILabel?
    @IBOutlet weak var detailedView: UIView?
    @IBOutlet weak var favicon: UIImageView?
    
    
    @IBOutlet weak var previewHeight: NSLayoutConstraint!
    
    @IBOutlet weak var heightPreviewLink: NSLayoutConstraint!
    @IBOutlet weak var imagePagerHeight: NSLayoutConstraint!

    //Other
    @IBOutlet weak var lblAuthor: UILabel!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblName2: UILabel!
    @IBOutlet weak var lblNameWidth: NSLayoutConstraint!
    @IBOutlet weak var lblName2Width: NSLayoutConstraint!
    
    @IBOutlet weak var imgTriangle: UIImageView!
    @IBOutlet weak var lblGroupName: UILabel!
    @IBOutlet weak var lblGroupName2: UILabel!
    
    @IBOutlet weak var groupNameLabelWidth: NSLayoutConstraint!
    @IBOutlet weak var groupNameLabel2Width: NSLayoutConstraint!
    @IBOutlet weak var imgTriangleWidth: NSLayoutConstraint!
    
    
    @IBOutlet weak var name_view: UIView!
    
    @IBOutlet weak var imgPoster: UIImageView!
    @IBOutlet weak var btnOption: UIButton!
    @IBOutlet weak var btnOptionWidth: NSLayoutConstraint!
    @IBOutlet weak var btnPinPost: UIButton!
  
    
    @IBOutlet weak var btnComment: UIButton!
    @IBOutlet weak var btnLike: UIButton!
    @IBOutlet weak var imgSender: UIImageView!
    @IBOutlet weak var imgSender2: UIImageView!
    @IBOutlet weak var lblSenderMsg: UILabel!
    @IBOutlet weak var lblSenderTime: UILabel!
    
    @IBOutlet weak var lblSenderName: UILabel!
    @IBOutlet weak var lblReciverName: UILabel!
    
    @IBOutlet weak var lblReceiverDate: UILabel!
    @IBOutlet weak var imgRecv: UIImageView!
    @IBOutlet weak var imgRecv2: UIImageView!
    @IBOutlet weak var lblReciever: UILabel!
    @IBOutlet weak var btnLoadMoreCmnt: UIButton!
    
    @IBOutlet weak var lblTOtalCmnt: UILabel!
    @IBOutlet weak var lblTOtalLike: UILabel!
    @IBOutlet weak var btnHeart: UIButton!
    

    @IBOutlet weak var txtPostTitle: ReadMoreTextView!
    @IBOutlet weak var txtMsg: UITextView!
    @IBOutlet weak var collectionPageHeight: NSLayoutConstraint!
    @IBOutlet weak var commentViewHeight: NSLayoutConstraint!
    
    
    @IBOutlet weak var viewComment1: UIView!
    @IBOutlet weak var viewComment2: UIView!
    
    @IBOutlet weak var viewLblComment1: UIView!
    @IBOutlet weak var viewLblComment2: UIView!

    @IBOutlet weak var heightViewComment1: NSLayoutConstraint!
    @IBOutlet weak var heightViewComment2: NSLayoutConstraint!
    @IBOutlet weak var widthCommenter1: NSLayoutConstraint!
    @IBOutlet weak var widthCommenter2: NSLayoutConstraint!
    @IBOutlet weak var widthCommenter3: NSLayoutConstraint!
    @IBOutlet weak var widthCommenter4: NSLayoutConstraint!

    @IBOutlet weak var btnHeightCell: NSLayoutConstraint!
    
    @IBOutlet weak var like_heart: Floater!
    
    @IBOutlet weak var multimediaCollectionView: UICollectionView!
    
    @IBOutlet weak var media_pagecontrol: UIPageControl!
    
    
    var mainCellIndexPath = IndexPath()
    
    override func awakeFromNib() {
        super.awakeFromNib()
        multimediaCollectionView.layoutMargins = UIEdgeInsetsMake(2, 2, 2, 2)
        media_pagecontrol.isHidden=true
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        
        txtPostTitle.onSizeChange = { _ in }
        txtPostTitle.shouldTrim = true
    }
    
    
    var player = AVPlayer()
    var playerController = AVPlayerViewController()
    var imgArray = [PostContent]()
    
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return imgArray.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "multimediacell", for: indexPath) as! multimediacell
        
        cell.imgMultimedia.contentMode = .scaleAspectFit
       // cell.imgMultimedia.layer.borderColor = UIColor.lightGray.cgColor
       // cell.imgMultimedia.layer.borderWidth = 0.5
        cell.imgMultimedia.contentMode = .scaleAspectFill

        
        let postimg = imgArray[indexPath.row]
        
        let imageURL:String = postimg.post
        let imgURL = URL.init(string: imageURL)
        
        cell.spinner.circleLayer.lineWidth = 2.0
        cell.spinner.circleLayer.strokeColor = UIColor.orange.cgColor
        
        cell.imgMultimedia.image = nil
        
        if let type_str = imgURL?.lastPathComponent
        {
            
            if type_str.lowercased().range(of:"mp4") != nil
            {
                
                
                //cell.imgMultimedia.sd_showActivityIndicatorView()
                cell.play_btn_image.isHidden = true
                cell.spinner.beginRefreshing()
                cell.imgMultimedia.sd_setImage(with: URL(string: postimg.preview_image), placeholderImage: UIImage.init(), completed: { (image, error, type, url) in
                                            cell.spinner.endRefreshing()
                                            cell.play_btn_image.isHidden = false
                    
                                        })
        
                
            }
            else
            {
                //cell.imgMultimedia.sd_showActivityIndicatorView()
                cell.play_btn_image.isHidden = true
                cell.spinner.beginRefreshing()
                cell.imgMultimedia.sd_setImage(with: URL(string: postimg.post), placeholderImage: UIImage.init(), completed: { (image, error, type, url) in
                    //cell.imgMultimedia.sd_removeActivityIndicator()
                    cell.spinner.endRefreshing()
                    
                    if error != nil
                    {
                        print(error.debugDescription)
                    }
                    
                    
                    DispatchQueue.main.async{
                    if let imageTOUSe = image {
                                           let originalImg = imageTOUSe
                        let wide = self.getImageSize(indexPath: indexPath).width
                        
                    
                    
                          cell.imageHeight.constant = originalImg.getHeightAcordingToWidth(wide)
                        
                        
                        if self.imgArray.count > indexPath.row{
                        let  newImgPost = self.imgArray[indexPath.row]
                        newImgPost.imageHeight = Double(cell.imageHeight.constant)
                        self.imgArray[indexPath.row] = newImgPost
                                         self.multimediaCollectionView.layoutIfNeeded()
                                        // self.multimediaCollectionView.reloadData()
                                         //self.multimediaCollectionView.layoutIfNeeded()
                        }


                           //  cell.imgMultimedia.image = originalImg
                                           
                                       }
                        

                    }
                    
                    
                })
                
            }
            
        }
        else
        {
            //cell.imgMultimedia.sd_removeActivityIndicator()
            cell.spinner.endRefreshing()
            cell.play_btn_image.isHidden = true
            cell.imgMultimedia.image = nil
        }
        return cell
    }
    
    func getImageSize(indexPath:IndexPath)->CGSize{
        
        
        
        if self.imgArray.count == 1 {
                   return CGSize(width: multimediaCollectionView.frame.size.width, height: 171.0)

               }
              else  if self.imgArray.count == 2 {
                 
           return CGSize(width: multimediaCollectionView.frame.size.width/2, height: 171.0)
                           

               }else if self.imgArray.count == 3 {
            return CGSize(width: multimediaCollectionView.frame.size.width/3, height: 171.0)

//                   switch indexPath.row {
//                   case 0,1:
//           return CGSize(width: multimediaCollectionView.frame.size.width/2, height: 171.0/2)
//                       default:
//           return CGSize(width: multimediaCollectionView.frame.size.width, height: 171.0/2)
//
//                   }
               }
               else if self.imgArray.count == 4 {
           // return CGSize(width: multimediaCollectionView.frame.size.width/4, height: 171.0)

                       switch indexPath.row {
                       case 0,1:
               return CGSize(width: multimediaCollectionView.frame.size.width/2, height: 171.0/2)
                           default:
               return CGSize(width: multimediaCollectionView.frame.size.width/2, height: 171.0/2)

                       }
               }else{
                   switch indexPath.row {
                           case 0,1:
                   return CGSize(width: multimediaCollectionView.frame.size.width/2, height: 171.0/2)
                               case 2,3,4:
                   return CGSize(width: multimediaCollectionView.frame.size.width/3, height: 171.0/2)
                               default:
                   return CGSize(width: multimediaCollectionView.frame.size.width/4, height: 171.0/2)
                                               
                           }
               }
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        let postimg = imgArray[indexPath.row]
    
        let imageURL:String = postimg.post
        let imgURL = URL.init(string: imageURL)
        var data_dict = [String:String]()
        var images = [SKPhotoProtocol]()

    
        
        if let type_str = imgURL?.lastPathComponent
        {
            if type_str.lowercased().range(of:"mp4") != nil
            {
                data_dict["video_url"] = postimg.post
                data_dict["postType"] = postimg.postType
                data_dict["preview_image"] = postimg.preview_image
                 NotificationCenter.default.post(name: NSNotification.Name(rawValue: "Play_video"), object: nil, userInfo: data_dict)
                
            }
            else
            {
                data_dict["image_url"] = postimg.post
                data_dict["postType"] = postimg.postType
                data_dict["preview_image"] = postimg.preview_image
                for imgPost in self.imgArray{
                    let imgURL = URL.init(string: imgPost.post)

                    if let type_str = imgURL?.lastPathComponent
                           {
                               if type_str.lowercased().range(of:"mp4") == nil
                               {
                                let photo = SKPhoto.photoWithImageURL(imgPost.post)
                            
                                photo.shouldCachePhotoURLImage = false
                                               photo.isVideo = false
                                               images.append(photo)
                            }
                            
                    }
                            }
//                let photo = SKPhoto.photoWithImageURL(postimg.post)
//                photo.shouldCachePhotoURLImage = false
//                photo.isVideo = false
//                images.append(photo)
                
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "Show_image"), object: images, userInfo: [SKBrowserStartIndex:indexPath.row])
            }
            
        }
        else
        {
            let img_url = Bundle.main.url(forResource: "noimage", withExtension: "png")
            
            let photo = SKPhoto.photoWithImageURL("\(String(describing: img_url))")
            photo.shouldCachePhotoURLImage = false // you can use image cache by true(NSCache)
            photo.isVideo = false
            images.append(photo)
            
            data_dict["image_url"] = "\(String(describing: img_url))"
            data_dict["postType"] = "image"
            data_dict["preview_image"] = ""
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "Show_image"), object: images, userInfo: nil)
        }
    }
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize
    {
        media_pagecontrol.isHidden=true
        let cellSize = self.getImageSize(indexPath: indexPath)
               
               let imgPost = imgArray[indexPath.row]
        
        
        let newImgArray = imgArray.sorted { (img1, img2) -> Bool in
            return img1.imageHeight > img2.imageHeight
        }
        
        self.imagePagerHeight.constant = CGFloat(newImgArray.first?.imageHeight ?? 171)
        self.previewHeight.constant = CGFloat(newImgArray.first?.imageHeight ?? 171)
        self.heightPreviewLink?.constant = CGFloat(newImgArray.first?.imageHeight ?? 171)
        
        if imgArray.count > 4 {
            self.imagePagerHeight.constant = CGFloat(newImgArray.first?.imageHeight ?? 171) + CGFloat(imgArray.last?.imageHeight ?? 171)
                   self.previewHeight.constant = CGFloat(newImgArray.first?.imageHeight ?? 171) + CGFloat(imgArray.last?.imageHeight ?? 171)
                   self.heightPreviewLink?.constant = CGFloat(newImgArray.first?.imageHeight ?? 171) + CGFloat(imgArray.last?.imageHeight ?? 171)
        }
        
      //  NotificationCenter.default.post(name: NSNotification.Name.init(NotificationManageCellHeightImage), object: nil, userInfo: ["mainCellIndex":self.mainCellIndexPath,"postContent":self.imgArray])
        
 
        self.layoutIfNeeded()
        return CGSize.init(width:  Double(cellSize.width), height: imgPost.imageHeight)

    }
    
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView)
    {
        var visibleRect = CGRect()
        
        visibleRect.origin = multimediaCollectionView.contentOffset
        visibleRect.size = multimediaCollectionView.bounds.size
        
        let visiblePoint = CGPoint(x: visibleRect.midX, y: visibleRect.midY)
        
        guard let indexPath = multimediaCollectionView.indexPathForItem(at: visiblePoint) else { return }
        
        
        media_pagecontrol.currentPage = indexPath.item
        
        
    }

    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    func setData(data:PostListModel, _ dataIndexPath:IndexPath = IndexPath.init(row: 0, section: 0 ))
    {
        //lblName.text=data.username
        self.setEmptyData()
        
        self.mainCellIndexPath = dataIndexPath
        self.multimediaCollectionView.reloadData()
        self.imgArray = data.postcontent
        multimediaCollectionView.delegate = self
        multimediaCollectionView.dataSource = self
        
        if data.postcomments.count > 0  {
        let  commentMod = data.postcomments[0]
        
                    let dateCreated = commentMod.created_At
                          
                              if dateCreated >  0
                              {
                                  let date = Date(timeIntervalSince1970: Double(dateCreated))
                           self.lblSenderTime?.text="\(date.timeAgoSinceDate(true))"
                              }
                    if commentMod.createdAtStr.count > 0 {
                        let dateFormatter = DateFormatter()
                        dateFormatter.timeZone = NSTimeZone.local //Set timezone that you want
                        dateFormatter.dateFormat = "yyyy-MM-dd hh:mm:ss" //Specify your format that you want
                        let strDate = dateFormatter.date(from: commentMod.createdAtStr)
                        self.lblSenderTime?.text="\(strDate!.timeAgoSinceDate(true))"

                    }
            
            if data.postcomments.count > 1 {
                  let  commentMod = data.postcomments[1]
                      
                                  let dateCreated = commentMod.created_At
                                        
                                            if dateCreated >  0
                                            {
    let date = Date(timeIntervalSince1970: Double(dateCreated))
    self.lblReceiverDate?.text="\(date.timeAgoSinceDate(true))"
                                            }
                                  if commentMod.createdAtStr.count > 0 {
    let dateFormatter = DateFormatter()
    dateFormatter.timeZone = NSTimeZone.local //Set timezone that you want
    dateFormatter.dateFormat = "yyyy-MM-dd hh:mm:ss" //Specify your format that you want
    let strDate = dateFormatter.date(from: commentMod.createdAtStr)
    self.lblReceiverDate?.text="\(strDate!.timeAgoSinceDate(true))"

                                  }

            }
            
            
        }
        
        
    }
    
    func setEmptyData()
    {
        self.imgArray.removeAll()
    }
}


public extension UIImage {

    func tint(with color: UIColor) -> UIImage {
        var image = withRenderingMode(.alwaysTemplate)
        UIGraphicsBeginImageContextWithOptions(size, false, scale)
        color.set()

        image.draw(in: CGRect(origin: .zero, size: size))
        image = UIGraphicsGetImageFromCurrentImageContext()!
        UIGraphicsEndImageContext()
        return image
    }

    func toAttributedString(with heightRatio: CGFloat, tint color: UIColor? = nil) -> NSAttributedString {
        let attachment = NSTextAttachment()
        var image = self

        if let tintColor = color {
            image.withRenderingMode(.alwaysTemplate)
            image = image.tint(with: tintColor)
        }

        attachment.image = image

        let ratio: CGFloat = image.size.width / image.size.height
        let attachmentBounds = attachment.bounds

        attachment.bounds = CGRect(x: attachmentBounds.origin.x,
                                   y: attachmentBounds.origin.y,
                                   width: ratio * heightRatio,
                                   height: heightRatio)

        return NSAttributedString(attachment: attachment)
    }
}
