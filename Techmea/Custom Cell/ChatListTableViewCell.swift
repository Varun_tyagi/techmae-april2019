//
//  ChatListTableViewCell.swift
//  Techmae
//
//  Created by varun tyagi on 27/08/18.
//  Copyright © 2018 VISHAL SETH. All rights reserved.
//

import UIKit

class ChatListTableViewCell: UITableViewCell {

    override func awakeFromNib() {
        super.awakeFromNib()
        self.layer.cornerRadius=22
        self.clipsToBounds=true
        self.layer.shadowColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.25).cgColor
        self.layer.shadowOffset = CGSize(width: 0, height: 3)
        self.layer.shadowOpacity = 1.0
        self.layer.shadowRadius = 3.0
        self.layer.masksToBounds = false
    }

   

}
