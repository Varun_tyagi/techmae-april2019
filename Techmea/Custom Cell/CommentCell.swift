//
//  CommentCell.swift
//  Techmae
//
//  Created by VISHAL SETH on 8/28/18.
//  Copyright © 2018 VISHAL SETH. All rights reserved.
//

import UIKit

class CommentCell: UITableViewCell {
    
    @IBOutlet weak var userImg: UIImageView!
    @IBOutlet weak var lblUsername: UILabel!
    @IBOutlet weak var viewcommet: UIView!
    @IBOutlet weak var lblComment: UILabel!
    
    @IBOutlet weak var widthUsername: NSLayoutConstraint!
    @IBOutlet weak var widthUsername2: NSLayoutConstraint!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        widthUsername.constant = UIScreen.main.bounds.width - 118.0
//        widthUsername2.constant = UIScreen.main.bounds.width - 118.0
    }

    override func layoutSubviews() {
        super.layoutSubviews()

        viewcommet.layer.cornerRadius = viewcommet.frame.size.height/2-10
        viewcommet.layer.masksToBounds = true
    }
    
    override func setSelected(_ selected: Bool, animated: Bool)
    {
        super.setSelected(selected, animated: animated)
    }

}
