//
//  PostFeedImageCell.swift
//  Techmea
//
//  Created by VISHAL SETH on 8/31/18.
//  Copyright © 2018 VISHAL SETH. All rights reserved.
//

import UIKit
import JTMaterialSpinner

class PostFeedImageCell: UICollectionViewCell {
    
    @IBOutlet weak var btnClose: UIButton!
    @IBOutlet weak var imgPostFeed: UIImageView!
    @IBOutlet weak var ImageSpinner: JTMaterialSpinner!
}
