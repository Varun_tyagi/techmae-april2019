//
//  LoginModel.swift
//  Techmae
//
//  Created by VISHAL SETH on 8/19/18.
//  Copyright © 2018 VISHAL SETH. All rights reserved.
//

import Foundation
// To parse the JSON, add this file to your project and do:
//
//   let loginModel = try LoginModel(json)
//
// To parse values from Alamofire responses:
//
//   Alamofire.request(url).responseLoginModel { response in
//     if let loginModel = response.result.value {
//       ...
//     }
//   }

import Foundation
import Alamofire

struct LoginModel: Codable {
    let code: Int?
    let status: String?
    let userDetails: UserDetails?
}

struct UserDetails: Codable {
    let userID: Int?
    let userType, fullname, email: String?
    let gender: Int?
    let dob, aboutme: JSONNull?
    let goals: String?
    let focusArea: JSONNull?
    let location: String?
    let city, country: JSONNull?
    let profession: String?
    let isVerified: Int?
    let userProfileImage: JSONNull?
    
    enum CodingKeys: String, CodingKey {
        case userID = "userId"
        case userType, fullname, email, gender, dob, aboutme, goals
        case focusArea = "focus_area"
        case location, city, country, profession
        case isVerified = "is_verified"
        case userProfileImage = "user_profile_image"
    }
}

// MARK: Convenience initializers and mutators

extension LoginModel {
    init(data: Data) throws {
        self = try newJSONDecoder().decode(LoginModel.self, from: data)
    }
    
    init(_ json: String, using encoding: String.Encoding = .utf8) throws {
        guard let data = json.data(using: encoding) else {
            throw NSError(domain: "JSONDecoding", code: 0, userInfo: nil)
        }
        try self.init(data: data)
    }
    
    init(fromURL url: URL) throws {
        try self.init(data: try Data(contentsOf: url))
    }
    
    func with(
        code: Int?? = nil,
        status: String?? = nil,
        userDetails: UserDetails?? = nil
        ) -> LoginModel {
        return LoginModel(
            code: code ?? self.code,
            status: status ?? self.status,
            userDetails: userDetails ?? self.userDetails
        )
    }
    
    func jsonData() throws -> Data {
        return try newJSONEncoder().encode(self)
    }
    
    func jsonString(encoding: String.Encoding = .utf8) throws -> String? {
        return String(data: try self.jsonData(), encoding: encoding)
    }
}

extension UserDetails {
    init(data: Data) throws {
        self = try newJSONDecoder().decode(UserDetails.self, from: data)
    }
    
    init(_ json: String, using encoding: String.Encoding = .utf8) throws {
        guard let data = json.data(using: encoding) else {
            throw NSError(domain: "JSONDecoding", code: 0, userInfo: nil)
        }
        try self.init(data: data)
    }
    
    init(fromURL url: URL) throws {
        try self.init(data: try Data(contentsOf: url))
    }
    
    func with(
        userID: Int?? = nil,
        userType: String?? = nil,
        fullname: String?? = nil,
        email: String?? = nil,
        gender: Int?? = nil,
        dob: JSONNull?? = nil,
        aboutme: JSONNull?? = nil,
        goals: String?? = nil,
        focusArea: JSONNull?? = nil,
        location: String?? = nil,
        city: JSONNull?? = nil,
        country: JSONNull?? = nil,
        profession: String?? = nil,
        isVerified: Int?? = nil,
        userProfileImage: JSONNull?? = nil
        ) -> UserDetails {
        return UserDetails(
            userID: userID ?? self.userID,
            userType: userType ?? self.userType,
            fullname: fullname ?? self.fullname,
            email: email ?? self.email,
            gender: gender ?? self.gender,
            dob: dob ?? self.dob,
            aboutme: aboutme ?? self.aboutme,
            goals: goals ?? self.goals,
            focusArea: focusArea ?? self.focusArea,
            location: location ?? self.location,
            city: city ?? self.city,
            country: country ?? self.country,
            profession: profession ?? self.profession,
            isVerified: isVerified ?? self.isVerified,
            userProfileImage: userProfileImage ?? self.userProfileImage
        )
    }
    
    func jsonData() throws -> Data {
        return try newJSONEncoder().encode(self)
    }
    
    func jsonString(encoding: String.Encoding = .utf8) throws -> String? {
        return String(data: try self.jsonData(), encoding: encoding)
    }
}

// MARK: Encode/decode helpers

class JSONNull: Codable {
    public init() {}
    
    public required init(from decoder: Decoder) throws {
        let container = try decoder.singleValueContainer()
        if !container.decodeNil() {
            throw DecodingError.typeMismatch(JSONNull.self, DecodingError.Context(codingPath: decoder.codingPath, debugDescription: "Wrong type for JSONNull"))
        }
    }
    
    public func encode(to encoder: Encoder) throws {
        var container = encoder.singleValueContainer()
        try container.encodeNil()
    }
}

func newJSONDecoder() -> JSONDecoder {
    let decoder = JSONDecoder()
    if #available(iOS 10.0, OSX 10.12, tvOS 10.0, watchOS 3.0, *) {
        decoder.dateDecodingStrategy = .iso8601
    }
    return decoder
}

func newJSONEncoder() -> JSONEncoder {
    let encoder = JSONEncoder()
    if #available(iOS 10.0, OSX 10.12, tvOS 10.0, watchOS 3.0, *) {
        encoder.dateEncodingStrategy = .iso8601
    }
    return encoder
}

// MARK: - Alamofire response handlers

extension DataRequest {
    fileprivate func decodableResponseSerializer<T: Decodable>() -> DataResponseSerializer<T> {
        return DataResponseSerializer { _, response, data, error in
            guard error == nil else { return .failure(error!) }
            
            guard let data = data else {
                return .failure(AFError.responseSerializationFailed(reason: .inputDataNil))
            }
            
            return Result { try newJSONDecoder().decode(T.self, from: data) }
        }
    }
    
    @discardableResult
    fileprivate func responseDecodable<T: Decodable>(queue: DispatchQueue? = nil, completionHandler: @escaping (DataResponse<T>) -> Void) -> Self {
        return response(queue: queue, responseSerializer: decodableResponseSerializer(), completionHandler: completionHandler)
    }
    
    @discardableResult
    func responseLoginModel(queue: DispatchQueue? = nil, completionHandler: @escaping (DataResponse<LoginModel>) -> Void) -> Self {
        return responseDecodable(queue: queue, completionHandler: completionHandler)
    }
}
