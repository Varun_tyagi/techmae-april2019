//
//  PostListModel.swift
//  Techmea
//
//  Created by VISHAL SETH on 8/29/18.
//  Copyright © 2018 VISHAL SETH. All rights reserved.
//

import UIKit

class PostListModel: NSObject
{

    var post_id:String = ""
    var user_id:Int?
    var username:String = ""
    var user_image:String = ""
    var date:String = ""
    var time:String = ""
    var user_type:String = ""
    var post_subtitle:String = ""
    var post_description:String = ""
    var postcontent=[PostContent]()
    var likes :String = ""
    var postcomments = [PostComment]()
    var postTitle:String?  = ""
    var isLike : String? = ""
    var groupData : [String:Any]?
    var comments_count : String = ""
    var isPin : String? = ""
    var postLink = ""
    var created_At = 0
    
    init(dict:NSDictionary)
    {
        
        if let post_id_str = dict["post_id"] as? String
        {
            post_id=post_id_str
        }
        
        if let post_created_time_str = dict["created_at"] as? Int
        {
            created_At = post_created_time_str
        }
        if let group_post_id_str = dict["postId"] as? String
        {
            post_id=group_post_id_str
        }
        
        if let user_id_str = dict["user_id"] as? String
        {
            user_id=Int(user_id_str)
        }
        if let group_user_id_str = dict["postByUserId"] as? String
        {
            user_id = Int(group_user_id_str)
        }
        
        if let user_name_str = dict["username"] as? String
        {
            username=user_name_str
        }
        if let group_user_name_str = dict["postByUserName"] as? String
        {
            username = group_user_name_str
        }
        if let user_image_str = dict["user_image"] as? String
        {
            user_image=user_image_str
        }
        if let group_user_image_str = dict["user_profile_pic"] as? String
        {
            user_image = group_user_image_str
        }
        
        if let user_type_str = dict["user_type"] as? String
        {
            user_type = user_type_str
        }
        
        if let user_post_title_str = dict["post_title"] as? String
        {
            postTitle=user_post_title_str
        }
        if let group_post_title_str = dict["postName"] as? String
        {
            postTitle = group_post_title_str
        }
        
        if let is_like_str = dict["is_like"] as? String
        {
            isLike = is_like_str
        }
        
        if let post_date_str = dict["date"] as? String
        {
            date = post_date_str
        }
        
        if let post_time_str = dict["time"] as? String
        {
            time = post_time_str
        }
        
        if let likes_str = dict["likes"] as? String
        {
            likes = likes_str
        }
        
        if let isPin_str = dict["is_pin"] as? String
        {
            isPin = isPin_str
        }
        
        if let postLinkStr = dict["postLink"] as? String
        {
            postLink = postLinkStr
        }
        
        if let post_groupdata_str = dict["groupData"] as? [String:Any]
        {
            groupData = post_groupdata_str
        }
        
        if let arrGroupContent=dict["posts"]as? NSArray
        {
            for dictt in arrGroupContent
            {
                let tempgroupDict=dictt as! NSDictionary
                self.postcontent.append(PostContent.init(dict: tempgroupDict))
            }
        }
        
        if let arrContent=dict["postContent"]as? NSArray
        {
            for dictt in arrContent
            {
                let tempDict=dictt as! NSDictionary
                self.postcontent.append(PostContent.init(dict: tempDict))
            }
        }
        
        if let arrContent=dict["postMediaView"]as? NSArray
               {
                   for dictt in arrContent
                   {
                       let tempDict=dictt as! NSDictionary
                       self.postcontent.append(PostContent.init(dict: tempDict))
                   }
               }
       if let post_arrComments=dict["postComments"]as? NSArray
       {
            for dictt in post_arrComments
            {
                let tempDict=dictt as! NSDictionary
                self.postcomments.append(PostComment.init(dict: tempDict))
            }
        }
        
        if let group_arrComments=dict["comment"]as? NSArray
        {
            for dictt in group_arrComments
            {
                let tempDict=dictt as! NSDictionary
                self.postcomments.append(PostComment.init(dict: tempDict))
            }
        }
        if let comment_count_str = dict["comments"] as? String
        {
            comments_count = comment_count_str
        }
        
    }


    
}

class PostContent: NSObject
{
    var postType:String = ""
    var post:String=""
    var preview_image=""
    var imageHeight = 171.0
    init(dict:NSDictionary)
    {
        postType=dict["postType"]as! String
        post=dict["post"]as! String
        if let img_str = dict["preview_image"]as? String
        {
            preview_image = img_str
        }
        else
        {
            preview_image = ""
        }
    }
}

class PostComment: NSObject
{
    //postComents":[{"commentId":"", "comment":"", "userId":"", userImage":""}]
    var commentId:String = ""
    var comment:String = ""
    var userId:String = ""
    var userImage:String = ""
    var userName : String = ""
    var commentDate = ""
    var commentTime = ""
    var commentedBy = ""
    var commentByName  = ""
    var created_At = 0
    var createdAtStr = ""
    var modifiedDate = ""
    
    
    init(dict:NSDictionary)
    {
        
          if let post_created_time_str = dict["created_date"] as? Int
              {
                  created_At = post_created_time_str
              }
        if let post_created_time_str = dict["created_date"] as? String
                     {
                         createdAtStr = post_created_time_str
                     }
        
        if let comment_id_str = dict["comment_id"] as? Int
        {
            commentId="\(comment_id_str)"
        }
        if let group_comment_id_str = dict["commentId"] as? Int
        {
            commentId="\(group_comment_id_str)"
        }
        
        if let commentby_userid_str = dict["user_id"] as? Int
        {
            userId="\(commentby_userid_str)"
        }
        if let group_commentby_userid_str = dict["commentedBy"] as? Int
        {
            userId="\(group_commentby_userid_str)"
        }
        
        
        if let commentby_username_str = dict["username"] as? String
        {
            userName="\(commentby_username_str)"
        }
       
        if let group_commentby_username_str = dict["commentByName"] as? String
        {
            userName="\(group_commentby_username_str)"
        }
        
        
        
        if let commentby_userimage_str = dict["user_image"] as? String
        {
            userImage="\(commentby_userimage_str)"
        }
        
        if let group_commentby_userimage_str = dict["user_profile_pic"] as? String
        {
            userImage="\(group_commentby_userimage_str)"
        }
       
        
        
        if let comment_str = dict["comments"] as? String
        {
            comment="\(comment_str)"
        }
        
        if let group_comment_str = dict["comment"] as? String
        {
            comment="\(group_comment_str)"
        }
        

    }
}
extension Date {
    var millisecondsSince1970:Int64 {
        return Int64((self.timeIntervalSince1970 * 1000.0).rounded())
    }

    init(milliseconds:Int64) {
      // self =  Date.init(timeIntervalSinceNow: TimeInterval(milliseconds) / 1000)
       self = Date(timeIntervalSince1970: TimeInterval(milliseconds) / 1000)
    }
}
