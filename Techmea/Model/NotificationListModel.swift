//
//  NotificationListModel.swift
//  Techmea
//
//  Created by Dhaval Panchani on 17/10/18.
//  Copyright © 2018 VISHAL SETH. All rights reserved.
//

import UIKit

class NotificationListModel: NSObject {

    var id = 0
    var author_id = ""
    var reciever_id = ""
    var created_at = 0
    var is_seen = ""
    var notification_code = ""
    var notfication_type = ""
    var message = ""
    var status = 0
    var object_id = ""
    var detail = [String:Any]()
    var author = [String:Any]()
    
    init(dict:NSDictionary)
    {
        
        if let id_str = dict["id"] as? Int
        {
            id = id_str
        }
        
        if let author_id_str = dict["author_id"] as? String
        {
            author_id = author_id_str
        }
        
        if let reciever_is_str = dict["receiver_id"] as? String
        {
            reciever_id = reciever_is_str
        }
        
        if let created_at_str = dict["created_at"] as? Int
        {
            created_at = created_at_str
        }
        if let is_seen_str = dict["is_seen"] as? String
        {
            is_seen = is_seen_str
        }
        if let notification_code_str = dict["notification_code"] as? String
        {
            notification_code = notification_code_str
        }
        if let notfication_type_str = dict["notfication_type"] as? String
        {
            notfication_type = notfication_type_str
        }
        if let message_str = dict["message"] as? String
        {
            message = message_str
        }
        if let status_str = dict["status"] as? Int
        {
            status = status_str
        }
        if let object_id_str = dict["object_id"] as? String
        {
            object_id = object_id_str
        }
        if let object_id_str = dict["object_id"] as? Int
               {
                   object_id = "\(object_id_str)"
               }
        if let detail_arr = dict["detail"] as?  [String:Any]
        {
                detail = detail_arr
        }
        
        if let author_arr = dict["author"] as?  [String:Any]
        {
            
            author = author_arr
        }
    }
}
