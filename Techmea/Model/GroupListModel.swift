//
//  GroupListModel.swift
//  Techmea
//
//  Created by Dhaval Panchani on 08/09/18.
//  Copyright © 2018 VISHAL SETH. All rights reserved.
//

import UIKit

class GroupListModel: NSObject {

    var created_by = Int()
    var groupId = Int()
    var groupName = ""
    var imageUrl = ""
    var noOfMember = ""
    var isMember = Bool()
    var groupAdminID = Int()
}
