//
//  CoachBookingListModel.swift
//  Techmea
//
//  Created by Dhaval Panchani on 22/10/18.
//  Copyright © 2018 VISHAL SETH. All rights reserved.
//

import UIKit

class CoachBookingListModel: NSObject {
    
    var id = 0
    var user_id = ""
    var coach_id = ""
    var booking_date = ""
    var start_time = ""
    var end_time = ""
    var rate_per_hour = ""
    var service_tax_percentage = ""
    var service_tax_amount = ""
    var total_amount : CGFloat = 0.0
    var is_accepted = ""
    var is_paid = ""
    var status = 0
    var note = ""
    var customer_id = ""
    var txn_id = ""
    var coach = [String : Any]()
    var userData = [String : Any]()
    
    
    init(dict:NSDictionary)
    {
        if let id_str = dict["id"] as? Int
        {
            id = id_str
        }
        
        if let user_id_str = dict["user_id"] as? String
        {
            user_id = user_id_str
        }
        if let coach_id_str = dict["coach_id"] as? String
        {
            coach_id = coach_id_str
        }
        if let booking_date_str = dict["booking_date"] as? String
        {
            booking_date = booking_date_str
        }
        
        if let start_time_str = dict["start_time"] as? String
        {
            start_time = start_time_str
        }
        
        if let end_time_str = dict["end_time"] as? String
        {
            end_time = end_time_str
        }
        
        if let rate_per_hour_str = dict["rate_per_hour"] as? String
        {
            rate_per_hour = rate_per_hour_str
        }
        
        if let service_tax_percentage_str = dict["service_tax_percentage"] as? String
        {
            service_tax_percentage = service_tax_percentage_str
        }
        if let service_tax_amount_str = dict["service_tax_amount"] as? String
        {
            service_tax_amount = service_tax_amount_str
        }
        
        if let total_amount_str = dict["total_amount"] as? CGFloat
        {
            total_amount = total_amount_str
        }
        
        if let is_accepted_str = dict["is_accepted"] as? String
        {
            is_accepted = is_accepted_str
        }
        if let is_paid_str = dict["is_paid"] as? String
        {
            is_paid = is_paid_str
        }
        if let status_str = dict["status"] as? Int
        {
            status = status_str
        }
        
        if let note_str = dict["note"] as? String
        {
            note = note_str
        }
        
        if let customer_id_str = dict["customer_id"] as? String
        {
            customer_id = customer_id_str
        }
        
        if let txn_id_str = dict["txn_id"] as? String
        {
            txn_id = txn_id_str
        }
        
        if let coach_str = dict["coach"] as? [String:Any]
        {
            coach = coach_str
        }
        
        if let userData_str = dict["userData"] as? [String:Any]
        {
            userData = userData_str
        }
    }

}
