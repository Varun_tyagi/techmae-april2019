//
//  GroupDetailModel.swift
//  Techmea
//
//  Created by Dhaval Panchani on 18/09/18.
//  Copyright © 2018 VISHAL SETH. All rights reserved.
//

import UIKit

class GroupDetailModel: NSObject {
    
    var groupAdminId = Int()
    var groupAdminName = ""
    var groupAdminUrl = ""
    var groupId = Int()
    var groupImage = ""
    var groupMembersList = [[String:Any]]()
    var groupName = ""
    var is_join = Int()
    var posts = [[String:Any]]()
    var noOfMembers = 1
}
