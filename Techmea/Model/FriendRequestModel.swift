//
//  FriendRequestModel.swift
//  Techmea
//
//  Created by Dhaval Panchani on 07/09/18.
//  Copyright © 2018 VISHAL SETH. All rights reserved.
//

import UIKit

class FriendRequestModel: NSObject {

    var aboutme = ""
    var city = ""
    var country = ""
    var location = ""
    var userId = Int()
    var userImage = ""
    var userName = ""
    var is_like = Bool()
    var is_coach = Bool()
    var friend_request_id = Int()
    var hourly_rate : Int?
}
