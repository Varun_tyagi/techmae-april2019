// To parse the JSON, add this file to your project and do:
//
//   let suggestedFriendModel = try SuggestedFriendModel(json)
//
// To read values from URLs:
//
//   let task = URLSession.shared.suggestedFriendModelTask(with: url) { suggestedFriendModel, response, error in
//     if let suggestedFriendModel = suggestedFriendModel {
//       ...
//     }
//   }
//   task.resume()
//
// To parse values from Alamofire responses:
//
//   Alamofire.request(url).responseSuggestedFriendModel { response in
//     if let suggestedFriendModel = response.result.value {
//       ...
//     }
//   }

import Foundation
import Alamofire

struct SuggestedFriendModel: Codable {
    let code: Int?
    let message: String?
    let userData: [UserDatum]?
}

struct UserDatum: Codable {
    let userID: Int?
    let fullname, email: String?
    let location, aboutme, userProfileImage: String?
    
    enum CodingKeys: String, CodingKey {
        case userID = "user_id"
        case fullname, email, location, aboutme
        case userProfileImage = "user_profile_image"
    }
}

// MARK: Convenience initializers and mutators

extension SuggestedFriendModel {
    init(data: Data) throws {
        self = try newJSONDecoder().decode(SuggestedFriendModel.self, from: data)
    }
    
    init(_ json: String, using encoding: String.Encoding = .utf8) throws {
        guard let data = json.data(using: encoding) else {
            throw NSError(domain: "JSONDecoding", code: 0, userInfo: nil)
        }
        try self.init(data: data)
    }
    
    init(fromURL url: URL) throws {
        try self.init(data: try Data(contentsOf: url))
    }
    
    func with(
        code: Int?? = nil,
        message: String?? = nil,
        userData: [UserDatum]?? = nil
        ) -> SuggestedFriendModel {
        return SuggestedFriendModel(
            code: code ?? self.code,
            message: message ?? self.message,
            userData: userData ?? self.userData
        )
    }
    
    func jsonData() throws -> Data {
        return try newJSONEncoder().encode(self)
    }
    
    func jsonString(encoding: String.Encoding = .utf8) throws -> String? {
        return String(data: try self.jsonData(), encoding: encoding)
    }
}

extension UserDatum {
    init(data: Data) throws {
        self = try newJSONDecoder().decode(UserDatum.self, from: data)
    }
    
    init(_ json: String, using encoding: String.Encoding = .utf8) throws {
        guard let data = json.data(using: encoding) else {
            throw NSError(domain: "JSONDecoding", code: 0, userInfo: nil)
        }
        try self.init(data: data)
    }
    
    init(fromURL url: URL) throws {
        try self.init(data: try Data(contentsOf: url))
    }
    
    func with(
        userID: Int?? = nil,
        fullname: String?? = nil,
        email: String?? = nil,
        location: String?? = nil,
        aboutme: String?? = nil,
        userProfileImage: String?? = nil
        ) -> UserDatum {
        return UserDatum(
            userID: userID ?? self.userID,
            fullname: fullname ?? self.fullname,
            email: email ?? self.email,
            location: location ?? self.location,
            aboutme: aboutme ?? self.aboutme,
            userProfileImage: userProfileImage ?? self.userProfileImage
        )
    }
    
    func jsonData() throws -> Data {
        return try newJSONEncoder().encode(self)
    }
    
    func jsonString(encoding: String.Encoding = .utf8) throws -> String? {
        return String(data: try self.jsonData(), encoding: encoding)
    }
}



// MARK: - URLSession response handlers

extension URLSession {
    fileprivate func codableTask<T: Codable>(with url: URL, completionHandler: @escaping (T?, URLResponse?, Error?) -> Void) -> URLSessionDataTask {
        return self.dataTask(with: url) { data, response, error in
            guard let data = data, error == nil else {
                completionHandler(nil, response, error)
                return
            }
            completionHandler(try? newJSONDecoder().decode(T.self, from: data), response, nil)
        }
    }
    
    func suggestedFriendModelTask(with url: URL, completionHandler: @escaping (SuggestedFriendModel?, URLResponse?, Error?) -> Void) -> URLSessionDataTask {
        return self.codableTask(with: url, completionHandler: completionHandler)
    }
}

// MARK: - Alamofire response handlers

extension DataRequest {
    fileprivate func decodableResponseSerializer<T: Decodable>() -> DataResponseSerializer<T> {
        return DataResponseSerializer { _, response, data, error in
            guard error == nil else { return .failure(error!) }
            
            guard let data = data else {
                return .failure(AFError.responseSerializationFailed(reason: .inputDataNil))
            }
            
            return Result { try newJSONDecoder().decode(T.self, from: data) }
        }
    }
    
    @discardableResult
    fileprivate func responseDecodable<T: Decodable>(queue: DispatchQueue? = nil, completionHandler: @escaping (DataResponse<T>) -> Void) -> Self {
        return response(queue: queue, responseSerializer: decodableResponseSerializer(), completionHandler: completionHandler)
    }
    
    @discardableResult
    func responseSuggestedFriendModel(queue: DispatchQueue? = nil, completionHandler: @escaping (DataResponse<SuggestedFriendModel>) -> Void) -> Self {
        return responseDecodable(queue: queue, completionHandler: completionHandler)
    }
}

