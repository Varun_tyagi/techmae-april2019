//
//  SearchGroupList.swift
//  Techmea
//
//  Created by Dhaval Panchani on 18/09/18.
//  Copyright © 2018 VISHAL SETH. All rights reserved.
//

import UIKit

class SearchGroupList: NSObject {
    var about = ""
    var groupId = Int()
    var groupName = ""
    var imageUrl = ""
    var all_member = [[String:Any]]()
    var is_join = Bool()
    var noOfMember = ""
    var groupAdminID = Int()
}
