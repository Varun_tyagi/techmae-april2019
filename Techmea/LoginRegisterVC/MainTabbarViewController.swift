//
//  MainTabbarViewController.swift
//  Techmea
//
//  Created by VISHAL SETH on 8/4/18.
//  Copyright © 2018 VISHAL SETH. All rights reserved.
//

import UIKit

class MainTabbarViewController: UITabBarController {

    //MARK:- viewDidLoad
    override func viewDidLoad() {
        super.viewDidLoad()

        for tabBarItem in tabBar.items!
        {
            tabBarItem.title = ""
            tabBarItem.imageInsets = UIEdgeInsetsMake(6, 0, -6, 0)
            tabBarItem.image = tabBarItem.image?.withRenderingMode(UIImageRenderingMode.alwaysOriginal)
        }
    }
}
