
//
//  HomeViewController.swift
//  Techmea
//
//  Created by VISHAL SETH on 8/5/18.
//  Copyright © 2018 VISHAL SETH. All rights reserved.
//

import UIKit
import MobileCoreServices
import MBProgressHUD
import SDWebImage
import Alamofire
import AVKit
import AVFoundation
import Photos
import Toast_Swift
import SwiftyDrop
import ImageSlideshow
import SwiftLinkPreview
import JTMaterialSpinner
import SKPhotoBrowser
import ReadMoreTextView
import SafariServices
import FirebaseMessaging
extension AVPlayer {
    var isPlaying: Bool {
        return rate != 0 && error == nil
    }
}
let HomePAgePodtEditNotification = "homePostEditNotification"
let NotificationManageCellHeightImage = "manageImageHeightNotification"

class HomeViewController: UIViewController,UITableViewDelegate,UITableViewDataSource,UINavigationControllerDelegate,UIImagePickerControllerDelegate,UITextViewDelegate,UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout,SKPhotoBrowserDelegate,SFSafariViewControllerDelegate,QMChatServiceDelegate, QMChatConnectionDelegate, QMAuthServiceDelegate
{
    
    //MARK:- Home Outlets
    var data_arr = [[String:Any]]()
    var arrPostList = [PostListModel]()
    var collectionInitialized:Bool = false
    var post_select_index = Int()
    @IBOutlet weak var tblFeeds: UITableView!
    var total_no_of_page = Int()
    var SelectedImage:UIImage? = nil
    var selectedIndex:Int=0
    var post_type = "text"
    
    var refreshControl = UIRefreshControl()
    var is_picker_open = false
    var perpage_data_count = Int()
    var is_next_page_available = Bool()
    var current_page = 1

    var  singleTap:UITapGestureRecognizer!
    
    
    //Link Outlets
     var result = SwiftLinkPreview.Response()
     let placeholderImages = [ImageSource(image: UIImage(named: "Placeholder")!)]
    
     let slp = SwiftLinkPreview(cache: InMemoryCache())
    
    var finalMessage = String()

    //----OUTLETS OF FIRST CELL
    @IBOutlet weak var btnSend: UIButton!
    @IBOutlet weak var btnVideo: UIButton!
    @IBOutlet weak var btnCamera: UIButton!
    @IBOutlet weak var viewMain: UIView!
    @IBOutlet weak var txtMessage: UITextView!
    @IBOutlet weak var collectionPageHeight: NSLayoutConstraint!
    
    @IBOutlet weak var previewView: UIView!
    @IBOutlet weak var previewImageView: UIImageView!
    @IBOutlet weak var previewTitle_lbl: UILabel!
    @IBOutlet weak var previewDesc_lbl: UILabel!
    @IBOutlet weak var previewView_height: NSLayoutConstraint!
    @IBOutlet weak var centerActivityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var collectionPostFeed: UICollectionView!
    @IBOutlet weak var previewloader: JTMaterialSpinner!
    
    @IBOutlet weak var viewParent: UIView!
    @IBOutlet weak var btnPreviewClose: UIButton!
    
    @IBOutlet weak var innerPreviewView: UIView!
    
    var isFirstTime = true
    var linkPreviewVisible = false
    var postLink = String()
    
    var isDeteced = false
    var linksProperties: [NSTextCheckingResult]?
    
    @IBOutlet weak var lblNotificationCount: UILabel!
    
    var expandedCells = Set<Int>()
    
    //Heart
    private struct HeartAttributes {
        static let heartSize: CGFloat = 36
        static let burstDelay: TimeInterval = 0.1
    }
    private var observer: NSObjectProtocol?

    var burstTimer: Timer?

    func getDialogs() {
        
        ServicesManager.instance().chatService.allDialogs(withPageLimit: kDialogsPageLimit, extendedRequest: nil, iterationBlock: { (response: QBResponse?, dialogObjects: [QBChatDialog]?, dialogsUsersIDS: Set<NSNumber>?, stop: UnsafeMutablePointer<ObjCBool>) -> Void in
            
           
        }, completion: { (response: QBResponse?) -> Void in
            
            guard response != nil && response!.isSuccess else {
                return
            }
           let _ =   self.dialogs()
            ServicesManager.instance().lastActivityDate = NSDate()
        })
        //}
    }
    
    // MARK: - DataSource
    
    func dialogs() -> [QBChatDialog]? {
        
        
        let dialogsArray = ServicesManager.instance().chatService.dialogsMemoryStorage.dialogsSortByUpdatedAt(withAscending: false)
        
        var unreadCount=0
        for messageDialog in dialogsArray{
            if messageDialog.unreadMessagesCount>0{
                unreadCount = unreadCount + 1
            }
        }
        
        if let tabItems = tabBarController?.tabBar.items {
            // In this case we want to modify the badge number of the third tab:
            let tabItem = tabItems[3]
            if unreadCount > 0 {
                tabItem.badgeValue = "\(unreadCount)"
            }else{
                tabItem.badgeValue = nil
            }
        }
        
        return dialogsArray
    }
    
    //MARK:- viewDidLoads
    override func viewDidLoad()
    {
        super.viewDidLoad()


        self.tabBarController?.delegate=self
        ServicesManager.instance().chatService.addDelegate(self)
        
        ServicesManager.instance().authService.add(self)
        
        self.observer = NotificationCenter.default.addObserver(forName: NSNotification.Name.UIApplicationDidBecomeActive, object: nil, queue: OperationQueue.main) { (notification) -> Void in
            
            if !QBChat.instance.isConnected {
            }
        }
        
        NotificationCenter.default.addObserver(self, selector: #selector(DialogsViewController.didEnterBackgroundNotification), name: NSNotification.Name.UIApplicationDidEnterBackground, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.reportNotification(sender:)), name: NSNotification.Name.init(kReportNotification), object: nil)

       // if (QBChat.instance.isConnected) {
            self.getDialogs()
       // }

        // Static setup
        SKPhotoBrowserOptions.displayAction = true
        SKPhotoBrowserOptions.displayStatusbar = true
        SKPhotoBrowserOptions.displayCounterLabel = true
        SKPhotoBrowserOptions.displayBackAndForwardButton = true
        SKPhotoBrowserOptions.displayCloseButton = false
        SKPhotoBrowserOptions.displayAction = false
                
        self.collectionPostFeed.dataSource=self
        self.collectionPostFeed.delegate=self
        self.viewMain.layer.borderColor=UIColor.init(red: 249/255, green: 225/255, blue: 233/255, alpha: 1.0).cgColor
        self.viewMain.layer.borderWidth = 1.0
        self.viewMain.layer.cornerRadius = 10.0
        self.viewMain.layer.shadowColor = UIColor(red: 253.0 / 255.0, green: 233.0 / 255.0, blue: 240.0 / 255.0, alpha: 1.0).cgColor
        self.viewMain.layer.shadowOpacity = 1.0
        self.viewMain.layer.shadowRadius = 15.0
        self.viewMain.layer.shadowOffset = CGSize(width: 2.0, height: 2.0)
        
        
        self.innerPreviewView.layer.borderColor=UIColor.init(red: 249/255, green: 225/255, blue: 233/255, alpha: 1.0).cgColor
        self.innerPreviewView.layer.borderWidth = 1.0
        self.innerPreviewView.layer.cornerRadius = 10.0
//        self.viewMain.layer.shadowColor = UIColor(red: 253.0 / 255.0, green: 233.0 / 255.0, blue: 240.0 / 255.0, alpha: 1.0).cgColor
//        self.viewMain.layer.shadowOpacity = 1.0
//        self.viewMain.layer.shadowRadius = 15.0
//        self.viewMain.layer.shadowOffset = CGSize(width: 2.0, height: 2.0)
        
        self.setupPostData()
        
        if (UserDefaults.standard.object(forKey: Constants.KDeviceToken) as? String) != nil
        {
            
           if let fcm_token = UserDefaults.standard.object(forKey: Constants.KDeviceToken) as? String
           {
                self.call_set_device_token(token_str: fcm_token)
           }
        }
        
        //self.automaticallyAdjustsScrollViewInsets = false
        
        //self.tblFeeds.estimatedRowHeight = 1000
       // self.tblFeeds.rowHeight = UITableViewAutomaticDimension
        
        self.refreshControl.addTarget(self, action: #selector(HomeViewController.pullto_reloadData),for: UIControlEvents.valueChanged)
        self.tblFeeds.addSubview(refreshControl)
        
        //NotificationCenter.default.addObserver(self, selector: #selector(self.show_tab), name: NSNotification.Name(rawValue: "Hidetab"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.show_image(_:)), name: NSNotification.Name(rawValue: "Show_image"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.play_video(_:)), name: NSNotification.Name(rawValue: "Play_video"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.homePAgeEditPostNotification(_:)), name: NSNotification.Name(rawValue: HomePAgePodtEditNotification), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.manageCellImageHeiight(_:)), name: NSNotification.Name(rawValue: NotificationManageCellHeightImage), object: nil)

        loginQuickBlox()

    }
    @objc func homePAgeEditPostNotification(_ sender:Notification ){
        self.arrPostList.removeAll()
        self.tblFeeds.reloadData()
        current_page = 1
        getPost()
    }
    @objc func manageCellImageHeiight(_ sender:Notification ){
           //               NotificationCenter.default.post(name: NSNotification.Name.init(NotificationManageCellHeightImage), object: nil, userInfo: ["mainCellIndex":self.mainCellIndexPath,"postContent":self.imgArray])


        
        if let indexpath = sender.userInfo?["mainCellIndex"]  as? IndexPath{
            
            if  let content = sender.userInfo?["postContent"] as? [PostContent]{
        self.arrPostList[indexpath.row].postcontent = content
                self.tblFeeds.reloadData()
        }
       }
    }
    //MARK:- Lgin QuickBlox
    func loginQuickBlox()
    {
        if let data = UserDefaults.standard.object(forKey: "quickBloxData") as? Data
        {
        let quickBloxData: NSDictionary? = NSKeyedUnarchiver.unarchiveObject(with: data) as? NSDictionary

        var userid = String()
        var pass = String()

        if let userid_str = quickBloxData!["login"] as? String
        {
             userid = userid_str
        }
            
        if let pass_str = quickBloxData!["password"] as? String
        {
            pass = pass_str
        }

        //ServicesManager.instance().currentUser.id = UInt(userid)
        let user = QBUUser()
        user.password = pass
        user.login = userid
        
       self.loadUserFromQuickblox(user)
            
    }
}
    
    //MARK:- Quick Blox
    func loadUserFromQuickblox(_ user: QBUUser) {
      
        self.logInChatWithUser(user)
    }
    
    func logInChatWithUser(_ user: QBUUser) {
        
        // Logging to Quickblox REST API and chat.
        ServicesManager.instance().logIn(with: user, completion:{
            [unowned self] (success:Bool,  errorMessage: String?) -> Void in
            
            if (success) {

                print("Success Login into QuickBlox")
                //self.view.makeToast("Success Login into QuickBlox")
                
            } else {
                
                print("\(errorMessage!)")
                //self.view.makeToast("\(errorMessage!)")
            }
            
        })
    }
    
    //MARK:- Call Device Token API
    func call_set_device_token(token_str:String)
    {
        if(CommonFunction.isInternetAvailable())
        {
            if let auth_token = UserDefaults.standard.value(forKey: "Access_Token") as? String
            {
                let param = ["device_id":"\(token_str)",
                    "device_type":"ios","user_id": "\(UserDefaults.standard.value(forKey: Constants.USERID) as! Int)" ]
                Alamofire.request(Constants.BASEURL + MethodName.registerToken + "?access-token=\(auth_token)&user_id=\(UserDefaults.standard.value(forKey: Constants.USERID) as! Int)" , method: .post, parameters: param, encoding: JSONEncoding.default, headers: ["Content-Type":"application/json"]).responseJSON { response in
                    
                    if let result_dict = response.result.value as? [String:Any]
                    {
                        if let code = result_dict["code"] as? Int
                        {
                            if(code==200)
                            {
                                print("Device Token Registered!!")
                            }
                            else
                            {
                                if let error_data = result_dict["errorData"] as? NSArray
                                {
                                    if let error_msg = error_data[0] as? String
                                    {
                                        if error_msg == "Your account is not eanbled. Please make sure your account is active."
                                        {
                                            
                                            
                                            
                                        }
                                        else
                                        {
                                            self.view.makeToast("Something Went Wrong!!")
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        else
        {
            //self.displayAlert(msg: "Please Check Your Internet Connection!!", title_str: Constants.APP_NAME)
        }
    }
    
    //MARK:- Show Images, Videos
    @objc func show_image(_ notification: NSNotification) {
        print(notification.userInfo ?? "")
        print(notification.object ?? "")
        let browser = SKPhotoBrowser(photos: notification.object as! [SKPhotoProtocol])
        if let startIndexDict = notification.userInfo as? [String:Int], let startIndex = startIndexDict[SKBrowserStartIndex] {
            browser.initializePageIndex(startIndex)
        }
        present(browser, animated: true, completion: {})
    }
    
    
    @objc func play_video(_ notification: NSNotification) {
        print(notification.userInfo ?? "")
        if let dict = notification.userInfo as NSDictionary? {
            if let video_str = dict["video_url"] as? String
            {
                let player = AVPlayer(url: URL(string: video_str)!)
                let vc = AVPlayerViewController()
                vc.player = player
                
                present(vc, animated: true) {
                    vc.player?.play()
                }
            }
        }
    }

    @objc func show_tab()
    {
         DispatchQueue.main.asyncAfter(deadline: .now() + 0.3) {
            self.tabBarController?.tabBar.isHidden = false
        }
    }
    
    //MARK:- viewWillAppear
    override func viewWillAppear(_ animated: Bool)
    {
        getUnreadNotificationCount()
        self.navigationController?.isNavigationBarHidden = true
        self.tabBarController?.tabBar.isHidden = false
        self.tabBarController?.tabBar.frame.size.height = 49

        //DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
            self.tblFeeds.reloadData()
        //}
        if(is_picker_open==false)
        {
            self.getPost()
        }
    }
    
    //MARK:- Pull To Reload
    @objc func pullto_reloadData()
    {
        current_page = 1
        self.getPost()
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func btnNotificationCountTapped(_ sender: UIButton) {
        
        let objNotificationViewController = Constants.mainStoryboard.instantiateViewController(withIdentifier: "NotificationViewController") as! NotificationViewController
        self.navigationController?.pushViewController(objNotificationViewController, animated: true)
    }
    
    //MARK:- Unread Notification Count Api
    func getUnreadNotificationCount()
    {
        if(CommonFunction.isInternetAvailable())
        {
            MBProgressHUD.showAdded(to: self.view, animated: true)
            
            let headerString = ["Content-Type" : "application/json"]
            //let paramString = ["user_id":"\(UserDefaults.standard.value(forKey: Constants.USERID) as! Int)"]
            if let current_user = UserDefaults.standard.value(forKey: Constants.USERID) as? Int
            {
                Alamofire.request(Constants.BASEURL + MethodName.unreadNotificationCount + "?access-token=\(UserDefaults.standard.value(forKey: "Access_Token") as! String)&receiver_id=\(current_user)", method: .get, parameters: nil , encoding: JSONEncoding.default, headers:headerString).responseJSON
                    { response in
                        MBProgressHUD.hide(for: self.view, animated: true)
                        if let result_dict = response.result.value as? [String:Any]
                        {
                            if let code = result_dict["code"] as? Int
                            {
                                if(code==200)
                                {
                                    if let data_dict = result_dict["data"] as? [String:Any]
                                    {
                                        if let noti_count = data_dict["totalCount"] as? Int
                                        {
                                            //self.lblNotificationCount.text = "\(noti_count)"
                                            
                                            
                                            if noti_count <= 0
                                            {
                                                self.lblNotificationCount.isHidden = true
                                            }
                                            else if noti_count > 0 && noti_count < 99
                                            {
                                                self.lblNotificationCount.isHidden = false
                                                self.lblNotificationCount.text = "\(noti_count)"
                                            }
                                            else
                                            {
                                                self.lblNotificationCount.isHidden = false
                                                self.lblNotificationCount.text = "99+"
                                            }
                                        }
                                        else
                                        {
                                            self.lblNotificationCount.isHidden = true
                                        }
                                    }
                                    else
                                    {
                                        self.lblNotificationCount.isHidden = true
                                    }
                                }
                                else
                                {
                                    self.lblNotificationCount.isHidden = true
                                    
                                    if let error_arr = result_dict["errorData"] as? [[String:Any]]
                                    {
                                        if let error_msg = error_arr[0]["message"] as? String
                                        {
                                            self.view.makeToast(error_msg)
                                        }
                                    }
                                }
                            }
                        }
                }
            }
        }
        else
        {
            self.view.makeToast("Please Check Your Internet Connection!!")
        }
    }
    
    
    
    //MARK: Get Post
    func getPost()
    {
        if(CommonFunction.isInternetAvailable())
        {
            let parameterString = ["access-token" : "\(UserDefaults.standard.value(forKey: "Access_Token") as! String)","page" : current_page] as [String : Any]
            MBProgressHUD.showAdded(to: self.view, animated: true)
        
            Alamofire.request(Constants.BASEURL+MethodName.postList, method: .get, parameters: parameterString, encoding: URLEncoding.default, headers: auth_header).responseJSON { response in
            
            MBProgressHUD.hide(for: self.view, animated: true)
            if let records_onepage = response.response?.allHeaderFields["X-Pagination-Per-Page"] as? String {
                self.perpage_data_count = Int(records_onepage)!
            }
            switch response.result {
            case .success:
                
                if let result_dict = response.result.value as? [String:Any]
                {
                    if(self.current_page==1)
                    {
                        self.arrPostList.removeAll()
                    }
                    
                    let code:Int = result_dict["code"]as! Int
                    if (code == 200)
                    {
                       if let arrData = result_dict["data"] as? NSArray
                       {
                            print(arrData)
                            if(arrData.count<self.perpage_data_count)
                            {
                                self.is_next_page_available = false
                            }
                            else
                            {
                                self.is_next_page_available = true
                            }
                            for dict in arrData
                            {
                                let tempDict = dict as! NSDictionary
                                self.arrPostList.append(PostListModel.init(dict: tempDict))
                            }
                            self.tblFeeds.reloadData()
                            self.refreshControl.endRefreshing()
                        }
                    }
                    else if (code==401)
                    {
                        if let error_data = result_dict["errorData"] as? [[String:Any]]
                        {
                            if let error_msg = error_data[0]["message"] as? String
                            {
                                let view = CongratulationView.instantiateFromNib()
                                view.lblTitle.text = error_msg
                                let modal = PathDynamicModal()
                                modal.showMagnitude = 200.0
                                modal.closeMagnitude = 130.0
                                modal.closeByTapBackground = false
                                modal.closeBySwipeBackground = false
                                view.OkButtonHandler = {[weak modal] in
                                    
                                    let storyBoard = UIStoryboard(name: "Main", bundle: nil)
                                    let objHomeNav = storyBoard.instantiateViewController(withIdentifier: "HomeNav") as! UINavigationController
                                    UIApplication.shared.keyWindow?.rootViewController = objHomeNav
                                    
                                    modal?.closeWithLeansRandom()
                                    return
                                }
                                modal.show(modalView: view, inView: (UIApplication.shared.delegate?.window!)!)
                            }
                        }
                        
                        
                    }
                    else
                    {
                        self.view.makeToast("Something Went Wrong!!")
                    }
                }

            case .failure(let error):
                print(error)
                self.refreshControl.endRefreshing()
            }
        }
        }
        else
        {
            self.view.makeToast("Please Check Your Internet Connection!!")
        }
        
    }
    
    //MARK: Button Actions
    @objc func saveTapped() {
        // ...
    }
    
    @objc func deleteTapped() {
        
    }
    
    @objc func showToolTipMenu(_ sender: UIButton)
    {
        guard (sender.superview?.superview?.superview as? FeedCell) != nil else
        {
            return // or fatalError() or whatever
        }
       // let selected_indexPath =  //tblFeeds.indexPath(for: cell)
        post_select_index =  sender.tag //(selected_indexPath?.row)!
        self.showOptionAlertUserType(btn: sender)
        
    }
    
    func initialSetup(collectionSlider:UICollectionView)
    {
        if self.collectionInitialized==false
        {
            self.collectionInitialized=true
            let cellWidth : CGFloat = 80
            let cellheight : CGFloat = 80
            let cellSize = CGSize(width: cellWidth , height:cellheight)
            
            let layout = UICollectionViewFlowLayout()
            layout.scrollDirection = .horizontal
            layout.itemSize = cellSize
            layout.sectionInset = UIEdgeInsets(top: 1, left: 1, bottom: 1, right: 2)
            layout.minimumLineSpacing = 1.0
            layout.minimumInteritemSpacing = 1.0
            collectionSlider.setCollectionViewLayout(layout, animated: true)
            collectionSlider.reloadData()
            collectionSlider.isPagingEnabled=true
        }
        else
        {
            collectionSlider.reloadData()
        }
        
    }
    
    func showOptionAlertUserType(btn:UIButton)
    {
        guard (btn.superview?.superview?.superview as? FeedCell) != nil else
        {
            return // or fatalError() or whatever
        }
        let selected_indexPath = IndexPath.init(row: btn.tag, section: 0)//tblFeeds.indexPath(for: cell)
        let alert = UIAlertController(title: Constants.APP_NAME, message: "Perform action", preferredStyle: UIAlertControllerStyle.actionSheet)
    
        
        let deletebtn = UIAlertAction(title: "Delete", style: UIAlertActionStyle.default) { (alert) -> Void in
            
            let view = ModalView.instantiateFromNib()
            view.updateModal("Delete Post", descMessage: "Are you sure you want to delete this post?", firstTitle: "YES", secondTitle: "NO", type: .modalAlert)
            let modal = PathDynamicModal()
            modal.showMagnitude = 200.0
            modal.closeMagnitude = 130.0
            modal.closeByTapBackground = false
            modal.closeBySwipeBackground = false
            view.Cancel2ButtonHandler = {[weak modal] in
                modal?.closeWithLeansRandom()
                return
            }
            view.OkButtonHandler = {[weak modal] in
                
                let list_model = self.arrPostList[self.post_select_index]
                if self.arrPostList[btn.tag].groupData != nil
                {
                    
                    self.callDeleteGroupPostAPI(delete_index: Int(list_model.post_id)!)
                }
                else
                {
                    self.callDeletePostAPI(delete_index: Int(list_model.post_id)!)
                }
                modal?.closeWithLeansRandom()
                return
            }
            modal.show(modalView: view, inView: (UIApplication.shared.delegate?.window!)!)
            
            
        }
        
        let cancelButton = UIAlertAction(title: "Cancel", style: UIAlertActionStyle.destructive) { (alert) -> Void in
            //self.txtUserType.text="Trader"
        }
        
        
        let editBtn = UIAlertAction.init(title: "Edit", style: UIAlertActionStyle.default) { (alert) in
            let reportVC = Constants.mainStoryboard.instantiateViewController(withIdentifier:  "EditPostViewController") as! EditPostViewController
                   
            let post = self.arrPostList[btn.tag]
               
                       reportVC.post = post
                       reportVC.indexPath = selected_indexPath
                       self.add(child: reportVC)
        }
        // now hide delete btn if not my post
        if btn.accessibilityElements != nil  {
            //  do not show  report btn as it is my post
            alert.addAction(deletebtn)
            alert.addAction(editBtn)

        }else{
            // show report btn and hide delete btn
            // show report btn
            let reportBtn = UIAlertAction.init(title: "Report", style: UIAlertActionStyle.default) { (alert) in
                self.showReportPostOptionAlert(btn)
            }
            alert.addAction(reportBtn)
        }
       
       // alert.addAction(deletebtn)
        alert.addAction(cancelButton)
        self.present(alert, animated: true, completion: nil)
    }
    
    func showReportPostOptionAlert(_ sender:UIButton){
        let reportOptionAlert = UIAlertController.init(title: Constants.APP_NAME, message: "Report Post", preferredStyle: UIAlertControllerStyle.actionSheet)
        
        
        guard let cell = sender.superview?.superview?.superview as? FeedCell else
              {
                  return // or fatalError() or whatever
              }
              let selected_indexPath = tblFeeds.indexPath(for: cell)
        
        let post = self.arrPostList[selected_indexPath?.row ?? 0 ]
    
        
        
        reportOptionAlert.addAction(UIAlertAction.init(title: "Pretending To Be Someone", style: UIAlertActionStyle.destructive, handler: { (alert) in
            // hit service for report
            ReportNetworkManager.ReportPost(message:"Pretending To Be Someone",postType: post.groupData == nil ? "POST" : "GROUP" , postId: post.post_id, reportType: "1", reportImage: nil, controller: self ) { (flag) in
                // hide port if true
                if flag {
                    DispatchQueue.main.async {
                        if let indexPath = selected_indexPath {
            self.arrPostList.remove(at: indexPath.row  )
                        self.tblFeeds.deleteRows(at: [indexPath], with: UITableViewRowAnimation.automatic)
                    }
                    }
                }
            }
            
        }))
        reportOptionAlert.addAction(UIAlertAction.init(title: "Posted Inappropriate  Material", style: UIAlertActionStyle.destructive, handler: { (alert) in
                   // hit service for report
            ReportNetworkManager.ReportPost(message: "Posted Inappropriate  Material", postType: post.groupData == nil ? "POST" : "GROUP" , postId: post.post_id, reportType: "2", reportImage: nil, controller: self ) { (flag) in
                           // hide port if true
                           if flag {
                               DispatchQueue.main.async {
                                   if let indexPath = selected_indexPath {
                       self.arrPostList.remove(at: indexPath.row  )
                                   self.tblFeeds.deleteRows(at: [indexPath], with: UITableViewRowAnimation.automatic)
                               }
                               }
                           }
                       }
               }))
        reportOptionAlert.addAction(UIAlertAction.init(title: "Inappropriate Profile Picture", style: UIAlertActionStyle.destructive, handler: { (alert) in
                   // hit service for report
            ReportNetworkManager.ReportPost(message: "Inappropriate Profile Picture", postType: post.groupData == nil ? "POST" : "GROUP" , postId: post.post_id, reportType: "3", reportImage: nil, controller: self ) { (flag) in
                           // hide port if true
                           if flag {
                               DispatchQueue.main.async {
                                   if let indexPath = selected_indexPath {
                       self.arrPostList.remove(at: indexPath.row  )
                                   self.tblFeeds.deleteRows(at: [indexPath], with: UITableViewRowAnimation.automatic)
                               }
                               }
                           }
                       }
               }))
        reportOptionAlert.addAction(UIAlertAction.init(title: "Other", style: UIAlertActionStyle.destructive, handler: { (alert) in
                   // show popup to add comment and screenshot and hit service for report
            
            let reportVC = Constants.mainStoryboard.instantiateViewController(withIdentifier:  "ReportViewController") as! ReportViewController
            reportVC.post = post
            reportVC.indexPath = selected_indexPath
            self.add(child: reportVC)

               }))
        reportOptionAlert.addAction(UIAlertAction.init(title: "Cancel", style: UIAlertActionStyle.cancel, handler: { (alert) in
               // do nothing
        
           }))
        self.present(reportOptionAlert, animated: true, completion: nil)
    }
    @objc func reportNotification(sender:Notification){
        if let indexPath = sender.object as? IndexPath{
            self.arrPostList.remove(at: indexPath.row )
    self.tblFeeds.deleteRows(at: [indexPath], with: UITableViewRowAnimation.automatic)
        }
    }
    @objc func btnDeleteAction(btn:UIButton)
    {
        
        guard let cell = btn.superview?.superview as? PostFeedImageCell else
        {
            return // or fatalError() or whatever
        }
        let selected_indexPath = collectionPostFeed.indexPath(for: cell)
        self.data_arr.remove(at: (selected_indexPath?.item)!)
        self.collectionPostFeed.reloadData()
        setupPostData()
    }
    
    
    //MARK:- LikeGroupPost Api
    func callLikeGroupPostAPI(post_id:String , select_index:Int, like_heart_view : Floater)
    {
       
        
        
        let headerString = [
            "Content-Type": "application/json"
        ]
        
        let paramString = ["post_id" : post_id]
        
        Alamofire.request(Constants.BASEURL+MethodName.groupPostLike + "?access-token=\(UserDefaults.standard.value(forKey: "Access_Token") as! String)" , method: .post, parameters: paramString, encoding: JSONEncoding.default, headers: headerString).responseJSON { response in
            
            if let responseDict = response.result.value as? [String:Any]
            {
                if let code:Int = responseDict["code"] as? Int
                {
                    if code == 200
                    {
                        if let data = responseDict["data"] as? [String : Any]
                        {
                            if let msg = data["message"] as? String
                            {
                                //let like_counter = Int(self.arrPostList[select_index].likes) as! Int
                                if (msg=="Liked Successfully")
                                {
                                    let tempDict:PostListModel = self.arrPostList[select_index]
                                    tempDict.likes = "\(Int(self.arrPostList[select_index].likes)!+1)"
                                    tempDict.isLike="1"
                                    self.arrPostList[select_index] = tempDict
                                    
    //                                let heart = HeartView(frame: CGRect(x: 0, y: 0, width: HeartAttributes.heartSize, height: HeartAttributes.heartSize))
    //                                self.view.addSubview(heart)
    //                                let fountainX = HeartAttributes.heartSize / 2.0 + 20
    //                                let fountainY = self.view.bounds.height - HeartAttributes.heartSize / 2.0 - 10
    //                                heart.center = CGPoint(x: fountainX, y: fountainY)
    //                                heart.animateInView(view: self.view)
                                    
                                       // like_heart_view.startAnimation()
                                }
                                else if (msg=="Disliked Successfully")
                                {
                                    let tempDict:PostListModel = self.arrPostList[select_index]
                                    tempDict.likes = "\(Int(self.arrPostList[select_index].likes)!-1)"
                                    tempDict.isLike="0"
                                    self.arrPostList[select_index] = tempDict
                                    
                                }
                                self.tblFeeds.reloadData()
                            }
                        }
                    }
                    else
                    {
                        if let result_error = response.result.value as? [String:Any]
                        {
                            if let error_array = result_error["errorData"] as? NSArray
                            {
                                if let error_dict = error_array[0] as? [String : Any]
                                {
                                    if let error_msg = error_dict["message"] as? String
                                    {
                                        self.view.makeToast(error_msg)
                                    }
                                }
                            }
                        }
                    }
                }
                
            }
        }
        
    }
    
    @objc func likeListAction(_ sender:UIButton){
        
        guard let cell = sender.superview?.superview?.superview as? FeedCell else
               {
                   return // or fatalError() or whatever
               }
               let selected_indexPath = tblFeeds.indexPath(for: cell)
               
        let feed = self.arrPostList[selected_indexPath!.row]
        
            print(feed.likes)
        let friendvc = Constants.mainStoryboard.instantiateViewController(withIdentifier: "LikeListViewController") as! LikeListViewController
               //friendvc.user_id_str = user_id_str
        friendvc.isGroupPost = (feed.groupData != nil)
        friendvc.feed = feed
               friendvc.isTab = false
               self.navigationController?.pushViewController(friendvc, animated: true)
        
        
       }
    @objc func press_like_btn(btn:UIButton)
    {
        guard let cell = btn.superview?.superview?.superview as? FeedCell else
        {
            return // or fatalError() or whatever
        }
        let selected_indexPath = tblFeeds.indexPath(for: cell)
        
        
        if self.arrPostList[(selected_indexPath?.row)!].groupData != nil
        {
            callLikeGroupPostAPI(post_id: self.arrPostList[(selected_indexPath?.row)!].post_id, select_index: (selected_indexPath?.row)!, like_heart_view: cell.like_heart)
            
            
        }
        else
        {
            callLikePostAPI(post_id: self.arrPostList[(selected_indexPath?.row)!].post_id, select_index: (selected_indexPath?.row)!, like_heart_view: cell.like_heart)
            
        }
    }
    
    
    //MARK:- LikeSimple Post Api
    func callLikePostAPI(post_id:String , select_index:Int, like_heart_view : Floater)
    {
        if(CommonFunction.isInternetAvailable())
        {
        let headerString = [
            "Content-Type": "application/json"
        ]
        
         Alamofire.request(Constants.BASEURL+MethodName.likePost + "?access-token=\(UserDefaults.standard.value(forKey: "Access_Token") as! String)&id=\(post_id)" , method: .post, parameters: nil, encoding: JSONEncoding.default, headers: headerString).responseJSON { response in
            
            if let responseDict = response.result.value as? [String:Any]
            {
                if let code:Int = responseDict["code"] as? Int
                {
                    
                    if code == 200
                    {
                        if let data = responseDict["data"] as? [String : Any]
                        {
                            if let msg = data["message"] as? String
                            {
                                if (msg=="Liked Successfully")
                                {
                                    let tempDict:PostListModel = self.arrPostList[select_index]
                                    tempDict.likes = "\(Int(self.arrPostList[select_index].likes)!+1)"
                                    tempDict.isLike="1"
                                    self.arrPostList[select_index] = tempDict
                                    
                                    //like_heart_view.startAnimation()

    //                                let heart = HeartView(frame: CGRect(x: 0, y: 0, width: HeartAttributes.heartSize, height: HeartAttributes.heartSize))
    //                                self.view.addSubview(heart)
    //                                let fountainX = HeartAttributes.heartSize / 2.0 + 20
    //                                let fountainY = btn.frame.origin.y //cell.bounds.height - HeartAttributes.heartSize / 2.0 - 10
    //                                heart.center = CGPoint(x: fountainX, y: fountainY)
    //                                heart.animateInView(view: cell)
                                    
    //                                let img1 = UIImageView.init(frame: CGRect.init(x: btn.frame.origin.x, y: btn.frame.origin.y, width: 30, height: 30))
    //                                img1.image = UIImage.init(named: "heart")
    //                                self.view.addSubview(img1)
    //
    //                                img1.isHidden = true
    //
    //                                UIView.animate(withDuration: 1, animations: {
    //                                    img1.isHidden = false
    //                                    img1.frame = CGRect.init(x: self.view.frame.origin.x, y: self.view.frame.origin.y, width: 30, height: 30)
    //                                    img1.alpha = 0
    //                                }, completion: nil)
                                    
                                    
                                    
                                    
                                }
                                else if (msg=="Disliked Successfully")
                                {
                                    let tempDict:PostListModel = self.arrPostList[select_index]
                                    tempDict.likes = "\(Int(self.arrPostList[select_index].likes)!-1)"
                                    tempDict.isLike="0"
                                    self.arrPostList[select_index] = tempDict
                                    
                                }
                                self.tblFeeds.reloadData()
                            }
                        }
                    }
                    else
                    {
                        if let result_error = response.result.value as? [String:Any]
                        {
                            if let error_array = result_error["errorData"] as? NSArray
                            {
                                if let error_dict = error_array[0] as? [String : Any]
                                {
                                    if let error_msg = error_dict["message"] as? String
                                    {
                                        self.view.makeToast(error_msg)
                                    }
                                }
                            }
                        }
                    }
                    
                }
                
            }
        }
        }
        else
        {
            self.view.makeToast("Please Check Your Internet Connection!!")
        }
         
    }
    
    //MARK:- Load More Comments
    @objc func loadMoreComments(btn:UIButton)
    {
        guard let cell = btn.superview?.superview?.superview as? FeedCell else
        {
            return // or fatalError() or whatever
        }
        let selected_indexPath = tblFeeds.indexPath(for: cell)
        
    
        let vc = Constants.mainStoryboard2.instantiateViewController(withIdentifier: "CommentsViewController")as! CommentsViewController
        let pid:Int=Int(self.arrPostList[(selected_indexPath?.row)!].post_id)!
        vc.postId=String(pid)
    
        let comment_count = self.arrPostList[(selected_indexPath?.row)!].postcomments.count
        if(comment_count>0)
        {
            vc.last_comment_id = self.arrPostList[(selected_indexPath?.row)!].postcomments[comment_count-1].commentId
        }
        if self.arrPostList[(selected_indexPath?.row)!].groupData != nil
        {
            vc.comment_type = "Group"
            if let dictGroupDetails = self.arrPostList[(selected_indexPath?.row)!].groupData
            {
                vc.groupId = Int(dictGroupDetails["groupId"] as! String)!
            }
            
        }
        vc.arrComments=self.arrPostList[(selected_indexPath?.row)!].postcomments
        self.navigationController?.pushViewController(vc, animated: true)
        
    }

    
    func showAlertUserType(media:String)
    {
        
        let alert = UIAlertController(title: Constants.APP_NAME, message: "Browse \(media) using", preferredStyle: UIAlertControllerStyle.actionSheet)
        
        let galleryBtn = UIAlertAction(title: "Gallery", style: UIAlertActionStyle.default) { (alert) -> Void in
            self.openGallery(type: media)
            self.btnCamera.setImage(UIImage(named: "camera"), for: .normal)
            self.btnVideo.setImage(UIImage(named: "video_camera"), for: .normal)
        }
        
        let cameraBtn = UIAlertAction(title: "Camera", style: UIAlertActionStyle.default) { (alert) -> Void in
            self.openCamera(type: media)
            self.btnCamera.setImage(UIImage(named: "camera"), for: .normal)
            self.btnVideo.setImage(UIImage(named: "video_camera"), for: .normal)
        }
        
        let cancelButton = UIAlertAction(title: "Cancel", style: UIAlertActionStyle.destructive) { (alert) -> Void in
            //self.txtUserType.text="Trader"
            self.btnCamera.setImage(UIImage(named: "camera"), for: .normal)
            self.btnVideo.setImage(UIImage(named: "video_camera"), for: .normal)
        }
        
        alert.addAction(galleryBtn)
        alert.addAction(cameraBtn)
        alert.addAction(cancelButton)
        DispatchQueue.main.async {
        self.present(alert, animated: true, completion: nil)
        }
    }
    
    
    //MARK:- Camera Video Action
    @IBAction func btnCameraAction(_ sender: Any)
    {
        self.view.endEditing(true)
        self.btnCamera.setImage(UIImage(named: "ic_camera"), for: .normal)
        post_type = "image"        
        self.showAlertUserType(media: "Photo")
    }
    @IBAction func btnVideoAction(_ sender: Any)
    {
        self.view.endEditing(true)
        self.btnVideo.setImage(UIImage(named: "ic_videocam"), for: .normal)
        post_type = "video"
        showAlertUserType(media: "Video")
    }
    
    //MARK:- Open Camera
    func openCamera(type:String)
    {
        if(Platform.isSimulator)
        {
            //self.displayAlert(msg: "No camera available", title_str: Constants.APP_NAME)
            self.view.makeToast("No camera available")
        }
        else
        {
            //let authorizationStatus = AVCaptureDevice.authorizationStatus(for: AVMediaType.video)
            AVCaptureDevice.requestAccess(for: AVMediaType.video, completionHandler: { (granted :Bool) -> Void in
                if granted == true
                {
                    self.is_picker_open = true
                    let cameraController = UIImagePickerController()
                    cameraController.sourceType = UIImagePickerControllerSourceType.camera
                    cameraController.delegate = self
                    if type == "Photo"
                    {
                        cameraController.mediaTypes = ["public.image"]
                    }
                    else
                    {
                        cameraController.mediaTypes = ["public.movie"]
                    }
                    cameraController.isEditing = true
                    cameraController.videoMaximumDuration = 60
                    DispatchQueue.main.async{
                    self.present(cameraController, animated: true, completion: nil)
                    }
                }
                else
                {
                    self.show_deny_alert(msg: "You deny to use camera.To allow camera you should allow camera permission from setting.")
                    
                }
            });
        }
    }
    func show_deny_alert(msg:String)
    {
        let alert = UIAlertController(title: Constants.APP_NAME, message: msg, preferredStyle: UIAlertControllerStyle.alert)
        
        let okBtn = UIAlertAction(title: "OK", style: UIAlertActionStyle.default) { (alert) -> Void in
            guard let settingsUrl = URL(string: UIApplicationOpenSettingsURLString) else {
                return
            }
            
            if UIApplication.shared.canOpenURL(settingsUrl) {
                UIApplication.shared.open(settingsUrl, completionHandler: { (success) in
                    print("Settings opened: \(success)") // Prints true
                })
            }
        }
        alert.addAction(okBtn)
        self.present(alert, animated: true, completion: nil)
    }
    
    //MARK:- Open Gallery
    func openGallery(type:String)
    {
        PHPhotoLibrary.requestAuthorization { (status) in
            let status = PHPhotoLibrary.authorizationStatus()
            print(status)
            if (status == PHAuthorizationStatus.authorized)
            {
                self.is_picker_open = true
                var config = TatsiConfig.default
                config.showCameraOption = false
                config.supportedMediaTypes = [.video, .image]
                config.firstView = .userLibrary
                config.maxNumberOfSelections = 5

                
                let pickerViewController = TatsiPickerViewController(config: config)
                pickerViewController.pickerDelegate = self
                DispatchQueue.main.async{
                self.present(pickerViewController, animated: true, completion: nil)
                }
            }
            else
            {
                self.show_deny_alert(msg: "You deny to use photo library .To allow photo library you should allow photos permission from setting.")
            }
        }
        
        
    }
    
    
    //MARK:- didFinishPickingMediaWithInfo
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any])
    {
        if let chosenImage = info[UIImagePickerControllerOriginalImage] as? UIImage
        {
            var dataDict = [String:Any]()
            dataDict["media_type"]  = "image"
            dataDict["data_str"]  = UIImageJPEGRepresentation(chosenImage, 0.5)
            dataDict["Preview"] = UIImage(data: UIImageJPEGRepresentation(chosenImage, 0.3)!)
            dataDict["URLPath"] = ""
            if self.data_arr.count < 5
            {
                self.data_arr.append(dataDict)
            }
            else
            {
                self.view.makeToast("Maximum five images/videos you can upload at a time")
            }
            
        }
        else
        {
            if let videofile = info[UIImagePickerControllerMediaURL] as? URL
            {
                var dataDict = [String:Any]()
                dataDict["media_type"]  = "video"
                dataDict["Preview"] = getThumbnailImage(forUrl: videofile)
                if let videoData = try? Data(contentsOf: videofile)
                {
                    dataDict["data_str"] =  videoData
                }
                dataDict["URLPath"] = videofile
                if self.data_arr.count < 5
                {
                    self.data_arr.append(dataDict)
                }
                else
                {
                   self.view.makeToast("Maximum five images/videos you can upload at a time")
                }
            }
        }
        picker.dismiss(animated: true, completion: {
            //self.setupPostData()
            
            self.collectionPostFeed.reloadData()
            
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.1, execute: {
                self.setupPostData()
            })
        })
        
       
    }
    
    //MARK:- encodeVideo
    func encodeVideo(at videoURL: URL, completionHandler: ((URL?, Error?) -> Void)?)  {
        let avAsset = AVURLAsset(url: videoURL, options: nil)
        
        let startDate = Date()
        
        //Create Export session
        guard let exportSession = AVAssetExportSession(asset: avAsset, presetName: AVAssetExportPresetPassthrough) else {
            completionHandler?(nil, nil)
            return
        }
        
        //Creating temp path to save the converted video
        let documentsDirectory = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)[0] as URL
        let filePath = documentsDirectory.appendingPathComponent("rendered-Video.mp4")
        
        //Check if the file already exists then remove the previous file
        if FileManager.default.fileExists(atPath: filePath.path) {
            do {
                try FileManager.default.removeItem(at: filePath)
            } catch {
                completionHandler?(nil, error)
            }
        }
        
        exportSession.outputURL = filePath
        exportSession.outputFileType = AVFileType.mp4
        exportSession.shouldOptimizeForNetworkUse = true
        let start = CMTimeMakeWithSeconds(0.0, 0)
        let range = CMTimeRangeMake(start, avAsset.duration)
        exportSession.timeRange = range
        
        exportSession.exportAsynchronously(completionHandler: {() -> Void in
            switch exportSession.status {
            case .failed:
                print(exportSession.error ?? "NO ERROR")
                completionHandler?(nil, exportSession.error)
            case .cancelled:
                print("Export canceled")
                completionHandler?(nil, nil)
            case .completed:
                //Video conversion finished
                let endDate = Date()
                
                let time = endDate.timeIntervalSince(startDate)
                print(time)
                print("Successful!")
                print(exportSession.outputURL ?? "NO OUTPUT URL")
                completionHandler?(exportSession.outputURL, nil)
                if let imgurl = exportSession.outputURL as? URL
                {
                    self.getThumbnailImage(forUrl: exportSession.outputURL!)
                }
                
            default: break
            }
            
        })
    }
    func getThumbnailImage(forUrl url: URL) -> UIImage
    {
        let img = UIImage()
        
        let asset: AVAsset = AVAsset(url: url)
        let imageGenerator = AVAssetImageGenerator(asset: asset)
        imageGenerator.appliesPreferredTrackTransform = true
        do {
            
            let thumbnailImage = try imageGenerator.copyCGImage(at: CMTimeMake(1, 2) , actualTime: nil)
            DispatchQueue.main.async
            {
                self.collectionPostFeed.reloadData()
            }
            return UIImage.init(cgImage: thumbnailImage)
            
        }
        catch let error
        {
            print(error)
            return img
        }
        return img
       
    }
    
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        picker.dismiss(animated: true, completion: nil)
    }
    
    //MARK: TextView delegates
    func textViewDidBeginEditing(_ textView: UITextView) {
        if textView.text == "Write Something...."
        {
            textView.text=""
        }
    }
    func textViewDidEndEditing(_ textView: UITextView) {
        if textView.text.count==0
        {
            textView.text="Write Something...."
        }
        
        
        
//        if  !linkPreviewVisible
//        {
//            if textView.text.contains(" ")
//            {
//                self.finalMessage = textView.text
//
//                let textInputContent = self.finalMessage
//
//                let result = textInputContent.getAllClickableLinks()
//                if let links = result {
//                    linksProperties = links
//                    for link in links {
//                        let content = textInputContent as NSString
//                        let value = content.substring(with: link.range)
//
//                        if isFirstTime == true
//                        {
//                            if link == links.first
//                            {
//                                self.finalMessage = completeURL(url : "\(value)")
//                                self.postLink = completeURL(url : "\(value)")
//                                self.setupPostData()
//                                break
//                            }
//                        }
//                        else
//                        {
//                            if link == links.last
//                            {
//                                self.finalMessage = completeURL(url : "\(value)")
//                                self.postLink = completeURL(url : "\(value)")
//                                self.setupPostData()
//                                break
//                            }
//                        }
//
//                    }
//                }
//
//
//
//            }
//            else
//            {
//                self.finalMessage = textView.text
//            }
//        }
//
    
    }
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {

        
     if  !linkPreviewVisible
     {
        if text.contains(" ") || text.contains("http") || text.contains("://") || text.contains("\n") || text.contains("\t") || text.contains(".com") || text.contains(".in") || text.contains("m") || text.contains("n")
        {
            self.finalMessage = textView.text
            if text.contains("http") || text.contains("://") || text.contains("\n") || text.contains("\t") || text.contains(".com") || text.contains(".in") || text.contains("m") || text.contains("n"){
                self.finalMessage = textView.text + text

                self.finalMessage = " " + self.finalMessage + " "

            }
            
             let textInputContent = self.finalMessage
            
            let result = textInputContent.getAllClickableLinks()
            if let links = result {
                linksProperties = links
                for link in links {
                    let content = textInputContent as NSString
                    let value = content.substring(with: link.range)
                    
                    if isFirstTime == true
                    {
                        if link == links.first
                        {
                            self.finalMessage = completeURL(url : "\(value)")
                            self.postLink = completeURL(url : "\(value)")
                            self.setupPostData()
                            break
                        }
                    }
                    else
                    {
                        if link == links.last
                        {
                            self.finalMessage = completeURL(url : "\(value)")
                            self.postLink = completeURL(url : "\(value)")
                            self.setupPostData()
                            break
                        }
                    }
                    
                }
            }
            
            
            
        }
        else
        {
            self.finalMessage = textView.text
        }
    }
        
        //tblFeeds.reloadRows(at: [IndexPath.init(row: 0, section: 0)], with: .automatic)
        return true
    }
    
    
    func completeURL(url : String) -> String
    {
        var newUrl = url
        
        
        if (newUrl.lowercased().hasPrefix("https://www.") || newUrl.lowercased().hasPrefix("http://www.") || newUrl.lowercased().hasPrefix("http://") || newUrl.lowercased().hasPrefix("https://")){
            
            return newUrl
        }
        else if (newUrl.lowercased().hasPrefix("www.") || newUrl.lowercased().hasPrefix("www."))
        {
            newUrl = "https://" + newUrl
            
            return newUrl
        }
        else
        {
            newUrl = "https://www." + newUrl
            return newUrl
        }
    
        //return newUrl
    }
    @IBAction func btnPreviewCloseTapped(_ sender: UIButton) {
        
        isFirstTime = false
        linkPreviewVisible = false
        self.postLink = ""
        self.previewView.isHidden = true
        self.collectionPostFeed.isHidden = true
        self.viewParent.frame.size.height = 148
        self.previewView_height.constant = 0
        self.collectionPageHeight.constant = 0
        self.tblFeeds.reloadData()
        self.view.layoutIfNeeded()
    }
    
    //MARK: Button Search Action
    @IBAction func btnSearchAction(_ sender: UIButton) {
        let searchPage = self.storyboard?.instantiateViewController(withIdentifier: "SearchFriendViewController") as! SearchFriendViewController
        self.navigationController?.pushViewController(searchPage, animated: false)
    }
    
    
    //MARK: Delete Post Api
    func callDeletePostAPI(delete_index : Int)
    {
        if(CommonFunction.isInternetAvailable())
        {
            MBProgressHUD.showAdded(to: self.view, animated: true)
            Alamofire.request(Constants.BASEURL + MethodName.deletePost + "?access-token=\(UserDefaults.standard.value(forKey: "Access_Token") as! String)&id=\(delete_index)", method: .delete, parameters: nil , encoding: JSONEncoding.default, headers:nil).responseJSON
            { response in
                MBProgressHUD.hide(for: self.view, animated: true)
                
                if let result_dict = response.result.value as? [String:Any]
                {
                    if let code = result_dict["code"] as? Int
                    {
                        if(code==200)
                        {
                            self.displayAlert(msg: (result_dict["data"] as! [String:Any])["message"] as! String, title_str: Constants.APP_NAME)
                            self.arrPostList.remove(at: self.post_select_index)
                            self.tblFeeds.reloadData()
                        }
                        else
                        {
                            if let error_arr = result_dict["errorData"] as? [[String:Any]]
                            {
                                if let error_msg = error_arr[0]["message"] as? String
                                {
                                    self.view.makeToast(error_msg)
                                }
                            }
                        }
                    }
                }
            }
        }
        else
        {
            self.view.makeToast("Please Check Your Internet Connection!!")
        }
    }
    
    //MARK: Delete Group Post Api
    func callDeleteGroupPostAPI(delete_index : Int)
    {
        MBProgressHUD.showAdded(to: self.view, animated: true)
        Alamofire.request(Constants.BASEURL + MethodName.groupPostDelete + "?access-token=\(UserDefaults.standard.value(forKey: "Access_Token") as! String)&id=\(delete_index)", method: .delete, parameters: nil , encoding: JSONEncoding.default, headers:nil).responseJSON
            { response in
                MBProgressHUD.hide(for: self.view, animated: true)
                
                if let result_dict = response.result.value as? [String:Any]
                {
                    if let code = result_dict["code"] as? Int
                    {
                        if(code==200)
                        {
                            self.displayAlert(msg: (result_dict["data"] as! [String:Any])["message"] as! String, title_str: Constants.APP_NAME)
                            self.arrPostList.remove(at: self.post_select_index)
                            self.tblFeeds.reloadData()
                        }
                        else
                        {
                            if let error_arr = result_dict["errorData"] as? [[String:Any]]
                            {
                                if let error_msg = error_arr[0]["message"] as? String
                                {
                                    self.view.makeToast(error_msg)
                                }
                            }
                            
                        }
                    }
                }
        }
    }
    
    
    func setupPostData()
    {
       
        //self.initialSetup(collectionSlider: self.collectionPostFeed)
        
        if self.data_arr.count>0
        {
            self.postLink = ""
            self.linkPreviewVisible = false
            self.previewView.isHidden = true
            self.collectionPostFeed.isHidden = false
            self.viewParent.frame.size.height = 170 + self.collectionPostFeed.collectionViewLayout.collectionViewContentSize.height
            self.previewView_height.constant = self.collectionPostFeed.collectionViewLayout.collectionViewContentSize.height
            //self.collectionPageHeight.constant = 87

            self.viewMain.layoutIfNeeded()
            self.viewParent.layoutIfNeeded()
            self.collectionPostFeed.layoutIfNeeded()
            self.view.layoutIfNeeded()
            self.collectionPostFeed.reloadData()
            
            
            self.collectionPageHeight.constant = self.collectionPostFeed.collectionViewLayout.collectionViewContentSize.height
            self.view.setNeedsLayout() //Or self.view.layoutIfNeeded()
            
            self.tblFeeds.layoutIfNeeded()
            self.tblFeeds.reloadData()

        }
        else
        {
            if (self.finalMessage.lowercased().range(of: "https://")) == nil
            {
                self.previewView.isHidden = true
                self.collectionPostFeed.isHidden = true
                self.viewParent.frame.size.height = 148
                self.previewView_height.constant = 0
                self.collectionPageHeight.constant = 0
                self.tblFeeds.layoutIfNeeded()
                self.tblFeeds.reloadData()
                self.view.layoutIfNeeded()
            }
            else
            {
                linkPreviewVisible = true
                self.previewView.isHidden = false
                self.collectionPostFeed.isHidden = true
                self.viewParent.frame.size.height = 235
                self.previewView_height.constant = 87
                self.collectionPageHeight.constant = 87
                self.view.layoutIfNeeded()
                self.tblFeeds.layoutIfNeeded()
                self.tblFeeds.reloadData()
                
                
                //self.centerActivityIndicator?.isHidden = false
                //self.centerActivityIndicator.startAnimating()
                self.previewloader.beginRefreshing()
                
                if let url = self.slp.extractURL(text: self.finalMessage),
                    let cached = self.slp.cache.slp_getCachedResponse(url: url.absoluteString) {
                    
                    self.result = cached
                    
                    if let value: [String] = self.result[.images] as? [String] {
                        
                        self.previewImageView.image = nil
                        if let img_str = self.result[.image] as? String
                        {
                            self.previewImageView.sd_setImage(with: URL(string: (self.result[.image] as? String)!), placeholderImage: UIImage.init(),completed: nil)
                        }
                        else
                        {
                            self.previewImageView.image = UIImage(named: "Placeholder")
                        }
                        
                        
                    } else {
                        
                        self.previewImageView.image = nil
                        
                        if let img_str = self.result[.image] as? String
                        {
                            self.previewImageView.sd_setImage(with: URL(string: (self.result[.image] as? String)!), placeholderImage: UIImage.init(),completed: nil)
                        }
                        else
                        {
                            self.previewImageView.image = UIImage(named: "Placeholder")
                        }
                    }
                    
                    if let value: String = self.result[.title] as? String {
                        
                        self.previewTitle_lbl?.text = value.isEmpty ? "" : value
                        
                    } else {
                        
                        self.previewTitle_lbl?.text = ""
                        
                    }
                    
                    
                    if let value: String = self.result[.description] as? String {
                        
                        self.previewDesc_lbl?.text = value.isEmpty ? "" : value
                        
                    } else {
                        
                        self.previewTitle_lbl?.text = ""
                        
                    }
                    
                    //self.centerActivityIndicator.stopAnimating()
                    //self.centerActivityIndicator.isHidden = true
                    self.previewloader.endRefreshing()
                    
                    result.forEach { print("\($0):", $1) }
                    
                } else {
                    self.slp.preview(
                        self.finalMessage,
                        onSuccess: { result in
                            
                            result.forEach { print("\($0):", $1) }
                            self.result = result
                            
                            if let value: [String] = self.result[.images] as? [String] {
                                
                                self.previewImageView.image = nil
                                if let img_str = self.result[.image] as? String
                                {
                                    self.previewImageView.sd_setImage(with: URL(string: (self.result[.image] as? String)!), placeholderImage: UIImage.init(),completed: nil)
                                }
                                else
                                {
                                    self.previewImageView.image = UIImage(named: "Placeholder")
                                }
                                
                                // }
                                
                            } else {
                                
                                self.previewImageView.image = nil
                                
                                if let img_str = self.result[.image] as? String
                                {
                                    self.previewImageView.sd_setImage(with: URL(string: (self.result[.image] as? String)!), placeholderImage: UIImage.init(),completed: nil)
                                }
                                else
                                {
                                    self.previewImageView.image = UIImage(named: "Placeholder")
                                }
                                
                            }
                            
                            if let value: String = self.result[.title] as? String {
                                
                                self.previewTitle_lbl?.text = value.isEmpty ? "" : value
                                
                            } else {
                                
                                self.previewTitle_lbl?.text = ""
                                
                            }
                            
                            if let value: String = self.result[.description] as? String {
                                
                                self.previewDesc_lbl?.text = value.isEmpty ? "" : value
                                
                            } else {
                                
                                self.previewTitle_lbl?.text = ""
                                
                            }
                            //self.centerActivityIndicator.stopAnimating()
                            //self.centerActivityIndicator.isHidden = true
                            self.previewloader.endRefreshing()
                    },
                        onError: { error in
                            
                            print(error)
                            
                            Drop.down(error.description, state: .error)
                            
                    }
                    )
                }
            }
            
        }
    }
    
    @IBAction func btnSendAction(_ sender: UIButton) {
        
        
        self.view.endEditing(true)
            var params = [String:String]()
            
            if data_arr.count > 0
            {
                if data_arr.contains(where: { $0["media_type"] as! String == "video" }) {
                    post_type = "video"
                } else {
                    post_type = "image"
                }
            }
            else
            {
                post_type = "text"
            }
            params.updateValue(post_type, forKey: "post_type")
        
            if self.txtMessage.text=="Write Something...."
            {
                
            }
            else
            {
                params.updateValue(self.txtMessage.text.encodeEmoji, forKey: "post_title")
            }
            params.updateValue("\(UserDefaults.standard.value(forKey: Constants.USERID) as! Int)", forKey: "user_id")
            params.updateValue(MethodName.Token, forKey: "Token")
            params.updateValue(self.postLink, forKey: "url")
            
            if post_type=="text"
            {
                
                if(CommonFunction.isInternetAvailable())
                {
                    MBProgressHUD.showAdded(to: self.view, animated: true)
                    let headerString = [
                        "Content-Type": "application/json"
                    ]
                    
                    Alamofire.request(Constants.BASEURL+MethodName.createPost + "?access-token=\(UserDefaults.standard.value(forKey: "Access_Token") as! String)", method: .post, parameters: params, encoding: JSONEncoding.default, headers: headerString).responseJSON { response in
                        MBProgressHUD.hide(for: self.view, animated: true)
                        switch response.result {
                        case .success:
                            
                            if let result_dict = response.result.value as? [String:Any]
                            {
                                let code:Int = result_dict["code"]as! Int
                                if  (code == 200)
                                {
                                    self.post_type = "text"
                                    self.finalMessage = ""
                                    self.txtMessage.text = "Write Something...."
                                    self.previewDesc_lbl.text = ""
                                    self.previewTitle_lbl.text = ""
                                    self.postLink = ""
                                    self.linkPreviewVisible = false
                                    self.isFirstTime = true
                                    self.previewImageView.image = nil
                                    self.previewView.isHidden = true
                                    self.collectionPostFeed.isHidden = true
                                    self.viewParent.frame.size.height = 148
                                    self.previewView_height.constant = 0
                                    self.collectionPageHeight.constant = 0
                                    self.getPost()
                                    
                                }
                                else
                                {
                                    if let result_error = response.result.value as? [String:Any]
                                    {
                                        if let error_array = result_error["errorData"] as? NSArray
                                        {
                                            if let error_dict = error_array[0] as? [String : Any]
                                            {
                                                
                                                 MBProgressHUD.hide(for: self.view, animated: true)
                                                
                                                if let error_msg = error_dict["message"] as? String
                                                {
                                                    self.view.makeToast(error_msg)
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                            
                        case .failure(let error):
                            print(error)
                             MBProgressHUD.hide(for: self.view, animated: true)
                        }
                    }
                }
                else
                {
                     MBProgressHUD.hide(for: self.view, animated: true)
                    self.view.makeToast("Please Check Your Internet Connection!!")
                }
            }
            else
            {
                if(CommonFunction.isInternetAvailable())
                {
                    MBProgressHUD.showAdded(to: self.view, animated: true)
                    Alamofire.upload( multipartFormData: { multipartFormData in
                        for (key, value) in params {
                            multipartFormData.append("\(value)".data(using: String.Encoding.utf8)!, withName: key as String)
                        }
                        for data_val in self.data_arr
                        {
                            let type_str = data_val["media_type"] as! String
                            if(type_str=="video")
                            {
                                multipartFormData.append(data_val["data_str"] as! Data, withName: "image[]", fileName: "\(arc4random()).mp4", mimeType: "video/mp4")
                                let img = data_val["Preview"] as! UIImage
                                multipartFormData.append(UIImagePNGRepresentation(img)!, withName: "image[]", fileName: "\(arc4random()).png", mimeType: "image/png")
                            }
                            else
                            {
                                multipartFormData.append(data_val["data_str"] as! Data, withName: "image[]", fileName: "\(arc4random()).png", mimeType: "image/png")
                                
                            }
                            
                        }
                        
                    }, to: Constants.BASEURL+MethodName.createPost + "?access-token=\(UserDefaults.standard.value(forKey: "Access_Token") as! String)", encodingCompletion: { encodingResult in
                        
                        switch encodingResult
                        {
                            
                        case .success(let upload, _, _):
                            
                            upload.responseJSON { response in
                                MBProgressHUD.hide(for: self.view, animated: true)
                                if let JSON = response.result.value as? NSDictionary {
                                    if(JSON.value(forKey: "code") as! Int == 200)
                                    {
                                        
                                        print(JSON)
                                        self.data_arr.removeAll()
                                        self.post_type = "text"
                                        self.finalMessage = ""
                                        self.txtMessage.text = "Write Something...."
                                        self.collectionPostFeed.reloadData()
                                        self.previewDesc_lbl.text = ""
                                        self.previewTitle_lbl.text = ""
                                        self.postLink = ""
                                        self.linkPreviewVisible = false
                                        self.isFirstTime = true
                                        self.previewImageView.image = nil
                                        self.previewView.isHidden = true
                                        self.collectionPostFeed.isHidden = true
                                        self.viewParent.frame.size.height = 148
                                        self.previewView_height.constant = 0
                                        self.collectionPageHeight.constant = 0
                                        self.getPost()
                                    }
                                    else
                                    {
                                        if let error_array = JSON.value(forKey: "errorData") as? NSArray
                                        {
                                            if let error_dict = error_array[0] as? [String : Any]
                                            {
                                                 MBProgressHUD.hide(for: self.view, animated: true)
                                                
                                                if let error_msg = error_dict["message"] as? String
                                                {
                                                    self.view.makeToast(error_msg)
                                                }
                                            }
                                        }
                                    }
                                }
                                else
                                {
                                    
                                    print(response)
                                }
                            }
                        case .failure(let encodingError):
                            MBProgressHUD.hide(for: self.view, animated: true)
                            print(encodingError)
                            
                        }
                    })
                }
                else
                {
                     MBProgressHUD.hide(for: self.view, animated: true)
                    self.view.makeToast("Please Check Your Internet Connection!!")
                }
            }
        
    }
}

extension HomeViewController
{
    
    override func viewDidLayoutSubviews() {
        tblFeeds.reloadData()
    }
    
    //MARK:- TableView Delegate Methods
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {

        return self.arrPostList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
            var cell = FeedCell()

            cell=tableView.dequeueReusableCell(withIdentifier: "FeedCell", for: indexPath) as! FeedCell
            
            //------------
            cell.btnLoadMoreCmnt.tag=indexPath.row
            cell.btnLoadMoreCmnt.addTarget(self, action: #selector(self.loadMoreComments(btn:)), for: .touchUpInside)
            cell.btnComment.tag=indexPath.row
        
        
            cell.btnOption.tag=indexPath.row
            cell.btnComment.addTarget(self, action: #selector(self.loadMoreComments(btn:)), for: .touchUpInside)
            cell.btnOption.addTarget(self, action: #selector(showToolTipMenu(_:)), for: .touchUpInside)
        
            cell.viewLblComment1.layer.cornerRadius=30
            cell.viewLblComment2.layer.cornerRadius=30
            cell.viewLblComment1.clipsToBounds=true
            cell.viewLblComment2.clipsToBounds=true
            
            
            if self.arrPostList[indexPath.row].postcontent.count>0
            {
                for i in 0..<self.arrPostList[indexPath.row].postcontent.count
                {
                    let post : PostContent = self.arrPostList[indexPath.row].postcontent[i]
                    
                    cell.txtPostTitle.text = self.arrPostList[indexPath.row].postTitle?.decodeEmoji
                    let readMoreTextAttributes: [NSAttributedStringKey: Any] = [
                        NSAttributedStringKey.foregroundColor: UIColor(red: 252.0/255.0, green: 89.0/255.0, blue: 138.0/255.0, alpha: 1.0),
                        NSAttributedStringKey.font: UIFont.systemFont(ofSize: 13)
                    ]
                    _ = [
                        NSAttributedStringKey.foregroundColor: UIColor(red: 252.0/255.0, green: 89.0/255.0, blue: 138.0/255.0, alpha: 1.0),
                        NSAttributedStringKey.font: UIFont.systemFont(ofSize: 13)
                    ]
                    cell.txtPostTitle.textColor = UIColor.black
                    cell.txtPostTitle.attributedReadMoreText = NSAttributedString(string: "...more", attributes: readMoreTextAttributes)

                    cell.txtPostTitle.shouldTrim = !expandedCells.contains(indexPath.row)
                    cell.txtPostTitle.setNeedsUpdateTrim()
                    cell.txtPostTitle.layoutIfNeeded()
                    
                    if post.postType == "text"
                    {
                        let result = self.arrPostList[indexPath.row].postTitle?.decodeEmoji.getAllClickableLinks()
                        if let links = result {
                            linksProperties = links
                            
                            if links.count < 1
                            {
                                cell.media_pagecontrol.isHidden = true
                                cell.multimediaCollectionView.isHidden = true
                                cell.previewArea?.isHidden = true
                                cell.btnPreviewLink.isHidden = true
                                cell.previewHeight.constant = 0
                                cell.imagePagerHeight.constant=0
                            }
                            else
                            {
                                if self.arrPostList[indexPath.row].postLink != ""
                                {
                                    cell.media_pagecontrol.isHidden = true
                                    cell.multimediaCollectionView.isHidden = true
                                    cell.previewArea?.isHidden = false
                                    cell.btnPreviewLink.isHidden = false
                                    cell.previewHeight.constant = 171
                                    cell.imagePagerHeight.constant=171

                                    cell.slideshow.image = nil
                                    
                                    cell.btnPreviewLink.addTarget(self, action: #selector(btnPreviewLinkTapped(sender:)), for: .touchUpInside)
                                    cell.detailedView?.isHidden = true
                                    cell.loaderspinner.beginRefreshing()
                                    
                                    if let url = self.slp.extractURL(text: self.arrPostList[indexPath.row].postLink),
                                        let cached = self.slp.cache.slp_getCachedResponse(url: url.absoluteString) {
                                        
                                        self.result = cached
                                        
                                        if let _: [String] = self.result[.images] as? [String] {
                                            
                                            cell.slideshow.image = nil
                                            cell.slideshow.sd_showActivityIndicatorView()
                                            if (self.result[.image] as? String) != nil
                                            {
                                                cell.slideshow.sd_setImage(with: URL(string: (self.result[.image] as? String)!), placeholderImage: UIImage.init(),completed: { (image, error, type, url) in
                                                    cell.slideshow.sd_removeActivityIndicator()
                                                })
                                            }
                                            else
                                            {
                                                cell.slideshow.image = UIImage(named: "Placeholder")
                                            }
                                            
                                        } else {
                                            
                                            cell.slideshow.image = nil
                                            cell.slideshow.sd_showActivityIndicatorView()
                                            if let img_str = self.result[.image] as? String
                                            {
                                                cell.slideshow.sd_setImage(with: URL(string: (self.result[.image] as? String)!), placeholderImage: UIImage.init(),completed: { (image, error, type, url) in
                                                    cell.slideshow.sd_removeActivityIndicator()
                                                })
                                            }
                                            else
                                            {
                                                cell.slideshow.image = UIImage(named: "Placeholder")
                                            }
                                        }
                                        
                                        if let value: String = self.result[.title] as? String {
                                            
                                            cell.previewTitle?.text = value.isEmpty ? "" : value
                                            
                                        } else {
                                            
                                            cell.previewTitle?.text = ""
                                            
                                        }
                                        
                                        if let value: String = self.result[.canonicalUrl] as? String {
                                            
                                            cell.previewCanonicalUrl?.text = value
                                            
                                        }
                                        
                                        if let value: String = self.result[.description] as? String {
                                            
                                            cell.previewDescription?.text = value.isEmpty ? "" : value
                                            
                                        } else {
                                            
                                            cell.previewTitle?.text = ""
                                            
                                        }
                                        
                                        if let value: String = self.result[.icon] as? String, let url = URL(string: value) {
                                            cell.favicon?.af_setImage(withURL: url)
                                        }
                                        else
                                        {
                                            cell.favicon?.image = UIImage(named: "Placeholder")
                                        }
                                        
                                        cell.detailedView?.isHidden = false
                                        cell.loaderspinner.endRefreshing()
                                        
                                        
                                    } else {
                                        self.slp.preview(
                                            self.arrPostList[indexPath.row].postLink,
                                            onSuccess: { result in
                                                
                                                result.forEach { print("\($0):", $1) }
                                                self.result = result
                                                
                                                if let value: [String] = self.result[.images] as? [String] {
                                                    
                                                    cell.slideshow.image = nil
                                                    cell.slideshow.sd_showActivityIndicatorView()
                                                    
                                                    if let img_str = self.result[.image] as? String
                                                    {
                                                        cell.slideshow.sd_setImage(with: URL(string: (self.result[.image] as? String)!), placeholderImage: UIImage.init(),completed: { (image, error, type, url) in
                                                            cell.slideshow.sd_removeActivityIndicator()
                                                        })
                                                    }
                                                    else
                                                    {
                                                        cell.slideshow.image = UIImage(named: "Placeholder")
                                                    }
                                                    
                                                } else {
                                                    
                                                    cell.slideshow.image = nil
                                                    
                                                    cell.slideshow.sd_showActivityIndicatorView()
                                                    if let img_str = self.result[.image] as? String
                                                    {
                                                        cell.slideshow.sd_setImage(with: URL(string: (self.result[.image] as? String)!), placeholderImage: UIImage.init(),completed: { (image, error, type, url) in
                                                            cell.slideshow.sd_removeActivityIndicator()
                                                        })
                                                    }
                                                    else
                                                    {
                                                        cell.slideshow.image = UIImage(named: "Placeholder")
                                                    }
                                                    
                                                }
                                                
                                                if let value: String = self.result[.title] as? String {
                                                    
                                                    cell.previewTitle?.text = value.isEmpty ? "" : value
                                                    
                                                } else {
                                                    
                                                    cell.previewTitle?.text = ""
                                                    
                                                }
                                                
                                                if let value: String = self.result[.canonicalUrl] as? String {
                                                    
                                                    cell.previewCanonicalUrl?.text = value
                                                    
                                                }
                                                
                                                if let value: String = self.result[.description] as? String {
                                                    
                                                    cell.previewDescription?.text = value.isEmpty ? "" : value
                                                    
                                                } else {
                                                    
                                                    cell.previewTitle?.text = ""
                                                    
                                                }
                                                
                                                if let value: String = self.result[.icon] as? String, let url = URL(string: value) {
                                                    cell.favicon?.af_setImage(withURL: url)
                                                }
                                                else
                                                {
                                                    cell.favicon?.image = UIImage(named: "Placeholder")
                                                }
                                                
                                                cell.detailedView?.isHidden = false
                                                cell.loaderspinner.endRefreshing()
                                        },
                                            onError: { error in
                                                
                                                print(error)
                                                
                                                Drop.down(error.description, state: .error)
                                                
                                        }
                                        )
                                    }
                                }
                                else
                                {
                                    cell.media_pagecontrol.isHidden = true
                                    cell.multimediaCollectionView.isHidden = true
                                    cell.previewArea?.isHidden = true
                                    cell.btnPreviewLink.isHidden = true
                                    cell.previewHeight.constant = 0
                                    cell.imagePagerHeight.constant=0
                                }
                            }
                        }
                        
                    }
                    else if post.postType == "image" || post.postType == "video"
                    {

                        
                        if self.arrPostList[indexPath.row].postcontent.count > 1
                        {
                            cell.media_pagecontrol.isHidden = false
                        }
                        else
                        {
                            cell.media_pagecontrol.isHidden = true
                        }
                        cell.multimediaCollectionView.isHidden = false
                        cell.previewArea?.isHidden = true
                        cell.btnPreviewLink.isHidden = true
                       

                        cell.slideshow.image = nil
                        
                        let postArr =  self.arrPostList[indexPath.row].postcontent
                        if postArr.count == 1 && (postArr.first?.post.contains("png"))!{

                            UIImageView().sd_setImage(with: URL(string: postArr.first?.post ?? ""), placeholderImage: UIImage.init(), completed: { (image, error, type, url) in
                               
                                
                                DispatchQueue.main.async{
                                if let imageTOUSe = image {
                                                       let originalImg = imageTOUSe
                                    let wide = cell.getImageSize(indexPath: indexPath).width
                                    
                                
    cell.previewHeight.constant =  originalImg.getHeightAcordingToWidth(wide)
    cell.imagePagerHeight.constant =    originalImg.getHeightAcordingToWidth(wide)
                                    cell.layoutIfNeeded()
                                    cell.reloadInputViews()
                                    
                       // self.tblFeeds.reloadRows(at: [indexPath], with: UITableViewRowAnimation.automatic)
                                            }
                                    

                                }
                                
                                
                            })
                        }else{
                            
                            cell.previewHeight.constant = 171
                            cell.imagePagerHeight.constant=171
                            if postArr.count > 3 {
                                cell.previewHeight.constant = 171*2
                                cell.imagePagerHeight.constant=171*2
                            }
                            
                        }
                        
                        
                        
                    }
                    else
                    {
                        print("")
                    }
                }
            }
            

            
            let colorPink = UIColor(red: 255.0 / 255.0, green: 235.0 / 255.0, blue: 242.0 / 255.0, alpha: 1.0)
            let colorGray = UIColor(red: 244.0 / 255.0, green: 244.0 / 255.0, blue: 244.0 / 255.0, alpha: 1.0)
            let myUserID : Int = UserDefaults.standard.value(forKey: Constants.USERID) as! Int

            if self.arrPostList[indexPath.row].postcomments.count>=2
            {
                cell.viewComment1.isHidden = false
                cell.viewComment2.isHidden = false
                cell.heightViewComment1.constant = 70.0
                cell.heightViewComment2.constant = 70.0
                
                if Int(self.arrPostList[indexPath.row].comments_count)! > 2
                {
                    cell.btnLoadMoreCmnt.isHidden = false
                    cell.btnHeightCell.constant = 30
                }
                else
                {
                    cell.btnLoadMoreCmnt.isHidden = true
                    cell.btnHeightCell.constant = 0
                }

                
                if Int(self.arrPostList[indexPath.row].postcomments[0].userId)! == myUserID
                {
                    cell.widthCommenter1.constant = 40.0
                    cell.widthCommenter2.constant = 0.0
                    cell.imgSender.sd_setImage(with: URL(string: self.arrPostList[indexPath.row].postcomments[0].userImage), placeholderImage: UIImage(named: "userPlaceHolder"))
                    cell.imgSender.tag = 0
                    self.add_tap_gester_chat(cell.imgSender)
                    cell.viewLblComment1.backgroundColor = colorPink
                }
                else
                {
                    cell.widthCommenter1.constant = 0.0
                    cell.widthCommenter2.constant = 40.0
                    cell.viewLblComment1.backgroundColor = colorGray
                    cell.imgSender2.sd_setImage(with: URL(string: self.arrPostList[indexPath.row].postcomments[0].userImage), placeholderImage: UIImage(named: "userPlaceHolder"))
                    cell.imgSender2.tag = 0
                    self.add_tap_gester_chat(cell.imgSender2)
            
                }
            
                if Int(self.arrPostList[indexPath.row].postcomments[1].userId)! == myUserID
                {
                    cell.widthCommenter3.constant = 40.0
                    cell.widthCommenter4.constant = 0.0
                    cell.imgRecv2.sd_setImage(with: URL(string: self.arrPostList[indexPath.row].postcomments[1].userImage), placeholderImage: UIImage(named: "userPlaceHolder"))
                    cell.imgRecv2.tag = 1
                    self.add_tap_gester_chat(cell.imgRecv2)
                    cell.viewLblComment2.backgroundColor = colorPink
                }
                else
                {
                    cell.widthCommenter3.constant = 0.0
                    cell.widthCommenter4.constant = 40.0
                    cell.imgRecv.sd_setImage(with: URL(string: self.arrPostList[indexPath.row].postcomments[1].userImage), placeholderImage: UIImage(named: "userPlaceHolder"))
                    cell.imgRecv.tag = 1
                    self.add_tap_gester_chat(cell.imgRecv)
                    cell.viewLblComment2.backgroundColor = colorGray
                }
                
                
             cell.lblSenderMsg.text=self.arrPostList[indexPath.row].postcomments[0].comment.decodeEmoji

                    
                cell.lblSenderName.text = self.arrPostList[indexPath.row].postcomments[0].userName
                cell.lblReciever.text=self.arrPostList[indexPath.row].postcomments[1].comment.decodeEmoji
                cell.lblReciverName.text = self.arrPostList[indexPath.row].postcomments[1].userName
                cell.lblSenderName.tag = 0
                cell.lblReciverName.tag = 1
                self.add_tap_gester_chatlbl(cell.lblSenderName)
                self.add_tap_gester_chatlbl(cell.lblReciverName)
            }
            else if self.arrPostList[indexPath.row].postcomments.count>=1
            {
                cell.viewComment1.isHidden = false
                cell.viewComment2.isHidden = true
                cell.heightViewComment1.constant = 70.0
                cell.heightViewComment2.constant = 0.0
            
                cell.btnLoadMoreCmnt.isHidden = true
                cell.btnHeightCell.constant = 0
                
                if Int(self.arrPostList[indexPath.row].postcomments[0].userId)! == myUserID
                {
                    cell.widthCommenter1.constant = 40.0
                    cell.widthCommenter2.constant = 0.0
                    cell.imgSender.sd_setImage(with: URL(string: self.arrPostList[indexPath.row].postcomments[0].userImage), placeholderImage: UIImage(named: "userPlaceHolder"))
                    cell.imgSender.tag = 0
                    self.add_tap_gester_chat(cell.imgSender)
                    cell.viewLblComment1.backgroundColor = colorPink
                }
                else
                {
                    cell.widthCommenter1.constant = 0.0
                    cell.widthCommenter2.constant = 40.0
                    cell.imgSender2.sd_setImage(with: URL(string: self.arrPostList[indexPath.row].postcomments[0].userImage), placeholderImage: UIImage(named: "userPlaceHolder"))
                    cell.imgSender2.tag = 0
                    self.add_tap_gester_chat(cell.imgSender2)
                    cell.viewLblComment1.backgroundColor = colorGray
                }
                
               cell.lblSenderMsg.text=self.arrPostList[indexPath.row].postcomments[0].comment.decodeEmoji

                                
                cell.lblSenderName.text = self.arrPostList[indexPath.row].postcomments[0].userName
                cell.lblSenderName.tag = 0
                self.add_tap_gester_chatlbl(cell.lblSenderName)
            }
            else
            {
                cell.viewComment1.isHidden = true
                cell.viewComment2.isHidden = true
                cell.heightViewComment1.constant = 0.0
                cell.heightViewComment2.constant = 0.0
                
                cell.btnLoadMoreCmnt.isHidden = true
                cell.btnHeightCell.constant = 0
            }
            cell.btnComment.tag=indexPath.row
            cell.btnLike.tag=indexPath.row
            
            if self.arrPostList[indexPath.row].isLike == "1"
            {
                cell.btnHeart.setImage(UIImage(named: "heart_fill"), for: .normal)
                
            }
            else
            {
                cell.btnHeart.setImage(UIImage(named: "like"), for: .normal)
            }

        
        
        cell.lblName.text = self.arrPostList[indexPath.row].username
        cell.lblName2.text = self.arrPostList[indexPath.row].username
        
        //dictGroupDetails["groupName"] as? String
        
        
        let dateCreated = self.arrPostList[indexPath.row].created_At
               
                 
        if self.arrPostList[indexPath.row].groupData != nil
        {
            cell.imgTriangle.isHidden = true
            cell.lblGroupName.isHidden = true
            cell.lblGroupName2.isHidden = true
            
            if let dictGroupDetails = self.arrPostList[indexPath.row].groupData
            {
//                cell.groupNameLabelWidth.constant = (cell.name_view.frame.width - 30)/2
//                cell.groupNameLabel2Width.constant = (cell.name_view.frame.width - 30)/2
//                cell.lblNameWidth.constant = (cell.name_view.frame.width - 30)/2
//                cell.lblName2Width.constant = (cell.name_view.frame.width - 30)/2
//                cell.imgTriangleWidth.constant = 20.0
//                cell.lblGroupName.layoutIfNeeded()
//                cell.lblGroupName2.layoutIfNeeded()
//                cell.lblName.layoutIfNeeded()
//                cell.lblName2.layoutIfNeeded()
//                cell.imgTriangle.layoutIfNeeded()
                

                
                cell.lblGroupName.text = dictGroupDetails["groupName"] as? String
                cell.lblGroupName2.text = dictGroupDetails["groupName"] as? String
                
                 cell.lblName.text =   "\(self.arrPostList[indexPath.row].username) > \(dictGroupDetails["groupName"] as? String ?? "")"
                if dateCreated == 0
                                 {
                                     cell.lblAuthor.text = ""
                                 }
                                 else
                                 {
                                     let date = Date(timeIntervalSince1970: Double(dateCreated))
                                     let dateFormatter = DateFormatter()
                                     dateFormatter.timeZone = NSTimeZone.local //Set timezone that you want
                                     dateFormatter.dateFormat = "dd MMM yyyy 'at' h:mm a" //Specify your format that you want
                                     let strDate = dateFormatter.string(from: date)
                             //Triangle
                                    let usernameAtt = NSMutableAttributedString.init(string:"\(self.arrPostList[indexPath.row].username) " , attributes: [NSAttributedStringKey.foregroundColor  :  #colorLiteral(red: 1, green: 0.2509803922, blue: 0.3803921569, alpha: 1) ])
                                   
                                    let imgAtt = #imageLiteral(resourceName: "Triangle").toAttributedString(with: 15)
                                    let grpNameAtt = NSAttributedString.init(string:" \(dictGroupDetails["groupName"] as? String ?? "") " , attributes: [NSAttributedStringKey.foregroundColor  : UIColor.black])
                                    
                               
                                    let autherAtt = NSMutableAttributedString.init(string: " \(strDate)", attributes: [NSAttributedStringKey.foregroundColor  : UIColor.lightGray])
                                    
                                    usernameAtt.append(imgAtt)
                                    usernameAtt.append(grpNameAtt)
                                    usernameAtt.append(autherAtt)
                                    cell.lblName.attributedText = usernameAtt
                                    cell.lblName.lineBreakMode = NSLineBreakMode.byWordWrapping
                                    //"\(self.arrPostList[indexPath.row].username) > \(dictGroupDetails["groupName"] as? String ?? "") \(strDate)"
                                     //cell.lblAuthor.text = strDate
                                 }
                      
                
              
                self.add_tap_gester_Grouplbl(cell.lblGroupName)
                self.add_tap_gester_Grouplbl(cell.lblGroupName2)
                
//                if cell.lblName.frame.size.height >  cell.lblGroupName.frame.size.height
//                {
//                    cell.lblGroupName.isHidden = true
//                    cell.lblGroupName2.isHidden = false
//                    cell.lblName.isHidden = false
//                    cell.lblName2.isHidden = true
//
//
//                }
//                else if cell.lblGroupName.frame.size.height >   cell.lblName.frame.size.height
//                {
//                    cell.lblName.isHidden = true
//                    cell.lblName2.isHidden = false
//                    cell.lblGroupName.isHidden = false
//                    cell.lblGroupName2.isHidden = true
//                }
//                else
//                {
//                    cell.lblGroupName.isHidden = true
//                    cell.lblGroupName2.isHidden = false
//                    cell.lblName.isHidden = true
//                    cell.lblName2.isHidden = false
//                }
                
            }
            
        }
        else
        {
            cell.imgTriangle.isHidden = true
            cell.lblGroupName.isHidden = true
            cell.lblGroupName2.isHidden = true
            cell.lblName.isHidden = true
            cell.lblName2.isHidden = false
            cell.groupNameLabelWidth.constant = 0
            cell.groupNameLabel2Width.constant = 0
            cell.lblGroupName.layoutIfNeeded()
            cell.lblGroupName2.layoutIfNeeded()
         
            cell.imgTriangleWidth.constant = 0
            cell.imgTriangle.layoutIfNeeded()
        }
        
        
        if cell.LikeListBtn != nil {
        cell.LikeListBtn.addTarget(self, action: #selector(self.likeListAction(_:)), for: UIControlEvents.touchUpInside)
        }
        
            cell.btnLike.addTarget(self, action: #selector(self.press_like_btn(btn:)), for: .touchUpInside)
        
            if let cuserid = UserDefaults.standard.value(forKey: Constants.USERID) as? Int
            {
                if self.arrPostList[indexPath.row].user_id == cuserid
                {
                        cell.btnOption.isHidden = false
                    cell.btnOption.accessibilityElements = [true]

                }
                else
                {
                    cell.btnOption.isHidden = false
                    cell.btnOption.accessibilityElements = nil

                }
            }
        
        

        cell.setData(data: self.arrPostList[indexPath.row],indexPath)
            cell.media_pagecontrol.numberOfPages = self.arrPostList[indexPath.row].postcontent.count
            cell.lblTOtalCmnt.text = "(\(self.arrPostList[indexPath.row].comments_count))"
            cell.lblTOtalLike.text = "(\(self.arrPostList[indexPath.row].likes))"
            cell.multimediaCollectionView.reloadData()
        

           
            
        
            let imageURL:String = self.arrPostList[indexPath.row].user_image
            cell.imgPoster.sd_showActivityIndicatorView()
            
            self.add_tap_gester_lbl(cell.lblName)
            self.add_tap_gester_lbl(cell.lblName2)
        

            cell.imgPoster.sd_setImage(with: URL.init(string: imageURL), placeholderImage: UIImage.init(named: "user"))
            { (image, error, type, url) in
                cell.imgPoster?.sd_removeActivityIndicator()
            }
            self.add_tap_gester(cell.imgPoster)
            
        //}
        cell.selectionStyle=UITableViewCellSelectionStyle.none
        
        cell.viewMain.layer.borderColor=UIColor.init(red: 249/255, green: 225/255, blue: 233/255, alpha: 1.0).cgColor
        cell.viewMain.layer.borderWidth = 1.0
        cell.viewMain.layer.cornerRadius = 10.0
        cell.viewMain.layer.shadowColor = UIColor(red: 253.0 / 255.0, green: 233.0 / 255.0, blue: 240.0 / 255.0, alpha: 1.0).cgColor
        cell.viewMain.layer.shadowOpacity = 1.0
        cell.viewMain.layer.shadowRadius = 15.0
        cell.viewMain.layer.shadowOffset = CGSize(width: 2.0, height: 2.0)
        
        
      
        
        
        return cell
    }
    
   
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        
        let cell:FeedCell = cell as! FeedCell
        
        cell.txtPostTitle.onSizeChange = { [unowned tableView, unowned self] r in
            let point = self.tblFeeds.convert(r.bounds.origin, from: r)
            guard let indexPath = self.tblFeeds.indexPathForRow(at: point) else { return }
            if r.shouldTrim {
                self.expandedCells.remove(indexPath.row)
            } else {
                self.expandedCells.insert(indexPath.row)
            }
            self.tblFeeds.reloadData()
        }
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return 1000.0
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    @objc func btnPreviewLinkTapped(sender : UIButton)
    {
        guard let cell = sender.superview?.superview?.superview as? FeedCell else
        {
            return
        }
        let selected_indexPath = tblFeeds.indexPath(for: cell)
        
        let safariVC = SFSafariViewController(url: URL(string: self.arrPostList[(selected_indexPath?.row)!].postLink)!)
        present(safariVC, animated: true, completion: nil)
        
    }
    
    func safariViewControllerDidFinish(_ controller: SFSafariViewController) {
        controller.dismiss(animated: true, completion: nil)
    }
    
    
    
//    @objc func showTheLove(gesture: UITapGestureRecognizer?) {
//        let heart = HeartView(frame: CGRect(x: 0, y: 0, width: HeartAttributes.heartSize, height: HeartAttributes.heartSize))
//        view.addSubview(heart)
//        let fountainX = HeartAttributes.heartSize / 2.0 + 20
//        let fountainY = view.bounds.height - HeartAttributes.heartSize / 2.0 - 10
//        heart.center = CGPoint(x: fountainX, y: fountainY)
//        heart.animateInView(view: view)
//    }

    
    //MARK:- Gestures
    func add_tap_gester_lbl(_ lbl : UILabel)
    {
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.tapFunction))
        lbl.isUserInteractionEnabled = true
        lbl.addGestureRecognizer(tap)
    }
    
    
    func add_tap_gester(_ imgview : UIImageView)
    {
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.tapFunctionImg))
        imgview.isUserInteractionEnabled = true
        imgview.addGestureRecognizer(tap)
    }
    
    func add_tap_gester_chatlbl(_ lbl : UILabel)
    {
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.label_tapFunction))
        lbl.isUserInteractionEnabled = true
        lbl.addGestureRecognizer(tap)
    }
    
    func add_tap_gester_Grouplbl(_ lbl : UILabel)
    {
        //lbl.textColor = UIColor.red
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.label_tapGroupFunction))
        lbl.isUserInteractionEnabled = true
        lbl.addGestureRecognizer(tap)
    }
    
    func add_tap_gester_chat(_ imgview : UIImageView)
    {
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.imageview_tapFunction))
        imgview.isUserInteractionEnabled = true
        imgview.addGestureRecognizer(tap)
    }
    @objc func imageview_tapFunction(sender:UITapGestureRecognizer)
    {
        guard let cell = sender.view?.superview?.superview?.superview?.superview as? FeedCell else
        {
            return
        }
        let selected_indexPath = tblFeeds.indexPath(for: cell)
        self.open_profile(user_id: "\(self.arrPostList[(selected_indexPath?.row)!].postcomments[(sender.view?.tag)!].userId)")
    }
    @objc func tapFunctionImg(sender:UITapGestureRecognizer)
    {
        guard let cell = sender.view?.superview?.superview?.superview as? FeedCell else
        {
            return
        }
        let selected_indexPath = tblFeeds.indexPath(for: cell)
        self.open_profile(user_id: "\(self.arrPostList[(selected_indexPath?.row)!].user_id!)")
    }
    @objc func tapFunction(sender:UITapGestureRecognizer)
    {
        guard let cell = sender.view?.superview?.superview?.superview?.superview as? FeedCell else
        {
            return
        }
        let selected_indexPath = tblFeeds.indexPath(for: cell)
        self.open_profile(user_id: "\(self.arrPostList[(selected_indexPath?.row)!].user_id!)")
    }
    @objc func label_tapFunction(sender:UITapGestureRecognizer)
    {
        guard let cell = sender.view?.superview?.superview?.superview?.superview?.superview as? FeedCell else
        {
            return // or fatalError() or whatever
        }
        let selected_indexPath = tblFeeds.indexPath(for: cell)
        self.open_profile(user_id: "\(self.arrPostList[(selected_indexPath?.row)!].postcomments[(sender.view?.tag)!].userId)")
    }
    
    @objc func label_tapGroupFunction(sender:UITapGestureRecognizer)
    {
        guard let cell = sender.view?.superview?.superview?.superview?.superview as? FeedCell else
        {
            return // or fatalError() or whatever
        }
        let selected_indexPath = tblFeeds.indexPath(for: cell)
        
//        if let dictGroupDetails = self.arrPostList[(selected_indexPath?.row)!].groupData
//        {
//            if let group_id = dictGroupDetails["groupId"] as? String
//            {
//                let storyBoard = UIStoryboard(name: "Main", bundle: nil)
//                let objGroupDetailViewController = storyBoard.instantiateViewController(withIdentifier: "GroupDetailViewController") as! GroupDetailViewController
//                objGroupDetailViewController.groupID = Int(group_id)!
//                self.navigationController?.pushViewController(objGroupDetailViewController, animated: true)
//            }
//        }
        
        //cell.lblGroupName.textColor = UIColor.red
        if let dictGroupDetails = self.arrPostList[(selected_indexPath?.row)!].groupData
        {
            if let group_id = dictGroupDetails["groupId"] as? String
            {
                // Fade out to set the text
                UIView.animate(withDuration: 0.1, delay: 0.0, options: UIViewAnimationOptions.curveEaseOut, animations: {
                    cell.lblGroupName.alpha = 0.0
                    cell.lblGroupName2.alpha = 0.0
                    cell.lblGroupName.textColor = UIColor.red
                    cell.lblGroupName2.textColor = UIColor.red
                }, completion: {
                    (finished: Bool) -> Void in
                    
                    //Once the label is completely invisible, set the text and fade it back in
                    cell.lblGroupName.text = dictGroupDetails["groupName"] as? String
                    cell.lblGroupName2.text = dictGroupDetails["groupName"] as? String
                    // Fade in
                    UIView.animate(withDuration: 0.1, delay: 0.0, options: UIViewAnimationOptions.curveEaseIn, animations: {
                        cell.lblGroupName.alpha = 1.0
                        cell.lblGroupName2.alpha = 1.0
                        cell.lblGroupName.textColor = UIColor.red
                        cell.lblGroupName2.textColor = UIColor.red

                    }, completion: {
                        (value: Bool) in
                        
                        cell.lblGroupName.textColor = UIColor.black
                        cell.lblGroupName2.textColor = UIColor.black
                        
                        let storyBoard = UIStoryboard(name: "Main", bundle: nil)
                        let objGroupDetailViewController = storyBoard.instantiateViewController(withIdentifier: "GroupDetailViewController") as! GroupDetailViewController
                        objGroupDetailViewController.groupID = Int(group_id)!
                        //cell.lblGroupName.textColor = UIColor.red
                        self.navigationController?.pushViewController(objGroupDetailViewController, animated: true)
                        
                    })
                    
                })
                
                
            }
            
        }
    }
    
    
    
    //MARK:- Open Profile
    func open_profile(user_id:String)
    {
        let profile_page = Constants.mainStoryboard.instantiateViewController(withIdentifier: "FriendListViewController") as! FriendListViewController
        profile_page.user_id_str = user_id
        self.navigationController?.pushViewController(profile_page, animated: true)
    }
    
    
    //MARK:- scrollViewDidEndDecelerating
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {

        if (self.tblFeeds.contentOffset.y >= (self.tblFeeds.contentSize.height - self.tblFeeds.bounds.size.height))
        {
            if(is_next_page_available)
            {
                current_page = current_page + 1
                getPost()
            }
        }
    }
    
    func convertStringToDictionary(text: String) -> [String: Any]? {
        if let data = text.data(using: .utf8) {
            do {
                return try JSONSerialization.jsonObject(with: data, options: []) as? [String: Any]
            } catch {
                print(error.localizedDescription)
            }
        }
        return nil
    }
    
     func endCrawling(centerLoadingActivityIndicatorView : UIActivityIndicatorView? ,indicator : UIActivityIndicatorView?, detailedView : UIView? ) {
        
        indicator?.isHidden = true
        indicator?.stopAnimating()
        
    }
    
//}
//
//
//
//
//extension HomeViewController
//{
    
    //MARK:- Collection View Delegate Methods
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.data_arr.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell
    {
        let cell:PostFeedImageCell = collectionView.dequeueReusableCell(withReuseIdentifier: "PostFeedImageCell", for: indexPath) as! PostFeedImageCell
        let img_dict = self.data_arr[indexPath.item]
        if(img_dict["media_type"] as! String == "image")
        {
            //cell.centerIndicator.startAnimating()
            cell.ImageSpinner.beginRefreshing()
            cell.imgPostFeed.image = img_dict["Preview"] as? UIImage
            cell.ImageSpinner.endRefreshing()
            //cell.centerIndicator.stopAnimating()
            
        }
        else
        {
            //cell.centerIndicator.startAnimating()
            cell.ImageSpinner.beginRefreshing()
            cell.imgPostFeed.image = img_dict["Preview"] as? UIImage
            cell.ImageSpinner.endRefreshing()
            //cell.centerIndicator.stopAnimating()
            
        }
        cell.imgPostFeed.contentMode = .scaleAspectFill
        cell.imgPostFeed.clipsToBounds = true
        cell.imgPostFeed.layer.borderWidth=1.0
        cell.imgPostFeed.layer.cornerRadius=5
        cell.imgPostFeed.clipsToBounds=true
        cell.imgPostFeed.layer.borderColor=UIColor.red.cgColor
        cell.btnClose.tag=indexPath.item
        cell.btnClose.addTarget(self, action: #selector(self.btnDeleteAction(btn:)), for: .touchUpInside)
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        //return CGSize(width: 85.0, height: 85.0)
        
        return CGSize(width: (self.collectionPostFeed.frame.size.width - 20)/4, height: (self.collectionPostFeed.frame.size.width - 20)/4)

    }
    
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath)
    {
        //self.tabBarController?.tabBar.isHidden = true
        let img_dict = self.data_arr[indexPath.item]
        if(img_dict["media_type"] as! String == "image")
        {
            
            var images = [SKPhotoProtocol]()
            
            
            let photo = SKPhoto.photoWithImage(UIImage(data: img_dict["data_str"] as! Data)!)
            photo.shouldCachePhotoURLImage = false // you can use image cache by true(NSCache)
            photo.isVideo = false
            images.append(photo)
            
            let browser = SKPhotoBrowser(photos: images)
            present(browser, animated: true, completion: {})
            
//            let photoBrowser: SYPhotoBrowser = SYPhotoBrowser(imageSourceArray: [img_dict], caption: "", delegate: self)
//            photoBrowser.initialPageIndex = 0
//            self.present(photoBrowser, animated: true, completion: nil)
        }
        else
        {
            let player = AVPlayer(url: img_dict["URLPath"] as! URL)
            let vc = AVPlayerViewController()
            vc.player = player
            
            present(vc, animated: true) {
                vc.player?.play()
            }
        }
        
    }
}

extension HomeViewController: TatsiPickerViewControllerDelegate
{
    
    //MARK:- TatsiPickerViewControllerDelegate
    func pickerViewController(_ pickerViewController: TatsiPickerViewController, didPickAssets assets: [PHAsset])
    {
        let status = PHPhotoLibrary.authorizationStatus()

        for asset in assets
        {
            if(asset.mediaType == .video)
            {
                PHImageManager.default().requestAVAsset(forVideo: asset, options: nil, resultHandler: { (asset, mix, nil) in
                    let myAsset = asset as? AVURLAsset
                    do
                    {
                        let videoData = try Data(contentsOf: (myAsset?.url)!)
                        var dataDict = [String:Any]()
                        dataDict["media_type"]  = "video"
                        dataDict["data_str"]  = videoData
                        dataDict["Preview"] = self.getThumbnailImage(forUrl: (myAsset?.url)!)
                        dataDict["URLPath"] = (myAsset?.url)!
                        
                        if self.data_arr.count < 5
                        {
                            self.data_arr.append(dataDict)
                        }
                        else
                        {
                            self.view.makeToast("Maximum five images/videos you can upload at a time")
                        }
                    }
                    catch
                    {
                        print("exception catch at block - while uploading video")
                    }
                })
            }
            else
            {
                let manager = PHImageManager.default()
                let options = PHImageRequestOptions()
                options.version = .original
                options.isSynchronous = true
                manager.requestImageData(for: asset, options: options)
                { data, _, _, _ in
                    if let imgdata = data
                    {
                        var dataDict = [String:Any]()
                        dataDict["media_type"]  = "image"
                        dataDict["data_str"]  = imgdata
                        //dataDict["Preview"] = UIImage(named: "buzz")
                        dataDict["Preview"] = UIImage(data: imgdata)
                        dataDict["URLPath"] = ""
                        if self.data_arr.count < 5
                        {
                            self.data_arr.append(dataDict)
                        }
                        else
                        {
                            self.view.makeToast("Maximum five images/videos you can upload at a time")
                        }
            
                    }
                }
            }
        }
        print(self.data_arr)
        pickerViewController.dismiss(animated: true, completion: {
        
                self.collectionPostFeed.reloadData()
                
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.1, execute: {
                    self.setupPostData()
                })
        })
    }
    func getThumbnailFrom(path: URL,index : Int) -> UIImage? {
        
        do {
            
            let asset = AVURLAsset(url: path , options: nil)
            let imgGenerator = AVAssetImageGenerator(asset: asset)
            imgGenerator.appliesPreferredTrackTransform = true
            let cgImage = try imgGenerator.copyCGImage(at: CMTimeMake(0, 1), actualTime: nil)
            let thumbnail = UIImage(cgImage: cgImage)
            
            return thumbnail
            
        } catch let error {
            
            print("*** Error generating thumbnail: \(error.localizedDescription)")
            return UIImage()
            
        }
        
    }
    
}
extension HomeViewController : UITabBarControllerDelegate{
    
    func tabBarController(_ tabBarController: UITabBarController, shouldSelect viewController: UIViewController) -> Bool {
        print(viewController)
        if viewController.childViewControllers.contains(self){
            self.getPost()
            if arrPostList.count > 0 {
            self.tblFeeds.scrollToRow(at: IndexPath.init(row: 0, section: 0), at: UITableViewScrollPosition.bottom, animated: true)
            }
        }
        return true
    }
    func tabBarController(_ tabBarController: UITabBarController, didSelect viewController: UIViewController) {
       print(viewController)
    }
}
