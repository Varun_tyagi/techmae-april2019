//
//  LoginViewController.swift
//  Techmea
//
//  Created by VISHAL SETH on 8/4/18.
//  Copyright © 2018 VISHAL SETH. All rights reserved.
//

import UIKit
import FacebookCore
import FacebookLogin
import MBProgressHUD
import GoogleSignIn
import Alamofire
import TwitterKit
import Toast_Swift
import LinkedinSwift
import OAuthSwift


struct Platform {
    static let isSimulator: Bool = {
        var isSim = false
        #if arch(i386) || arch(x86_64)
        isSim = true
        #endif
        return isSim
    }()
}


class LoginViewController: UIViewController,UITextFieldDelegate,GIDSignInDelegate ,GIDSignInUIDelegate{

    //MARK:- Outlets
    @IBOutlet weak var viewPassword: UIView!
    @IBOutlet weak var viewEmail: UIView!
    @IBOutlet weak var btnLogin: ScaleButton!
    @IBOutlet weak var txtEmailID: UITextField!
    @IBOutlet weak var txtPassword: UIShowHideTextField!
    @IBOutlet weak var view_instagram: UIView!
    
    
    @IBOutlet weak var btnTwitter: ScaleButton!
    @IBOutlet weak var btnGoogle: ScaleButton!
    @IBOutlet weak var btnFacebook: ScaleButton!
    @IBOutlet weak var btnLinkedIn: ScaleButton!
    
    @IBOutlet weak var webview_instagram: UIWebView!
    var login_user_dict = [String:Any]()
    var login_type = String()
    private let linkedinHelper = LinkedinSwiftHelper(configuration: LinkedinSwiftConfiguration(clientId: "7819kal4nv9tsa", clientSecret: "pZ9x1IWmVAXCRNfc", state: "linkedin\(Int(NSDate().timeIntervalSince1970))", permissions: ["r_liteprofile", "r_emailaddress"], redirectUrl: "https://techmae.com.oauth/oauth"))

    var oauthswift: OAuthSwift?
    
    //MARK:- viewDidLoad
    override func viewDidLoad()
    {
        super.viewDidLoad()
        viewEmail.setBorder()
        viewPassword.setBorder()
        btnLogin.cornorRadius()
        btnLogin.setCornerRadius(22.0)
        self.navigationItem.title="Login"
        
        if (Platform.isSimulator)
        {
            txtEmailID.text = "techmae100@gmail.com"
            txtPassword.text = "aaaaaa"
        }
    }

   //MARK:- Buttons Resend Password Action
    @IBAction func resetPasswordAction(_ sender: Any) {
        
        let storyBoard = UIStoryboard(name: "Main", bundle: nil)
        let objForgotPasswordViewController = storyBoard.instantiateViewController(withIdentifier: "ForgotPasswordViewController") as! ForgotPasswordViewController
        self.navigationController?.pushViewController(objForgotPasswordViewController, animated: true)
    }
    
    //MARK:- Button Signup Actions
    @IBAction func signupAction(_ sender: UIButton)
    {
        let signupVC = Constants.mainStoryboard2.instantiateViewController(withIdentifier: "SignupFirstViewController") as! SignupFirstViewController
        self.navigationController?.pushViewController(signupVC, animated: true)
    }
    
    //MARK:- Button Login Action
    @IBAction func btnLoginAction(_ sender: ScaleButton)
    {
        
        if(CommonFunction.isInternetAvailable())
        {
        if self.validate()
        {
            MBProgressHUD.showAdded(to: self.view, animated: true)
            
            var params = [String:String]()
            params.updateValue("", forKey: "userType")
            params.updateValue("sbfvdsfvsdhjsd", forKey: "deviceId")
            params.updateValue(txtEmailID.text!, forKey: "email")
            params.updateValue("ios", forKey: "deviceType")
            params.updateValue(txtPassword.text!, forKey: "password")
            
            Alamofire.request(Constants.BASEURL+MethodName.login , method: .post, parameters: params, encoding: JSONEncoding.default, headers: auth_header).responseJSON { response in
                
                
                MBProgressHUD.hide(for: self.view, animated: true)
                if let result_dict = response.result.value as? [String:Any]
                {
                    if let code_val = result_dict["code"] as? Int
                    {
                        if(code_val == 200)
                        {
                            if let userDict = result_dict["data"] as? [String:Any]
                            {
                                print(userDict)
                                UserDefaults.standard.set(userDict["userId"], forKey: Constants.USERID)
                                let userData: Data = NSKeyedArchiver.archivedData(withRootObject: userDict)
                                UserDefaults.standard.set(userData, forKey: "userData")
                                
                                UserDefaults.standard.set(userDict["auth_key"], forKey: "Access_Token")
                                UserDefaults.standard.synchronize()
                                if let user_verify = userDict["is_identity_verify"] as? Bool
                                {
                                    if(user_verify)
                                    {
                                        UserDefaults.standard.set(true, forKey: "is_login")
//                                        let vc = Constants.mainStoryboard.instantiateViewController(withIdentifier: "FindFriendViewController") as! FindFriendViewController
                                        
                                        let vc = self.storyboard?.instantiateViewController(withIdentifier: "MainTabbarViewController")as! MainTabbarViewController
                                        
                                        vc.modalPresentationStyle = .fullScreen
                                        if let quickBlox_dict = userDict["quickblox_data"] as? [String:Any]
                                        {
                                            let QuickBlData: Data = NSKeyedArchiver.archivedData(withRootObject: quickBlox_dict)
                                            UserDefaults.standard.set(QuickBlData, forKey: "quickBloxData")
                                        }
                                        self.navigationController?.present(vc, animated: true, completion: nil)
                                    }
                                    else
                                    {
                                        let vc = Constants.mainStoryboard.instantiateViewController(withIdentifier: "FaceRecognizerViewController") as! FaceRecognizerViewController
                                        vc.already_added = true
                                        self.navigationController?.pushViewController(vc, animated: true)
                                    }
                                }
                            }
                            
                        }
                        else
                        {
                            if let error_arr = result_dict["errorData"] as? [[String:Any]]
                            {
                                if let error_msg = error_arr[0]["message"] as? String
                                {
                                    self.view.makeToast(error_msg)
                                }
                            }
                        }
                    }
                }
            }
            
           
        }
        }
        else
        {
            self.view.makeToast("Please Check Your Internet Connection!!")
        }
        
    }
    
    
    //MARK:- Button Facebook Ligin Action
    @IBAction func btnFacebookLoginAction(_ sender: ScaleButton)
    {
        let loginManager = LoginManager()

        loginManager.logIn(readPermissions: [ReadPermission.email,ReadPermission.publicProfile], viewController: self) { (loginResult) in
            switch loginResult
            {
            case .failed(let error):
                print(error.localizedDescription)
            case .cancelled:
                print("User Cancelled")
                
            case .success(grantedPermissions: _, declinedPermissions: _, token: _):
                let connection = GraphRequestConnection()
                connection.add(GraphRequest(graphPath: "/me", parameters: ["fields": "id, name, first_name, last_name, email"]))
                { httpResponse, result in
                    switch result
                    {
                    case .success(let response):
                        print("Graph Request Succeeded: \(response)")
                        
                        if let responseDictionary = response.dictionaryValue
                        {
                            print(responseDictionary)
                            guard let email=responseDictionary["email"]
                                else
                            {
                                self.view.makeToast("Email not linked with this id.")
                                return
                                
                            }
                            
                            self.login_user_dict = responseDictionary
                            
                            self.login_user_dict.removeAll()
                            self.login_user_dict ["name"] = responseDictionary["name"] as? String ?? ""
                            self.login_user_dict ["first_name"] = responseDictionary["first_name"] as? String ?? ""
                            self.login_user_dict ["last_name"] = responseDictionary["last_name"] as? String ?? ""
                            self.login_user_dict ["email"] = responseDictionary["email"] as? String ?? ""
                            self.login_user_dict ["id"] = responseDictionary["id"] as? String ?? ""
                            
                            self.login_type = "facebook"
                            
                            if let emailadd = email as? String
                            {
                                self.get_user_info(email_id: emailadd )
                            }
                            
                            
                        }
                    // send response to server
                    case .failed(let error):
                        print("Graph Request Failed: \(error)")
                    }
                }
                connection.start()
                
            }
            }
        }
    
    
    //MARK:- Get User Info Api
    func get_user_info(email_id:String)
    {
        if(CommonFunction.isInternetAvailable())
        {

        MBProgressHUD.showAdded(to: self.view, animated: true)
            
            let params = ["email":email_id] //email_id
            //let params = ["email": "malhotrasunaina9@yahoo.co.in"]
            //let params = ["email": "techanswers823@gmail.com"]
        
        Alamofire.request(Constants.BASEURL + MethodName.getUserInfo , method: .post, parameters: params, encoding: JSONEncoding.default, headers: auth_header).responseJSON { response in
            print(response.result.value!)
            MBProgressHUD.hide(for: self.view, animated: true)
            if let result_dict = response.result.value as? [String:Any]
            {
                if let status_val = result_dict["code"] as? Int
                {
                    if(status_val == 200)
                    {
                        if let userDict = result_dict ["data"] as? [String:Any]
                        {
                            if(userDict["message"] as? String) != nil
                            {
                                print()
                                let vc = Constants.mainStoryboard2.instantiateViewController(withIdentifier: "SignupFirstViewController")as! SignupFirstViewController
                                vc.login_user_dict = self.login_user_dict
                                vc.socialLoginType = self.login_type
                                if let login_user_id = self.login_user_dict["id"] as? String
                                {
                                    vc.socialId = login_user_id
                                }
                                self.navigationController?.pushViewController(vc, animated: true)
                            }
                            else
                            {
                                if let userDict = result_dict["data"] as? [String:Any]
                                {
                                    if let user_verify = userDict["is_identity_verify"] as? Bool
                                    {
                                        if(user_verify)
                                        {
                                            UserDefaults.standard.set(true, forKey: "is_login")
                                            let userData: Data = NSKeyedArchiver.archivedData(withRootObject: userDict)
                                            UserDefaults.standard.set(userData, forKey: "userData")
                                            UserDefaults.standard.set(userDict["userId"], forKey: Constants.USERID)
                                            UserDefaults.standard.set(userDict["auth_key"], forKey: "Access_Token")
                                            
                                            UserDefaults.standard.synchronize()
//                                            let vc = Constants.mainStoryboard.instantiateViewController(withIdentifier: "FindFriendViewController") as! FindFriendViewController
                                            
                                            let vc = self.storyboard?.instantiateViewController(withIdentifier: "MainTabbarViewController")as! MainTabbarViewController
                                            
                                            
                                            if let quickBlox_dict = userDict["quickblox_data"] as? [String:Any]
                                            {
                                                let QuickBlData: Data = NSKeyedArchiver.archivedData(withRootObject: quickBlox_dict)
                                                UserDefaults.standard.set(QuickBlData, forKey: "quickBloxData")
                                            }
                                            self.present(vc, animated: true, completion: nil)
                                        }
                                        else
                                        {
                                            
                                            let userData: Data = NSKeyedArchiver.archivedData(withRootObject: userDict)
                                            UserDefaults.standard.set(userData, forKey: "userData")
                                            UserDefaults.standard.set(userDict["userId"], forKey: Constants.USERID)
                                            UserDefaults.standard.set(userDict["auth_key"], forKey: "Access_Token")
                                            
                                            UserDefaults.standard.synchronize()
                                            let vc = Constants.mainStoryboard.instantiateViewController(withIdentifier: "FaceRecognizerViewController") as! FaceRecognizerViewController
                                            vc.already_added = true
                                            self.navigationController?.pushViewController(vc, animated: true)
                                        }
                                    }
                                }
                            }
                        }
                    }
                    else
                    {
                        if let error_arr = result_dict["errorData"] as? [[String:Any]]
                        {
                            if let error_msg = error_arr[0]["message"] as? String
                            {
                                self.view.makeToast(error_msg)
                            }
                        }
                    }
                }
            }
        }
        }
        else
        {
            self.view.makeToast("Please Check Your Internet Connection!!")
        }
    }
    
    
    //MARK:- Button Insta Cancel Action
    @IBAction func btnInstaCancelAction(_ sender: UIButton)
    {
        view_instagram.isHidden = true
    }
    
    //MARK:- Button Insta Login Action
    @IBAction func btnInstaAction(_ sender: ScaleButton)
    {
        TWTRTwitter.sharedInstance().logIn(completion: { (session, error) in
            if (session != nil) {
                print("signed in as \(String(describing: session?.userName))");
                
                let client = TWTRAPIClient.withCurrentUser()
                client.requestEmail
                {
                    email, error in
                    if (email != nil)
                    {
                        client.loadUser(withID: (session?.userID)!, completion:
                        {
                            (user, error) in
                            
                            if (user != nil)
                            {
                                self.login_user_dict.removeAll()
                                
                                self.login_user_dict ["name"] = "\(String(describing: user?.name))"
                                
                                let septatedName = String(describing: user?.name).components(separatedBy: " ")
                                
                                if(septatedName.count > 0 )
                                {
                                    self.login_user_dict ["first_name"] = septatedName[0]
                                }
                                
                                if(septatedName.count > 1 )
                                {
                                    self.login_user_dict ["last_name"] = septatedName[1]
                                }
                                
                                self.login_user_dict ["email"] = email
                                self.login_user_dict ["id"] = "\(String(describing: user?.userID))"
                                self.login_type = "twitter"
                                
                                if email != ""
                                {
                                    self.get_user_info(email_id: email ?? "")
                                }
                            }
                            else
                            {
                                self.view.makeToast("Sorry! This account will not work, please with some another account.")
                            }
                        })
                    }
                    else
                    {
                        self.view.makeToast("Email not linked with this id.")
                        print("error: \(String(describing: error?.localizedDescription))");
                    }
                }
            } else {
                print("error: \(String(describing: error?.localizedDescription))");
            }
        })
    }
    
    
    //MARK:- Button Google Login Action
    @IBAction func btnGooglePlusAction(_ sender: ScaleButton)
    {
        GIDSignIn.sharedInstance().signOut()
        GIDSignIn.sharedInstance().delegate=self
        GIDSignIn.sharedInstance().uiDelegate=self
        GIDSignIn.sharedInstance().signIn()
    }
    
    
    //MARK:- Button LinkedIn Login Action
    @IBAction func btnLinkedInAction(_ sender: ScaleButton)
    {
    
        linkedinHelper.authorizeSuccess({ (token) in

            //This token is useful for fetching profile info from LinkedIn server

          //  self.linkedinHelper.requestURL("https://api.linkedin.com/v1/people/~:(id,first-name,last-name,email-address,picture-url,picture-urls::(original),positions,date-of-birth,phone-numbers,location)?format=json", requestType: LinkedinSwiftRequestGet, success: { (response) -> Void in

//            self.linkedinHelper.requestURL("https://api.linkedin.com/v2/emailAddress?q=members&projection=(elements*(handle~))", requestType: LinkedinSwiftRequestGet, success: { (response) -> Void in


//                if let dataarr =  response.jsonObject["elements"] as? [[String:Any]]
//                {
//                    if let dataDict = dataarr[0] as? [String:Any]
//                    {
//                        if let emailDict = dataDict["handle~"] as? [String:Any]
//                        {
//                            if let email = emailDict["emailAddress"] as? String
//                            {
                                self.linkedinHelper.requestURL("https://api.linkedin.com/v1/people/~:(id,first-name,last-name,email-address,picture-url,picture-urls::(original),positions,date-of-birth,phone-numbers,location)?format=json", requestType: LinkedinSwiftRequestGet, success: { (response) -> Void in


                                    if let emailID =  response.jsonObject["emailAddress"] as? String
                                    {
                                        print(response)
                                        self.login_user_dict.removeAll()

                                        if let firstName = response.jsonObject["firstName"] as? String
                                        {
                                            self.login_user_dict ["first_name"] = firstName
                                        }

                                        if let last_name = response.jsonObject["lastName"] as? String
                                        {
                                            self.login_user_dict ["last_name"] = last_name
                                        }

                                        if let email = response.jsonObject["emailAddress"] as? String
                                        {
                                            self.login_user_dict ["email"] = email
                                        }

                                        if let id = response.jsonObject["id"]
                                        {
                                            self.login_user_dict ["id"] = id
                                        }
                                        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
                                            self.get_user_info(email_id: emailID)
                                        }
                                        self.login_type = "Linked"
                                    }
                                    else
                                    {
                                        self.view.makeToast("Email not linked with this id.")
                                    }

                                }) {(error) -> Void in

                                    print(error.localizedDescription)
                                }
//                            }
//                            else
//                            {
//                                self.view.makeToast("Email not linked with this id.")
//                            }
                        //}
                    //}
                //}




//            }) {(error) -> Void in
//
//                print(error.localizedDescription)
//            }


        }, error: { (error) in

            print(error.localizedDescription)
        })
        
        

       
    
//        let oauthswift = OAuth2Swift(
//            consumerKey:    "7819kal4nv9tsa",
//            consumerSecret: "pZ9x1IWmVAXCRNfc",
//            authorizeUrl:   "https://www.linkedin.com/uas/oauth2/authorization",
//            accessTokenUrl: "https://www.linkedin.com/uas/oauth2/accessToken",
//            responseType:   "code"
//        )
//        self.oauthswift = oauthswift
//        oauthswift.authorizeURLHandler = OAuthSwiftOpenURLExternally.sharedInstance//getURLHandler()
//        let state = generateState(withLength: 20)
//        let _ = oauthswift.authorize(
//            withCallbackURL: URL(string: "https://techmae.com.oauth/oauth")!, scope: "", state: state,
//            success: { credential, response, parameters in
//                //self.showTokenAlert(name: serviceParameters["name"], credential: credential)
//                self.testLinkedin2(oauthswift)
//
//        },
//            failure: { error in
//                print(error.description)
//        }
//        )
    }
    
    func testLinkedin2(_ oauthswift: OAuth2Swift) {
        let _ = oauthswift.client.get(
            "https://api.linkedin.com/v1/people/~?format=json", parameters: [:],
            success: { response in
                let dataString = response.string!
                print(dataString)
        },
            failure: { error in
                print(error)
        }
        )
    }
    
    
    
    //MARK:- Google SignIn Delegate
    func sign(_ signIn: GIDSignIn!, present viewController: UIViewController!) {
        self.present(viewController, animated: true, completion: nil)
    }
    
    func sign(_ signIn: GIDSignIn!, dismiss viewController: UIViewController!) {
        self.dismiss(animated: true, completion: nil)
    }
    
    func sign(_ signIn: GIDSignIn!, didSignInFor user: GIDGoogleUser!, withError error: Error!)
    {
        if (error == nil)
        {
            if(user.profile.email != nil)
            {
                login_user_dict.removeAll()
                self.login_type = "google"
                login_user_dict["name"] = user.profile.name ?? ""
                login_user_dict["first_name"] = user.profile.givenName ?? ""
                login_user_dict["last_name"] = user.profile.familyName ?? ""
                login_user_dict["email"] = user.profile.email ?? ""
                login_user_dict["id"] = user.userID ?? ""
                self.get_user_info(email_id: user.profile.email ?? "")
            }
            else
            {
                //self.displayAlert(msg: "Email not linked with this id.", title_str: Constants.APP_NAME)
                self.view.makeToast("Email not linked with this id.")
            }
        }
        else
        {
            print("\(error.localizedDescription)")
        }
    }

    //MARK:- Social Login Api
    func socialLogin(socialID:String,emailID:String,type:String)
    {
        
        //"facebook/google/insta/linkedin"
        var params = [String:String]()
        params.updateValue(type, forKey: "loginType")
        params.updateValue("sfadgfsgdfsgsfgg", forKey: "deviceId")
        params.updateValue("ios", forKey: "deviceType")
        params.updateValue(emailID, forKey: "email")
        params.updateValue(socialID, forKey: "socialId")
        MBProgressHUD.showAdded(to: self.view, animated: true)
        TMApiHelper.apiCall(serviceName: Constants.BASEURL+MethodName.socialLogin, param: params) { (response, error) in
            print(response ?? "")
            let code=response!["code"] as! Int
            let msg=response!["error"] as?  String
            DispatchQueue.main.async {
            MBProgressHUD.hide(for: self.view, animated: true)
            if code==500
            {
                PKSAlertController.alert(Constants.APP_NAME, message:msg ?? "Some error has occured! Please try again")
            }
            else
            {
               if let userDetailsDict:NSDictionary = response!["userDetails"] as?
                NSDictionary
               {
                
                var userDict = [String:String]()
                
                for key in userDetailsDict.allKeys
                {
                    if let userDetailsDictKey = key as? String
                    {
                        if userDetailsDict.value(forKey: userDetailsDictKey) is NSNull || userDetailsDict.value(forKey: userDetailsDictKey) == nil
                        {
                            userDict.updateValue("", forKey: userDetailsDictKey)
                        }
                        else
                        {
                            if userDetailsDict.value(forKey: userDetailsDictKey) is String
                            {
                                if let v = userDetailsDict.value(forKey: userDetailsDictKey) as? String
                                {
                                    userDict.updateValue(v, forKey: userDetailsDictKey)
                                }
                            }
                            else
                            {
                                if let intValue:Int = userDetailsDict.value(forKey: userDetailsDictKey) as? Int
                                {
                                    userDict.updateValue(String(intValue), forKey: userDetailsDictKey)
                                }
                                
                            }
                        }
                    }
                }
                UserDefaults.standard.set(userDict["userId"], forKey: Constants.USERID)
                UserDefaults.standard.set(userDict, forKey: "userData")
                UserDefaults.standard.synchronize()
                }
                
//                let vc = Constants.mainStoryboard.instantiateViewController(withIdentifier: "FindFriendViewController")as! FindFriendViewController
//                self.navigationController?.pushViewController(vc, animated: true)
                
                let vc = self.storyboard?.instantiateViewController(withIdentifier: "MainTabbarViewController")as! MainTabbarViewController
                self.navigationController?.present(vc, animated: true, completion: nil)
                }
            }
        }
    }
    
    
    //MARK:- Button Signup Action
    @IBAction func btnSignUpAction(_ sender: Any)
    {
        let vc = Constants.mainStoryboard2.instantiateViewController(withIdentifier: "SignupFirstViewController")as! SignupFirstViewController
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
}

extension LoginViewController
{
    //MARK:- Validation TextFields
    fileprivate  func validate() -> Bool  {
         if (txtEmailID.text?.isEmpty)!{
            txtEmailID.becomeFirstResponder()
            
            PKSAlertController.alert(Constants.APP_NAME, message: AlertConstant.EnterEmail)
            return false
        }
        else if !Helper.isValidEmail(txtEmailID.text!) {
            txtEmailID.becomeFirstResponder()
            
            PKSAlertController.alert(Constants.APP_NAME, message: AlertConstant.EnterValidEmail)
            return false
        }
        else if (txtPassword.text?.isEmpty)!{
            txtPassword.becomeFirstResponder()
            PKSAlertController.alert(Constants.APP_NAME, message: AlertConstant.EnterPassword)
            return false
        }
        

        return true
    }
}


