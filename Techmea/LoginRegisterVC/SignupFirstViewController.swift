

import UIKit
import GoogleSignIn
import FacebookLogin
import FacebookCore
import Alamofire
import MBProgressHUD
import TwitterKit
import Toast_Swift
import LinkedinSwift


class SignupFirstViewController: UIViewController,GIDSignInDelegate ,GIDSignInUIDelegate {
   
    //MARK:- Outlets
    let linkedinHelper = LinkedinSwiftHelper(configuration: LinkedinSwiftConfiguration(clientId: "7819kal4nv9tsa", clientSecret: "pZ9x1IWmVAXCRNfc", state: "linkedin\(Int(NSDate().timeIntervalSince1970))", permissions: ["r_basicprofile", "r_emailaddress"], redirectUrl: "https://techmae.com.oauth/oauth"))
    @IBOutlet weak var viewCoach: UIView!
    @IBOutlet weak var viewFirstName: UIView!
    @IBOutlet weak var viewEmailid: UIView!
    @IBOutlet weak var viewPassword: UIView!
    @IBOutlet weak var viewBirthday: UIView!
    @IBOutlet weak var viewHourlyRate: UIView!
    @IBOutlet weak var btnNext: UIButton!
    
    @IBOutlet weak var lastNameTxt: UITextField!
    @IBOutlet weak var viewLastName: UIView!
    @IBOutlet weak var userTypeTxt: CustomPickerView!
    
    @IBOutlet weak var birthdayTxt: VSTextField!
    @IBOutlet weak var passwordTxt: UIShowHideTextField!
    @IBOutlet weak var emailTxt: UITextField!
    @IBOutlet weak var firstNameTxt: UITextField!
    @IBOutlet weak var hourlyRateTxt: UITextField!
    
    @IBOutlet weak var emailview_top_constraint: NSLayoutConstraint!
    @IBOutlet weak var passwordview_top_constraint: NSLayoutConstraint!
    @IBOutlet weak var passwrdview_height_constarint: NSLayoutConstraint!
    @IBOutlet weak var emailview_height_constraint: NSLayoutConstraint!
    @IBOutlet weak var hourlyrate_height_constraint: NSLayoutConstraint!
    @IBOutlet weak var btnRadio_top_constraint: NSLayoutConstraint!
    var login_user_dict = [String:Any]()
    var socialLoginType = ""
    var socialId = ""
    
    
    //MARK:- viewDidLoad
    override func viewDidLoad() {
        super.viewDidLoad()
        userTypeTxt.text = "User"
        setupView()
        if(login_user_dict.count>0)
        {
            hide_Email_password()
        }
        else
        {
            socialLoginType = "email"
        }
        
        hourlyrate_height_constraint.constant = 0
        btnRadio_top_constraint.constant = 0
        if let fname = self.login_user_dict["first_name"] as? String
        {
            firstNameTxt.text = fname
        }
        if let lname = self.login_user_dict["last_name"] as? String
        {
            lastNameTxt.text = lname
        }
        if let email_str = self.login_user_dict["email"] as? String
        {
            emailTxt.text = email_str
        }
        if let id_str = self.login_user_dict["id"] as? String
        {
            self.socialId = id_str
        }
        self.birthdayTxt.formatting = .socialSecurityNumber

    }
    
    //MARK:- HideEmailPassword
    func hide_Email_password()
    {
        passwordview_top_constraint.constant = 0
        passwrdview_height_constarint.constant = 0
        emailview_height_constraint.constant = 0
        emailview_top_constraint.constant = 0
    }
    
    
    //MARK:- Button Already Registered Action
    @IBAction func btnAlreadyAction(_ sender: Any)
    {
        let vc = Constants.mainStoryboard.instantiateViewController(withIdentifier: StoryBoardName.login)as! LoginViewController
        
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    
    //MARK:- Button Next Action
    @IBAction func btnNextAction(_ sender: Any)
    {
        if validate()
        {
        
            var bday = birthdayTxt.text ?? ""
            if birthdayTxt.text.count > 0 {
            bday.insert("/", at: bday.index(bday.startIndex, offsetBy: 2))
            bday.insert("/", at: bday.index(bday.startIndex, offsetBy: 5))
            }
            var params1VC = [String:String]()
            if(userTypeTxt.text! == "Expert")
            {
                params1VC.updateValue("coach", forKey: "userType")
                //params1VC.updateValue(hourlyRateTxt.text!, forKey: "hourly_rate")
                 params1VC.updateValue("0", forKey: "hourly_rate")
            }
            else
            {
                params1VC.updateValue((userTypeTxt.text?.lowercased())!, forKey: "userType")
            }
            params1VC.updateValue(firstNameTxt.text!, forKey: "first_name")
            params1VC.updateValue(lastNameTxt.text!, forKey: "last_name")
            params1VC.updateValue(emailTxt.text!, forKey: "email")
            //params1VC.updateValue("techmae215@mailinator.com", forKey: "email")
            params1VC.updateValue(passwordTxt.text!, forKey: "password")
            params1VC.updateValue(bday, forKey: "dob")
            
            let vc = Constants.mainStoryboard2.instantiateViewController(withIdentifier: StoryBoardName.signupSecond)as! SignupSecondViewController
            vc.params2VC=params1VC
            vc.socialId = self.socialId
            vc.loginType = self.socialLoginType
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    
    //MARK:- Button Insta Action
    @IBAction func btnInstaAction(_ sender: Any) {
        TWTRTwitter.sharedInstance().logIn(completion: { (session, error) in
            if (session != nil) {
                print("signed in as \(String(describing: session?.userName))");
                
                let client = TWTRAPIClient.withCurrentUser()
                
                client.requestEmail { email, error in
                    if (email != nil) {
                        client.loadUser(withID: (session?.userID)!, completion:
                            {
                                (user, error) in
                                if (user != nil)
                                {
                                    self.hide_Email_password()
                                    self.login_user_dict.removeAll()
                                    self.login_user_dict ["name"] = "\(String(describing: user?.name))"
                                    let septatedName = String(describing: user!.name).components(separatedBy: " ")
                                    if(septatedName.count > 0 )
                                    {
                                        self.login_user_dict ["first_name"] = septatedName[0]
                                    }
                                    
                                    if(septatedName.count > 1 )
                                    {
                                        self.login_user_dict ["last_name"] = septatedName[1]
                                    }
                                    self.login_user_dict ["email"] = email
                                    self.login_user_dict ["id"] = "\(String(describing: user?.userID))"
                                    self.socialLoginType = "twitter"
                                    
                                    if email != ""
                                    {
                                        self.get_user_info(email_id: email ?? "")
                                    }
                                }
                                else
                                {
                                    //self.displayAlert(msg: "Sorry! This account will not work, please with some another account.", title_str: Constants.APP_NAME)
                                    self.view.makeToast("Sorry! This account will not work, please with some another account.")
                                }
                        })
                        
                    } else {
                        //self.displayAlert(msg: "Email not linked with this id.", title_str: Constants.APP_NAME)
                        self.view.makeToast("Email not linked with this id.")
                        print("error: \(String(describing: error?.localizedDescription))");
                    }
                }
                
            } else {
                print("error: \(String(describing: error?.localizedDescription))");
            }
        })
    }
    
    //MARK:- Button Google Action
    @IBAction func btnGooglePlusAction(_ sender: Any)
    {
        GIDSignIn.sharedInstance().signOut()
        GIDSignIn.sharedInstance().delegate=self
        GIDSignIn.sharedInstance().uiDelegate=self
        GIDSignIn.sharedInstance().shouldFetchBasicProfile = true
        GIDSignIn.sharedInstance().signIn()
    }
    
    //MARK:- Button Facebook Action
    @IBAction func btnFacbookAction(_ sender: Any) {
        
        let loginManager = LoginManager()
        
        loginManager.logIn(readPermissions: [ReadPermission.email,ReadPermission.publicProfile,ReadPermission.userBirthday], viewController: self) { (loginResult) in
            switch loginResult
            {
            case .failed(let error):
                print(error.localizedDescription)
            case .cancelled:
                print("User Cancelled")
                
            case .success(grantedPermissions: _, declinedPermissions: _, token: _):
                
                let params = ["fields": "id, name, first_name, last_name, email"]
                let graphRequest = GraphRequest(graphPath: "me", parameters: params)
                graphRequest.start {
                    (urlResponse, requestResult) in
                    
                    switch requestResult {
                    case .failed(let error):
                        print("error in graph request:", error)
                        break
                    case .success(let graphResponse):
                        if let responseDictionary = graphResponse.dictionaryValue {
                            
                            guard responseDictionary["email"] != nil
                                else
                            {
                                PKSAlertController.alert(Constants.APP_NAME, message: "Email not linked with this id.")
                                return
                            }
                            
                            self.socialLoginType = "facebook"
                            self.hide_Email_password()
                            self.login_user_dict.removeAll()
                            self.login_user_dict ["name"] = responseDictionary["name"] as? String ?? ""
                            self.login_user_dict ["first_name"] = responseDictionary["first_name"] as? String ?? ""
                            self.login_user_dict ["last_name"] = responseDictionary["last_name"] as? String ?? ""
                            self.login_user_dict ["email"] = responseDictionary["email"] as? String ?? ""
                            self.login_user_dict ["id"] = responseDictionary["id"] as? String ?? ""
                            
                            if let emailadd = responseDictionary["email"] as? String
                            {
                                self.get_user_info(email_id: emailadd)
                            }
                        }
                    }
                }
            }
        }
    }
    
    //MARK:- Button LinkedIn Action
    @IBAction func btnLinkedInAction(_ sender: Any)
    {
        linkedinHelper.authorizeSuccess({ (token) in

            print(token)
            //This token is useful for fetching profile info from LinkedIn server

            self.linkedinHelper.requestURL("https://api.linkedin.com/v1/people/~:(id,first-name,last-name,email-address,picture-url,picture-urls::(original),positions,date-of-birth,phone-numbers,location)?format=json", requestType: LinkedinSwiftRequestGet, success: { (response) -> Void in

                if (response.jsonObject["emailAddress"] as? String) != nil
                {
                    self.hide_Email_password()
                    self.login_user_dict.removeAll()
                    if let firstName = response.jsonObject["firstName"] as? String
                    {
                        self.login_user_dict ["first_name"] = firstName
                    }

                    if let last_name = response.jsonObject["last_name"] as? String
                    {
                        self.login_user_dict ["last_name"] = last_name
                    }

                    if let email = response.jsonObject["emailAddress"] as? String
                    {
                        self.login_user_dict ["email"] = email
                    }

                    if let id = response.jsonObject["id"]
                    {
                        self.login_user_dict ["id"] = id
                    }

                    self.socialLoginType = "Linked"
                    DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
                        self.get_user_info(email_id: "\(String(describing: response.jsonObject["emailAddress"]!))")
                    }
                }
                else
                {
                    //self.displayAlert(msg: "Email not linked with this id.", title_str: Constants.APP_NAME)
                    self.view.makeToast("Email not linked with this id.")
                    return
                }

            }) {(error) -> Void in

                print(error.localizedDescription)
                //handle the error
            }


        }, error: { (error) in

            print(error.localizedDescription)
            //show respective error
        }) {
            //show sign in cancelled event
        }
    }
    
    //MARK:- Google SignIn Delegate
    func sign(_ signIn: GIDSignIn!, present viewController: UIViewController!) {
        self.present(viewController, animated: true, completion: nil)
    }
    
    func sign(_ signIn: GIDSignIn!, dismiss viewController: UIViewController!) {
        self.dismiss(animated: true, completion: nil)
    }
    
    func sign(_ signIn: GIDSignIn!, didSignInFor user: GIDGoogleUser!, withError error: Error!) {
        if (error == nil) {
            
            let idToken = user.userID ?? "" // Safe to send to the server
            let fullName = user.profile.name ?? ""
            
            let email = user.profile.email ?? ""
            self.hide_Email_password()
            self.socialLoginType = "google"
            login_user_dict.removeAll()
            login_user_dict ["name"] = fullName
            login_user_dict["first_name"] = user.profile.givenName ?? ""
            login_user_dict["last_name"] = user.profile.familyName ?? ""
            login_user_dict ["email"] = email
            login_user_dict ["id"] = idToken
            get_user_info(email_id: email)
            print("Social id = \(self.socialId)")
            
        }
        else
        {
            print("\(error.localizedDescription)")
            self.view.makeToast("\(error.localizedDescription)")
        }
    }
    
    
    //MARK:- Get User Info Api
    func get_user_info(email_id:String)
    {
        
        if(CommonFunction.isInternetAvailable())
        {
            
        MBProgressHUD.showAdded(to: self.view, animated: true)
        let params = ["email":email_id]
        
        Alamofire.request(Constants.BASEURL + MethodName.getUserInfo , method: .post, parameters: params, encoding: JSONEncoding.default, headers: auth_header).responseJSON { response in
            MBProgressHUD.hide(for: self.view, animated: true)
            if let result_dict = response.result.value as? [String:Any]
            {
                if let status_val = result_dict["code"] as? Int
                {
                    if(status_val == 200)
                    {
                        if let userDict = result_dict ["data"] as? [String:Any]
                        {
                            if(userDict["message"] as? String) != nil
                            {
                                if let fname = self.login_user_dict["first_name"] as? String
                                {
                                    self.firstNameTxt.text = fname
                                }
                                
                                if let lname = self.login_user_dict["last_name"] as? String
                                {
                                    self.lastNameTxt.text = lname
                                }
                                if let email = self.login_user_dict["email"] as? String
                                {
                                    self.emailTxt.text = email
                                }
                                
                                if let socialID = self.login_user_dict["id"] as? String
                                {
                                    self.socialId = socialID
                                    self.passwordTxt.text = self.socialId
                                }
                            }
                            else
                            {
                                if let userDict = result_dict["data"] as? [String:Any]
                                {
                                    if let user_verify = userDict["is_identity_verify"] as? Bool
                                    {
                                        if(user_verify)
                                        {
                                            UserDefaults.standard.set(true, forKey: "is_login")
                                            let userData: Data = NSKeyedArchiver.archivedData(withRootObject: userDict)
                                            UserDefaults.standard.set(userData, forKey: "userData")
                                            UserDefaults.standard.set(userDict["userId"], forKey: Constants.USERID)
                                            UserDefaults.standard.set(userDict["auth_key"], forKey: "Access_Token")
                                            UserDefaults.standard.synchronize()
//                                            let vc = Constants.mainStoryboard.instantiateViewController(withIdentifier: "FindFriendViewController") as! FindFriendViewController
//                                            self.navigationController?.pushViewController(vc, animated: true)
                                            
                                            let vc = Constants.mainStoryboard.instantiateViewController(withIdentifier: "MainTabbarViewController")as! MainTabbarViewController
                                            self.navigationController?.present(vc, animated: true, completion: nil)
                                        }
                                        else
                                        {
                                    

                                            let vc = Constants.mainStoryboard.instantiateViewController(withIdentifier: "FaceRecognizerViewController") as! FaceRecognizerViewController
                                            vc.already_added = true
                                            self.navigationController?.pushViewController(vc, animated: true)
                                        }
                                    }
                                }
                            }
                        }
                    }
                    else
                    {
                        if let error_arr = result_dict["errorData"] as? [[String:Any]]
                        {
                            if let error_msg = error_arr[0]["message"] as? String
                            {
                                self.view.makeToast(error_msg)
                            }
                        }
                    }
                }
            }
        }
        }
        else
        {
            self.displayAlert(msg: "Please Check Your Internet Connection!!", title_str: "TechMea")
        }
    }    
}
extension SignupFirstViewController
{
    fileprivate  func setupView()
    {
        viewCoach.setBorder()
        viewFirstName.setBorder()
        viewLastName.setBorder()
        viewEmailid.setBorder()
        viewPassword.setBorder()
        viewBirthday.setBorder()
        viewHourlyRate.setBorder()
        btnNext.cornorRadius()
        //configDOBPicker()
        configUSerPicker()
        if let name_str = login_user_dict["name"] as? String
        {
            firstNameTxt.text = name_str
        }
        if let email_str = login_user_dict["email"] as? String
        {
            emailTxt.text = email_str
        }
    }
    
    
    //MARK:- TextField Validation
    fileprivate  func validate() -> Bool
    {
        if (userTypeTxt.text?.isEmpty)!{
            userTypeTxt.becomeFirstResponder()
            
            PKSAlertController.alert(Constants.APP_NAME, message: AlertConstant.SelectUserType)
            return false
        }
        else if (firstNameTxt.text?.isEmpty)!{
            firstNameTxt.becomeFirstResponder()
            
            PKSAlertController.alert(Constants.APP_NAME, message: AlertConstant.EnterFirstName)
            return false
        }
        else if (lastNameTxt.text?.isEmpty)!{
            lastNameTxt.becomeFirstResponder()
            
            PKSAlertController.alert(Constants.APP_NAME, message: AlertConstant.EnterLastName)
            return false
        }
        else if (emailTxt.text?.isEmpty)!{
            emailTxt.becomeFirstResponder()
            PKSAlertController.alert(Constants.APP_NAME, message: AlertConstant.EnterEmail)
            return false
        }
            
        else if !Helper.isValidEmail(emailTxt.text!){
            emailTxt.becomeFirstResponder()
            PKSAlertController.alert(Constants.APP_NAME, message: AlertConstant.EnterValidEmail)
            return false
        }
        else if (passwordTxt.text?.isEmpty)!
        {
            if(socialLoginType == "email")
            {
                passwordTxt.becomeFirstResponder()
                
                PKSAlertController.alert(Constants.APP_NAME, message: AlertConstant.EnterPassword)
                return false
            }
            
        }
        else if (socialLoginType == "email"){
            
            if (isPasswordValid(passwordTxt.text!) == false)
            {
                passwordTxt.becomeFirstResponder()
                
                PKSAlertController.alert(Constants.APP_NAME, message: AlertConstant.PasswordTooShort)
                return false
            }
        }
//        else if (birthdayTxt.text?.isEmpty)!{
//            birthdayTxt.becomeFirstResponder()
//
//            PKSAlertController.alert(Constants.APP_NAME, message: AlertConstant.EnterBirthday)
//            return false
//        }
    //    if self.userTypeTxt.text == "Expert"
      //  {
//            if (hourlyRateTxt.text?.isEmpty)!{
//                hourlyRateTxt.becomeFirstResponder()
//                
//                PKSAlertController.alert(Constants.APP_NAME, message: AlertConstant.EnterHourate)
//                return false
//            }
      //  }
//        if(!birthdayTxt.text.isEmpty){
//            if(birthdayTxt.text.count<8)
//            {
//                birthdayTxt.becomeFirstResponder()
//                //self.displayAlert(msg: "Please enter valid birthday", title_str: Constants.APP_NAME)
//                self.view.makeToast("Please enter valid birthday")
//                return false
//            }
//        }
        return true
    }
    
    func isPasswordValid(_ password : String) -> Bool{
        let passwordTest = NSPredicate(format: "SELF MATCHES %@", "^(?=.*[A-Z])(?=.*[!@#$&*])(?=.*[0-9])(?=.*[a-z]).{6,12}$")
        return passwordTest.evaluate(with: password)
    }
    private  func configUSerPicker() {
        userTypeTxt.isToopBarNeeded=false
        userTypeTxt.pickerType = CustomPickerType.StringPicker
        let cityArray : [String] = ["User","Expert"]
        userTypeTxt.stringPickerData = cityArray
        userTypeTxt.stringPicker?.selectedRow(inComponent: 0)
        userTypeTxt.stringDidChange = { index in
            self.userTypeTxt.text = "\(cityArray[index])"
            
            if self.userTypeTxt.text == "Expert"
            {
                self.viewHourlyRate.isHidden = false
                //self.hourlyrate_height_constraint.constant = 47.0
                self.hourlyrate_height_constraint.constant = 0
                self.btnRadio_top_constraint.constant = 13
            }
            else
            {
                self.viewHourlyRate.isHidden = true
                self.hourlyrate_height_constraint.constant = 0
                self.btnRadio_top_constraint.constant = 0
            }
        }
    }
}

