//
//  SignupSecondViewController.swift
//  Techmea
//
//  Created by VISHAL SETH on 8/6/18.
//  Copyright © 2018 VISHAL SETH. All rights reserved.
//

import UIKit
import GooglePlaces
import MBProgressHUD
import Alamofire

class SignupSecondViewController: UIViewController,UITextFieldDelegate,TLTagsControlDelegate,UIScrollViewDelegate,UITableViewDelegate,UITableViewDataSource, UIGestureRecognizerDelegate {

    
    //MARK:- Outlets
    @IBOutlet weak var viewAboutme: UIView!
    @IBOutlet weak var viewLocation: UIView!
    @IBOutlet weak var btnNext: UIButton!
    @IBOutlet weak var txtViewAboutMe: RSKPlaceholderTextView!
    @IBOutlet weak var viewProfessions: UIView!
    @IBOutlet weak var txtLocation: UITextField!
    
    @IBOutlet weak var tagsControlProfession: TLTagsControl!
    @IBOutlet weak var tblProfession: UITableView!
    @IBOutlet weak var tblProfessionHeight: NSLayoutConstraint!
    
    var arrProfessions =  NSMutableArray()
    var arrProfesstionsID = [String]()
    var arrProfessionSelected = [NSDictionary]()
    var SortArray =  NSMutableArray()
    
    var socialId = ""
    var loginType = "email"
    
    var params2VC = [String:Any]()
    var city_str = String()
    var country_str = String()
    var pincode_str = String()
    var latitude_val = Double()
    var longitude_val = Double()
    
    //MARK:- viewDidLoad
    override func viewDidLoad()
    {
        super.viewDidLoad()
        getProfessions()
        
        
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(self.showTableView),
            name: NSNotification.Name(rawValue: "showTable"),
            object: nil)
        
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(self.removeTags),
            name: NSNotification.Name(rawValue: "removeTags"),
            object: nil)
    
        
        tagsControlProfession.tagPlaceholder = "Professions"
        tagsControlProfession.placeholderColor = UIColor.white
        tagsControlProfession.placeholderFont = UIFont.systemFont(ofSize: 15.0)
        tagsControlProfession.delegate = self
        tagsControlProfession.mode = .edit
        tagsControlProfession.reloadTagSubviews()
        tagsControlProfession.tagsBackgroundColor = UIColor.white
        tagsControlProfession.tagsTextColor = UIColor.red
        tagsControlProfession.tagsDeleteButtonColor = UIColor.red
        tagsControlProfession.txtfieldTextColor = UIColor.white

        
        tblProfession.tableFooterView = UIView()
        
        viewAboutme.layer.borderWidth=1.0
        viewAboutme.layer.borderColor=UIColor.init(rgb: 0xFFBECC).cgColor
        viewAboutme.layer.cornerRadius=20
        viewLocation.setBorder()
        viewProfessions.setBorder()
        btnNext.cornorRadius()
        print(params2VC)
    }
    
//    override func viewDidLayoutSubviews() {
//
//        self.tblProfessionHeight.constant = self.tblProfession.contentSize.height
//    }
    
    
    @objc func showTableView(notification: NSNotification){
        
        
        if let newDict = notification.object as? NSDictionary
        {
            print(newDict)
            
            if let place = newDict.value(forKey: "place") as? String
            {
                
                if place == "Professions"
                {
                    SortArray =  NSMutableArray()
                    if let newString = newDict.value(forKey: "name") as? String
                    {
                        let predicate = NSPredicate(format: "(name BEGINSWITH[c] %@)", newString)
                        let arr : NSArray = arrProfessions.filtered(using: predicate) as NSArray
                        SortArray = NSMutableArray(array: arr)
                        print(arrProfessions)
                        print(SortArray)
                        
                        self.tblProfession.isHidden = false
                        self.tblProfession.reloadData()
                        //self.tblProfessionHeight.constant = self.tblProfession.contentSize.height
                        
                        if SortArray.count == 1
                        {
                            self.tblProfessionHeight.constant = 50.0
                        }
                        else if SortArray.count == 2
                        {
                            self.tblProfessionHeight.constant = 100.0
                        }
                        else if SortArray.count > 3
                        {
                            self.tblProfessionHeight.constant = 196.0
                        }
                        else
                        {
                            self.tblProfessionHeight.constant = 0.0
                        }
                    }
                }
            }
            
        }
    }
    
    @objc func removeTags(notification: NSNotification){
        
        if let dict = notification.object as? NSDictionary
        {
            if let place = dict.value(forKey: "place") as? String
            {
                if place == "Professions"
                {
                    if let newString = dict.value(forKey: "name") as? String
                    {
                        
                        let filteredArray = arrProfessions.filter { ($0 as! NSDictionary)["name"] as! String == newString }
                        
                        if let dict_filter = filteredArray[0] as? NSDictionary
                        {
//                            if let id_professtion = dict_filter.value(forKey: "id") as? Int
//                            {
//                                let ind = arrProfesstionsID.index(of: "\(id_professtion)")
//                                self.arrProfesstionsID.remove(at: ind!)
//                            }
                            
                            if let id_professtion = dict_filter.value(forKey: "id")  as? Int
                            {
                                let ind = arrProfesstionsID.index(of: "\(id_professtion)")
                                self.arrProfesstionsID.remove(at: ind!)
                            }
                            
                            if let indexd = self.arrProfessionSelected.index(of: dict_filter)
                           {
                                self.arrProfessionSelected.remove(at: indexd)
                           }
                            
//                            for i in 0..<self.arrProfessionSelected.count
//                            {
//                                if let d = self.arrProfessionSelected[i] as? [String:Any]
//                                {
//                                    if NSDictionary(dictionary: dict_filter).isEqual(to: d)
//                                    {
//                                        self.arrProfessionSelected.remove(at: i)
//                                    }
//
//                                }
//                            }
                            
                        }
                    }
                    
                }
                
            }
        }
        
    }
    
    func getProfessions()
    {
        if(CommonFunction.isInternetAvailable())
        {
            MBProgressHUD.showAdded(to: self.view, animated: true)
            
            Alamofire.request(Constants.BASEURL + "profession", method: .get, parameters: nil , encoding: JSONEncoding.default, headers:nil).responseJSON
                { response in
                    
                    MBProgressHUD.hide(for: self.view, animated: true)
                    self.arrProfessions.removeAllObjects()
                    if let result_dict = response.result.value as? [String:Any]
                    {
                        if let rcode = result_dict["code"] as? Int
                        {
                            if rcode == 200
                            {
                                if let data_dict = result_dict["data"] as? [[String:Any]]
                                {
                                    for i in 0..<data_dict.count
                                    {
                                        let profession_dict = data_dict[i]
                                        if let profession_name = profession_dict["name"] as? String
                                        {
                                            var name_dict = [String:Any]()
                                            name_dict["name"] = profession_name
                                            if let profession_id = profession_dict["id"] as? Int
                                            {
                                                name_dict["id"] = profession_id
                                            }
                                            self.arrProfessions.add(name_dict)
                                        }
                                    }
                                }
                            }
                            else
                            {
                                if let error_array = result_dict["errorData"] as? NSArray
                                {
                                    if let error_dict = error_array[0] as? [String : Any]
                                    {
                                        if let error_msg = error_dict["message"] as? String
                                        {
                                            self.view.makeToast(error_msg)
                                        }
                                    }
                                }
                            }
                        }
                    }
            }
        }
        else
        {
            self.view.makeToast("Please Check Your Internet Connections!!")
        }
    }
    
    
    func tagsControl(_ tagsControl: TLTagsControl!, tappedAt index: Int) {
        
        if tagsControl == tagsControlProfession
        {
            print(index)
        }
    }
    
    func tagsControl(_ tagsControl: TLTagsControl!, removedAt index: Int) {
        
        print(index)
    }
    
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return SortArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
            let cell = tableView.dequeueReusableCell(withIdentifier: "ProfesstionsCell", for: indexPath)
            
            if let dict = SortArray[indexPath.row] as? [String : Any]
            {
                if let pname = dict["name"] as? String
                {
                    cell.textLabel?.text = pname
                }
            }
            return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
       
            
            if let dict = SortArray[indexPath.row] as? [String : Any]
            {
                if let professiontag = dict["name"] as? String
                {
                    if let professionid = dict["id"] as? Int
                    {
                        self.arrProfesstionsID.append("\(professionid)")
                        
                       if let dict_p = SortArray[indexPath.row] as? NSDictionary
                       {
                            self.arrProfessionSelected.append(dict_p)
                       }
                    }
                    tagsControlProfession.addTag("\(professiontag)")
                    self.tblProfession.isHidden = true
                }
            }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return 40.0
    }
    
    //MARK:- Back Action
    
    @IBAction func tncBtn(_ sender: UIButton) {
        sender.isSelected = !sender.isSelected
        let tncVc = Constants.mainStoryboard.instantiateViewController(withIdentifier: "TNCViewController") as! TNCViewController
               self.navigationController?.pushLikePresent(tncVc)
    }
    
    @IBOutlet weak var tncBtnOutlet: UIButton!
    @IBAction func showTncAction(_ sender: Any) {
        let tncVc = Constants.mainStoryboard.instantiateViewController(withIdentifier: "TNCViewController") as! TNCViewController
        self.navigationController?.pushLikePresent(tncVc)
    }
    @IBAction func backAction(_ sender: Any)
    {
        self.navigationController?.popViewController(animated: true)
    }
    
    //MARK:- Button Register Action
    @IBAction func btnNextAction(_ sender: Any)
    {
        
        if !tncBtnOutlet.isSelected{
            self.displayAlert(msg: "Please agree terms and conditions.", title_str: "")
            return
        }
        
        if self.loginType.count==0
        {
            self.loginType = "email"
        }
        params2VC.updateValue(self.loginType, forKey: "registerType")
        params2VC.updateValue(txtViewAboutMe.text.encodeEmoji, forKey: "aboutme")
        params2VC.updateValue(self.socialId, forKey: "socialId")
        params2VC.updateValue("", forKey: "house_number")
        params2VC.updateValue("", forKey: "region")
        params2VC.updateValue(city_str, forKey: "city")
        params2VC.updateValue(country_str, forKey: "country")
        params2VC.updateValue(pincode_str, forKey: "pincode")
        params2VC.updateValue("", forKey: "phone")
        params2VC.updateValue("\(latitude_val)", forKey: "latitude")
        params2VC.updateValue("\(longitude_val)", forKey: "longitude")
        params2VC.updateValue(self.arrProfessionSelected, forKey: "profession")
        
//        let vc=Constants.mainStoryboard.instantiateViewController(withIdentifier: "FaceRecognizerViewController")as! FaceRecognizerViewController
//        vc.parameter_dict = params2VC
//        vc.already_added = false
//        vc.loginType_str = self.loginType
//        self.navigationController?.pushViewController(vc, animated: true)
        
        let vc = Constants.mainStoryboard2.instantiateViewController(withIdentifier: "ProfesstionsInterestsViewController") as! ProfesstionsInterestsViewController
            vc.params2VC = params2VC
            vc.already_added = false
            vc.loginType = self.loginType
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    
    //MARK:- Button Already Register Action
    @IBAction func btnAlreadyRegisterAction(_ sender: Any)
    {
        let viewControllers=self.navigationController?.viewControllers
        for vc in viewControllers!
        {
            if vc.isKind(of: LoginViewController.self)
            {
                self.navigationController?.popToViewController(vc, animated: true)
            }
            else
            {
                self.navigationController?.popToRootViewController(animated: true)
            }
        }
    }
    
    //MARK:- textFieldDidBeginEditing
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if(textField==txtLocation)
        {
            let autocompleteController = GMSAutocompleteViewController()
            autocompleteController.delegate = self
            present(autocompleteController, animated: true, completion: nil)
        }
    }
    
    func open_facerecognizer()
    {
        
    }

}
extension SignupSecondViewController: GMSAutocompleteViewControllerDelegate {
    
    //MARK:- Google Auto Complete Place Delegates
    func viewController(_ viewController: GMSAutocompleteViewController, didAutocompleteWith place: GMSPlace) {
        print("Place name: \(place.name)")
        print("Place address: \(place.formattedAddress)")
        print("Place attributions: \(place.attributions)")
        txtLocation.text = "\(place.formattedAddress!)"
        
        
        latitude_val = place.coordinate.latitude
        longitude_val = place.coordinate.longitude
        var keys = [String]()
        place.addressComponents?.forEach{keys.append($0.type)}
        var values = [String]()
        place.addressComponents?.forEach({ (component) in
            keys.forEach{ component.type == $0 ? values.append(component.name): nil}
            print(component.type)
            if component.type == "locality"
            {
                city_str = component.name
            }
            if component.type == "country"
            {
                country_str = component.name
            }
            if component.type == "postal_code"
            {
                pincode_str = component.name
            }
        })
        dismiss(animated: true, completion: nil)
    }
    
    
    func viewController(_ viewController: GMSAutocompleteViewController, didFailAutocompleteWithError error: Error) {
        // TODO: handle the error.
        print("Error: ", error.localizedDescription)
    }
    
    // User canceled the operation.
    func wasCancelled(_ viewController: GMSAutocompleteViewController) {
        dismiss(animated: true, completion: nil)
    }
    
    // Turn the network activity indicator on and off again.
    func didRequestAutocompletePredictions(_ viewController: GMSAutocompleteViewController) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
    }
    
    func didUpdateAutocompletePredictions(_ viewController: GMSAutocompleteViewController) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = false
    }
    
}
