//
//  ViewController.swift
//  UPCarouselFlowLayoutDemo
//
//  Created by Paul Ulric on 23/06/2016.
//  Copyright © 2016 Paul Ulric. All rights reserved.
//

import UIKit
import MBProgressHUD
import SDWebImage
import Alamofire
import Toast_Swift

class FindFriendViewController: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {

    //MARK:- Outlets
    @IBOutlet weak var nextBtn: UIButton!
    var arrRequestSent:[Int] = []
    @IBOutlet weak var viewMain: UIView!
    var errorLbl:UILabel? = nil
    @IBOutlet weak var pageControl: UIPageControl!
    @IBOutlet weak var collectionView: UICollectionView!
    
    fileprivate var items = [UserModel]()
    
    fileprivate var currentPage: Int = 0 {
        didSet {
           
        }
    }
    
    fileprivate var pageSize: CGSize {
        let layout = self.collectionView.collectionViewLayout as! UPCarouselFlowLayout
        var pageSize = layout.itemSize
        if layout.scrollDirection == .horizontal {
            pageSize.width += layout.minimumLineSpacing
        } else {
            pageSize.height += layout.minimumLineSpacing
        }
        return pageSize
    }
    
    fileprivate var orientation: UIDeviceOrientation {
        return UIDevice.current.orientation
    }
    
    //MARK:- viewDidLoad
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupLayout()
       // self.items = self.createItems()
        
        self.currentPage = 0
        
        
        pageControl.hidesForSinglePage = true
       
        NotificationCenter.default.addObserver(self, selector: #selector(FindFriendViewController.rotationDidChange), name: NSNotification.Name.UIDeviceOrientationDidChange, object: nil)
    }
    
    //MARK:-viewWillAppear
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        getFriendList()
    }
    
    
    //MARK:- Get FriendList Api
    func getFriendList()  {
        
        if(CommonFunction.isInternetAvailable())
        {
            MBProgressHUD.showAdded(to: self.view, animated: true)
            var params = [String:Any]()
//            params["longitude"] = "\(LocationServiceManager.sharedLocation?.lastLocation?.coordinate.longitude ?? 23.0168)"
//            params["latitude"] = "\(LocationServiceManager.sharedLocation?.lastLocation?.coordinate.latitude ?? 72.5003)"
            params["user_id"] = "\(UserDefaults.standard.value(forKey: Constants.USERID) as! Int)"
            //params["latitude"] = "23.0168"
           // params["longitude"] = "72.5003"
            
            let header_dict  =  ["Content-Type":"application/json"]
            
            Alamofire.request(Constants.BASEURL + MethodName.suggestedFriendList + "?access-token=\(UserDefaults.standard.value(forKey: "Access_Token") as! String)", method: .post, parameters: params, encoding: JSONEncoding.default, headers: header_dict).responseJSON { response in
                if let result_dict = response.result.value as? [String:Any]
                {
                    MBProgressHUD.hide(for: self.view, animated: true)
                    if let code = result_dict["code"] as? Int
                    {
                        if(code==200)
                        {
                            if let user_arr = result_dict["data"] as? [[String:Any]]
                            {
                                if user_arr.count > 0
                                {
                                    for i in 0..<user_arr.count
                                    {
                                        let user_model = UserModel()
                                        if let user_dict = user_arr[i] as? [String:Any]
                                        {
                                            if let about_str = user_dict["aboutme"] as? String
                                            {
                                                user_model.aboutme = about_str
                                            }
                                            if let name_str = user_dict["fullname"] as? String
                                            {
                                                user_model.userName = name_str
                                            }
                                            if let img_str = user_dict["user_profile_image"] as? String
                                            {
                                                user_model.userImage = img_str
                                            }
                                            if let userid_str = user_dict["userId"] as? Int
                                            {
                                                user_model.userId = "\(userid_str)"
                                            }
                                            
                                            self.items.append(user_model)
                                        }
                                    }
                                }
                                else
                                {
                                    self.errorLbl = UILabel.init(frame: self.view.bounds)
                                    self.errorLbl?.text = "No suggestion available"
                                    self.errorLbl?.textAlignment = .center
                                    self.errorLbl?.textColor = Colors.borderColor
                                    self.view.addSubview(self.errorLbl!)
                                }
                                self.collectionView.reloadData()
                                
                                self.pageControl.numberOfPages=self.items.count
                            }
                        }
                        else
                        {
                            if let error_arr = result_dict["errorData"] as? [[String:Any]]
                            {
                                if let error_msg = error_arr[0]["message"] as? String
                                {
                                    self.view.makeToast(error_msg)
                                }
                            }
                        }
                    }
                }
            }
        }
    }
    
    fileprivate func setupLayout() {
        let layout = self.collectionView.collectionViewLayout as! UPCarouselFlowLayout
        layout.spacingMode = UPCarouselFlowLayoutSpacingMode.overlap(visibleOffset: 30)
    }
    
    fileprivate func createItems() -> [ProfileCharacter] {
        let characters = [
            ProfileCharacter(imageName: "wall-e", name: "Wall-E", movie: "Wall-E"),
            ProfileCharacter(imageName: "nemo", name: "Nemo", movie: "Finding Nemo"),
            ProfileCharacter(imageName: "ratatouille", name: "Remy", movie: "Ratatouille"),
            ProfileCharacter(imageName: "buzz", name: "Buzz Lightyear", movie: "Toy Story"),
            ProfileCharacter(imageName: "monsters", name: "Mike & Sullivan", movie: "Monsters Inc."),
            ProfileCharacter(imageName: "brave", name: "Merida", movie: "Brave")
        ]
        return characters
    }
    
    
    @objc fileprivate func rotationDidChange() {
        guard !orientation.isFlat else { return }
        let layout = self.collectionView.collectionViewLayout as! UPCarouselFlowLayout
        let direction: UICollectionViewScrollDirection = UIDeviceOrientationIsPortrait(orientation) ? .horizontal : .vertical
        layout.scrollDirection = direction
        if currentPage > 0 {
            let indexPath = IndexPath(item: currentPage, section: 0)
            let scrollPosition: UICollectionViewScrollPosition = UIDeviceOrientationIsPortrait(orientation) ? .centeredHorizontally : .centeredVertically
            self.collectionView.scrollToItem(at: indexPath, at: scrollPosition, animated: false)
        }
    }
    
    // MARK:- Card Collection Delegate & DataSource
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize.init(width: collectionView.frame.size.width-60, height: collectionView.frame.size.height-10)
    }
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return items.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: CarouselCollectionViewCell.identifier, for: indexPath) as! CarouselCollectionViewCell
        
        let user_model = items[indexPath.row]
        cell.nameLbl.text = user_model.userName
        cell.descLbl.text = user_model.aboutme
        cell.image.sd_setImage(with: URL(string: user_model.userImage), placeholderImage: UIImage(named: "userPlaceHolder"))
    
        cell.addFendBtn.tag = indexPath.row
        cell.addFendBtn.addTarget(self, action: #selector(AddFriendAction(_:)), for: UIControlEvents.touchUpInside)
        cell.image.cornorRadius()
        return cell
    }
    
//    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
//        self.pageControl.currentPage = indexPath.item
//    }
    
    //MARK:- Add Friend Action
    @objc func AddFriendAction(_ sender : UIButton){
       
        guard let cell = sender.superview?.superview as? CarouselCollectionViewCell else
        {
            return // or fatalError() or whatever
        }
        let selected_indexPath = collectionView.indexPath(for: cell)
        let user_model = items[(selected_indexPath?.row)!]
        
        if sender.title(for: .normal) == "Add as friend"
        {
            if(CommonFunction.isInternetAvailable())
            {
                let params = ["friend_user_id":user_model.userId,
                              "user_id":"\(UserDefaults.standard.value(forKey: Constants.USERID) as! Int)"]
                let header_dict = ["Token":"31528198109743225ff9d0cf04d1fdd1",
                                   "user_id":"\(UserDefaults.standard.value(forKey: Constants.USERID) as! Int)"]
                
                Alamofire.request(Constants.BASEURL + MethodName.addFriendRequest + "?access-token=\(UserDefaults.standard.value(forKey: "Access_Token") as! String)" , method: .post, parameters: params, encoding: URLEncoding.default, headers: header_dict).responseJSON { response in
                    
                    if let result_dict = response.result.value as? [String:Any]
                    {
                        print(result_dict)
                        if(result_dict["code"] as! Int == 200)
                        {
                            self.view.makeToast("Friend Request Sent Successfully")
                            sender.setTitle("Request Sent", for: .normal)
                        }
                        else
                        {
                            if let error_arr = result_dict["errorData"] as? [[String:Any]]
                            {
                                if let error_msg = error_arr[0]["message"] as? String
                                {
                                    self.view.makeToast(error_msg)
                                }
                            }
                        }
                    }
                }
            }
            else
            {
                self.view.makeToast("Please Check Your Internet Connection!!")
            }
        }
        else if sender.title(for: .normal) == "Request Sent"
        {
            if(CommonFunction.isInternetAvailable())
            {
                Alamofire.request(Constants.BASEURL + MethodName.cancelFriendRequest + "?access-token=\(UserDefaults.standard.value(forKey: "Access_Token") as! String)&id=\(user_model.userId)" , method: .post, parameters: nil, encoding: URLEncoding.default, headers: ["Content-Type":"application/json"]).responseJSON { response in
                    
                    if let result_dict = response.result.value as? [String:Any]
                    {
                        print(result_dict)
                        if let code = result_dict["code"] as? Int
                        {
                            if(code==200)
                            {
                                sender.setTitle("Add as Friend", for: .normal)
                                self.view.makeToast("Cancel request successfully")
                            }
                            else
                            {
                                if let error_arr = result_dict["errorData"] as? [[String:Any]]
                                {
                                    if let error_msg = error_arr[0]["message"] as? String
                                    {
                                        self.view.makeToast(error_msg)
                                    }
                                }
                            }
                        }
                    }
                    
                }
            }
            else
            {
                self.view.makeToast("Please Check Your Internet Connection!!")
            }
        }
        
        
    }
    
//    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
//        let character = items[(indexPath as NSIndexPath).row]
//        let alert = UIAlertController(title: character.name, message: nil, preferredStyle: .alert)
//        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
//        present(alert, animated: true, completion: nil)
//    }

    
    // MARK:- UIScrollViewDelegate
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        let layout = self.collectionView.collectionViewLayout as! UPCarouselFlowLayout
        let pageSide = (layout.scrollDirection == .horizontal) ? self.pageSize.width : self.pageSize.height
        let offset = (layout.scrollDirection == .horizontal) ? scrollView.contentOffset.x : scrollView.contentOffset.y
        currentPage = Int(floor((offset - pageSide / 2) / pageSide) + 1)
        self.pageControl.currentPage=currentPage
    }
    
    
    //MARK:- Button Skip Action
    @IBAction func btnSkipction(_ sender: Any)
    {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "MainTabbarViewController")as! MainTabbarViewController
        self.present(vc, animated: true, completion: nil)
        
        
    }
    
}

