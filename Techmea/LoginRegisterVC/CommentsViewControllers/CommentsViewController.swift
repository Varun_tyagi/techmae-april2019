//
//  CommentsViewController.swift
//  Techmea
//
//  Created by VISHAL SETH on 8/27/18.
//  Copyright © 2018 VISHAL SETH. All rights reserved.
//

import UIKit
import MBProgressHUD
import Alamofire
import Toast_Swift
import GrowingTextView


protocol CommentsPostedDelegate
{
    func refreshFeedData()
}

extension String {
    var encodeEmoji: String{
        
        
        let data = self.data(using: .nonLossyASCII, allowLossyConversion: true)!
        return String(data: data, encoding: .utf8)!
      
    }
    
    var decodeEmoji: String{
        let newStr = self.replacingOccurrences(of: "\n", with: "@@@@@@@@@@!!!!!@@@@@@@@@@")
        let data = ("\""+newStr+"\"").data(using: .utf8)!
        var result = try? JSONSerialization.jsonObject(with: data, options: .allowFragments) as! String
        result = result?.replacingOccurrences(of: "@@@@@@@@@@!!!!!@@@@@@@@@@", with: "\n")
        return result ?? self
    }
}

extension String {
    var jsonStringRedecoded: String? {
        let data = ("\""+self+"\"").data(using: .utf8)!
        let result = try? JSONSerialization.jsonObject(with: data, options: .allowFragments) as! String
        return result ?? self
    }
}

class CommentsViewController: UIViewController,UITableViewDelegate,UITableViewDataSource,UITextFieldDelegate {

    //MARK:- Outlets
    var postId = ""
    var groupId = Int()
    var arrComments = [PostComment]()
    var delegate:CommentsPostedDelegate?
    var arrGroupComments = [PostComment]()
    var comment_type = String()
    @IBOutlet weak var tblComment: UITableView!
    
    @IBOutlet weak var commentTxt: GrowingTextView!
    @IBOutlet weak var viewPostComment: UIView!
    var login_username = String()
    var user_img_str = String()
    var last_comment_id = String()
    var current_page = 1
    var is_next_page_available = Bool()
    var perpage_data_count = Int()
    
    @IBOutlet weak var lbl_nodata: UILabel!
    
    //MARK:- viewDidLoad
    override func viewDidLoad()
    {
        super.viewDidLoad()
        automaticallyAdjustsScrollViewInsets = false
        commentTxt.maxLength = 240
        commentTxt.trimWhiteSpaceWhenEndEditing = false
        commentTxt.placeholder = "Write something here"
        commentTxt.placeholderColor = UIColor(white: 0.8, alpha: 1.0)
        
        commentTxt.layer.cornerRadius = 4.0
        
        
        viewPostComment.dropShadow()
        self.title="Comments"
        self.navigationController?.navigationBar.isHidden=true
        tblComment.estimatedRowHeight=80
        if let data = UserDefaults.standard.object(forKey: "userData") as? Data
        {
            let oD1: NSDictionary? = NSKeyedUnarchiver.unarchiveObject(with: data) as? NSDictionary
            if let usernamestring = oD1?.value(forKey: "fullname") as? String
            {
                login_username = usernamestring
            }
        }
        if let data = UserDefaults.standard.object(forKey: "userData") as? Data
        {
            let oD1: NSDictionary? = NSKeyedUnarchiver.unarchiveObject(with: data) as? NSDictionary
            if let imageurlstring = oD1?.value(forKey: "user_profile_image") as? String
            {
                user_img_str =  imageurlstring
            }
        }
        
        if(comment_type == "Group")
        {
            if(last_comment_id.count>0)
            {
                get_grouppost_comment()
            }
            
        }
        else
        {
            if(last_comment_id.count>0)
            {
                get_post_comment()
            }
        }
        //tblComment.reloadData()
        if(arrComments.count>0)
        {
            self.lbl_nodata.isHidden = true
        }
        else
        {
            self.lbl_nodata.isHidden = true
        }
        // Do any additional setup after loading the view.
    }
    
    
    //MARK:- textFieldShouldReturn
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        self.view.endEditing(true)
        if(comment_type == "Group")
        {
            callGroupAddCommentApi()
        }
        else
        {
            callAddCommentApi()
        }
        return false
    }
    
    
    //MARK:- Get Post Comment Api
    func get_post_comment()
    {
        if(CommonFunction.isInternetAvailable())
        {
            MBProgressHUD.showAdded(to: self.view, animated: true)
            Alamofire.request(Constants.BASEURL+MethodName.postLoadMoreComment + "?access-token=\(UserDefaults.standard.value(forKey: "Access_Token") as! String)&post_id=\(postId)&page=\(current_page)&id=\(last_comment_id)&mode=load-more-comment", method: .get, parameters: nil, encoding: URLEncoding.default, headers: ["Content-Type": "application/json"]).responseJSON { response in
                
                    if let records_onepage = response.response?.allHeaderFields["X-Pagination-Per-Page"] as? String {
                        self.perpage_data_count = Int(records_onepage)!
                    }
                
                    MBProgressHUD.hide(for: self.view, animated: true)
                    if let result_dict = response.result.value as? [String:Any]
                    {
                        print(result_dict)
                        if let code = result_dict["code"] as? Int
                        {
                            if(code==200)
                            {
                                if let arrData = result_dict["data"] as? [[String:Any]]
                                {
                                    if(arrData.count<self.perpage_data_count)
                                    {
                                        self.is_next_page_available = false
                                    }
                                    else
                                    {
                                        self.is_next_page_available = true
                                    }
                                    for dict in arrData
                                    {
                                        if let tempDict = dict as? NSDictionary
                                        {
                                            self.arrComments.append(PostComment.init(dict: tempDict))
                                        }
                                    }
                                    if(self.arrComments.count>0)
                                    {
                                        self.lbl_nodata.isHidden = true
                                    }
                                    else
                                    {
                                        self.lbl_nodata.isHidden = true
                                    }
                                    self.tblComment.reloadData()
                                }
                            }
                            else
                            {
                                if let error_arr = result_dict["errorData"] as? [[String:Any]]
                                {
                                    if let error_msg = error_arr[0]["message"] as? String
                                    {
                                        self.view.makeToast(error_msg)
                                    }
                                }
                            }
                        }
                    }
                }
        }
        else
        {
            self.view.makeToast("Please Check Your Internet Connection!!")
        }
    }
    
    
    //MARK:- Get Group Post Comment Api
    func get_grouppost_comment()
    {
        if(CommonFunction.isInternetAvailable())
        {
            MBProgressHUD.showAdded(to: self.view, animated: true)
            Alamofire.request(Constants.BASEURL+MethodName.groupPostLoadMoreComment + "?access-token=\(UserDefaults.standard.value(forKey: "Access_Token") as! String)&post_id=\(postId)&page=\(current_page)&id=\(last_comment_id)&mode=load-more-comment", method: .get, parameters: nil, encoding: URLEncoding.default, headers: ["Content-Type": "application/json"]).responseJSON { response in
                    if let records_onepage = response.response?.allHeaderFields["X-Pagination-Per-Page"] as? String {
                        self.perpage_data_count = Int(records_onepage)!
                    }
                    MBProgressHUD.hide(for: self.view, animated: true)
                    if let result_dict = response.result.value as? [String:Any]
                    {
                        print(result_dict)
                        if let code = result_dict["code"] as? Int
                        {
                            if(code==200)
                            {
                                if let arrData = result_dict["data"] as? [[String:Any]]
                                {
                                    if(arrData.count<self.perpage_data_count)
                                    {
                                        self.is_next_page_available = false
                                    }
                                    else
                                    {
                                        self.is_next_page_available = true
                                    }
                                    for dict in arrData
                                    {
                                        if let tempDict = dict as? NSDictionary
                                        {
                                            self.arrComments.append(PostComment.init(dict: tempDict))
                                        }
                                    }
                                    if(self.arrComments.count>0)
                                    {
                                        self.lbl_nodata.isHidden = true
                                    }
                                    else
                                    {
                                        self.lbl_nodata.isHidden = true
                                    }
                                    self.tblComment.reloadData()
                                }
                            }
                            else
                            {
                                if let error_arr = result_dict["errorData"] as? [[String:Any]]
                                {
                                    if let error_msg = error_arr[0]["message"] as? String
                                    {
                                        self.view.makeToast(error_msg)
                                    }
                                }
                            }
                        }
                    }
                }
        }
        else
        {
            self.view.makeToast("Please Check Your Internet Connection!!")
        }
    }
    
    //MARK:- viewWillDisappear
    override func viewWillDisappear(_ animated: Bool)
    {
        super.viewWillDisappear(animated)
        self.navigationController?.navigationBar.isHidden=false
    }
    
    //MARK:- Back Action
    @IBAction func btnBackAction(_ sender: Any)
    {
        self.navigationController?.popViewController(animated: true)
    }
    
    @objc func closeVC()
    {
        self.dismiss(animated: true)
        {
        }
    }
    
    //MARK:- Button Post Comment
    @IBAction func btnPostComment(_ sender: Any)
    {
        self.view.endEditing(true)
        if(comment_type == "Group")
        {
            callGroupAddCommentApi()
        }
        else
        {
            callAddCommentApi()
        }
    }
    
    
    func editCommentApi(_ newText:String , commentId:String)
    {
        var params = [String:String]()
        params.updateValue(newText, forKey: "comment_text")

        let headerString = ["Content-Type": "application/json"]
        MBProgressHUD.showAdded(to: self.view, animated: true)
        Alamofire.request(Constants.BASEURL + (comment_type == "Group" ? MethodName.editGroupComment(postId: commentId) :  MethodName.editPostComment(postId: commentId) ) , method: .post, parameters: params, encoding: JSONEncoding.default, headers: headerString).responseJSON { response in
            MBProgressHUD.hide(for: self.view, animated: true)
            if let response = response.result.value as? [String:Any]
            {
                let code:Int = response["code"]as! Int
                if code==200
                {
                    self.tblComment.reloadData()
                    
                }
            }
        }
    }
    
    //MARK:- Button Add Comment Api
    func callAddCommentApi()
    {
        var params = [String:String]()
        params.updateValue(commentTxt.text!.encodeEmoji, forKey: "comment_text")
        params.updateValue(self.postId, forKey: "post_id")
        params.updateValue("\(UserDefaults.standard.value(forKey: Constants.USERID) as! Int)", forKey: "user_id")
        params.updateValue(MethodName.Token, forKey: "Token")
        
        let headerString = ["Content-Type": "application/json"]
        MBProgressHUD.showAdded(to: self.view, animated: true)
        Alamofire.request(Constants.BASEURL+MethodName.commentPost + "?access-token=\(UserDefaults.standard.value(forKey: "Access_Token") as! String)&id=\(self.postId)", method: .post, parameters: params, encoding: JSONEncoding.default, headers: headerString).responseJSON { response in
            MBProgressHUD.hide(for: self.view, animated: true)
            if let response = response.result.value as? [String:Any]
            {
                let code:Int = response["code"]as! Int
                if code==200
                {
                    var dict = [String:Any]()
                    dict.updateValue(11, forKey: "comment_id")
                    dict.updateValue(UserDefaults.standard.value(forKey: Constants.USERID) as! Int, forKey: "user_id")
                    dict.updateValue(self.commentTxt.text!, forKey: "comments")
                    dict.updateValue(self.user_img_str, forKey: "user_image")
                    dict.updateValue(self.login_username, forKey: "username")
                    let commentDict = PostComment.init(dict: dict as NSDictionary)
                    //self.arrComments.insert(commentDict, at: 0)
                    self.arrComments.append(commentDict)
                    if(self.arrComments.count>0)
                    {
                        self.lbl_nodata.isHidden = true
                    }
                    else
                    {
                        self.lbl_nodata.isHidden = false
                    }
                    self.tblComment.reloadData()
                    self.commentTxt.text=""
                    
                }
            }
        }
    }
    
    
    //MARK:- Button Add Group Comment Api
    func callGroupAddCommentApi()
    {
        var params = [String:String]()
        params.updateValue(commentTxt.text!.encodeEmoji, forKey: "comment_text")
        params.updateValue(self.postId, forKey: "post_id")
        params.updateValue("\(self.groupId)", forKey: "group_id")

        
        let headerString = ["Content-Type": "application/json"]
        MBProgressHUD.showAdded(to: self.view, animated: true)
        Alamofire.request(Constants.BASEURL+MethodName.groupPostComment + "?access-token=\(UserDefaults.standard.value(forKey: "Access_Token") as! String)", method: .post, parameters: params, encoding: JSONEncoding.default, headers: headerString).responseJSON { response in
            MBProgressHUD.hide(for: self.view, animated: true)
            if let result_dict = response.result.value as? [String:Any]
            {
                
                let code:Int = result_dict["code"]as! Int
                if code==200
                {
                    var dict = [String:Any]()
                    let info_dict  = result_dict["data"] as! [String:Any]
                    dict.updateValue(info_dict["id"], forKey: "commentId")
                    dict.updateValue(UserDefaults.standard.value(forKey: Constants.USERID) as! Int, forKey: "commentedBy")
                    dict.updateValue(self.commentTxt.text!, forKey: "comment")
                    dict.updateValue(self.user_img_str, forKey: "user_profile_pic")
                    dict.updateValue(self.login_username, forKey: "commentByName")
                    dict.updateValue(info_dict["commentDate"] as! String, forKey: "commentDate")
                
                    let commentDict = PostComment.init(dict: dict as NSDictionary)
                    //self.arrComments.insert(commentDict, at: 0)
                    self.arrComments.append(commentDict)
                    if(self.arrComments.count>0)
                    {
                        self.lbl_nodata.isHidden = true
                    }
                    else
                    {
                        self.lbl_nodata.isHidden = true
                    }
                    self.tblComment.reloadData()
                    self.commentTxt.text=""
                    
                }
            }
        }
    }
    
    //MARK:- scrollViewDidEndDecelerating
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        if (self.tblComment.contentOffset.y >= (self.tblComment.contentSize.height - self.tblComment.bounds.size.height))
        {
            if(is_next_page_available)
            {
                current_page = current_page + 1
                if(current_page>1)
                {
                    let comment_count = self.arrComments.count
                    last_comment_id = self.arrComments[comment_count-1].commentId
                }
                if(comment_type == "Group")
                {
                    self.get_grouppost_comment()
                }
                else
                {
                    self.get_post_comment()
                }
            }
            
        }
    }
}

extension CommentsViewController
{
    
    //MARK:- Tableview Delegate Methods
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return self.arrComments.count
        
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        var cell = CommentCell()
        if (self.arrComments[indexPath.row].userId==CommonFunction.getUserIDFromDefaults())
        {
            cell = tableView.dequeueReusableCell(withIdentifier: "CellMe") as! CommentCell
        }
        else
        {
            cell = tableView.dequeueReusableCell(withIdentifier: "CellYou") as! CommentCell
        }
        
        let commentMod = self.arrComments[indexPath.row]
        cell.lblComment.text=self.arrComments[indexPath.row].comment.decodeEmoji
       
       
        
        let dateCreated = commentMod.created_At
              
                  if dateCreated >  0
                  {
                      let date = Date(timeIntervalSince1970: Double(dateCreated))
 cell.lblComment.text="\(self.arrComments[indexPath.row].comment.decodeEmoji)\n\(date.timeAgoSinceDate(true))"

                  }
        if commentMod.createdAtStr.count > 0 {
            let dateFormatter = DateFormatter()
            dateFormatter.timeZone = NSTimeZone.local //Set timezone that you want
            dateFormatter.dateFormat = "yyyy-MM-dd hh:mm:ss" //Specify your format that you want
            let strDate = dateFormatter.date(from: commentMod.createdAtStr)
            cell.lblComment.text="\(self.arrComments[indexPath.row].comment.decodeEmoji)\n\(strDate!.timeAgoSinceDate(true))"

        }

        
        cell.lblUsername.text=self.arrComments[indexPath.row].userName.decodeEmoji
        cell.userImg.sd_setImage(with: URL(string: self.arrComments[indexPath.row].userImage), placeholderImage: UIImage(named: "userPlaceHolder"))
        self.add_tap_gester_lbl(cell.lblUsername)
        self.add_tap_gester(cell.userImg)
        cell.selectionStyle=UITableViewCellSelectionStyle.none
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if (self.arrComments[indexPath.row].userId==CommonFunction.getUserIDFromDefaults())
               {
        let view = ModalView.instantiateFromNib()
                view.isTxtViewNeeded=true
                view.updateModal("TechMae", descMessage: self.arrComments[indexPath.row].comment.decodeEmoji, firstTitle: "UPDATE", secondTitle: "CANCEL", type: .modalAlert)
                      let modal = PathDynamicModal()
                      modal.showMagnitude = 200.0
                      modal.closeMagnitude = 130.0
                      modal.closeByTapBackground = false
                      modal.closeBySwipeBackground = false
                      view.Cancel2ButtonHandler = {[weak modal] in
                          modal?.closeWithLeansRandom()
                          return
                      }
                      view.OkButtonHandler = {[weak modal] in
                        // hit edit comment service
                        print(view.txtView.text)
                        let newModel = self.arrComments[indexPath.row]
                        newModel.comment = view.txtView.text
                        self.tblComment.reloadData()
                        self.editCommentApi(view.txtView.text, commentId:self.arrComments[indexPath.row].commentId)
                          modal?.closeWithLeansRandom()
                          return
                      }
                      modal.show(modalView: view, inView: (UIApplication.shared.delegate?.window!)!)
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    func add_tap_gester_lbl(_ lbl : UILabel)
    {
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.tapFunction))
        lbl.isUserInteractionEnabled = true
        lbl.addGestureRecognizer(tap)
    }
    func add_tap_gester(_ imgview : UIImageView)
    {
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.imageview_tapFunction))
        imgview.isUserInteractionEnabled = true
        imgview.addGestureRecognizer(tap)
    }
    @objc func tapFunction(sender:UITapGestureRecognizer)
    {
        guard let cell = sender.view?.superview?.superview?.superview as? CommentCell else
        {
            return
        }
        let selected_indexPath = tblComment.indexPath(for: cell)
        self.open_profile(user_id: "\(self.arrComments[(selected_indexPath?.row)!].userId)")
    }
    @objc func imageview_tapFunction(sender:UITapGestureRecognizer)
    {
        guard let cell = sender.view?.superview?.superview as? CommentCell else
        {
            return
        }
        let selected_indexPath = tblComment.indexPath(for: cell)
        self.open_profile(user_id: "\(self.arrComments[(selected_indexPath?.row)!].userId)")
    }
   
    func open_profile(user_id:String)
    {
        let profile_page = Constants.mainStoryboard.instantiateViewController(withIdentifier: "FriendListViewController") as! FriendListViewController
        profile_page.user_id_str = user_id
        self.navigationController?.pushViewController(profile_page, animated: true)
    }
}
extension CommentsViewController:  GrowingTextViewDelegate {
    func textViewDidChangeHeight(_ textView: GrowingTextView, height: CGFloat) {
       UIView.animate(withDuration: 0.2) {
           self.view.layoutIfNeeded()
       }
    }
}
