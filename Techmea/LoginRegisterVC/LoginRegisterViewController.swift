//
//  LoginRegisterViewController.swift
//  Techmea
//
//  Created by VISHAL SETH on 8/4/18.
//  Copyright © 2018 VISHAL SETH. All rights reserved.
//

import UIKit

class LoginRegisterViewController: UIViewController {

    //MARK:- Outlets
    //@IBOutlet weak var btnSignup:ButtonWithShadow!
    @IBOutlet weak var btnSignup: ScaleButton!
     //@IBOutlet weak var btnLogin: ButtonWithShadow!
    @IBOutlet weak var btnLogin: ScaleButton!
    
    //MARK:- viewDidLoad
     override func viewDidLoad()
     {
        super.viewDidLoad()
        btnLogin.cornorRadius()
        btnSignup.cornorRadius()
        self.btnAddShadow(btn: self.btnLogin)
        self.btnAddShadow(btn: self.btnSignup)
        
        
        self.navigationController?.setNavigationBarHidden(true, animated: true)
        if (UserDefaults.standard.value(forKey: Constants.USERID) as? String) != nil
        {
            let vc =  Constants.mainStoryboard.instantiateViewController(withIdentifier: "MainTabbarViewController")as! MainTabbarViewController
            Constants.appDelegate.window?.rootViewController = vc
        }
    }
    
    func btnAddShadow(btn : ScaleButton)
    {
        btn.layer.shadowColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.25).cgColor
        btn.layer.shadowOffset = CGSize(width: 0, height: 3)
        btn.layer.shadowOpacity = 1.0
        btn.layer.shadowRadius = 10.0
        btn.layer.masksToBounds = false
    }
    
    //MARK:- viewDidDisappear
    override func viewDidDisappear(_ animated: Bool)
    {
        super.viewDidAppear(true)
        self.navigationController?.setNavigationBarHidden(true, animated: true)
    }
    
    //MARK:- Button Login Action
    
    @IBAction func btnLoginAction(_ sender: ScaleButton) {
        
    let vc = Constants.mainStoryboard.instantiateViewController(withIdentifier: "LoginViewController")as! LoginViewController
    self.navigationController?.pushViewController(vc, animated: true)
        
    }
    
    //MARK:- Button Signup Action
    @IBAction func btnSignupAction(_ sender: Any)
    {
        let vc = Constants.mainStoryboard2.instantiateViewController(withIdentifier: "SignupFirstViewController")as! SignupFirstViewController
        self.navigationController?.pushViewController(vc, animated: true)
    }
}
