//
//  GetImages.h
//  Techmea
//
//  Created by VISHAL SETH on 9/4/18.
//  Copyright © 2018 VISHAL SETH. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
@interface GetImages : NSObject
-(void)setArray:(NSArray*)info;
-(NSArray*)getArray;
@property(nonatomic,strong) NSMutableArray *arrImages;
-(UIImage *)imageWithImage:(UIImage *)image convertToSize:(CGSize)size;
@end
