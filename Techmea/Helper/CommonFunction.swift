//
//  CommonFunction.swift
//  JustCase
//
//  Created by VISHAL SETH on 10/8/17.
//  Copyright © 2017 Infoicon. All rights reserved.
//

import UIKit
import Foundation
import AudioToolbox
import Alamofire


class CommonFunction: NSObject
{
    
    static let GetFunctions = CommonFunction()
    
    override private init()
    {
        
    }
    class func set_attributed_string(str1:String, color1:UIColor, str2:String, color2:UIColor, fontsize:CGFloat) -> NSMutableAttributedString
    {
        let font_type = UIFont(name: "Avenir Next-Medium", size: fontsize)
        let attrs1 = [NSAttributedStringKey.font : font_type, NSAttributedStringKey.foregroundColor : color1]
        let attrs2 = [NSAttributedStringKey.font : font_type, NSAttributedStringKey.foregroundColor : color2]
        let attributedString1 = NSMutableAttributedString(string:str1, attributes:attrs1)
        let attributedString2 = NSMutableAttributedString(string:str2, attributes:attrs2)
        attributedString1.append(attributedString2)
        return attributedString1
    }
    class func call_set_device_token(token_str:String)
    {
        if(CommonFunction.isInternetAvailable())
        {
            if let auth_token = UserDefaults.standard.value(forKey: "Access_Token") as? String
            {
                let param = ["device_id":"\(token_str)",
                    "device_type":"ios" ]
                Alamofire.request(Constants.BASEURL + MethodName.registerToken + "?access-token=\(auth_token)" , method: .post, parameters: param, encoding: JSONEncoding.default, headers: ["Content-Type":"application/json"]).responseJSON { response in
                    
                    if let result_dict = response.result.value as? [String:Any]
                    {
                        if let code = result_dict["code"] as? Int
                        {
                            if(code==200)
                            {
                                print(result_dict)
                            }
                        }
                    }
                }
            }
        }
        else
        {
            //self.displayAlert(msg: "Please Check Your Internet Connection!!", title_str: Constants.APP_NAME)
        }
    }
    
    class func is_login_user_coach()->Bool
    {
        if let data = UserDefaults.standard.object(forKey: "userData") as? Data
        {
            let oD1: NSDictionary? = NSKeyedUnarchiver.unarchiveObject(with: data) as? NSDictionary
            
            if let usertype_str = oD1?.value(forKey: "userType") as? String
            {
                if(usertype_str.lowercased()=="user")
                {
                    return false
                }
                else
                {
                    return true
                }
            }
            
        }
        return false
    }
    
    
    class func setCornorRadius(view:UIView,Radius:CGFloat)
    {
        view.layer.cornerRadius=Radius
        view.clipsToBounds=true
    }
    class func get_unit(min:Int)-> CGFloat
    {
        return CGFloat(CGFloat(min)/60.0)
    }
    
    
    class func isInternetAvailable()->Bool
    {
        return NetworkReachabilityManager()!.isReachable
    }
    
    class func getLoginType() -> String
    {
        return UserDefaults.standard.value(forKey: "loginType") as! String
    }
    
    class func startLoaderWithoutTitle()
    {
        
    }
    
    class func startLoader(title:String)
    {
        
    }
    
    class func stopLoader()
    {
        
    }
    
    /*
     * Set color with hex
     */
    class func setColorWithHex(hex:String)->UIColor
    {
        
        var cString:String = hex.trimmingCharacters(in: .whitespacesAndNewlines).uppercased()
        
        if (cString.hasPrefix("#"))
        {
            cString.remove(at: cString.startIndex)
        }
        
        if ((cString.characters.count) != 6)
        {
            return UIColor.gray
        }
        
        var rgbValue:UInt32 = 0
        Scanner(string: cString).scanHexInt32(&rgbValue)
        
        return UIColor(
            red: CGFloat((rgbValue & 0xFF0000) >> 16) / 255.0,
            green: CGFloat((rgbValue & 0x00FF00) >> 8) / 255.0,
            blue: CGFloat(rgbValue & 0x0000FF) / 255.0,
            alpha: CGFloat(1.0)
        )
    }
    
    static func showStatusBarNotification(title:String,color:UIColor){
        
        
    }
    
    /*
     * Show Alert view
     */
    class func showAlert(message:String)
    {
        let alert=UIAlertView.init(title:Constants.APP_NAME, message: message, delegate: nil, cancelButtonTitle: "OK")
        alert.show()
    }
    
    class func getUserIDFromDefaults() -> String
    {
        if let data=UserDefaults.standard.value(forKey: "userData")
        {
            //let userDictionary:NSDictionary=UserDefaults.standard.value(forKey: "userData") as! NSDictionary
            
            let userDictionary: Dictionary? = NSKeyedUnarchiver.unarchiveObject(with: data as! Data) as? [String : Any]
            
            let userId = userDictionary!["userId"]as! Int
            return "\(userId)"
        }
        else
        {
            return ""
        }
    }
    class func getUserNameFromDefaults() -> String {
        let userDictionary:NSDictionary=UserDefaults.standard.value(forKey: "userData") as! NSDictionary
        return userDictionary["fullname"]as! String
    }
    class func getUserTypeFromDefaults() -> String {
        let userDictionary:NSDictionary=UserDefaults.standard.value(forKey: "userData") as! NSDictionary
        return userDictionary["userType"]as! String
    }
    
    
    class func addShadow(view:UIView)
    {
        view.layer.masksToBounds = false;
        view.layer.shadowColor=UIColor.darkGray.cgColor
        view.layer.shadowOffset=CGSize.init(width: -2, height: -2)
        view.layer.shadowRadius = 5;
        view.layer.shadowOpacity = 0.5;
    }
    class func setShadowtable(view:UITableView)
    {
        view.layer.masksToBounds = false;
        view.layer.shadowColor=UIColor.darkGray.cgColor
        view.layer.shadowOffset=CGSize.init(width: -2, height: -2)
        view.layer.shadowRadius = 5;
        view.layer.shadowOpacity = 0.5;
    }
    
    class func SetBoundryButton(btn:UIButton)
    {
        btn.layer.borderColor = UIColor.darkGray.cgColor
        btn.layer.borderWidth = 1.0
    }
    class func SetBoundryLabel(lbl:UILabel)
    {
        lbl.layer.borderColor = UIColor.darkGray.cgColor
        lbl.layer.borderWidth = 1.0
    }
    
    func imageArrayToNSData(array: [UIImage],boundary:String) -> NSData
    {
        let body = NSMutableData()
        var i = 0;
        for image in array
        {
            let filename = "image\(i).jpg"
            let data = UIImageJPEGRepresentation(image,0.8);
            let mimetype = "image/jpeg"
            let key = "images[]"
            body.append("--\(boundary)\r\n".data(using: String.Encoding.utf8)!)
            body.append("--\(boundary)\r\n".data(using: String.Encoding.utf8)!)
            body.append("Content-Disposition: form-data; name=\"\(key)\"; filename=\"\(filename)\"\r\n".data(using: String.Encoding.utf8)!)
            body.append("Content-Type: \(mimetype)\r\n\r\n".data(using: String.Encoding.utf8)!)
            body.append(data!)
            body.append("\r\n".data(using: String.Encoding.utf8)!)
            i += 1
        }
        body.append("--\(boundary)--\r\n".data(using: String.Encoding.utf8)!)
        return body
    }
    
    func convertToDictionary(text: String) -> [String: Any]? {
        if let data = text.data(using: .utf8) {
            do {
                return try JSONSerialization.jsonObject(with: data, options: []) as? [String: Any]
            } catch {
                print(error.localizedDescription)
            }
        }
        return nil
    }
    
    class func addshadow_view(view: UIView , shadow_radius: CGFloat, shadow_color: UIColor,shadow_opecity: Float, shadow_offset: CGSize, is_clipBound : Bool, corner_radius:CGFloat )
    {
        view.layer.cornerRadius = corner_radius
        view.layer.shadowColor = shadow_color.cgColor
        view.layer.shadowOpacity = shadow_opecity
        view.layer.shadowRadius = shadow_radius
        view.layer.shadowOffset = shadow_offset
        view.clipsToBounds = false
    }
    class func addshadow_button(btn: UIButton , shadow_radius: CGFloat, shadow_color: UIColor,shadow_opecity: Float, shadow_offset: CGSize, is_clipBound : Bool, corner_radius:CGFloat )
    {
        btn.layer.cornerRadius = corner_radius
        btn.layer.shadowColor = shadow_color.cgColor
        btn.layer.shadowOpacity = shadow_opecity
        btn.layer.shadowRadius = shadow_radius
        btn.layer.shadowOffset = shadow_offset
        btn.clipsToBounds = false
    }
    class func addshadow_imageview(view: UIView , shadow_radius: CGFloat, shadow_color: UIColor,shadow_opecity: Float, shadow_offset: CGSize, is_clipBound : Bool, corner_radius:CGFloat )
    {
        view.layer.cornerRadius = corner_radius
        view.layer.shadowColor = shadow_color.cgColor
        view.layer.shadowOpacity = shadow_opecity
        view.layer.shadowRadius = shadow_radius
        view.layer.shadowOffset = shadow_offset
        view.clipsToBounds = false
    }
    
    class func change_date_format(date_str:String ,current_format: String ,new_format:String) -> String
    {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = current_format
        guard let newdate = dateFormatter.date(from: date_str) else {
            fatalError("ERROR: Date conversion failed due to mismatched format.")
        }
        dateFormatter.dateFormat = new_format
        return dateFormatter.string(from: newdate)
    }
    
}
