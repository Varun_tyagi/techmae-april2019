//
//  Use this file to import your target's public headers that you would like to expose to Swift.
//


#ifndef Techmea_Bridging_Header_h
#define Techmea_Bridging_Header_h



#import <ProjectOxfordFace/MPOFaceServiceClient.h>
#import <JTCalendar/JTCalendar.h>
#import <CropViewController/TOCropViewController.h>

#import <Quickblox/Quickblox.h>

#import <QMServices/QMServices.h>
//#import "UIAlertDialog.h"

#import <QMChatViewController/QMChatViewController.h>
#import "QMMessageNotification.h"
#import "QMMessageNotificationManager.h"
#import "UIImage+fixOrientation.h"
#import "TLTagsControl.h"
#import "CustomSliderView.h"
#import <linkedin-sdk/LISDK.h>
#import "DGActivityIndicatorView.h"
#endif
