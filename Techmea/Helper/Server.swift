
import UIKit
import Foundation
import CryptoSwift
import Alamofire

class Server: NSObject {
    
    class func postMultiPartMultiImage(param: [String:Any], urlStr: String , imageArray:[UIImage] , imageNameArray: [String], completion: @escaping (_ response: Dictionary <String, Any>) -> Void ) {
        
        var newHeaders = HTTPHeaders()

        if urlStr.contains(MethodName.createPost){
            newHeaders["Content-Type"] = "application/json"
        }
        
        print(newHeaders)
        print(param)
        Alamofire.upload(multipartFormData:{ multipartFormData in
            for (key, value) in param {
                multipartFormData.append("\(value)".data(using: String.Encoding.utf8)!, withName: key as String)
            }
            
            var i=0
            for img in imageArray
            {
                if let data = UIImagePNGRepresentation(img)
                {
                    if urlStr.contains(MethodName.createPost)
                    {
                        multipartFormData.append(data, withName:"url[]" , fileName: imageNameArray[i], mimeType: "image/png")
                    }
                    else
                    {
                        multipartFormData.append(data, withName:"image[]" , fileName: imageNameArray[i], mimeType: "image/png")
                        
                    }
                }
                i = i + 1
            }
            
            
            
            
            
            
            
        }, usingThreshold:UInt64.init(),  to:urlStr,  method:.post,
           headers:newHeaders,
           encodingCompletion: { encodingResult in
            switch encodingResult {
            case .success(let upload, _, _):
                upload.responseJSON { response in
                    do {
                        if let json = try JSONSerialization.jsonObject(with: response.data!, options: .mutableContainers) as? [String: Any] {
                            print(json)
                            completion(json as Dictionary<String, Any>)
                        }else{
                            print(response.result)
                        }
                    }
                    catch let error {
                        print(error.localizedDescription)
                        completion([:])
                    }
                    
                }
            case .failure(let encodingError):
                print(encodingError)
                completion([:])
            }
        })
        
        
    }
    
    
    
    class func postMultiPartMultiImage(param: [String:Any], urlStr: String , imageArray:[UIImage], videoArray:[URL],completion: @escaping (_ response: Dictionary <String, Any>) -> Void ) {
        
        print(imageArray)
        print(videoArray)
        
        var newHeaders = HTTPHeaders()
        
        if urlStr.contains(MethodName.createPost){
            newHeaders["Content-Type"] = "application/json"
        }
        
        print(newHeaders)
        print(param)
        Alamofire.upload(multipartFormData:{ multipartFormData in
            for (key, value) in param {
                multipartFormData.append("\(value)".data(using: String.Encoding.utf8)!, withName: key as String)
            }

            let timestamp = NSDate().timeIntervalSince1970
            if(imageArray.count>0)
            {
                for img in imageArray
                {
                    
                    if let data = UIImagePNGRepresentation(img)
                    {
                        multipartFormData.append(data, withName:"url[]" , fileName: String(timestamp+1)+".png", mimeType: "image/png")
                    }
                }
            }
            if(videoArray.count>0)
            {
                for video in videoArray
                {
                    multipartFormData.append(video, withName: "url[]", fileName: String(timestamp+1)+".mp4", mimeType: "video/mp4")
                }
            }         
            
        }, usingThreshold:UInt64.init(),  to:urlStr,  method:.post,
           headers:newHeaders,
           encodingCompletion: { encodingResult in
            switch encodingResult {
            case .success(let upload, _, _):
                upload.responseJSON { response in
                    do {
                        if let json = try JSONSerialization.jsonObject(with: response.data!, options: .mutableContainers) as? [String: Any] {
                            print(json)
                            completion(json as Dictionary<String, Any>)
                        }else{
                            print(response.result)
                        }
                    }
                    catch let error {
                        print(error.localizedDescription)
                        completion([:])
                    }
                    
                }
            case .failure(let encodingError):
                print(encodingError)
                completion([:])
            }
        })
        
        
    }
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    static func getRequestWithURL(_ urlString: String, completionHandler:@escaping (_ response: [String:Any]?) -> Void) {
        
        let defaultConfigObject = URLSessionConfiguration.default
        let defaultSession = URLSession(configuration: defaultConfigObject, delegate: nil, delegateQueue: nil)
        
        let urlRequest = NSMutableURLRequest(url: NSURL(string: urlString)! as URL)
        urlRequest.httpMethod = "GET"
        urlRequest.timeoutInterval = 100.0
        
        urlRequest.addValue("application/json", forHTTPHeaderField: "Accept")
        urlRequest.addValue("application/json", forHTTPHeaderField: "Content-Type")
        urlRequest.addValue(Constants.AuthorisationStr, forHTTPHeaderField: "Authorization")
        
        let task = defaultSession.dataTask(with: urlRequest as URLRequest) { ( data, response, error) -> Void in
            
            if error != nil {
                print("Error occurred: "+(error?.localizedDescription)!)
                completionHandler(nil)
                return
            }
            do {
                let responseObjc = try JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions.mutableLeaves)
                if let responseO = responseObjc as? [String:AnyObject]{
                    responseO.printJson()
                    completionHandler(responseO)
                    return
                }
                if let responseO = responseObjc as? [Any]{
                    responseO.printJson()
                    completionHandler(["data":responseO as AnyObject])
                    return
                }
                if responseObjc is String{
                    completionHandler(nil)
                    return
                }
                if responseObjc is Int{
                    completionHandler(nil)
                    return
                }
                
            }
            catch {
                print("Error occurred parsing data: \(error)")
                completionHandler(nil)
            }
        }
        task.resume()
    }
    
    static  func PostDataInDictionary(_ urlString: String, paramString: [String:Any], completionHandler:@escaping (_ response: Dictionary <String, Any>?) -> Void)
    {
        
        
        var  request = URLRequest(url: URL(string: urlString)!,
                                  cachePolicy: .useProtocolCachePolicy,
                                  timeoutInterval: 10.0)
        request.httpMethod = "POST"
        var newHeaders = headers
        if let userId = UserDefaults.standard.value(forKey: Constants.USERID) as? String{
            newHeaders["user_id"] = userId
        }
//        if urlString.contains(MethodName.createPost){
//            newHeaders["Content-Type"] = "application/x-www-form-urlencoded"
//        }
        print(newHeaders)
        print(paramString)
        let postData =  try? JSONSerialization.data(withJSONObject: paramString, options: [])
        
        request.allHTTPHeaderFields = newHeaders
        request.httpBody =  postData
        
        let session = URLSession.shared
        let dataTask = session.dataTask(with: request as URLRequest, completionHandler: { data, response, error in
            
            guard error == nil else {
                completionHandler([:])
                return
            }
            guard let data = data else {
                completionHandler([:])
                return
            }
            do {
                if let json = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? [String: Any] {
                    json.printJson()
                    completionHandler(json as Dictionary<String, Any>)
                }
            }
            catch let error {
                print(error.localizedDescription)
                completionHandler([:])
            }
        })
        
        dataTask.resume()
        
    }
    
    static  func PostDataHomeInDictionary(_ urlString: String, paramString: String, completionHandler:@escaping (_ response: Dictionary <String, Any>?) -> Void)
    {
        
        
        let url:NSURL = NSURL(string: urlString)!
        let session = URLSession.shared
        
        let request = NSMutableURLRequest(url: url as URL)
        request.httpMethod = "POST"
        
        
        let paramString = paramString
        request.httpBody = paramString.data(using: String.Encoding.utf8)
        
        
        let task = session.downloadTask(with: request as URLRequest) {
            (
            location,  response,  error) in
            
            
            let urlContents = try! NSString(contentsOf: location!, encoding: String.Encoding.utf8.rawValue)
            
            guard let _:NSString = urlContents else {
                print("error")
                return
            }
            
        }
        
        task.resume()
        
    }
    
    class func convertToDictionary(text: String) -> [String: Any]? {
        if let data = text.data(using: .utf8) {
            do {
                return try JSONSerialization.jsonObject(with: data, options: []) as? [String: Any]
            } catch {
                print(error.localizedDescription)
            }
        }
        return nil
    }
    
    static  func PUTDataInDictionary(_ urlString: String, dateString: String, source : String, paramterDic: Dictionary <String, Any>, completionHandler:@escaping (_ response: Dictionary <String, Any>) -> Void) {
        
        let defaultConfigObject = URLSessionConfiguration.default
        let defaultSession = URLSession(configuration: defaultConfigObject, delegate: nil, delegateQueue: nil)
        
        let signature = source.sha256()
        
        let urlRequest = NSMutableURLRequest(url: NSURL(string: urlString)! as URL)
        urlRequest.httpMethod = "PUT"
        urlRequest.timeoutInterval = 100.0
        
        urlRequest.addValue("application/json", forHTTPHeaderField: "Accept")
        urlRequest.addValue("application/json", forHTTPHeaderField: "Content-Type")
        urlRequest.addValue(dateString, forHTTPHeaderField: "X-Liquid-Timestamp")
        urlRequest.addValue(signature, forHTTPHeaderField: "X-Liquid-Signature")
        
        do {
            let jsonData = try JSONSerialization.data(withJSONObject: paramterDic, options: .prettyPrinted)
            urlRequest.httpBody = jsonData
            print(jsonData)
        }
        catch {
            print(error)
        }
        
        let task = defaultSession.dataTask(with: urlRequest as URLRequest, completionHandler: { data, response, error in
            
            guard error == nil else {
                completionHandler([:])
                return
            }
            guard let data = data else {
                completionHandler([:])
                return
            }
            do {
                if let json = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? [String: Any] {
                    json.printJson()
                    completionHandler(json as Dictionary<String, Any>)
                }
            }
            catch let error {
                print(error.localizedDescription)
                completionHandler([:])
            }
        })
        
        task.resume()
    }
    
}






