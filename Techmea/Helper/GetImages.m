//
//  GetImages.m
//  Techmea
//
//  Created by VISHAL SETH on 9/4/18.
//  Copyright © 2018 VISHAL SETH. All rights reserved.
//

#import "GetImages.h"
#import <MobileCoreServices/MobileCoreServices.h>
#import <AssetsLibrary/AssetsLibrary.h>
#import <UIKit/UIKit.h>

@implementation GetImages

-(void)setArray:(NSArray *)info
{
        //NSMutableArray *images = [NSMutableArray arrayWithCapacity:[info count]];
        self.arrImages=[NSMutableArray arrayWithCapacity:[info count]];
        for (NSDictionary *dict in info)
        {
            if ([dict objectForKey:UIImagePickerControllerMediaType] == ALAssetTypePhoto)
            {
                if ([dict objectForKey:UIImagePickerControllerOriginalImage])
                {
                    UIImage* image=[dict objectForKey:UIImagePickerControllerOriginalImage];
                    [_arrImages addObject:image ]  ;
                    
                }
                else if ([dict objectForKey:UIImagePickerControllerMediaType] == ALAssetTypeVideo)
                {
                    if ([dict objectForKey:UIImagePickerControllerOriginalImage])
                    {
                        UIImage* image=[dict objectForKey:UIImagePickerControllerOriginalImage];
                        [_arrImages addObject:image]  ;
                    }
                    else
                    {
                        NSLog(@"UIImagePickerControllerReferenceURL = %@", dict);
                    }
                }
                else
                {
                    NSLog(@"Uknown asset type");
                }
        }
    }
}

- (UIImage *)imageWithImage:(UIImage *)image convertToSize:(CGSize)size {
    UIGraphicsBeginImageContext(size);
    [image drawInRect:CGRectMake(0, 0, size.width, size.height)];
    UIImage *destImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return destImage;
}

-(NSArray*)getArray
{
    return self.arrImages;
}
@end
