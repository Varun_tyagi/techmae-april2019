//
//  SLApiHelper.swift
//  marblestore
//
//  Created by VISHAL SETH on 7/1/18.
//  Copyright © 2018 VISHAL SETH. All rights reserved.
//

import UIKit
import Alamofire
class TMApiHelper: NSObject {
    class func apiCall(serviceName: String,param: Any?,showLoader: Bool? = nil,
                       completionClosure: @escaping(NSDictionary?,Error?) ->())
    {
        
        if CommonFunction.isInternetAvailable()
        {
            var isShowLoader=true
            if let show = showLoader
            {
                if show == false
                {
                    isShowLoader=false
                }
                else
                {
                    isShowLoader=true
                }
            }
            
            if isShowLoader
            {
                CommonFunction.startLoader(title: "Loading...")
            }
            
            var paramValues:Parameters?
            paramValues = param as? Parameters
            
            
            print("REQUEST URL :: \(serviceName)")
            print("REQUEST PARAMETERS :: \(String(describing: paramValues))")
            
            
            var newHeaders = headers
            if let userId = UserDefaults.standard.value(forKey: Constants.USERID) as? String{
                newHeaders["user_id"] = userId
            }
            print(newHeaders)
            // Remove above code if you dont want to Header
            let manager = Alamofire.SessionManager.default
            manager.session.configuration.timeoutIntervalForRequest = 30
            
            manager.request(serviceName,method:.post,parameters:paramValues,encoding: JSONEncoding.default,headers:newHeaders).responseJSON { response in
                
                CommonFunction.stopLoader()
                switch response.result
                {
                case .success:
                    
                    if let JSON = response.result.value{
                        
                        //debugPrint("Repsonse::\(JSON)");
                        
                        DispatchQueue.main.async
                        {
                            let responseJson=JSON as? NSDictionary
                            completionClosure(responseJson, nil)
                        }
                    }
                    
                case .failure(let error):
                    print(error)
                    //CommonFunction.ShowError(message: error.localizedDescription)
                    
                }
                
            }
            
        }
        
    }
    
    // GET API CALLING
    class func getApiCall(serviceName: String,param: Any?,showLoader: Bool? = nil,
                       completionClosure: @escaping(NSDictionary?,Error?) ->())
    {
        
        if CommonFunction.isInternetAvailable()
        {
            var isShowLoader=true
            if let show = showLoader
            {
                if show == false
                {
                    isShowLoader=false
                }
                else
                {
                    isShowLoader=true
                }
            }
            
            if isShowLoader
            {
                CommonFunction.startLoader(title: "Loading...")
            }
            
            var paramValues:Parameters?
            paramValues = param as? Parameters
            
            print("REQUEST URL :: \(serviceName)")
            print("REQUEST PARAMETERS :: \(String(describing: paramValues))")
            
            
            var newHeaders = [
                "Token": "31528198109743225ff9d0cf04d1fdd1"
            ]
//            if let userId = UserDefaults.standard.value(forKey: Constants.USERID) as? String{
//                newHeaders["user_id"] = userId
//            }
            
            //newHeaders["Token"] = MethodName.Token
            print(newHeaders)

            // Remove above code if you dont want to Header
            let manager = Alamofire.SessionManager.default
            manager.session.configuration.timeoutIntervalForRequest = 30
            
            manager.request(serviceName,method:.get,parameters:nil,encoding: URLEncoding.default,headers:newHeaders).responseJSON { response in
                
                CommonFunction.stopLoader()
                switch response.result
                {
                case .success:
                    
                    if let JSON = response.result.value{
                        
                        //debugPrint("Repsonse::\(JSON)");
                        
                        DispatchQueue.main.async
                            {
                                let responseJson=JSON as? NSDictionary
                                completionClosure(responseJson, nil)
                        }
                    }
                    
                case .failure(let error):
                    print(error)
                    //CommonFunction.ShowError(message: error.localizedDescription)
                    
                }
                
            }
            
        }
        
    }
}
