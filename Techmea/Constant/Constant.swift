  
    import Foundation
    import UIKit
    import Alamofire

  let auth_header: HTTPHeaders = ["Content-Type":"application/json","Token":"31528198109743225ff9d0cf04d1fdd1"]
  
  func getBaseURL()-> String
  {
    #if DEBUG
    // I'm running in DEBUG mode
    return "http://18.223.116.233/stagging/api/web/v1/" // Staging
    #else
    // I'm running in a non-DEBUG mode
    return "http://18.223.116.233/api/web/v1/" // Production
    #endif
  }
    struct ScreenSize {
        static let SCREEN               = UIScreen.main.bounds
        static let SCREEN_WIDTH         = UIScreen.main.bounds.size.width
        static let SCREEN_HEIGHT        = UIScreen.main.bounds.size.height
        static let SCREEN_MAX_LENGTH    = max(ScreenSize.SCREEN_WIDTH, ScreenSize.SCREEN_HEIGHT)
        static let SCREEN_MIN_LENGTH    = min(ScreenSize.SCREEN_WIDTH, ScreenSize.SCREEN_HEIGHT)
    }

    struct DeviceType {
        static let IS_IPHONE_4_OR_LESS  = UIDevice.current.userInterfaceIdiom == .phone && ScreenSize.SCREEN_MAX_LENGTH < 568.0
        static let IS_IPHONESE          = UIDevice.current.userInterfaceIdiom == .phone && ScreenSize.SCREEN_MAX_LENGTH == 568.0
        static let IS_IPHONE8           = UIDevice.current.userInterfaceIdiom == .phone && ScreenSize.SCREEN_MAX_LENGTH == 667.0
        static let IS_IPHONE8PLUS       = UIDevice.current.userInterfaceIdiom == .phone && ScreenSize.SCREEN_MAX_LENGTH == 736.0
        static let IS_IPHONE_X          = UIDevice.current.userInterfaceIdiom == .phone && ScreenSize.SCREEN_MAX_LENGTH == 812.0
        static let IS_IPAD              = UIDevice.current.userInterfaceIdiom == .pad
    }

    struct iOSVersion {
        static let SYS_VERSION_FLOAT = (UIDevice.current.systemVersion as NSString).floatValue
        static let iOS7 = (iOSVersion.SYS_VERSION_FLOAT < 8.0 && iOSVersion.SYS_VERSION_FLOAT >= 7.0)
        static let iOS8 = (iOSVersion.SYS_VERSION_FLOAT >= 8.0 && iOSVersion.SYS_VERSION_FLOAT < 9.0)
        static let iOS9 = (iOSVersion.SYS_VERSION_FLOAT >= 9.0 && iOSVersion.SYS_VERSION_FLOAT < 10.0)
        static let iOS10 = (iOSVersion.SYS_VERSION_FLOAT >= 10.0 && iOSVersion.SYS_VERSION_FLOAT < 11.0)
        static let iOS11 = (iOSVersion.SYS_VERSION_FLOAT >= 11.0 && iOSVersion.SYS_VERSION_FLOAT < 12.0)
    }


    struct DeviceOrientation {
        static let IS_PORTRAIT = UIDevice.current.orientation.isPortrait
        static let IS_LANDSCAPE = UIDevice.current.orientation.isLandscape
    }

  struct  StoryBoardName {
    static let  signupSecond = "SignupSecondViewController"
    static let login = "LoginViewController"
  }
    struct Constants {

        static let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main",bundle: nil)
        static let mainStoryboard2: UIStoryboard = UIStoryboard(name: "Main2",bundle: nil)

        static let APP_NAME = "TechMae"
        static let appDelegate = UIApplication.shared.delegate as! AppDelegate
        static let imageBaseURL = "http://18.223.116.233/stagging/backend/web/index.php?r=site/pic&"

        static let BASEURL = "http://18.223.116.233/stagging/api/web/v1/"//getBaseURL()
        
        static let DateFormate = "yyyy-MM-dd HH:mm:ss"
        static let AuthorisationStr = "Basic dGFwZG9jOk5ldXJvQDEyMw=="
        
        static let Device_UUID = UIDevice.current.identifierForVendor?.uuidString.replacingOccurrences(of: "-", with: "")
        static let AuthenticUser = "UserAuthenticated"
        
        static let USERID = "UserId"
        static let KDeviceToken = "deviceToken"
        
        //ca-app-pub-4024721014848055/1568288651
        static let kFACEBOOK_AD_ID =
       // "356354691692959_356357841692644"
        //"1594119340664004_2362502727158991"
        "356354691692959_419021335426294"
        static let kGOOGLE_AD_ID =
        "ca-app-pub-8981694945335723/6450668257"
    }

  struct MethodName {
    static let getSplash = "getSplash"
    static let register = "home/register"
    static let socialRegister = "home/social-register"
    static let login = "home/login"
    static let socialLogin = "home/social-login"
    static let editProfile = "editProfile"
    static let updateProfileImage  = "updateProfileImage"
    static let forgotPassword = "forgotPassword"
    static let suggestedFriendList = "friend/suggested-friend-list"
    static let addFriendRequest = "friend/add-friend-request"
    static let registerLocation = "registerLocation"
    static let acceptFriendRequest = "friend/accept-friend-request"
    static let postList = "post/list"
    static let search = "user-info"
    static let uploadProfile = "user-info/upload-profile-pic"
    static let uploadPassport = "user-info/upload-passport-pic"
    static let likePost = "post/like-post"
    static let createPost = "post/create-post"
    //Edit Post
    //post/update-post
    static let editPost = "post/update-post"
    // edit group post
    static let editGroupPost = "group-posts"
    
    static let commentPost = "post/comment"
    static let deletePost = "post/delete"
    static let Token = "31528198109743225ff9d0cf04d1fdd1"
    static let getUserInfo = "home/get-user-info"
    static let getFriendlist = "friend/friend-list"
    static let Unfriend = "friend/un-friend"
    static let cancelFriendRequest = "friend/cancel-friend-request"
    static let friendLike = "friend-likes"
    static let getPendingRequest = "friend/pending-friend-list"
    static let getLikeList = "friend-likes"
    
    //END POINT : BASE_URL + post/post-liked-user?access-token=xxx&id=post_id
    static let getLikeListNew = "post/post-liked-user"
    
    //getGroupLikeListNew
    //END POINT : BASE_URL + group-posts/post-liked-user?access-token=xxx&id=post_id

    static let getGroupLikeListNew = "group-posts/post-liked-user"

    
    static let groupPostDelete = "group-posts/delete"
    static let groupPostComment = "group-post-comments"
    static let removeGroupMember = "group/remove-member"
    static let editGroup = "group/update-group"
    static let reportPost = "home/report-new"
    static let reportUSer = "home/report-profile"  //eport-profile"
    static let deleteGroup = "group/delete"
    static let searchGroup = "group/search-group"
    static let groupDetail = "group/group-detail"
    static let addMember = "group/add-member-group"
    static let createGroupPost = "group-posts"
    static let groupPostLike = "group-post-likes"
    static let groupList = "group/group-list"
    static let createGroup = "group/create-group"
    static let leaveGroup = "group/leave-member-group"
    static let joinGroup = "group/join"
    static let postLoadMoreComment = "comments"
    static let groupPostLoadMoreComment = "group-post-comments"
    static let profileState = "user-info/me"
    static let profileTimeline = "user-info/time-line"
    static let profileVideos = "media/video"
    static let profilePhotos = "media/photo"
    static let registerToken = "user-device-tokens"
    static let notificationList = "notification"
    static let bookCoach = "coach-bookings"
    static let acceptBooking = "coach-bookings/accept"
    static let cancelBooking = "coach-bookings/cancel"
    static let getUserBookingList = "coach-bookings/my-booking"
    static let getCoachBookingList = "coach-bookings"
    static let closeNotification = "notifications/close-notification"
    static let aboutus = "pages/slug"
    static let fourmQuestionList = "forum-questions"
    static let answerFourmQuestion = "forum-question-answers"
    static let likeUnlikeFourmQuestion = "forum-questions/like"
    static let surveyQuestion = "survey-question"
    static let answerSurveyQuestion = "survey-answers"
    static let readunread = "notification/read"
    static let deactivateAccount = "home/deactive-account"
    static let unreadNotificationCount = "notification/unread-notification"
    static let report = "home/report"
    static let checkaccess = "home/check-access"
    static let pinpost = "group-posts/pin-post"
    
    //BASE_URL + post/view?access-token=xxxx&id=3&expand=postMediaView
    //            let parameterString = ["access-token" : "\(UserDefaults.standard.value(forKey: "Access_Token") as! String)","page" : current_page] as [String : Any]

    static func  getPostDetailView(postId:String) -> String{
     return   "post/view?access-token=\(UserDefaults.standard.value(forKey: "Access_Token") as! String)&id=\(postId)&expand=postMediaView"
    }
    
    //END POINT : BASE_URL + group-posts/view?access-token=xxxx&id=3&expand=postMediaView
    static func  getGroupPostDetailView(postId:String) -> String{
     return  "group-posts/view?access-token=\(UserDefaults.standard.value(forKey: "Access_Token") as! String)&id=\(postId)&expand=postMediaView"
    }
    
    
   // END POINT        : Base Url + friend/blocked-list?access-token=auth_key
   static func  getBlockList() -> String{
       return   "friend/blocked-list?access-token=\(UserDefaults.standard.value(forKey: "Access_Token") as! String)"
      }


    //END POINT        : Base Url + friend/unblock?access-token=auth_key
    //{"user_id":"10"}
    static func  unblockUser() -> String{
     return   "friend/unblock?access-token=\(UserDefaults.standard.value(forKey: "Access_Token") as! String)"
        }
    
    //END_POINT        : Base Url + post/comment-update?access-token=auth_key&id=postId
    static func editPostComment(postId:String) -> String{
        return "post/comment-update?access-token=\(UserDefaults.standard.value(forKey: "Access_Token") as! String)&id=\(postId)"
    }
    //Base Url + group-post-comments/update-comment?access-token=auth_key&id=56

    static func editGroupComment(postId:String) -> String{
           return "group-post-comments/update-comment?access-token=\(UserDefaults.standard.value(forKey: "Access_Token") as! String)&id=\(postId)"
       }
    
    //post/view?access-token=xxxx&id=3&expand=postMediaView
    static func singlePostDetail(postId:String) -> String{
        return "post/view?access-token=\(UserDefaults.standard.value(forKey: "Access_Token") as! String)&id=\(postId)&expand=postMediaView"

    }

    //END POINT : BASE_URL + group-posts/view?access-token=xxxx&id=3&expand=postMediaView
    static func singleGroupPostDetail(postId:String) -> String{
           return "group-posts/view?access-token=\(UserDefaults.standard.value(forKey: "Access_Token") as! String)&id=\(postId)&expand=postMediaView"

       }

  }


    struct Colors {
       
        static let borderColor = #colorLiteral(red: 1, green: 0.7450980392, blue: 0.8, alpha: 1)
        static let textColor = #colorLiteral(red: 0.9960784314, green: 0.8941176471, blue: 0.9137254902, alpha: 1)
     }

    struct FONTS {
        static let Helvetica_Regular = "HelveticaNeue"
        static let Helvetica_Light = "HelveticaNeue-Light"
        static let Helvetica_Medium = "HelveticaNeue-Medium"
        static let Helvetica_Bold = "HelveticaNeue-Bold"
        static let Helvetica_Thin = "HelveticaNeue-Thin"
        static let Helvetica_UltraLight = "HelveticaNeue-UltraLight"
        static let Helvetica_Italic = "HelveticaNeue-Italic"
        static let Helvetica_LightItalic = "HelveticaNeue-LightItalic"
        static let Helvetica_MediumItalic = "HelveticaNeue-MediumItalic"
        static let Helvetica_ThinItalic = "HelveticaNeue-ThinItalic"
        static let Helvetica_UltraLightItalic = "HelveticaNeue-UltraLightItalic"
        static let Helvetica_BoldItalic = "HelveticaNeue-BoldItalic"
        static let Helvetica_CondensedBlack = "HelveticaNeue-CondensedBlack"
        static let Helvetica_CondensedBold = "HelveticaNeue-CondensedBold"
        
        static let Brandon_Thin = "BrandonText-Thin"
        static let Brandon_Regular = "BrandonText-Regular"
        static let Brandon_Regulat_Italic = "BrandonText-RegularItalic"
        static let Brandon_Lightn = "BrandonText-Lightn"
        static let Brandon_Bold = "BrandonText-Bold"

        static let Avenir_Light = "Avenir-Light"
    }
  enum ModalAlertType: NSInteger {
    case modalAlert = 0 , modalAlertPicker, modalAlertSaved , modalAlertSelectImageSource
  }
  

  
 

