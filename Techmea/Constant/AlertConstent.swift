//
//  AlertConstent.swift
//  Techmae
//
//  Created by varun tyagi on 19/08/18.
//  Copyright © 2018 VISHAL SETH. All rights reserved.
//

import Foundation


struct AlertConstant
{
    static let SelectUserType         = "Please select login type"
    static let EnterFirstName = "Please enter your full name"
    static let EnterLastName = "Please enter your last name"
    static let EnterEmail = "Please enter your email"
    static let EnterValidEmail = "Please enter valid email"

    static let EnterPassword = "Please enter your password"
    static let PasswordTooShort = "Password needs to be of minimum length of 6 and maximum length of 12 characters and should have atleast 1 digit, 1 upppercase letter and 1 special character."
    static let EnterBirthday = "Please enter your birthday"
    static let EnterHourate = "Please enter your hourly rate"
    static let EnterAbout = "Please Enter Short Description."
    static let EnterProfession = "Please Enter Profession."
}
