//
//  AppDelegate.swift
//  Techmea
//
//  Created by VISHAL SETH on 8/4/18.
//  Copyright © 2018 VISHAL SETH. All rights reserved.
//

import UIKit
import FacebookLogin
import FacebookCore
import FBSDKCoreKit
import FBSDKLoginKit
import IQKeyboardManagerSwift
import UserNotificationsUI
import UserNotifications
import CoreLocation
import GoogleSignIn
import GooglePlaces
import TwitterKit
import TwitterCore
import Alamofire
import UserNotifications
import Firebase
import FirebaseInstanceID
import FirebaseMessaging
import Quickblox
import LinkedinSwift
import AppsFlyerLib

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate,GIDSignInDelegate,UNUserNotificationCenterDelegate,MessagingDelegate ,AppsFlyerTrackerDelegate{

    //MARK:- Outlets
    var window: UIWindow?
    let loc = LocationServiceManager()
    let kQBApplicationID:UInt = 74821
    let kQBAuthKey = "kJXEDU5L-L9MEcS"
    let kQBAuthSecret = "kfybWu5j673WRmY"
    let kQBAccountKey = "jLCGvwY-j8dKAdPGEPxz"
    func applicationDidBecomeActive(application: UIApplication) {
        // attribute Installs, updates & sessions(app opens)
        // (You must include this API to enable SDK functions)
        AppsFlyerTracker.shared().trackAppLaunch()
        // your other code here....
        
    }
    func onConversionDataReceived(_ installData: [AnyHashable: Any]) {
        //Handle Conversion Data (Deferred Deep Link)
    }
    
    func onConversionDataRequestFailure(_ error: Error?) {
        //    print("\(error)")
    }
    
    func onAppOpenAttribution(_ attributionData: [AnyHashable: Any]) {
        //Handle Deep Link Data
        
    }
    
    func onAppOpenAttributionFailure(_ error: Error?) {
    }
    // Reports app open from a Universal Link for iOS 9 or later
    func application(_ application: UIApplication, continue userActivity: NSUserActivity, restorationHandler: @escaping ([Any]?) -> Void) -> Bool {
        AppsFlyerTracker.shared().continue(userActivity, restorationHandler: restorationHandler)
        return true
    }
    
    // Reports app open from deep link from apps which do not support Universal Links (Twitter) and for iOS8 and below
   
    
    func application(_ application: UIApplication, open url: URL, sourceApplication: String?, annotation: Any) -> Bool {
        AppsFlyerTracker.shared().handleOpen(url, sourceApplication: sourceApplication, withAnnotation: annotation)

        var _: [String: AnyObject] = [UIApplicationOpenURLOptionsKey.sourceApplication.rawValue: sourceApplication as AnyObject,
                                            UIApplicationOpenURLOptionsKey.annotation.rawValue: annotation as AnyObject]
        return GIDSignIn.sharedInstance().handle(url as URL?,
                                                 sourceApplication: sourceApplication,
                                                 annotation: annotation)
    }
    
    // Reports app open from deep link for iOS 10 or later
//    func application(_ app: UIApplication, open url: URL, options: [UIApplicationOpenURLOptionsKey : Any] = [:]) -> Bool {
//        AppsFlyerTracker.shared().handleOpen(url, options: options)
//        return true
//    }
    //MARK:- didFinishLaunchingWithOptions
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        
        
        // aps flyer
        
        AppsFlyerTracker.shared().appsFlyerDevKey = "5H3znWvRrW9PucCK4dQ8N3"
        AppsFlyerTracker.shared().appleAppID = "1458730169"
        
        AppsFlyerTracker.shared().delegate = self

//Set isDebud to true to see AppsFlyer deb
        
        ///end
        UINavigationBar.appearance().backgroundColor = UIColor.init(red: 245/255, green: 0/255, blue: 62/255, alpha: 1.0)
        UINavigationBar.appearance().tintColor=UIColor.white
        UIBarButtonItem.appearance().tintColor = UIColor.white
        UINavigationBar.appearance().barTintColor = UIColor.init(red: 255/255, green: 64/255, blue: 97/255, alpha: 1.0)
        UINavigationBar.appearance().titleTextAttributes = [NSAttributedStringKey.foregroundColor: UIColor.white]
        
        IQKeyboardManager.shared.enable=true
        let center = UNUserNotificationCenter.current()
        center.requestAuthorization(options:[.badge, .alert, .sound]) { (granted, error) in
            // Enable or disable features based on authorization.
        }
        application.registerForRemoteNotifications()
       // loc.delegate=self
      //  loc.startUpdatingLocation()
//        LocationServices.shared.getAdress(28.644800,77.216721) { address, error in
//
//            if address != nil {
//             print(address ?? "")
//            }
//
//    }
        TWTRTwitter.sharedInstance().start(withConsumerKey:"XMy4xVAXyvdMFPnyjiTtuAyJZ", consumerSecret:"VOi8SNI1pxBy53GlGcASC6ABAjQbjoMNnmxH1n50p1IcitzSba")
        // Initialize sign-in
        GIDSignIn.sharedInstance().clientID = "982255449007-eh10kbenfgn76umm409h1utj3147kms9.apps.googleusercontent.com"
        GIDSignIn.sharedInstance().delegate = self
        GMSPlacesClient.provideAPIKey("AIzaSyBaJwfnnjGYJnRWki_mkOd_9jkbXjHft9U")
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        var navigationController = UINavigationController()
        if UserDefaults.standard.bool(forKey: "is_login")
        {
            let vc = storyboard.instantiateViewController(withIdentifier: "MainTabbarViewController")as! MainTabbarViewController
             navigationController = UINavigationController.init(rootViewController: vc)
            
        }
        else
        {
            
//            let viewController = storyboard.instantiateViewController(withIdentifier: "LoginRegisterViewController") as! LoginRegisterViewController
//            navigationController = UINavigationController.init(rootViewController: viewController)
            
    let viewController = storyboard.instantiateViewController(withIdentifier: "OnBoardingViewController") as! OnBoardingViewController
            navigationController = UINavigationController.init(rootViewController: viewController)
           
        }
        navigationController.setNavigationBarHidden(true, animated: false)
        self.window?.rootViewController = navigationController
        registerForRemoteNotification()
        Messaging.messaging().isAutoInitEnabled = true
        FirebaseApp.configure()
        
        // Set QuickBlox credentials (You must create application in admin.quickblox.com).
        QBSettings.applicationID = kQBApplicationID;
        QBSettings.authKey = kQBAuthKey
        QBSettings.authSecret = kQBAuthSecret
        QBSettings.accountKey = kQBAccountKey
        
        // enabling carbons for chat
        QBSettings.carbonsEnabled = true
        // Enables Quickblox REST API calls debug console output.
        QBSettings.logLevel = .debug
        
        // Enables detailed XMPP logging in console output.
        QBSettings.enableXMPPLogging()
        
                
        return true
    }
    
    func application(_ app: UIApplication, open url: URL, options: [UIApplicationOpenURLOptionsKey : Any] = [:]) -> Bool
    {
        AppsFlyerTracker.shared().handleOpen(url, options: options)

        options[UIApplicationOpenURLOptionsKey.annotation]
        if LinkedinSwiftHelper.shouldHandle(url) {
            return LinkedinSwiftHelper.application(app, open: url, sourceApplication: nil, annotation: nil)
        }
        
//        if LISDKCallbackHandler.shouldHandle(url){
//
//            return LISDKCallbackHandler.application(app, open: url, sourceApplication: nil, annotation: nil)
//        }
        
        if(TWTRTwitter.sharedInstance().application(app, open: url, options: options))
        {
            return true
        }
        
        if url.scheme == "fb1594119340664004"
        {
            return FBSDKApplicationDelegate.sharedInstance().application(app, open: url, sourceApplication: options[.sourceApplication] as! String, annotation: options[.annotation])
        }
        else
        {
            return GIDSignIn.sharedInstance().handle(url as URL?,
                                                     sourceApplication: options[UIApplicationOpenURLOptionsKey.sourceApplication] as? String,
                                                     annotation: options[UIApplicationOpenURLOptionsKey.annotation])
        }
        
        
    }
    
    
    func sign(_ signIn: GIDSignIn!, didSignInFor user: GIDGoogleUser!,
              withError error: Error!) {
        if let error = error {
            print("\(error.localizedDescription)")
        } else {
            // Perform any operations on signed in user here.
        
            // ...
        }
    }
    
    func sign(_ signIn: GIDSignIn!, didDisconnectWith user: GIDGoogleUser!,
              withError error: Error!) {
        
    }

    
     //MARK:- applicationWillResignActive
    func applicationWillResignActive(_ application: UIApplication) {
    }

    //MARK:- applicationDidEnterBackground
    func applicationDidEnterBackground(_ application: UIApplication) {
       
        application.applicationIconBadgeNumber = 0
        // Logging out from chat.
        ServicesManager.instance().chatService.disconnect(completionBlock: nil)
    }

    
    //MARK:- applicationWillEnterForeground
    func applicationWillEnterForeground(_ application: UIApplication) {
        
        // Logging in to chat.
        ServicesManager.instance().chatService.connect(completionBlock: nil)
    }

    //MARK:- applicationDidBecomeActive
    func applicationDidBecomeActive(_ application: UIApplication) {
    }

    //MARK:- applicationWillTerminate
    func applicationWillTerminate(_ application: UIApplication) {
        
        // Logging out from chat.
        ServicesManager.instance().chatService.disconnect(completionBlock: nil)
    }
    
    
    //MARK:- Notification Delegate Methods
    func application(received remoteMessage: MessagingRemoteMessage) {
        print(remoteMessage.appData)
    }
    
    func messaging(_ messaging: Messaging, didReceiveRegistrationToken fcmToken: String) {
        print(fcmToken)
        UserDefaults.standard.setValue(fcmToken, forKey: Constants.KDeviceToken)
    }
    
    func registerForRemoteNotification() {
        DispatchQueue.main.async {
            let center  = UNUserNotificationCenter.current()
            center.delegate = self
            center.requestAuthorization(options: [.sound, .alert, .badge]) { (granted, error) in
                
            }
            UIApplication.shared.registerForRemoteNotifications()
            Messaging.messaging().delegate = self
        }
        
    }
    func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        print("User Info = ",notification.request.content.userInfo)
        completionHandler([.alert, .badge, .sound])
    }
    
    func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Void) {
        print("User Info = ",response.notification.request.content.userInfo)
        completionHandler()
    }
    
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data)
    {
        let deviceTokenString = deviceToken.map { String(format: "%02.2hhx", $0) }.joined()
    }
    
    func application(_ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Error) {
        print("i am not available in simulator \(error)")
    }
    
    

    }





extension AppDelegate{
    //MARK:- UNIQUE Identifier
    static func getUniqueDeviceIdentifierAsString() -> String {
        
        let appUUID = UIDevice.current.identifierForVendor?.uuidString
        return appUUID!
    }
    
    static func getUniqueIdWithoutdash()  -> String{
        
        return self.getUniqueDeviceIdentifierAsString().replacingOccurrences(of: "-", with: "")
        
    }
    
    //MARK:- DEVICE Token
    static func getDeviceToken() -> String {
        var str = UserDefaults.standard.string(forKey: "deviceToken")
        if str == nil || (str?.isEmpty)! {
            str = "6aa2790400e270717622b325f412951bd8b3183c9b4a099c497681b8ea2dd8db"
        }
        return str!
    }
    
}
extension AppDelegate: LocationServiceDelegate{
    func tracingLocation(currentLocation: CLLocation) {
        print(currentLocation )
        LocationServiceManager.sharedLocation = loc
    }
    func tracingLocationDidFailWithError(error: NSError) {
        print("error lcoation")
    }
}







