//
//  Character.swift
//  UPCarouselFlowLayoutDemo
//
//  Created by Paul Ulric on 28/06/2016.
//  Copyright © 2016 Paul Ulric. All rights reserved.
//

import Foundation

struct ProfileCharacter {
    let imageName: String!
    let name: String!
    let movie: String!
}
