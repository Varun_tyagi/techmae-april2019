//
//  SuggestedFriendList.swift
//  Techmae
//
//  Created by varun tyagi on 20/08/18.
//  Copyright © 2018 VISHAL SETH. All rights reserved.
//

import Foundation
import Alamofire
import CoreLocation
typealias SuggestListCompletionHandler = (_ suggestList: SuggestedFriendModel?) -> Void


class SuggestedFriendList{
    
    class func getSuggestedFriendList(completionHandler: @escaping SuggestListCompletionHandler){
        //{"latitude":"","longitude":""}
        var params = [String:Any]()
        params["longitude"] = "\(LocationServiceManager.sharedLocation?.lastLocation?.coordinate.longitude ?? 27.000000)"
        params["latitude"] = "\(LocationServiceManager.sharedLocation?.lastLocation?.coordinate.latitude ?? 43.8979)"
        params["user_id"] = "\(UserDefaults.standard.value(forKey: Constants.USERID) as! Int)"
        
        let url = Constants.BASEURL + MethodName.suggestedFriendList
        print(params)
        print(url)
        Server.PostDataInDictionary(url, paramString:params) { (response) in
            if let feedJson = response?.json {
                let docModel = try? SuggestedFriendModel.init(feedJson)
                return completionHandler(docModel)
            }else{
                completionHandler(nil)
            }
        }
        
        
    }
}
