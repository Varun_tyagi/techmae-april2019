//
//  LoginNetworkManager.swift
//  Techmae
//
//  Created by VISHAL SETH on 8/19/18.
//  Copyright © 2018 VISHAL SETH. All rights reserved.
//

import Foundation
import Alamofire

typealias LoginSuccessComplitionHandler = (_ loginResponse: LoginModel?) -> Void

let headers = [
    "Content-Type": "application/json",
    "Accept": "application/json",
    "Token": "31528198109743225ff9d0cf04d1fdd1"
]

class LoginRegisterNetworkManager {
    
    class func loginUser(_ user : String ,_ email:String,_ password:String,  completionHandler: @escaping LoginSuccessComplitionHandler)
    {
        
        let url = Constants.BASEURL + MethodName.login
        print(url)
        
        
        
        /*
         {
         "userType":"user",
         "deviceId":"ezjdhhh455hh5jh565",
         "deviceType":"android",
         "email":"essasda@gmsil.com",
         "password":"abc@123"
         }
         */
        
        let param = ["userType":user,"email":email,"password":password, "deviceId":AppDelegate.getUniqueIdWithoutdash(),"device_token":UserDefaults.standard.value(forKey: Constants.KDeviceToken) ?? "fytfytfytffytf67r67rf", "deviceType":"ios"]
        print(param)
        Alamofire.request(URL.init(string: url)!, method: .post, parameters:param , encoding: JSONEncoding.default, headers: headers).responseLoginModel { response in
            if let loginModel = response.result.value
            {
                completionHandler(loginModel)
            }
            else{completionHandler(nil)}
        }
    }
}
