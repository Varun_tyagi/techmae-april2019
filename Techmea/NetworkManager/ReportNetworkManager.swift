//
//  ReportNetworkManager.swift
//  Techmea
//
//  Created by varun tyagi on 31/10/19.
//  Copyright © 2019 VISHAL SETH. All rights reserved.
//

import Foundation
import Alamofire
import UIKit
import MBProgressHUD

typealias  ReportCompletionHandler  = (_ flag: Bool) -> Void

class ReportNetworkManager{
    
    class  func ReportPost(message: String, postType:String , postId:String , reportType:String ,  reportImage:UIImage?,  controller:UIViewController, completion : @escaping ReportCompletionHandler){
      /*
         post_type (POST or GROUP)
         user_id
         post_id
         image,
         report_type ( value should 1 2 3 4 as
         */

        let parameters = [
            "message": message,
            "post_type": postType,
                           "user_id" : "\(UserDefaults.standard.value(forKey: Constants.USERID) as! Int)",
            "post_id":postId,
               // image,
            "report_type":reportType] as [String : Any]
        let timestamp = NSDate().timeIntervalSince1970
        
        DispatchQueue.main.async {
        MBProgressHUD.showAdded(to: controller.view, animated: true)
        }
        
        Alamofire.upload(multipartFormData: { (multipartFormData) in
        
            if let ReportedImage = reportImage{ multipartFormData.append(UIImageJPEGRepresentation(ReportedImage, 0.5)!, withName: "image", fileName: "\(timestamp).jpeg", mimeType: "image/jpeg")
            }
            for (key, value) in parameters {
                multipartFormData.append((value as AnyObject).data(using: String.Encoding.utf8.rawValue)!, withName: key)
            }
        }, to:Constants.BASEURL+MethodName.reportPost + "?access-token=\(UserDefaults.standard.value(forKey: "Access_Token") as! String)",method: .post, headers: ["Content-type": "multipart/form-data"])
        { (result) in
            switch result {
            case .success(let upload, _, _):
                
                upload.uploadProgress(closure: { (progress) in
                    //Print progress
                   // print(progress)
                })
                
                upload.responseJSON { response in
                    
                    DispatchQueue.main.async {
                    MBProgressHUD.hide(for: controller.view, animated: true)
                    }
                    if let result = response.result.value as? [String:Any]
                    {
                        print(result)
             
                        if let rcode = result["code"] as? Int
                        {
                            if rcode == 200
                            {
                                print("Success  \(response.result)")
                                DispatchQueue.main.async {
                                if let error_arr = result["data"] as? [String:Any] , let message = error_arr["message"] as? String
                                {
                                    
                                        DispatchQueue.main.async {
                                        controller.view.makeToast(message)
                                        }
                                    
                                }
                                    
                                }
                               completion(true)
                            }
                            else
                            {
                                if let error_arr = result["errorData"] as? [[String:Any]]
                                {
                                    if let error_msg = error_arr[0]["message"] as? String
                                    {
                                        DispatchQueue.main.async {
                                        controller.view.makeToast(error_msg)
                                        }
                                    }
                                }
                                completion(false)

                            }
                        }
                    }
                }
                
            case .failure(let encodingError):
                DispatchQueue.main.async {
                controller.view.makeToast(encodingError.localizedDescription)
                }
                break
            }
        }
    }
    
    
    class  func BlockUser(message:String, userId:String ,  controller:UIViewController, completion : @escaping ReportCompletionHandler){
      /*
        message
         user_id

         */

        let parameters = [ "message": message,
                           "user_id" : userId,
           ] as [String : Any]
       // let timestamp = NSDate().timeIntervalSince1970
        
        DispatchQueue.main.async {
        MBProgressHUD.showAdded(to: controller.view, animated: true)
        }
        
        Alamofire.upload(multipartFormData: { (multipartFormData) in
        
//            if let ReportedImage = reportImage{ multipartFormData.append(UIImageJPEGRepresentation(ReportedImage, 0.5)!, withName: "image", fileName: "\(timestamp).jpeg", mimeType: "image/jpeg")
//            }
            for (key, value) in parameters {
                multipartFormData.append((value as AnyObject).data(using: String.Encoding.utf8.rawValue)!, withName: key)
            }
        }, to:Constants.BASEURL+MethodName.reportUSer + "?access-token=\(UserDefaults.standard.value(forKey: "Access_Token") as! String)",method: .post, headers: ["Content-type": "multipart/form-data"])
        { (result) in
            switch result {
            case .success(let upload, _, _):
                
                upload.uploadProgress(closure: { (progress) in
                    //Print progress
                   // print(progress)
                })
                
                upload.responseJSON { response in
                    
                    DispatchQueue.main.async {
                    MBProgressHUD.hide(for: controller.view, animated: true)
                    }
                    if let result = response.result.value as? [String:Any]
                    {
                        print(result)
             
                        if let rcode = result["code"] as? Int
                        {
                            if rcode == 200
                            {
                                print("Success  \(response.result)")
                                DispatchQueue.main.async {
                                    if let responseData = result["data"] as? [String:Any] , let message = responseData["message"] as? String{
                                        UIApplication.shared.windows.first?.rootViewController!.view.makeToast(message)
                                    }
                                }
                               completion(true)
                            }
                            else
                            {
                                if let error_arr = result["errorData"] as? [[String:Any]]
                                {
                                    if let error_msg = error_arr[0]["message"] as? String
                                    {
                                        DispatchQueue.main.async {
                                        controller.view.makeToast(error_msg)
                                        }
                                    }
                                }
                                completion(false)

                            }
                        }
                    }
                }
                
            case .failure(let encodingError):
                DispatchQueue.main.async {
                controller.view.makeToast(encodingError.localizedDescription)
                }
                break
            }
        }
    }
}


